module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        concurrent: {
            dev: {
                tasks: ['watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        concat: {

            options: {
                banner: '/*<%= grunt.template.today() %> */\n',
                separator: ';',
            },
            dist: {
                src: ['./website/app/**/*.js'],
                dest: './website/dist/microconnectweb.js',
            },
        },
        uglify: {
            options: {
                banner: '/*<%= grunt.template.today() %> */\n',
                mangle: true,
                compress: {
                    drop_console: true
                },
            },
            task1: {
                files: [{
                    expand: true,
                    cwd: './website/dist/',
                    src: ['microconnectweb.js'],
                    dest: './website/dist/',
                    ext: '.min.js'
                }]
            }
        },
        cssmin: {
            minStyle: {
                files: [{
                    expand: true,
                    cwd: './website/assets/css/',
                    src: 'style.css',
                    dest: './website/dist/css',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            js: {
                files: ['./website/app/**/*.js'],
                tasks: ['concat', 'uglify'],
                option: {
                    spawn: false
                }
            },
            css: {
                files: ['./website/assets/css/style.css'],
                tasks: ['cssmin'],
                option: {
                    spawn: false
                }
            }
        }
    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //  grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    //grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-concurrent');

    //grunt.registerTask('default',['concat','uglify','less', 'cssmin','concurrent']);
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'concurrent']);
    //grunt.registerTask('start',['concurrent']);

    grunt.event.on('watch', function (action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });
};