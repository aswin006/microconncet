app.factory('services', services);
services.$inject = ['$http', 'Upload'];

function services($http, Upload) {
   // const apiUrl = 'http://104.218.53.125/mcconnect/api/v1.0/'
  const apiUrl = __env.apiUrl;
    var services = {
        SlideJsonData: SlideJsonData,
        FaqJsonData: FaqJsonData,
        ExamTabJsonData: ExamTabJsonData,
        SyllabusTabJsonData: SyllabusTabJsonData,
        uploadMarkSheet: uploadMarkSheet,
        uploadAadhar: uploadAadhar,
        uploadStudentPhoto: uploadStudentPhoto,
        uploadBonofideLetter:uploadBonofideLetter,
        StudentEnroll: StudentEnroll,
        getInTouch: getInTouch,
        commonJsonData: commonJsonData,
        demoRegData: demoRegData,
        ccAvenuePaymentDataSend:ccAvenuePaymentDataSend,
        PaymentLinkCheck:PaymentLinkCheck,
        exp_token_get : exp_token_get,
        exp_courses_demo:exp_courses_demo,
        exp_allowed_Countries:exp_allowed_Countries,
        exp_demo_register:exp_demo_register,
        getInitpopupdata:getInitpopupdata,
        getExpCountry:getExpCountry,
        getExpCourse:getExpCourse
    }
    return services;

    function commonJsonData() {
        return $http.get('./common/data/common.json').then(function (res) {
            return res.data;
        });
    }

    function SlideJsonData() {
        return $http.get('./common/data/slide.json').then(function (res) {
            return res.data;
        });
        // return $http.get(apiUrl + 'slides').then(function (res) {
        //     return res.data;
        // });
    }

    function FaqJsonData() {
        // return $http.get('./common/data/faq.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getfaqdata', {}).then(function (res) {
            return res.data;
        });
    }

    function ExamTabJsonData() {
        // return $http.get('./common/data/examtab.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getexams', {}).then(function (res) {
            return res.data;
        });
    }

    function SyllabusTabJsonData() {
        // return $http.get('./common/data/syllabustab.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getsyllabus', {}).then(function (res) {
            return res.data;
        });
    }

    function uploadMarkSheet(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'marksheet',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadAadhar(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'aadharsave',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadStudentPhoto(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'profileimage',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadBonofideLetter(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'studentbonafide',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function StudentEnroll(studentEnrollData) {
        return $http.post(apiUrl + 'studentenroll', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function getInTouch(getInTouchData) {
        return $http.post(apiUrl + 'contactemail', getInTouchData).then(function (res) {
            return res.data;
        });
    }

    function demoRegData(demoRegData) {
        return $http.post(apiUrl + 'requestdemo', demoRegData).then(function (res) {
            return res.data;
        });
    }

    // function ccAvenuePaymentDataSend(paymentData){
    //     debugger;
    //      $http.post('ccavRequestHandler.php', paymentData)
    // }
    function ccAvenuePaymentDataSend(paymentData){
        debugger;
        return $http.post(apiUrl + 'paymentverify', paymentData).then(function (res) {
            return res.data;
        });
       
    }
    function PaymentLinkCheck(discardData) {
        return $http.post(apiUrl + 'ordercheck', 
            discardData
        ).then(function (res) {
            return res.data;
        });
    }

    function exp_token_get(expdata){
        return $http.post('https://api3.exp247.com/v1sandbox/sessions', 
        expdata
        ).then(function (res) {
            return res.data;
        });
    }
    function exp_allowed_Countries(token){
        $http.defaults.headers.common['Authorization'] =  token;
     
        return $http.get('https://api3.exp247.com/v1sandbox/countries').then(function (res) {
          
            return res.data;
        });
    }
    
    function exp_courses_demo(token){
        $http.defaults.headers.common['Authorization'] =  token;
       
        return $http.get('https://api3.exp247.com/v1sandbox/courses/demo').then(function (res) {
          
            return res.data;
        });
    }
    function exp_demo_register(expdata,token){
        $http.defaults.headers.common['Authorization'] =  token;
        return $http.post(' https://api3.exp247.com/v1sandbox/register', 
        expdata
        ).then(function (res) {
            return res.data;
        });
    }


    function getInitpopupdata(){
        
        return $http.post(apiUrl + 'popubdata', 
            {}
        ).then(function (res) {
            return res.data;
        });
    }


    function getExpCourse() {

        return $http.post(apiUrl + 'expcourse',
            {}
        ).then(function (res) {
            return res.data;
        });
    };

    function getExpCountry() {

        return $http.post(apiUrl + 'expcountry',
            {}
        ).then(function (res) {
            return res.data;
        });
    }

}