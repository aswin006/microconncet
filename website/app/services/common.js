function timeToUnix(dateTime) {
    var UnixtimeStamp = new Date(dateTime).getTime();
    return UnixtimeStamp
}
function setLocalStorage(key, value) {
    debugger;
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorageData(key) {
    debugger;
    var data = JSON.parse(localStorage.getItem(key));
    return data;
}
function setSessionStorage(key, value) {
    debugger;
    sessionStorage.setItem(key, value);
}

function getSessionStorageData(key) {
    debugger;
    var data = sessionStorage.getItem(key);
    return data;
}

function clearLocalStorage(key) {
    localStorage.removeItem(key);
}
function clearSessionStorage(key) {
    sessionStorage.removeItem(key);
}

function hidePaymentLoader() {
    $("#showLoading").hide()

}
function encrypt(str){
    if(str){
        var enc = window.btoa(str);
        return enc;
    }else{
        return false
    }
    
}

function decrypt(str){
    if(str){
        var dec = window.atob(str);
        return dec;
    }else{
        return false
    }
    
}
function paymentshowLoader(e) {
    e || (e = "Loading....!");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader2"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
};

function expirycheck(exp) {
    if (Date.now() <= exp * 1000) {
      console.log(false, 'token is not expired');
      return false;
    } else { 
      console.log(true, 'token is expired') ;
      return true;
    }
  }



!(function ($) {
    "use strict";

    // Preloader
    $(window).on('load', function () {
        if ($('#preloader').length) {
            $('#preloader').delay(100).fadeOut('slow', function () {
                $(this).remove();
            });
        }
    });


    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });

    // jQuery counterUp
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 1000
    });

    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });

    // Initiate the venobox plugin
    $(document).ready(function () {
        $('.venobox').venobox();
    });

    // Initiate the datepicker plugin
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true
        });
    });

    function amountFormat(amount) {
        if(amount){
            var amountValue = parseInt(amount)
            const formatter = new Intl.NumberFormat('en-IN', {
         
                currency: 'INR',
                minimumFractionDigits: 2
              })
              
              
              var value = formatter.format(amountValue) // "10.00"
              
             return value;
        }
        
        }
    

})(jQuery);