app.directive('headerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'headerctrl',
        templateUrl: './website/views/partials/headerComponent/header.html'
    };
});

app.controller('headerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;
    $rootScope.btnactive = 'login';


    // var windowwidth = window.innerWidth;
    // if (windowwidth > 1299) {
    //     debugger;
    //     var topbar = document.getElementById('topbar')[0]; // 
    //     var header = document.getElementById('header')[0]; // 
    //     // root.setAttribute('class', 'container');
    //     root.setAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // } else {
    //     var topbar = document.getElementById('topbar')[0]; // 
    //     var header = document.getElementById('header')[0]; // 
    //     //root.removeAttribute('class', 'container');
    //     root.removeAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // }

    // Smooth scroll for the navigation menu and links with .scrollto classes
    var scrolltoOffset = $('#header').outerHeight() - 1;
    $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function (e) {
        debugger;
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                e.preventDefault();

                var scrollto = target.offset().top - scrolltoOffset;

                if ($(this).attr("href") == '#header') {
                    scrollto = 0;
                }

                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu, .mobile-nav').length) {
                    $('.nav-menu .active, .mobile-nav .active').removeClass('active');
                    $(this).closest('li').addClass('active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
                return false;
            }
        }
    });

    // // Activate smooth scroll on page load with hash links in the url
    // $(document).ready(function () {
    //     var scrolltoOffset2 = $('#header').outerHeight() - 1;
    //     if (window.location.hash) {

    //         var initial_nav = window.location.hash.replace('/', '');
    //         if ($(initial_nav).length) {
    //             var scrollto = $(initial_nav).offset().top.scrolltoOffset2
    //             $('html, body').animate({
    //                 scrollTop: scrollto
    //             }, 1500, 'easeInOutExpo');
    //         }
    //     }
    // });

    // Navigation active state on scroll
    var nav_sections = $('section');
    var main_nav = $('.nav-menu, .mobile-nav');

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop() + 200;

        nav_sections.each(function () {
            var top = $(this).offset().top,
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {
                if (cur_pos <= bottom) {
                    main_nav.find('li').removeClass('active');
                }
                main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
            }
            if (cur_pos < 300) {
                $(".nav-menu ul:first li:first, .mobile-nav ul:first li:first").addClass('active');
            }
        });
    });

    // // Mobile Navigation
    // if ($('.nav-menu').length) {
    //     var $mobile_nav = $('.nav-menu').clone().prop({
    //         class: 'mobile-nav d-lg-none'
    //     });
    //     $('body').append($mobile_nav);
    //     $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    //     $('body').append('<div class="mobile-nav-overly"></div>');

    //     $(document).on('click', '.mobile-nav-toggle', function (e) {
    //         debugger;
    //         $('body').toggleClass('mobile-nav-active');
    //         $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
    //         $('.mobile-nav-overly').toggle();
    //         if ($('body').hasClass('mobile-nav-active')) {
    //             autocomplete(document.getElementById("searchInput"), keys);
    //         }
    //     });

    //     $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
    //         e.preventDefault();
    //         $(this).next().slideToggle(300);
    //         $(this).parent().toggleClass('active');
    //     });

    //     $(document).click(function (e) {
    //         var container = $(".mobile-nav, .mobile-nav-toggle");
    //         if (!container.is(e.target) && container.has(e.target).length === 0) {
    //             if ($('body').hasClass('mobile-nav-active')) {
    //                 $('body').removeClass('mobile-nav-active');
    //                 $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
    //                 $('.mobile-nav-overly').fadeOut();
    //             }
    //         }
    //     });
    // } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    //     $(".mobile-nav, .mobile-nav-toggle").hide();
    // }
    // Toggle .header-scrolled class to #header when page is scrolled
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
            $('#topbar').addClass('topbar-scrolled');
        } else {
            $('#header').removeClass('header-scrolled');
            $('#topbar').removeClass('topbar-scrolled');
        }
    });

    if ($(window).scrollTop() > 100) {
        $('#header').addClass('header-scrolled');
        $('#topbar').addClass('topbar-scrolled');
    }



    ////auto fill search

    function autocomplete(inp, arr) {
        debugger;
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        debugger;
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                        var id = "#" + inp.value;
                        //$('a').find(id).trigger('click');
                        document.getElementById(inp.value).click();
                        //window.location.href = $(id).attr('href');
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        var isMobile = checkMobile()
        if(!isMobile){
            var eventTrigger = "keydown"
        }else{
            var eventTrigger = "keypress"
            
        }
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    };

    /*An array containing all the country names in the world:*/
    var loadData = {
        "acca": "index.html",
        "go-to-market": "go-to-market.html",
        "arbook": "arbook.html",
        "faq": "faq.html",
        "about": "about.html",
       // "blog": "https://www.theexpgroup.com/exp-blog/",
        "getintouch": "get-touch.html",
        "bfat": "bfat.html",
       // "enroll": "student-enroll.html",
         "enroll": "enroll.html",
        "login": "http://www.microconnect.org",
        "privacypolicy": "privacypolicy.html",
        "termsofservices": "termofconditions.html",

    };
    var keys = [];
    for (var k in loadData) keys.push(k);
    //  console.log(keys)

    autocomplete(document.getElementById("searchInput"), keys);

    $scope.search = function () {
        var keys = [];
        for (var k in loadData) keys.push(k);
        console.log(keys);
        var value = loadData[k];
        window.location.assign.href = value;
    };

    $scope.searchShow = false;
    $scope.searchhover = function () {
        //document.getElementById('getintouch').style = 'display:none'
        document.getElementById('faq').style = 'display:none'
        document.getElementById('about').style = 'display:none';
        // document.getElementById('blog').style = 'display:none';

        document.getElementById('searchInput').style = 'animation: fade-in 1s;';

    }
    $scope.searchleave = function () {
        // document.getElementById('getintouch').style = 'display:block'
        document.getElementById('faq').style = 'display:block'
        document.getElementById('about').style = 'display:block'
        // document.getElementById('blog').style = 'display:block';
        document.getElementById('searchInput').style = 'animation: fade-in 1s;';
    };

    $scope.searchClick = function () {

        if ($scope.searchShow == false) {
            $scope.searchShow = true;
            $scope.searchhover();
        } else {
            $scope.searchShow = false;
            $scope.searchleave();
        }

    };



    // Mobile Navigation
    if ($('.nav-menu').length) {
        var $mobile_nav = $('.nav-menu').clone().prop({
            class: 'mobile-nav d-lg-none'
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
        $('body').append('<div class="mobile-nav-overly"></div>');

        $(document).on('click', '.mobile-nav-toggle', function (e) {
            debugger;
            $('body').toggleClass('mobile-nav-active');
            $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
            $('.mobile-nav-overly').toggle();
            if ($('body').hasClass('mobile-nav-active')) {
                autocomplete(document.getElementById("searchInput"), keys);
            }
        });

        $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
            e.preventDefault();
            $(this).next().slideToggle(300);
            $(this).parent().toggleClass('active');
        });

        $(document).click(function (e) {
            var container = $(".mobile-nav, .mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
            }
        });
    } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
        $(".mobile-nav, .mobile-nav-toggle").hide();
    }


}]);