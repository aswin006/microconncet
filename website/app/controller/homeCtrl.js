app.directive('homeComponent', function () {
    return {
        restrict: 'E',
        controller: 'homectrl',
        templateUrl: './website/views/homeComponent/home.html'
    };
});


app.controller('homectrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce, ) {
    debugger;
   
    $scope.isMobile = checkMobile()

    services.SlideJsonData().then((res) => {
        $scope.myInterval = 3000;

        var sliderJSON = res.slideimages;
        $scope.slides = res.slideimages;
        //сортируем массив по order
        sliderJSON.sort(function (a, b) {
            return a.order - b.order;
        });
        if(!$scope.isMobile){
            var slider = $('#slidesection .carousel-inner');
            var bulletList = $('#slidesection .carousel-indicators')
   
           $.each(sliderJSON, function (index, element) {
               if (index == 0) {
                   var addclass = 'active'
               } else {
                   var addclass = ''
               }
   
               // slider.append(`<div class="carousel-item ${addclass}" ng-click="goToPage()"><img src="${element.imagePath}"  class="d-block w-100" alt="First slide"  /></div>`);
   
               slider.append(`<div class="carousel-item ${addclass}" ><a href="javascript:void(0)"  data-url="${element.url}" onclick="angular.element(this).scope().goToElementUrl(this)"><img src="${element.imagePath}"  class="d-block w-100" alt="First slide"  /></a></div>`);
   
               bulletList.append('<li data-target="#carouselExampleIndicators" data-slide-to="' + index + '" class="' + addclass + '"></li>');
   
   
           });
        }else{

        }
         
        //---------------------------------------------
        //Nivo slider
        //---------------------------------------------
        // $('#ensign-nivoslider').nivoSlider({
        //     effect: 'random',
        //     slices: 15,
        //     boxCols: 12,
        //     boxRows: 8,
        //     animSpeed: 500,
        //     pauseTime: 5000,
        //     startSlide: 0,
        //     directionNav: true,
        //     controlNavThumbs: false,
        //     pauseOnHover: true,
        //     manualAdvance: false,
        // });
    })


    services.SyllabusTabJsonData().then((res) => {
        // $scope.syllabustabdata = res.syllabustabdata
        // $scope.syllabustabdata = res.syllabustabdata.filter((item) => item.status === true);
        $scope.syllabustabdata = res.filter((item) => item.status === true)

    });
    services.ExamTabJsonData().then((res) => {
      //  $scope.examtabData = res.examtabdata
    
      $scope.examtabData = res.filter((item) => item.status === true)
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $('#vidBox1').VideoPopUp({
        backgroundColor: "#171717d9",
        opener: "videopopup1",
        maxweight: "340",
        idvideo: "v1",
        pausevideo: true

    });

    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        items: 1,
        nav: true,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"]

    });
    $("#studentplacemen-carousel").owlCarousel({

        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true

        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

    var popupInitiated = sessionStorage.getItem('popupinitiated');
    if (popupInitiated == null || popupInitiated == false) {

        /****popub init */
        services.getInitpopupdata().then((res) =>{
            $scope.popupdata = res[0];
            $scope.popupcontent =  $scope.popupdata.content;
            $scope.popupRedirectUrl =  $scope.popupdata.url;

            $('#popupmodal').modal('show')
            sessionStorage.setItem('popupinitiated', true);
        })
       
    };

    $scope.goToUrl = function (url) {
        window.open(url, "_blank")
    }
    $scope.goToElementUrl = function (element) {
        debugger;
        var url = element.attributes["data-url"].value
        if (url != "") {
            window.open(url, '_blank')
        }

        // window.open("https://www.google.com", '_blank');
    }

    // Activate smooth scroll on page load with hash links in the url
    $(document).ready(function () {
        var scrolltoOffset2 = $('#header').outerHeight() - 1;
        if (window.location.hash) {

            var initial_nav = window.location.hash.replace('/', '');
            if ($(initial_nav).length) {
                var scrollto = $(initial_nav).offset().top - scrolltoOffset2
                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');
            }
        }
    });

    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }



}]);

function goToPath(event) {
    console.log('event', event)
    window.open("https://www.google.com", '_blank');
}