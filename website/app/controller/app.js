var app = angular.module('microconnect', ['ngMessages', 'ngFileUpload', 'vcRecaptcha']);

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
app.constant('__env', env);
app.run(['$location', '$rootScope', 'services', '$anchorScroll', '$http', function ($location, $rootScope, services, $anchorScroll, $http) {

    function redirectUrl(pathname) {

        if (pathname == '/acca') {
            window.location.assign('index.html')
        } else if (pathname == '/students-to-enrol') {
            window.location.assign('enroll.html')
        }
    }



    if (window.location.host == 'localhost') {
        $rootScope.domain = window.location.host + location.pathname;
        var pathname = location.pathname.replace('/microconnect', '');
        //  redirectUrl(pathname);
    } else {
        $rootScope.domain = window.location.host;
        var pathname = location.pathname
        // redirectUrl(pathname);
    }
   
  //  $rootScope.publicKey = "6Le8l-cZAAAAAD0Z8Ivtx8hAUVo4fUw68flehrTh";
     $rootScope.publicKey = __env.vcRecaptchaKey;
    services.commonJsonData().then((res) => {

        $rootScope.commonJsonData = res;
    })
    
    /****get exp allowed country list */
    services.getExpCountry().then((res) => {
        $rootScope.expCountry = res.countries;
    })
    

    // var windowwidth = window.innerWidth;
    // if (windowwidth > 1299) {
    //     debugger;
    //     var root = document.getElementsByTagName('html')[0]; // 
    //     // root.setAttribute('class', 'container');
    //     root.setAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // } else {
    //     var root = document.getElementsByTagName('html')[0]; // 
    //     //root.removeAttribute('class', 'container');
    //     root.removeAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // }
    //$anchorScroll();

    
}]);
app.directive('allowOnlyNumbers', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9]/g, '')
                $input.val(value);
                if (event.which == 64 || event.which == 16) {
                    // to allow numbers  
                    return false;
                } else if (event.which >= 48 && event.which <= 57) {
                    // to allow numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // to allow numpad number  
                    return true;
                } else if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // to allow backspace, enter, escape, arrows  
                    return true;
                } else {
                    event.preventDefault();
                    // to stop others  
                    //alert("Sorry Only Numbers Allowed");  
                    return false;
                }
            });
        }
    }
});


/*****aathar number validations */
app.directive('aadharCheck', function() {
    var aadharcardRegex = /^\d{4}-\d{4}-\d{4}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attr, ngModelCtrl) {
      elm.on('keyup', function (event) {
      debugger;
              var $input = $(this);
              var value = $input.val();
              value =
                   value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s =>s.length > 0).join("-");
              $input.val(value);
              
               if(value.length == 14){
                event.preventDefault();
                return false;
               }
               
      });
        scope.$watch(attr.ngModel, function (value) {
              debugger;
            
              console.log('value changed, new value is: ' + value);
          });
        // ngModelCtrl.$parsers.unshift(function(input) {
        //   var valid = aadharcardRegex.test(input);
        //   ngModelCtrl.$setValidity('aadharcard', valid);
        //   return input;
        // }); 
        ngModelCtrl.$validators.aadharcard = function(modelValue, viewValue) {
            var valid = aadharcardRegex.test(modelValue);

      return valid;
    };
      }
    };
  });
  


 
// app.directive('aadharCheck',function(){
//     return{
//         restrict: 'A',
//         link: function(scope,elm,attrs,ctrl){
//             elm.on('change',function(event){
//                 var $input = $(this);
//                 var value = $input.val();
//             })
//         }
//     }
// })


// function expirycheck(){
//     if (Date.now() >= exp * 1000) {
//         return false;
//       }else{
//         return true;
//       }
// }


// app.factory('httpInterceptorSerivce', ['$rootScope', function httpInterceptorSerivce($rootScope) {
//     return {

//         request: function (config) {
//             //debugger;
//             //$rootScope.isLoader = 1;
//             showLoader();
//             return config;
//         },

//         requestError: function (config) {
//             //debugger;
//             //$rootScope.isLoader = 0;
//             hideLoader();
//             return config;
//         },

//         response: function (res) {
//             //$rootScope.isLoader = 0;
//             // setTimeout(function() {
//             hideLoader();
//             // }, 2000);

//             return res;
//         },

//         responseError: function (res) {
//             //debugger;

//             return res;
//         }
//     }
// }]).config(['$httpProvider', function ($httpProvider) {
//     $httpProvider.interceptors.push('httpInterceptorSerivce');
// }]);;

function hideLoader() {
    // $("#showLoading").hide()
    $("#showLoading").hide()

}

function showLoader(e) {
    // e || (e = "Loading....!");
    // var t = $("#showLoading");
    // t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
    //     $("#showLoading h4").html(e),
    //     $("#showLoading").show()
    e || (e = "Loading. Please Wait.");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:#fff;z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="d-flex justify-content-center"><div class="spinner-border" role="status"> <span class="sr-only">Loading...</span></div></div><h4 class="text-center space font-weight-normal">Loading...</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
}