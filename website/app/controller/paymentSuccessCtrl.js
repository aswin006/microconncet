app.controller('paymentSuccesCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', '$http', function ($scope, services, $rootScope, $location, $sce, $http) {
  debugger;


  var ccAvenueResponseString = getLocalStorageData("ccAvenueResponse");
  paymentshowLoader('Processing your  Payment,please  do not refresh or close the page ')
  var result = {};
  if (ccAvenueResponseString) {
    var ccAvenueResponseArray = ccAvenueResponseString;
    for (var i = 0; i < ccAvenueResponseArray.length; i++) {
      debugger;
      for (var k in ccAvenueResponseArray[i]) {
        if (ccAvenueResponseArray[i].hasOwnProperty(k)) {

          result[k] = ccAvenueResponseArray[i][k];
        }
      }

    }
    console.log(result);

    
    services.ccAvenuePaymentDataSend(result).then((res) =>{
         hidePaymentLoader(); 
         var studentname = res.se_name;
         var enrollmentno = res.se_studentid;
         var paidamount = res.paidamount
         if (res.paymentstatus === "Success") {
          $scope.title = "Payment Success";
          // $scope.paymentMessage = '<p>Thanks for your payment.we will process the admission and send you the login and password to start the International Diploma in Accounting and Business within next 7-10 days.<br>Happy Learning.</p>'
          $scope.paymentMessage = '<p>Dear '+ studentname +'-'+ enrollmentno +' </p><br><p>Your payment for Rs.'+ paidamount +' is successfully made. We will be in touch with you with next steps.<br>for any clarification please write to us at <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>-please don`t forget to mention your enrollment number in all communications.</p><br><p>Happy Learning.</p><br>Team MICROCONNECT';

    
        }
        else if (res.paymentstatus === "Aborted") {
          $scope.title = "Payment Aborted";
          $scope.paymentMessage = result.status_message;
    
        }
        else if (res.paymentstatus === "Failure") {
          $scope.title = "Payment Failure";
          $scope.paymentMessage = result.status_message;
        }
        else {
          $scope.title = "Payment Invalid";
          $scope.paymentMessage = result.status_message;
    
        }
    })
  }else{
    alert('Payment Failed');
    window.open('index.html')
  }

  //  for (var i = 0; i < arr.length; i++) {
  //     debugger;
  //        for (var k in arr[i]) {
  //             if (arr[i].hasOwnProperty(k)) {

  //                result[k] = arr[i][k];
  //             }
  //         }

  //     }
  $scope.encodeData = function(html){
   
    var trustedHtml = $sce.trustAsHtml(html);
    return trustedHtml;
  }

}]);