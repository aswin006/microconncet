app.directive('studentEnrollComponent', function () {
    return {
        restrict: 'EA',
        controller: 'studentEnrollCtrl',
        templateUrl: './website/views/studentEnrollComponent/studentEnrollComponent.html'
    };
});

app.controller('studentEnrollCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', 'vcRecaptchaService', function ($scope, services, $rootScope, $location, $sce, vcRecaptchaService) {
    //debugger;
    $rootScope.btnactive = 'enroll';
    $scope.studentenrolldata = {
        se_address: {
            country: "",
            state: "",
            city: ""
        }
    }
    $scope.country = '';
    $scope.state = '';
    $scope.city = '';
    $scope.parentnum_dialcode = "+91";
    $scope.studentnum_dialcode = "+91"
    // $scope.studentenrolldata.se_address["country"] = '';
    // $scope.studentenrolldata.se_address["state"] = '';

    $('[data-type="adhaar-number"]').keyup(function () {
        var value = $(this).val();
        value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
        $(this).val(value);
    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $scope.uploadedFile = function (element, id) {
        debugger;
        showLoader();
        var maxSize = 5 * 1024;
        $scope.currentFile = element.files[0];
        var fileSize = Math.round((element.files[0].size / 1024));
        var reader = new FileReader();
        if (fileSize > maxSize) {
            alert('file size is more than 5mb');
            hideLoader();
            return false;
        } else {
            reader.onload = function (event) {


                $scope.$apply(function ($scope) {
                    //$scope.files = element.files;
                    if (id == 'marksheet') {
                        $scope.marksheet = event.target.result
                        $scope.marksheetSrc = element.files[0];
                        services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_marksheet = res
                            console.log(res);
                            hideLoader();

                        })
                    } else if (id == 'aadharcard') {
                        $scope.aadharcard = event.target.result
                        $scope.aadharcardSrc = element.files[0]
                        services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_aadharimage = res;
                            console.log(res)
                            hideLoader();
                        })
                    } else if (id == 'studentphoto') {
                        $scope.studentphoto = event.target.result
                        $scope.studentphotoImgSrc = element.files[0];
                        services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_studentimage = res;
                            console.log(res)
                            hideLoader();
                        })
                    } else if (id == 'bonofide') {
                        $scope.bonofide = event.target.result
                        $scope.bonofideSrc = element.files[0]
                        services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_bonafide = res;
                            console.log(res)
                            hideLoader();
                        })
                    }
                });
            }
            reader.readAsDataURL(element.files[0]);
        }




    };

    $scope.setCountry = function (countrycode) {
        $scope.studentenrolldata.se_address.country = countrycode;
        $scope.studentenrolldata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.studentenrolldata.se_address.state = state;
        $scope.studentenrolldata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
       // alert( $scope.studentenrolldata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.studentenrolldata.se_address.city = city;
        $scope.studentenrolldata.se_city = city;

        //alert(city);

    }


    $scope.submitEnrollData = function () {
        if ($scope.studentenrolldata.agree) {

            showLoader();

            if (vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please resolve the captcha and submit!")
            } else {

                 $scope.studentenrolldata.se_dateofbirth = timeToUnix($scope.dateofbirth);
                 $scope.studentenrolldata.se_parentmobileno ={
                    dialcode: $scope.parentnum_dialcode,
                    mobileno:  $scope.parentnum_dialcode + $scope.parentmobileno
                 }
                 $scope.studentenrolldata.se_studentmobileno ={
                    dialcode: $scope.studentnum_dialcode,
                    mobileno:  $scope.studentnum_dialcode + $scope.studentmobileno
                 }
                 $scope.studentenrolldata.se_schoolcode = 'MC-'+$scope.se_schoolcode
                services.StudentEnroll($scope.studentenrolldata).then((res) => {
                    debugger;
                    // $scope.studentenrolldata = res;
                    console.log(res)
                    hideLoader();
                   // alert('Auto email confirmation to be sent to  both the email ids (Parent & Student)');

                    // alert('Thanks for registering for Free ACCA course demo. you will hear from us soon');
                    alert('Successfully registered')
                    location.reload();


                })
            }


        } else {
            alert('Please select ACCA’s mandatory declaration')

        }
    };




}]);