app.directive('qualificationComponent', function () {
    return {
        restrict: 'EA',
        controller: 'qualificationCtrl',
        templateUrl: './website/views/qualificationComponent/qualificationComponent.html'
    };
});

app.controller('qualificationCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;
    $scope.currenShowPage = 'accaqualification';
    if (window.location.hash) {

        var initial_nav = window.location.hash.replace('/', '');
        if (initial_nav == '#acca-qualification') {
            $scope.currenShowPage = 'accaqualification';

        } else if (initial_nav == '#bsc-applied-accounting') {
            $scope.currenShowPage = 'bsc-aa';
        } else if (initial_nav == '#msc-professional-accounting') {
            $scope.currenShowPage = 'msc-pa';
        }
    }



}]);