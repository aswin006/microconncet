app.directive('paymentComponent', function () {
    return {
        restrict: 'EA',
        controller: 'paymentCtrl',
        templateUrl: './website/views/paymentComponent/paymentComponent.html'
    };
});
function decodeData(value,type){
    if(value){
        var decodedData = atob(value);
        if(type == 'number'){
             return parseInt(decodedData)
        }else{
            return decodedData
        }
       // return decodedData
    }else{
        return false
    }
}
function encodeData(value){
    if(value){
        var encodedData = btoa(value);
        return encodedData
    }else{
        return false
    }
}


app.controller('paymentCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce','$http', function ($scope, services, $rootScope, $location, $sce,$http) {
    debugger;
   var urlParams = new URLSearchParams(window.location.search);
   $scope.enrollmentno =  decodeData(urlParams.get('param1')); 
   $scope.email = decodeData(urlParams.get('param2')); 
   $scope.order_id =decodeData(urlParams.get('param3'));
   $scope.amount =decodeData(urlParams.get('param4'),'number');


   function getCountry(ccode){
    var countrycode = ccode ? ccode : 'IN';
   
    var index = $rootScope.commonJsonData.countrylist.findIndex(x => x.code == countrycode)
    if(index > -1){
        var country = $rootScope.commonJsonData.countrylist[index].name
    }else{
        var country = '';
    }
     return country;
   


}


   if ( $scope.enrollmentno == false || $scope.email == false || $scope.order_id == false) {
    // var domain = window.location.host + location.pathname;
    //  var url =  window.location.protocol + '//' + window.location.host +  '/microconnect2/paymentstatus.html'
    //  self.location = url;
     window.open('index.html', "_self")
    
     //self.location = 'index.html';

 } else {
   
    var discardData = {
        discard : false,
        orderid: $scope.order_id
    }
    services.PaymentLinkCheck(discardData).then((res) => {
        if(res.statuscode == 'NT-200'){
            $scope.paymentData = {
                enrollmentnumber: $scope.enrollmentno,
                email:$scope.email,
                amount:parseInt($scope.amount)
             }
          
  

             $scope.paymentDetails = {
              tid: new Date().getTime(),
              merchant_id: __env.merchantId,
              order_id:  $scope.order_id,
              amount: $scope.amount,
              currency: "INR",
              redirect_url:__env.ccavenueredirectUri,
             // cancel_url:  __env.hostUrl,
              cancel_url: __env.ccavenueredirectUri,
              language: "EN",
              billing_name: res.se_name,
              billing_address: res.se_address.line1,
              billing_city: res.se_city,
              billing_state: res.se_state,
              billing_zip: res.pincode,
              billing_country:getCountry(res.se_country),
              billing_tel: parseInt(res.se_studentmobileno.mobileno),
              billing_email: $scope.paymentData.email,
              delivery_name: "",
              delivery_address: "",
              delivery_city: "",
              delivery_state: "",
              delivery_zip: "",
              delivery_country: "",
              delivery_tel: "",
              merchant_param1: "",
              merchant_param2: "",
              merchant_param3: "",
              merchant_param4: "",
              merchant_param5: "",
              emi_plan_id: "",
              emi_tenure_id: "",
              card_type: "",
              card_name: "",
              data_accept: "",
              card_number: "",
              expiry_month: "",
              expiry_year: "",
              cvv_number: "",
              issuing_bank: "",
              mobile_number: "",
              mm_id: "",
              otp: "",
              promo_code: ""
           };
        }else{
            alert('Payment url  already used  or expired')
            window.open('index.html', "_self")  
        }
       
    })

   
  
   $scope.paynow = function(){
    //   $scope.paymentDetails = {
    //       tid: new Date().getTime(),
    //       merchant_id: __env.merchantId,
    //       order_id: $scope.order_id,
    //       amount: $scope.amount,
    //       currency: "INR",
    //       redirect_url: __env.hostUrl,
    //       cancel_url:  __env.hostUrl,
    //       language: "EN",
    //       billing_name: "",
    //       billing_address: "",
    //       billing_city: "",
    //       billing_state: "",
    //       billing_zip: "",
    //       billing_country: "",
    //       billing_tel: "",
    //       billing_email: $scope.paymentData.email,
    //       delivery_name: "",
    //       delivery_address: "",
    //       delivery_city: "",
    //       delivery_state: "",
    //       delivery_zip: "",
    //       delivery_country: "",
    //       delivery_tel: "",
    //       merchant_param1: "",
    //       merchant_param2: "",
    //       merchant_param3: "",
    //       merchant_param4: "",
    //       merchant_param5: "",
    //       emi_plan_id: "",
    //       emi_tenure_id: "",
    //       card_type: "",
    //       card_name: "",
    //       data_accept: "",
    //       card_number: "",
    //       expiry_month: "",
    //       expiry_year: "",
    //       cvv_number: "",
    //       issuing_bank: "",
    //       mobile_number: "",
    //       mm_id: "",
    //       otp: "",
    //       promo_code: ""
    //    };
      
       $http({
          method: 'POST',
          url: 'ccavRequestHandler.php',
          data: $.param($scope.paymentDetails),
         headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
      }).then((res) =>{
          
          console.log(res);
          // window.open(`<form method="post" name="redirect" target="_blank" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"><?php echo "<input type=hidden name=encRequest value=$encrypted_data>";echo "<input type=hidden name=access_code value=$access_code>";  ?></form>`);
        //  window.open(res.data);
          document.open();
         document.write(`<form method="post" name="redirect" target="_blank" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"></form>`);
         document.close();
  
      })
       //services.ccAvenuePaymentDataSend($scope.paymentDetails);
   }
 }


 


//  for (var i = 0; i < arr.length; i++) {
//     debugger;
//        for (var k in arr[i]) {
//             if (arr[i].hasOwnProperty(k)) {
              
//                result[k] = arr[i][k];
//             }
//         }
      
//     }

}]);