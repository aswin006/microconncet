app.directive('footerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'footerctrl',
        templateUrl: './website/views/partials/footerComponent/footer.html'
    };
});

app.controller('footerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;

  $scope.openPopup= function(popup){
      if(popup == 'cancellation_policy'){
         $scope.footer_modal_header = 'Cancellation policy';
         $scope.footer_modal_body_text = 'Microconnect reserves the right to accept or reject any cancellation once registration and payment is made. Microconnect will categorically metion wherever and whenever needed. This clause will supercede all terms, unless specified specifically.';
      }else if(popup == 'refund_policy'){
        $scope.footer_modal_header = 'Refund policy';
        $scope.footer_modal_body_text = 'Microconnect will honour the refund if any, under circumstances where the same is defined. In case refund policy is amended at any time, the refund policy shall be clearly mentioned on the activities/ engagements Microconnect creates from time to time. In case of no such specific mention, Microconnect follows "No Refund Policy".'
    }else if(popup == 'rejoinder_to_refund_policy'){
        $scope.footer_modal_header = 'Rejoinder to Refund Policy';
        $scope.footer_modal_body_text = 'If Microconnect, whenever, wherever, refund policy is mentioned, Microconnect will honour with reasonable commitment unless otherwise the same is beyond comprehension.'
    }
    $('#footerModal').modal('show')
  }

}]);