app.directive('getTouchComponent', function () {
    return {
        restrict: 'EA',
        controller: 'getTouchpageCtrl',
        templateUrl: './website/views/getInTouchComponent/getInTouchComponent.html'
    };
});

app.controller('getTouchpageCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', 'vcRecaptchaService', function ($scope, services, $rootScope, $location, $sce, vcRecaptchaService) {
    //debugger;

    $rootScope.btnactive = 'getintouch';

    $scope.contact = {}
    $scope.submitForm = function () {
        showLoader();
        console.log($scope.contact);

        services.getInTouch($scope.contact).then((res) => {
            debugger;
            // $scope.studentenrolldata = res;
            console.log(res)
            hideLoader();
            alert('Your message has been sent. Thank you!');
            grecaptcha.reset();
            location.reload();


        })

    }
    window.verifyRecaptchaCallback = function (response) {
        $('input[data-recaptcha]').val(response).trigger('change')
    }

    window.expiredRecaptchaCallback = function () {
        $('input[data-recaptcha]').val("").trigger('change')
    }



    $('#getintouchform').on('submit', function (e) {
        debugger;
        // if (!e.isDefaultPrevented()) {


        //     $scope.submitForm();
        //     return false;
        // }
        if (vcRecaptchaService.getResponse() === "") { //if string is empty
            alert("Please resolve the captcha and submit!")
        } else {

            $scope.submitForm();
        }
    })



}]);