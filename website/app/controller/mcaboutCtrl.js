app.directive('mcaboutComponent', function () {
    return {
        restrict: 'EA',
        controller: 'mcaboutCtrl',
        templateUrl: './website/views/aboutComponent/aboutComponent.html'
    };
});

app.controller('mcaboutCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;

    // $("#foundertestimonials").owlCarousel({

    //     navigation: false, // Show next and prev buttons
    //     slideSpeed: 300,
    //     paginationSpeed: 400,
    //     singleItem: true

    //     // "singleItem:true" is a shortcut for:
    //     // items : 1, 
    //     // itemsDesktop : false,
    //     // itemsDesktopSmall : false,
    //     // itemsTablet: false,
    //     // itemsMobile : false

    // });
    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 450,
        autoplayHoverPause: true, // Stops autoplay
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });

}]);