app.directive('faqComponent', function () {
    return {
        restrict: 'EA',
        controller: 'faqCtrl',
        templateUrl: './website/views/faqComponent/faqComponent.html'
    };
});

app.controller('faqCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;

    services.FaqJsonData().then((res) => {
        // $scope.syllabustabdata = res.syllabustabdata
       // $scope.faqJSON = res.faqdata.filter((item) => item.status === true)
        $scope.faqJSON = res.filter((item) => item.status === true);
        for(var i = 0; i < $scope.faqJSON.length ; i++){
            
            $scope.faqJSON[i]['collape'] = '';
             
        }

    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    };
    $scope.expand = false;
    $scope.expandBtnText = 'Expand All';
    $scope.toggleCollapse = function(){
        $scope.expand = !$scope.expand;
        $scope.expandBtnText =  $scope.expand == true ? 'Collapse All' : 'Expand All'; 
        var parent = $('.faq-ul-list').find('li');
        parent.each(function( index ) {
            var child =  parent.find('#faq-list-'+index);
            if($scope.expand ){
                if(child.hasClass('show') == false){
                   // child.removeClass('show')
                    child.addClass('show')
                }
            }else{
                if(child.hasClass('show') == true){
                  
                    child.removeClass('show')
                }
            }
           
            
            console.log( index  );
          });
           
    }
    // $("#accordion").on("click", function() {
    //     //$('#faq-ul-list').find('li');
    //     $scope.expand = !$scope.expand;
    //   //  $scope.expandBtnText =  $scope.expand == true ? 'Collapse All' : 'Expand All'; 
    //     var parent = $('.faq-ul-list').find('li');
    //     parent.each(function( index ) {
    //         var child =  parent.find('#faq-list-'+index);
    //         if($scope.expand ){
    //             if(child.hasClass('show')){
    //                 child.removeClass('show')
    //             }else{
    //                 child.addClass('show')
    //             }
    //         }else{
    //             if(child.hasClass('show') == false){
    //                 child.addClass('show')
    //             }
    //         }
           
            
    //         console.log( index  );
    //       });
    // });
    $scope.$watch('expand', function(){
        $scope.expandBtnText = $scope.expand ? 'Collapse All!' : 'Expand All';
    })

    // $(".toggle-accordion").on("click", function() {
    //     debugger;
    //     var accordionId = $(this).attr("accordion-id"),
    //       numPanelOpen = $(accordionId + ' .collapse.in').length;
        
    //     $(this).toggleClass("active");
    
    //     if (numPanelOpen == 0) {
    //       openAllPanels(accordionId);
    //     } else {
    //       closeAllPanels(accordionId);
    //     }
    //   });

    //   openAllPanels = function(aId) {
    //     console.log("setAllPanelOpen");
    //     $(aId + ' .panel-collapse:not(".in")').collapse('show');
    //   }
    //   closeAllPanels = function(aId) {
    //     console.log("setAllPanelclose");
    //     $(aId + ' .panel-collapse.in').collapse('hide');
    //   }

}]);