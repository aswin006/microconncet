/*Fri Aug 27 2021 12:32:35 */
var app = angular.module('microconnect', ['ngMessages', 'ngFileUpload', 'vcRecaptcha']);

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
app.constant('__env', env);
app.run(['$location', '$rootScope', 'services', '$anchorScroll', '$http', function ($location, $rootScope, services, $anchorScroll, $http) {

    function redirectUrl(pathname) {

        if (pathname == '/acca') {
            window.location.assign('index.html')
        } else if (pathname == '/students-to-enrol') {
            window.location.assign('enroll.html')
        }
    }



    if (window.location.host == 'localhost') {
        $rootScope.domain = window.location.host + location.pathname;
        var pathname = location.pathname.replace('/microconnect', '');
        //  redirectUrl(pathname);
    } else {
        $rootScope.domain = window.location.host;
        var pathname = location.pathname
        // redirectUrl(pathname);
    }
   
  //  $rootScope.publicKey = "6Le8l-cZAAAAAD0Z8Ivtx8hAUVo4fUw68flehrTh";
     $rootScope.publicKey = __env.vcRecaptchaKey;
    services.commonJsonData().then((res) => {

        $rootScope.commonJsonData = res;
    })
    
    /****get exp allowed country list */
    services.getExpCountry().then((res) => {
        $rootScope.expCountry = res.countries;
    })
    

    // var windowwidth = window.innerWidth;
    // if (windowwidth > 1299) {
    //     debugger;
    //     var root = document.getElementsByTagName('html')[0]; // 
    //     // root.setAttribute('class', 'container');
    //     root.setAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // } else {
    //     var root = document.getElementsByTagName('html')[0]; // 
    //     //root.removeAttribute('class', 'container');
    //     root.removeAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // }
    //$anchorScroll();

    
}]);
app.directive('allowOnlyNumbers', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9]/g, '')
                $input.val(value);
                if (event.which == 64 || event.which == 16) {
                    // to allow numbers  
                    return false;
                } else if (event.which >= 48 && event.which <= 57) {
                    // to allow numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // to allow numpad number  
                    return true;
                } else if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // to allow backspace, enter, escape, arrows  
                    return true;
                } else {
                    event.preventDefault();
                    // to stop others  
                    //alert("Sorry Only Numbers Allowed");  
                    return false;
                }
            });
        }
    }
});


/*****aathar number validations */
app.directive('aadharCheck', function() {
    var aadharcardRegex = /^\d{4}-\d{4}-\d{4}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attr, ngModelCtrl) {
      elm.on('keyup', function (event) {
      debugger;
              var $input = $(this);
              var value = $input.val();
              value =
                   value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s =>s.length > 0).join("-");
              $input.val(value);
              
               if(value.length == 14){
                event.preventDefault();
                return false;
               }
               
      });
        scope.$watch(attr.ngModel, function (value) {
              debugger;
            
              console.log('value changed, new value is: ' + value);
          });
        // ngModelCtrl.$parsers.unshift(function(input) {
        //   var valid = aadharcardRegex.test(input);
        //   ngModelCtrl.$setValidity('aadharcard', valid);
        //   return input;
        // }); 
        ngModelCtrl.$validators.aadharcard = function(modelValue, viewValue) {
            var valid = aadharcardRegex.test(modelValue);

      return valid;
    };
      }
    };
  });
  


 
// app.directive('aadharCheck',function(){
//     return{
//         restrict: 'A',
//         link: function(scope,elm,attrs,ctrl){
//             elm.on('change',function(event){
//                 var $input = $(this);
//                 var value = $input.val();
//             })
//         }
//     }
// })


// function expirycheck(){
//     if (Date.now() >= exp * 1000) {
//         return false;
//       }else{
//         return true;
//       }
// }


// app.factory('httpInterceptorSerivce', ['$rootScope', function httpInterceptorSerivce($rootScope) {
//     return {

//         request: function (config) {
//             //debugger;
//             //$rootScope.isLoader = 1;
//             showLoader();
//             return config;
//         },

//         requestError: function (config) {
//             //debugger;
//             //$rootScope.isLoader = 0;
//             hideLoader();
//             return config;
//         },

//         response: function (res) {
//             //$rootScope.isLoader = 0;
//             // setTimeout(function() {
//             hideLoader();
//             // }, 2000);

//             return res;
//         },

//         responseError: function (res) {
//             //debugger;

//             return res;
//         }
//     }
// }]).config(['$httpProvider', function ($httpProvider) {
//     $httpProvider.interceptors.push('httpInterceptorSerivce');
// }]);;

function hideLoader() {
    // $("#showLoading").hide()
    $("#showLoading").hide()

}

function showLoader(e) {
    // e || (e = "Loading....!");
    // var t = $("#showLoading");
    // t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
    //     $("#showLoading h4").html(e),
    //     $("#showLoading").show()
    e || (e = "Loading. Please Wait.");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:#fff;z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="d-flex justify-content-center"><div class="spinner-border" role="status"> <span class="sr-only">Loading...</span></div></div><h4 class="text-center space font-weight-normal">Loading...</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
};app.directive('arbookComponent', function () {
    return {
        restrict: 'EA',
        controller: 'arbookCtrl',
        templateUrl: './website/views/arbookComponent/arbookComponent.html'
    };
});

app.controller('arbookCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;



}]);;app.directive('bfatComponent', function () {
    return {
        restrict: 'EA',
        controller: 'bfatCtrl',
        templateUrl: './website/views/bfatComponent/bfatComponent.html'
    };
});

app.controller('bfatCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {

}]);;app.directive('faqComponent', function () {
    return {
        restrict: 'EA',
        controller: 'faqCtrl',
        templateUrl: './website/views/faqComponent/faqComponent.html'
    };
});

app.controller('faqCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;

    services.FaqJsonData().then((res) => {
        // $scope.syllabustabdata = res.syllabustabdata
       // $scope.faqJSON = res.faqdata.filter((item) => item.status === true)
        $scope.faqJSON = res.filter((item) => item.status === true);
        for(var i = 0; i < $scope.faqJSON.length ; i++){
            
            $scope.faqJSON[i]['collape'] = '';
             
        }

    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    };
    $scope.expand = false;
    $scope.expandBtnText = 'Expand All';
    $scope.toggleCollapse = function(){
        $scope.expand = !$scope.expand;
        $scope.expandBtnText =  $scope.expand == true ? 'Collapse All' : 'Expand All'; 
        var parent = $('.faq-ul-list').find('li');
        parent.each(function( index ) {
            var child =  parent.find('#faq-list-'+index);
            if($scope.expand ){
                if(child.hasClass('show') == false){
                   // child.removeClass('show')
                    child.addClass('show')
                }
            }else{
                if(child.hasClass('show') == true){
                  
                    child.removeClass('show')
                }
            }
           
            
            console.log( index  );
          });
           
    }
    // $("#accordion").on("click", function() {
    //     //$('#faq-ul-list').find('li');
    //     $scope.expand = !$scope.expand;
    //   //  $scope.expandBtnText =  $scope.expand == true ? 'Collapse All' : 'Expand All'; 
    //     var parent = $('.faq-ul-list').find('li');
    //     parent.each(function( index ) {
    //         var child =  parent.find('#faq-list-'+index);
    //         if($scope.expand ){
    //             if(child.hasClass('show')){
    //                 child.removeClass('show')
    //             }else{
    //                 child.addClass('show')
    //             }
    //         }else{
    //             if(child.hasClass('show') == false){
    //                 child.addClass('show')
    //             }
    //         }
           
            
    //         console.log( index  );
    //       });
    // });
    $scope.$watch('expand', function(){
        $scope.expandBtnText = $scope.expand ? 'Collapse All!' : 'Expand All';
    })

    // $(".toggle-accordion").on("click", function() {
    //     debugger;
    //     var accordionId = $(this).attr("accordion-id"),
    //       numPanelOpen = $(accordionId + ' .collapse.in').length;
        
    //     $(this).toggleClass("active");
    
    //     if (numPanelOpen == 0) {
    //       openAllPanels(accordionId);
    //     } else {
    //       closeAllPanels(accordionId);
    //     }
    //   });

    //   openAllPanels = function(aId) {
    //     console.log("setAllPanelOpen");
    //     $(aId + ' .panel-collapse:not(".in")').collapse('show');
    //   }
    //   closeAllPanels = function(aId) {
    //     console.log("setAllPanelclose");
    //     $(aId + ' .panel-collapse.in').collapse('hide');
    //   }

}]);;app.directive('footerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'footerctrl',
        templateUrl: './website/views/partials/footerComponent/footer.html'
    };
});

app.controller('footerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;

  $scope.openPopup= function(popup){
      if(popup == 'cancellation_policy'){
         $scope.footer_modal_header = 'Cancellation policy';
         $scope.footer_modal_body_text = 'Microconnect reserves the right to accept or reject any cancellation once registration and payment is made. Microconnect will categorically metion wherever and whenever needed. This clause will supercede all terms, unless specified specifically.';
      }else if(popup == 'refund_policy'){
        $scope.footer_modal_header = 'Refund policy';
        $scope.footer_modal_body_text = 'Microconnect will honour the refund if any, under circumstances where the same is defined. In case refund policy is amended at any time, the refund policy shall be clearly mentioned on the activities/ engagements Microconnect creates from time to time. In case of no such specific mention, Microconnect follows "No Refund Policy".'
    }else if(popup == 'rejoinder_to_refund_policy'){
        $scope.footer_modal_header = 'Rejoinder to Refund Policy';
        $scope.footer_modal_body_text = 'If Microconnect, whenever, wherever, refund policy is mentioned, Microconnect will honour with reasonable commitment unless otherwise the same is beyond comprehension.'
    }
    $('#footerModal').modal('show')
  }

}]);;app.directive('getTouchComponent', function () {
    return {
        restrict: 'EA',
        controller: 'getTouchpageCtrl',
        templateUrl: './website/views/getInTouchComponent/getInTouchComponent.html'
    };
});

app.controller('getTouchpageCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', 'vcRecaptchaService', function ($scope, services, $rootScope, $location, $sce, vcRecaptchaService) {
    //debugger;

    $rootScope.btnactive = 'getintouch';

    $scope.contact = {}
    $scope.submitForm = function () {
        showLoader();
        console.log($scope.contact);

        services.getInTouch($scope.contact).then((res) => {
            debugger;
            // $scope.studentenrolldata = res;
            console.log(res)
            hideLoader();
            alert('Your message has been sent. Thank you!');
            grecaptcha.reset();
            location.reload();


        })

    }
    window.verifyRecaptchaCallback = function (response) {
        $('input[data-recaptcha]').val(response).trigger('change')
    }

    window.expiredRecaptchaCallback = function () {
        $('input[data-recaptcha]').val("").trigger('change')
    }



    $('#getintouchform').on('submit', function (e) {
        debugger;
        // if (!e.isDefaultPrevented()) {


        //     $scope.submitForm();
        //     return false;
        // }
        if (vcRecaptchaService.getResponse() === "") { //if string is empty
            alert("Please resolve the captcha and submit!")
        } else {

            $scope.submitForm();
        }
    })



}]);;app.directive('gtmComponent', function () {
    return {
        restrict: 'EA',
        controller: 'gtmCtrl',
        templateUrl: './website/views/gtmComponent/gtmComponent.html'
    };
});

app.controller('gtmCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;



}]);;app.directive('headerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'headerctrl',
        templateUrl: './website/views/partials/headerComponent/header.html'
    };
});

app.controller('headerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;
    $rootScope.btnactive = 'login';


    // var windowwidth = window.innerWidth;
    // if (windowwidth > 1299) {
    //     debugger;
    //     var topbar = document.getElementById('topbar')[0]; // 
    //     var header = document.getElementById('header')[0]; // 
    //     // root.setAttribute('class', 'container');
    //     root.setAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // } else {
    //     var topbar = document.getElementById('topbar')[0]; // 
    //     var header = document.getElementById('header')[0]; // 
    //     //root.removeAttribute('class', 'container');
    //     root.removeAttribute('class', 'container-responsive' + ' ' + 'container-lg');
    // }

    // Smooth scroll for the navigation menu and links with .scrollto classes
    var scrolltoOffset = $('#header').outerHeight() - 1;
    $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function (e) {
        debugger;
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                e.preventDefault();

                var scrollto = target.offset().top - scrolltoOffset;

                if ($(this).attr("href") == '#header') {
                    scrollto = 0;
                }

                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu, .mobile-nav').length) {
                    $('.nav-menu .active, .mobile-nav .active').removeClass('active');
                    $(this).closest('li').addClass('active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
                return false;
            }
        }
    });

    // // Activate smooth scroll on page load with hash links in the url
    // $(document).ready(function () {
    //     var scrolltoOffset2 = $('#header').outerHeight() - 1;
    //     if (window.location.hash) {

    //         var initial_nav = window.location.hash.replace('/', '');
    //         if ($(initial_nav).length) {
    //             var scrollto = $(initial_nav).offset().top.scrolltoOffset2
    //             $('html, body').animate({
    //                 scrollTop: scrollto
    //             }, 1500, 'easeInOutExpo');
    //         }
    //     }
    // });

    // Navigation active state on scroll
    var nav_sections = $('section');
    var main_nav = $('.nav-menu, .mobile-nav');

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop() + 200;

        nav_sections.each(function () {
            var top = $(this).offset().top,
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {
                if (cur_pos <= bottom) {
                    main_nav.find('li').removeClass('active');
                }
                main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
            }
            if (cur_pos < 300) {
                $(".nav-menu ul:first li:first, .mobile-nav ul:first li:first").addClass('active');
            }
        });
    });

    // // Mobile Navigation
    // if ($('.nav-menu').length) {
    //     var $mobile_nav = $('.nav-menu').clone().prop({
    //         class: 'mobile-nav d-lg-none'
    //     });
    //     $('body').append($mobile_nav);
    //     $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    //     $('body').append('<div class="mobile-nav-overly"></div>');

    //     $(document).on('click', '.mobile-nav-toggle', function (e) {
    //         debugger;
    //         $('body').toggleClass('mobile-nav-active');
    //         $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
    //         $('.mobile-nav-overly').toggle();
    //         if ($('body').hasClass('mobile-nav-active')) {
    //             autocomplete(document.getElementById("searchInput"), keys);
    //         }
    //     });

    //     $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
    //         e.preventDefault();
    //         $(this).next().slideToggle(300);
    //         $(this).parent().toggleClass('active');
    //     });

    //     $(document).click(function (e) {
    //         var container = $(".mobile-nav, .mobile-nav-toggle");
    //         if (!container.is(e.target) && container.has(e.target).length === 0) {
    //             if ($('body').hasClass('mobile-nav-active')) {
    //                 $('body').removeClass('mobile-nav-active');
    //                 $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
    //                 $('.mobile-nav-overly').fadeOut();
    //             }
    //         }
    //     });
    // } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    //     $(".mobile-nav, .mobile-nav-toggle").hide();
    // }
    // Toggle .header-scrolled class to #header when page is scrolled
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
            $('#topbar').addClass('topbar-scrolled');
        } else {
            $('#header').removeClass('header-scrolled');
            $('#topbar').removeClass('topbar-scrolled');
        }
    });

    if ($(window).scrollTop() > 100) {
        $('#header').addClass('header-scrolled');
        $('#topbar').addClass('topbar-scrolled');
    }



    ////auto fill search

    function autocomplete(inp, arr) {
        debugger;
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        debugger;
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                        var id = "#" + inp.value;
                        //$('a').find(id).trigger('click');
                        document.getElementById(inp.value).click();
                        //window.location.href = $(id).attr('href');
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        var isMobile = checkMobile()
        if(!isMobile){
            var eventTrigger = "keydown"
        }else{
            var eventTrigger = "keypress"
            
        }
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    };

    /*An array containing all the country names in the world:*/
    var loadData = {
        "acca": "index.html",
        "go-to-market": "go-to-market.html",
        "arbook": "arbook.html",
        "faq": "faq.html",
        "about": "about.html",
       // "blog": "https://www.theexpgroup.com/exp-blog/",
        "getintouch": "get-touch.html",
        "bfat": "bfat.html",
       // "enroll": "student-enroll.html",
         "enroll": "enroll.html",
        "login": "http://www.microconnect.org",
        "privacypolicy": "privacypolicy.html",
        "termsofservices": "termofconditions.html",

    };
    var keys = [];
    for (var k in loadData) keys.push(k);
    //  console.log(keys)

    autocomplete(document.getElementById("searchInput"), keys);

    $scope.search = function () {
        var keys = [];
        for (var k in loadData) keys.push(k);
        console.log(keys);
        var value = loadData[k];
        window.location.assign.href = value;
    };

    $scope.searchShow = false;
    $scope.searchhover = function () {
        //document.getElementById('getintouch').style = 'display:none'
        document.getElementById('faq').style = 'display:none'
        document.getElementById('about').style = 'display:none';
        // document.getElementById('blog').style = 'display:none';

        document.getElementById('searchInput').style = 'animation: fade-in 1s;';

    }
    $scope.searchleave = function () {
        // document.getElementById('getintouch').style = 'display:block'
        document.getElementById('faq').style = 'display:block'
        document.getElementById('about').style = 'display:block'
        // document.getElementById('blog').style = 'display:block';
        document.getElementById('searchInput').style = 'animation: fade-in 1s;';
    };

    $scope.searchClick = function () {

        if ($scope.searchShow == false) {
            $scope.searchShow = true;
            $scope.searchhover();
        } else {
            $scope.searchShow = false;
            $scope.searchleave();
        }

    };



    // Mobile Navigation
    if ($('.nav-menu').length) {
        var $mobile_nav = $('.nav-menu').clone().prop({
            class: 'mobile-nav d-lg-none'
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
        $('body').append('<div class="mobile-nav-overly"></div>');

        $(document).on('click', '.mobile-nav-toggle', function (e) {
            debugger;
            $('body').toggleClass('mobile-nav-active');
            $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
            $('.mobile-nav-overly').toggle();
            if ($('body').hasClass('mobile-nav-active')) {
                autocomplete(document.getElementById("searchInput"), keys);
            }
        });

        $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
            e.preventDefault();
            $(this).next().slideToggle(300);
            $(this).parent().toggleClass('active');
        });

        $(document).click(function (e) {
            var container = $(".mobile-nav, .mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
                    $('.mobile-nav-overly').fadeOut();
                }
            }
        });
    } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
        $(".mobile-nav, .mobile-nav-toggle").hide();
    }


}]);;app.directive('homeComponent', function () {
    return {
        restrict: 'E',
        controller: 'homectrl',
        templateUrl: './website/views/homeComponent/home.html'
    };
});


app.controller('homectrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce, ) {
    debugger;
   
    $scope.isMobile = checkMobile()

    services.SlideJsonData().then((res) => {
        $scope.myInterval = 3000;

        var sliderJSON = res.slideimages;
        $scope.slides = res.slideimages;
        //сортируем массив по order
        sliderJSON.sort(function (a, b) {
            return a.order - b.order;
        });
        if(!$scope.isMobile){
            var slider = $('#slidesection .carousel-inner');
            var bulletList = $('#slidesection .carousel-indicators')
   
           $.each(sliderJSON, function (index, element) {
               if (index == 0) {
                   var addclass = 'active'
               } else {
                   var addclass = ''
               }
   
               // slider.append(`<div class="carousel-item ${addclass}" ng-click="goToPage()"><img src="${element.imagePath}"  class="d-block w-100" alt="First slide"  /></div>`);
   
               slider.append(`<div class="carousel-item ${addclass}" ><a href="javascript:void(0)"  data-url="${element.url}" onclick="angular.element(this).scope().goToElementUrl(this)"><img src="${element.imagePath}"  class="d-block w-100" alt="First slide"  /></a></div>`);
   
               bulletList.append('<li data-target="#carouselExampleIndicators" data-slide-to="' + index + '" class="' + addclass + '"></li>');
   
   
           });
        }else{

        }
         
        //---------------------------------------------
        //Nivo slider
        //---------------------------------------------
        // $('#ensign-nivoslider').nivoSlider({
        //     effect: 'random',
        //     slices: 15,
        //     boxCols: 12,
        //     boxRows: 8,
        //     animSpeed: 500,
        //     pauseTime: 5000,
        //     startSlide: 0,
        //     directionNav: true,
        //     controlNavThumbs: false,
        //     pauseOnHover: true,
        //     manualAdvance: false,
        // });
    })


    services.SyllabusTabJsonData().then((res) => {
        // $scope.syllabustabdata = res.syllabustabdata
        // $scope.syllabustabdata = res.syllabustabdata.filter((item) => item.status === true);
        $scope.syllabustabdata = res.filter((item) => item.status === true)

    });
    services.ExamTabJsonData().then((res) => {
      //  $scope.examtabData = res.examtabdata
    
      $scope.examtabData = res.filter((item) => item.status === true)
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $('#vidBox1').VideoPopUp({
        backgroundColor: "#171717d9",
        opener: "videopopup1",
        maxweight: "340",
        idvideo: "v1",
        pausevideo: true

    });

    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        items: 1,
        nav: true,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"]

    });
    $("#studentplacemen-carousel").owlCarousel({

        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true

        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

    var popupInitiated = sessionStorage.getItem('popupinitiated');
    if (popupInitiated == null || popupInitiated == false) {

        /****popub init */
        services.getInitpopupdata().then((res) =>{
            $scope.popupdata = res[0];
            $scope.popupcontent =  $scope.popupdata.content;
            $scope.popupRedirectUrl =  $scope.popupdata.url;

            $('#popupmodal').modal('show')
            sessionStorage.setItem('popupinitiated', true);
        })
       
    };

    $scope.goToUrl = function (url) {
        window.open(url, "_blank")
    }
    $scope.goToElementUrl = function (element) {
        debugger;
        var url = element.attributes["data-url"].value
        if (url != "") {
            window.open(url, '_blank')
        }

        // window.open("https://www.google.com", '_blank');
    }

    // Activate smooth scroll on page load with hash links in the url
    $(document).ready(function () {
        var scrolltoOffset2 = $('#header').outerHeight() - 1;
        if (window.location.hash) {

            var initial_nav = window.location.hash.replace('/', '');
            if ($(initial_nav).length) {
                var scrollto = $(initial_nav).offset().top - scrolltoOffset2
                $('html, body').animate({
                    scrollTop: scrollto
                }, 1500, 'easeInOutExpo');
            }
        }
    });

    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }



}]);

function goToPath(event) {
    console.log('event', event)
    window.open("https://www.google.com", '_blank');
};app.directive('mcaboutComponent', function () {
    return {
        restrict: 'EA',
        controller: 'mcaboutCtrl',
        templateUrl: './website/views/aboutComponent/aboutComponent.html'
    };
});

app.controller('mcaboutCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    //debugger;

    // $("#foundertestimonials").owlCarousel({

    //     navigation: false, // Show next and prev buttons
    //     slideSpeed: 300,
    //     paginationSpeed: 400,
    //     singleItem: true

    //     // "singleItem:true" is a shortcut for:
    //     // items : 1, 
    //     // itemsDesktop : false,
    //     // itemsDesktopSmall : false,
    //     // itemsTablet: false,
    //     // itemsMobile : false

    // });
    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 450,
        autoplayHoverPause: true, // Stops autoplay
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });

}]);;app.directive('paymentComponent', function () {
    return {
        restrict: 'EA',
        controller: 'paymentCtrl',
        templateUrl: './website/views/paymentComponent/paymentComponent.html'
    };
});
function decodeData(value,type){
    if(value){
        var decodedData = atob(value);
        if(type == 'number'){
             return parseInt(decodedData)
        }else{
            return decodedData
        }
       // return decodedData
    }else{
        return false
    }
}
function encodeData(value){
    if(value){
        var encodedData = btoa(value);
        return encodedData
    }else{
        return false
    }
}


app.controller('paymentCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce','$http', function ($scope, services, $rootScope, $location, $sce,$http) {
    debugger;
   var urlParams = new URLSearchParams(window.location.search);
   $scope.enrollmentno =  decodeData(urlParams.get('param1')); 
   $scope.email = decodeData(urlParams.get('param2')); 
   $scope.order_id =decodeData(urlParams.get('param3'));
   $scope.amount =decodeData(urlParams.get('param4'),'number');


   function getCountry(ccode){
    var countrycode = ccode ? ccode : 'IN';
   
    var index = $rootScope.commonJsonData.countrylist.findIndex(x => x.code == countrycode)
    if(index > -1){
        var country = $rootScope.commonJsonData.countrylist[index].name
    }else{
        var country = '';
    }
     return country;
   


}


   if ( $scope.enrollmentno == false || $scope.email == false || $scope.order_id == false) {
    // var domain = window.location.host + location.pathname;
    //  var url =  window.location.protocol + '//' + window.location.host +  '/microconnect2/paymentstatus.html'
    //  self.location = url;
     window.open('index.html', "_self")
    
     //self.location = 'index.html';

 } else {
   
    var discardData = {
        discard : false,
        orderid: $scope.order_id
    }
    services.PaymentLinkCheck(discardData).then((res) => {
        if(res.statuscode == 'NT-200'){
            $scope.paymentData = {
                enrollmentnumber: $scope.enrollmentno,
                email:$scope.email,
                amount:parseInt($scope.amount)
             }
          
  

             $scope.paymentDetails = {
              tid: new Date().getTime(),
              merchant_id: __env.merchantId,
              order_id:  $scope.order_id,
              amount: $scope.amount,
              currency: "INR",
              redirect_url:__env.ccavenueredirectUri,
             // cancel_url:  __env.hostUrl,
              cancel_url: __env.ccavenueredirectUri,
              language: "EN",
              billing_name: res.se_name,
              billing_address: res.se_address.line1,
              billing_city: res.se_city,
              billing_state: res.se_state,
              billing_zip: res.pincode,
              billing_country:getCountry(res.se_country),
              billing_tel: parseInt(res.se_studentmobileno.mobileno),
              billing_email: $scope.paymentData.email,
              delivery_name: "",
              delivery_address: "",
              delivery_city: "",
              delivery_state: "",
              delivery_zip: "",
              delivery_country: "",
              delivery_tel: "",
              merchant_param1: "",
              merchant_param2: "",
              merchant_param3: "",
              merchant_param4: "",
              merchant_param5: "",
              emi_plan_id: "",
              emi_tenure_id: "",
              card_type: "",
              card_name: "",
              data_accept: "",
              card_number: "",
              expiry_month: "",
              expiry_year: "",
              cvv_number: "",
              issuing_bank: "",
              mobile_number: "",
              mm_id: "",
              otp: "",
              promo_code: ""
           };
        }else{
            alert('Payment url  already used  or expired')
            window.open('index.html', "_self")  
        }
       
    })

   
  
   $scope.paynow = function(){
    //   $scope.paymentDetails = {
    //       tid: new Date().getTime(),
    //       merchant_id: __env.merchantId,
    //       order_id: $scope.order_id,
    //       amount: $scope.amount,
    //       currency: "INR",
    //       redirect_url: __env.hostUrl,
    //       cancel_url:  __env.hostUrl,
    //       language: "EN",
    //       billing_name: "",
    //       billing_address: "",
    //       billing_city: "",
    //       billing_state: "",
    //       billing_zip: "",
    //       billing_country: "",
    //       billing_tel: "",
    //       billing_email: $scope.paymentData.email,
    //       delivery_name: "",
    //       delivery_address: "",
    //       delivery_city: "",
    //       delivery_state: "",
    //       delivery_zip: "",
    //       delivery_country: "",
    //       delivery_tel: "",
    //       merchant_param1: "",
    //       merchant_param2: "",
    //       merchant_param3: "",
    //       merchant_param4: "",
    //       merchant_param5: "",
    //       emi_plan_id: "",
    //       emi_tenure_id: "",
    //       card_type: "",
    //       card_name: "",
    //       data_accept: "",
    //       card_number: "",
    //       expiry_month: "",
    //       expiry_year: "",
    //       cvv_number: "",
    //       issuing_bank: "",
    //       mobile_number: "",
    //       mm_id: "",
    //       otp: "",
    //       promo_code: ""
    //    };
      
       $http({
          method: 'POST',
          url: 'ccavRequestHandler.php',
          data: $.param($scope.paymentDetails),
         headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
      }).then((res) =>{
          
          console.log(res);
          // window.open(`<form method="post" name="redirect" target="_blank" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"><?php echo "<input type=hidden name=encRequest value=$encrypted_data>";echo "<input type=hidden name=access_code value=$access_code>";  ?></form>`);
        //  window.open(res.data);
          document.open();
         document.write(`<form method="post" name="redirect" target="_blank" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"></form>`);
         document.close();
  
      })
       //services.ccAvenuePaymentDataSend($scope.paymentDetails);
   }
 }


 


//  for (var i = 0; i < arr.length; i++) {
//     debugger;
//        for (var k in arr[i]) {
//             if (arr[i].hasOwnProperty(k)) {
              
//                result[k] = arr[i][k];
//             }
//         }
      
//     }

}]);;app.controller('paymentSuccesCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', '$http', function ($scope, services, $rootScope, $location, $sce, $http) {
  debugger;


  var ccAvenueResponseString = getLocalStorageData("ccAvenueResponse");
  paymentshowLoader('Processing your  Payment,please  do not refresh or close the page ')
  var result = {};
  if (ccAvenueResponseString) {
    var ccAvenueResponseArray = ccAvenueResponseString;
    for (var i = 0; i < ccAvenueResponseArray.length; i++) {
      debugger;
      for (var k in ccAvenueResponseArray[i]) {
        if (ccAvenueResponseArray[i].hasOwnProperty(k)) {

          result[k] = ccAvenueResponseArray[i][k];
        }
      }

    }
    console.log(result);

    
    services.ccAvenuePaymentDataSend(result).then((res) =>{
         hidePaymentLoader(); 
         var studentname = res.se_name;
         var enrollmentno = res.se_studentid;
         var paidamount = res.paidamount
         if (res.paymentstatus === "Success") {
          $scope.title = "Payment Success";
          // $scope.paymentMessage = '<p>Thanks for your payment.we will process the admission and send you the login and password to start the International Diploma in Accounting and Business within next 7-10 days.<br>Happy Learning.</p>'
          $scope.paymentMessage = '<p>Dear '+ studentname +'-'+ enrollmentno +' </p><br><p>Your payment for Rs.'+ paidamount +' is successfully made. We will be in touch with you with next steps.<br>for any clarification please write to us at <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>-please don`t forget to mention your enrollment number in all communications.</p><br><p>Happy Learning.</p><br>Team MICROCONNECT';

    
        }
        else if (res.paymentstatus === "Aborted") {
          $scope.title = "Payment Aborted";
          $scope.paymentMessage = result.status_message;
    
        }
        else if (res.paymentstatus === "Failure") {
          $scope.title = "Payment Failure";
          $scope.paymentMessage = result.status_message;
        }
        else {
          $scope.title = "Payment Invalid";
          $scope.paymentMessage = result.status_message;
    
        }
    })
  }else{
    alert('Payment Failed');
    window.open('index.html')
  }

  //  for (var i = 0; i < arr.length; i++) {
  //     debugger;
  //        for (var k in arr[i]) {
  //             if (arr[i].hasOwnProperty(k)) {

  //                result[k] = arr[i][k];
  //             }
  //         }

  //     }
  $scope.encodeData = function(html){
   
    var trustedHtml = $sce.trustAsHtml(html);
    return trustedHtml;
  }

}]);;app.directive('qualificationComponent', function () {
    return {
        restrict: 'EA',
        controller: 'qualificationCtrl',
        templateUrl: './website/views/qualificationComponent/qualificationComponent.html'
    };
});

app.controller('qualificationCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    debugger;
    $scope.currenShowPage = 'accaqualification';
    if (window.location.hash) {

        var initial_nav = window.location.hash.replace('/', '');
        if (initial_nav == '#acca-qualification') {
            $scope.currenShowPage = 'accaqualification';

        } else if (initial_nav == '#bsc-applied-accounting') {
            $scope.currenShowPage = 'bsc-aa';
        } else if (initial_nav == '#msc-professional-accounting') {
            $scope.currenShowPage = 'msc-pa';
        }
    }



}]);;app.directive('studentEnrollComponent', function () {
    return {
        restrict: 'EA',
        controller: 'studentEnrollCtrl',
        templateUrl: './website/views/studentEnrollComponent/studentEnrollComponent.html'
    };
});

app.controller('studentEnrollCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', 'vcRecaptchaService', function ($scope, services, $rootScope, $location, $sce, vcRecaptchaService) {
    //debugger;
    $rootScope.btnactive = 'enroll';
    $scope.studentenrolldata = {
        se_address: {
            country: "",
            state: "",
            city: ""
        }
    }
    $scope.country = '';
    $scope.state = '';
    $scope.city = '';
    $scope.parentnum_dialcode = "+91";
    $scope.studentnum_dialcode = "+91"
    // $scope.studentenrolldata.se_address["country"] = '';
    // $scope.studentenrolldata.se_address["state"] = '';

    $('[data-type="adhaar-number"]').keyup(function () {
        var value = $(this).val();
        value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
        $(this).val(value);
    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $scope.uploadedFile = function (element, id) {
        debugger;
        showLoader();
        var maxSize = 5 * 1024;
        $scope.currentFile = element.files[0];
        var fileSize = Math.round((element.files[0].size / 1024));
        var reader = new FileReader();
        if (fileSize > maxSize) {
            alert('file size is more than 5mb');
            hideLoader();
            return false;
        } else {
            reader.onload = function (event) {


                $scope.$apply(function ($scope) {
                    //$scope.files = element.files;
                    if (id == 'marksheet') {
                        $scope.marksheet = event.target.result
                        $scope.marksheetSrc = element.files[0];
                        services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_marksheet = res
                            console.log(res);
                            hideLoader();

                        })
                    } else if (id == 'aadharcard') {
                        $scope.aadharcard = event.target.result
                        $scope.aadharcardSrc = element.files[0]
                        services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_aadharimage = res;
                            console.log(res)
                            hideLoader();
                        })
                    } else if (id == 'studentphoto') {
                        $scope.studentphoto = event.target.result
                        $scope.studentphotoImgSrc = element.files[0];
                        services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_studentimage = res;
                            console.log(res)
                            hideLoader();
                        })
                    } else if (id == 'bonofide') {
                        $scope.bonofide = event.target.result
                        $scope.bonofideSrc = element.files[0]
                        services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
                            debugger;
                            $scope.studentenrolldata.se_bonafide = res;
                            console.log(res)
                            hideLoader();
                        })
                    }
                });
            }
            reader.readAsDataURL(element.files[0]);
        }




    };

    $scope.setCountry = function (countrycode) {
        $scope.studentenrolldata.se_address.country = countrycode;
        $scope.studentenrolldata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.studentenrolldata.se_address.state = state;
        $scope.studentenrolldata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
       // alert( $scope.studentenrolldata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.studentenrolldata.se_address.city = city;
        $scope.studentenrolldata.se_city = city;

        //alert(city);

    }


    $scope.submitEnrollData = function () {
        if ($scope.studentenrolldata.agree) {

            showLoader();

            if (vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please resolve the captcha and submit!")
            } else {

                 $scope.studentenrolldata.se_dateofbirth = timeToUnix($scope.dateofbirth);
                 $scope.studentenrolldata.se_parentmobileno ={
                    dialcode: $scope.parentnum_dialcode,
                    mobileno:  $scope.parentnum_dialcode + $scope.parentmobileno
                 }
                 $scope.studentenrolldata.se_studentmobileno ={
                    dialcode: $scope.studentnum_dialcode,
                    mobileno:  $scope.studentnum_dialcode + $scope.studentmobileno
                 }
                 $scope.studentenrolldata.se_schoolcode = 'MC-'+$scope.se_schoolcode
                services.StudentEnroll($scope.studentenrolldata).then((res) => {
                    debugger;
                    // $scope.studentenrolldata = res;
                    console.log(res)
                    hideLoader();
                   // alert('Auto email confirmation to be sent to  both the email ids (Parent & Student)');

                    // alert('Thanks for registering for Free ACCA course demo. you will hear from us soon');
                    alert('Successfully registered')
                    location.reload();


                })
            }


        } else {
            alert('Please select ACCA’s mandatory declaration')

        }
    };




}]);;app.directive('demoComponent', function () {
    return {
        restrict: 'EA',
        controller: 'demoFormCtrl',
        templateUrl: './website/views/tryBeforeFormComponent/tryBeforeFormComponent.html'
    };
});

app.controller('demoFormCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', 'vcRecaptchaService', function ($scope, services, $rootScope, $location, $sce, vcRecaptchaService) {
    debugger;

    // var countryJson = ``;

    // // var selectBox = $('#country')
    // document.getElementById('country').innerHTML = countryJson
    // selectBox.append = countryJson;

    $scope.demoRegData = {};
    //  $scope.exp_token_set = function(){
    //         var exp_sessiondata = {
    //             "username":decrypt(__env.expusername),
    //             "password":decrypt(__env.exppassword),
    //         }
    //         services.exp_token_get(exp_sessiondata).then((res) =>{
    //             debugger;
    //             console.log(res);
    //             if(res.statusCode == 201){
    //                 setSessionStorage('tokenset',true);
    //                  setSessionStorage('exp_token',res.data.access_token);
    //                  setSessionStorage('exp_token_expirytime',res.data.access_token_expiry);
    //                  $scope.initData(res.data.access_token); 
    //             }
               
    //         })
    // };

    // $scope.initData = function(token){
    //     services.exp_allowed_Countries(token).then((res) =>{
    //         debugger;
    //         console.log(res);
    //         if(res.statusCode == 201){
    //            $scope.country = res.data;
    //         }
           
    //     })
    //     services.exp_courses_demo(token).then((res) =>{
    //         debugger;
    //         console.log(res);
    //         if(res.statusCode == 201){
    //             $scope.country = res.data;
    //          }
           
    //     })
    // }

    // var expInitAccessTokenData = getSessionStorageData('tokenset')
    // if(expInitAccessTokenData){
    //         var token = getSessionStorageData('exp_token');
    //         var expirytime = getSessionStorageData('exp_token_expirytime')
    //         if(expirycheck(expirytime)){
    //             $scope.exp_token_set();
    //         }else{
    //             $scope.initData(token);  
    //         }
        
    // }else{
    //     $scope.exp_token_set();
    // }
    function expcourseArrayConstruct(expCoursesobj){
        expcourseArray = [];
        for(const property in expCoursesobj){

            var arrayobj =  expCoursesobj[property];
            if(arrayobj){
                for(var value of arrayobj){
                    expcourseArray.push(value);
                }
            }
            console.log(`${property}: ${expCoursesobj[property]}`);
        }
       return expcourseArray;
    }
    services.getExpCourse().then((res) =>{
        debugger;
        $scope.expCoursesobj = expcourseArrayConstruct(res.courses);
        console.log($scope.expCoursesobj);
    })


    $scope.selectCourse = function(demoCourseID){
           var index = $scope.expCoursesobj.findIndex(x => x.id === demoCourseID);
           if(index > -1){
              $scope.demoRegData.demopaperName = $scope.expCoursesobj[index].name;
           }else{
            $scope.demoRegData.demopaperName = '';
           }

    }

    $scope.submitDemoData = function () {


        showLoader();
        if ($scope.demoRegData.agree) {
            if (vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please resolve the captcha and submit!")
            } else {

                services.demoRegData($scope.demoRegData).then((res) => {
                    debugger;
                    // $scope.studentenrolldata = res;
                    console.log(res)
                    hideLoader();
                    // alert('Auto email confirmation to be sent to  both the email ids (Parent & Student)');
                    alert('Thanks for registering for Free ACCA course demo. you will hear from us soon')
                    location.reload();


                })
            }
        } else {
            alert('Please select ACCA’s mandatory declaration')
        }




    }










}]);;! function (a) {
    "use strict";
    a.module("vcRecaptcha", [])
}(angular),
function (a) {
    "use strict";

    function b() {
        throw new Error('You need to set the "key" attribute to your public reCaptcha key. If you don\'t have a key, please get one from https://www.google.com/recaptcha/admin/create')
    }
    a.module("vcRecaptcha").provider("vcRecaptchaService", function () {
        var c = this,
            d = {};
        c.onLoadFunctionName = "vcRecaptchaApiLoaded", c.setDefaults = function (b) {
            a.copy(b, d)
        }, c.setSiteKey = function (a) {
            d.key = a
        }, c.setTheme = function (a) {
            d.theme = a
        }, c.setStoken = function (a) {
            d.stoken = a
        }, c.setSize = function (a) {
            d.size = a
        }, c.setType = function (a) {
            d.type = a
        }, c.setLang = function (a) {
            d.lang = a
        }, c.setBadge = function (a) {
            d.badge = a
        }, c.setOnLoadFunctionName = function (a) {
            c.onLoadFunctionName = a
        }, c.$get = ["$rootScope", "$window", "$q", "$document", "$interval", function (e, f, g, h, i) {
            function j() {
                return m ? g.when(m) : o
            }

            function k() {
                if (!m) throw new Error("reCaptcha has not been loaded yet.")
            }

            function l() {
                return a.isFunction((f.grecaptcha || {}).render)
            }
            var m, n = g.defer(),
                o = n.promise,
                p = {};
            f.vcRecaptchaApiLoadedCallback = f.vcRecaptchaApiLoadedCallback || [];
            var q = function () {
                m = f.grecaptcha, n.resolve(m)
            };
            if (f.vcRecaptchaApiLoadedCallback.push(q), f[c.onLoadFunctionName] = function () {
                    f.vcRecaptchaApiLoadedCallback.forEach(function (a) {
                        a()
                    })
                }, l()) q();
            else if (f.document.querySelector('script[src^="https://www.google.com/recaptcha/api.js"]')) var r = i(function () {
                l() && (i.cancel(r), q())
            }, 25);
            else {
                var s = f.document.createElement("script");
                s.async = !0, s.defer = !0, s.src = "https://www.google.com/recaptcha/api.js?onload=" + c.onLoadFunctionName + "&render=explicit", h.find("body")[0].appendChild(s)
            }
            return {
                create: function (a, c) {
                    return c.sitekey = c.key || d.key, c.theme = c.theme || d.theme, c.stoken = c.stoken || d.stoken, c.size = c.size || d.size, c.type = c.type || d.type, c.hl = c.lang || d.lang, c.badge = c.badge || d.badge, c.sitekey || b(), j().then(function (b) {
                        var d = b.render(a, c);
                        return p[d] = a, d
                    })
                },
                reload: function (a) {
                    k(), m.reset(a), e.$broadcast("reCaptchaReset", a)
                },
                execute: function (a) {
                    k(), m.execute(a)
                },
                useLang: function (a, b) {
                    var c = p[a];
                    if (!c) throw new Error("reCaptcha Widget ID not exists", a);
                    var d = c.querySelector("iframe");
                    if (!b) return d && d.src && /[?&]hl=\w+/.test(d.src) ? d.src.replace(/.+[?&]hl=(\w+)([^\w].+)?/, "$1") : null;
                    if (d && d.src) {
                        var e = d.src;
                        /[?&]hl=/.test(e) ? e = e.replace(/([?&]hl=)\w+/, "$1" + b) : e += (-1 === e.indexOf("?") ? "?" : "&") + "hl=" + b, d.src = e
                    }
                },
                getResponse: function (a) {
                    return k(), m.getResponse(a)
                },
                getInstance: function (a) {
                    return p[a]
                },
                destroy: function (a) {
                    delete p[a]
                }
            }
        }]
    })
}(angular),
function (a) {
    "use strict";
    a.module("vcRecaptcha").directive("vcRecaptcha", ["$document", "$timeout", "vcRecaptchaService", function (b, c, d) {
        return {
            restrict: "A",
            require: "?^^form",
            scope: {
                response: "=?ngModel",
                key: "=?",
                stoken: "=?",
                theme: "=?",
                size: "=?",
                type: "=?",
                lang: "=?",
                badge: "=?",
                tabindex: "=?",
                required: "=?",
                onCreate: "&",
                onSuccess: "&",
                onExpire: "&",
                onError: "&"
            },
            link: function (e, f, g, h) {
                function i() {
                    h && h.$setValidity("recaptcha", null), m()
                }

                function j() {
                    c(function () {
                        e.response = "", l(), e.onExpire({
                            widgetId: e.widgetId
                        })
                    })
                }

                function k() {
                    var a = arguments;
                    c(function () {
                        e.response = "", l(), e.onError({
                            widgetId: e.widgetId,
                            arguments: a
                        })
                    })
                }

                function l() {
                    h && h.$setValidity("recaptcha", !1 === e.required ? null : Boolean(e.response))
                }

                function m() {
                    d.destroy(e.widgetId), a.element(b[0].querySelectorAll(".pls-container")).parent().remove()
                }
                e.widgetId = null, h && a.isDefined(g.required) && e.$watch("required", l);
                var n = e.$watch("key", function (b) {
                    var h = function (a) {
                        c(function () {
                            e.response = a, l(), e.onSuccess({
                                response: a,
                                widgetId: e.widgetId
                            })
                        })
                    };
                    d.create(f[0], {
                        callback: h,
                        key: b,
                        stoken: e.stoken || g.stoken || null,
                        theme: e.theme || g.theme || null,
                        type: e.type || g.type || null,
                        lang: e.lang || g.lang || null,
                        tabindex: e.tabindex || g.tabindex || null,
                        size: e.size || g.size || null,
                        badge: e.badge || g.badge || null,
                        "expired-callback": j,
                        "error-callback": g.onError ? k : void 0
                    }).then(function (b) {
                        l(), e.widgetId = b, e.onCreate({
                            widgetId: b
                        }), e.$on("$destroy", i), e.$on("reCaptchaReset", function (c, d) {
                            (a.isUndefined(d) || b === d) && (e.response = "", l())
                        })
                    }), n()
                })
            }
        }
    }])
}(angular);;app.factory('services', services);
services.$inject = ['$http', 'Upload'];

function services($http, Upload) {
   // const apiUrl = 'http://104.218.53.125/mcconnect/api/v1.0/'
  const apiUrl = __env.apiUrl;
    var services = {
        SlideJsonData: SlideJsonData,
        FaqJsonData: FaqJsonData,
        ExamTabJsonData: ExamTabJsonData,
        SyllabusTabJsonData: SyllabusTabJsonData,
        uploadMarkSheet: uploadMarkSheet,
        uploadAadhar: uploadAadhar,
        uploadStudentPhoto: uploadStudentPhoto,
        uploadBonofideLetter:uploadBonofideLetter,
        StudentEnroll: StudentEnroll,
        getInTouch: getInTouch,
        commonJsonData: commonJsonData,
        demoRegData: demoRegData,
        ccAvenuePaymentDataSend:ccAvenuePaymentDataSend,
        PaymentLinkCheck:PaymentLinkCheck,
        exp_token_get : exp_token_get,
        exp_courses_demo:exp_courses_demo,
        exp_allowed_Countries:exp_allowed_Countries,
        exp_demo_register:exp_demo_register,
        getInitpopupdata:getInitpopupdata,
        getExpCountry:getExpCountry,
        getExpCourse:getExpCourse
    }
    return services;

    function commonJsonData() {
        return $http.get('./common/data/common.json').then(function (res) {
            return res.data;
        });
    }

    function SlideJsonData() {
        return $http.get('./common/data/slide.json').then(function (res) {
            return res.data;
        });
        // return $http.get(apiUrl + 'slides').then(function (res) {
        //     return res.data;
        // });
    }

    function FaqJsonData() {
        // return $http.get('./common/data/faq.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getfaqdata', {}).then(function (res) {
            return res.data;
        });
    }

    function ExamTabJsonData() {
        // return $http.get('./common/data/examtab.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getexams', {}).then(function (res) {
            return res.data;
        });
    }

    function SyllabusTabJsonData() {
        // return $http.get('./common/data/syllabustab.json').then(function (res) {
        //     return res.data;
        // });
        return $http.post(apiUrl + 'getsyllabus', {}).then(function (res) {
            return res.data;
        });
    }

    function uploadMarkSheet(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'marksheet',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadAadhar(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'aadharsave',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadStudentPhoto(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'profileimage',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadBonofideLetter(imgfile) {
        debugger;
        return Upload.upload({
            url: apiUrl + 'studentbonafide',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function StudentEnroll(studentEnrollData) {
        return $http.post(apiUrl + 'studentenroll', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function getInTouch(getInTouchData) {
        return $http.post(apiUrl + 'contactemail', getInTouchData).then(function (res) {
            return res.data;
        });
    }

    function demoRegData(demoRegData) {
        return $http.post(apiUrl + 'requestdemo', demoRegData).then(function (res) {
            return res.data;
        });
    }

    // function ccAvenuePaymentDataSend(paymentData){
    //     debugger;
    //      $http.post('ccavRequestHandler.php', paymentData)
    // }
    function ccAvenuePaymentDataSend(paymentData){
        debugger;
        return $http.post(apiUrl + 'paymentverify', paymentData).then(function (res) {
            return res.data;
        });
       
    }
    function PaymentLinkCheck(discardData) {
        return $http.post(apiUrl + 'ordercheck', 
            discardData
        ).then(function (res) {
            return res.data;
        });
    }

    function exp_token_get(expdata){
        return $http.post('https://api3.exp247.com/v1sandbox/sessions', 
        expdata
        ).then(function (res) {
            return res.data;
        });
    }
    function exp_allowed_Countries(token){
        $http.defaults.headers.common['Authorization'] =  token;
     
        return $http.get('https://api3.exp247.com/v1sandbox/countries').then(function (res) {
          
            return res.data;
        });
    }
    
    function exp_courses_demo(token){
        $http.defaults.headers.common['Authorization'] =  token;
       
        return $http.get('https://api3.exp247.com/v1sandbox/courses/demo').then(function (res) {
          
            return res.data;
        });
    }
    function exp_demo_register(expdata,token){
        $http.defaults.headers.common['Authorization'] =  token;
        return $http.post(' https://api3.exp247.com/v1sandbox/register', 
        expdata
        ).then(function (res) {
            return res.data;
        });
    }


    function getInitpopupdata(){
        
        return $http.post(apiUrl + 'popubdata', 
            {}
        ).then(function (res) {
            return res.data;
        });
    }


    function getExpCourse() {

        return $http.post(apiUrl + 'expcourse',
            {}
        ).then(function (res) {
            return res.data;
        });
    };

    function getExpCountry() {

        return $http.post(apiUrl + 'expcountry',
            {}
        ).then(function (res) {
            return res.data;
        });
    }

};function timeToUnix(dateTime) {
    var UnixtimeStamp = new Date(dateTime).getTime();
    return UnixtimeStamp
}
function setLocalStorage(key, value) {
    debugger;
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorageData(key) {
    debugger;
    var data = JSON.parse(localStorage.getItem(key));
    return data;
}
function setSessionStorage(key, value) {
    debugger;
    sessionStorage.setItem(key, value);
}

function getSessionStorageData(key) {
    debugger;
    var data = sessionStorage.getItem(key);
    return data;
}

function clearLocalStorage(key) {
    localStorage.removeItem(key);
}
function clearSessionStorage(key) {
    sessionStorage.removeItem(key);
}

function hidePaymentLoader() {
    $("#showLoading").hide()

}
function encrypt(str){
    if(str){
        var enc = window.btoa(str);
        return enc;
    }else{
        return false
    }
    
}

function decrypt(str){
    if(str){
        var dec = window.atob(str);
        return dec;
    }else{
        return false
    }
    
}
function paymentshowLoader(e) {
    e || (e = "Loading....!");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader2"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
};

function expirycheck(exp) {
    if (Date.now() <= exp * 1000) {
      console.log(false, 'token is not expired');
      return false;
    } else { 
      console.log(true, 'token is expired') ;
      return true;
    }
  }



!(function ($) {
    "use strict";

    // Preloader
    $(window).on('load', function () {
        if ($('#preloader').length) {
            $('#preloader').delay(100).fadeOut('slow', function () {
                $(this).remove();
            });
        }
    });


    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });

    // jQuery counterUp
    $('[data-toggle="counter-up"]').counterUp({
        delay: 10,
        time: 1000
    });

    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });

    // Initiate the venobox plugin
    $(document).ready(function () {
        $('.venobox').venobox();
    });

    // Initiate the datepicker plugin
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true
        });
    });

    function amountFormat(amount) {
        if(amount){
            var amountValue = parseInt(amount)
            const formatter = new Intl.NumberFormat('en-IN', {
         
                currency: 'INR',
                minimumFractionDigits: 2
              })
              
              
              var value = formatter.format(amountValue) // "10.00"
              
             return value;
        }
        
        }
    

})(jQuery);