module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        concurrent: {
            dev: {
                tasks: ['watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        concat: {

            options: {
                banner: '/*<%= grunt.template.today() %> */\n',
                separator: ';',
                mangle: true,
                compress: {
                    drop_console: true
                },
            },
            dist: {
                src: ['./app/**/*.js'],
                dest: './dist/microconnectAdmin.js',
            },
        },
        uglify: {
            options: {
                banner: '/*<%= grunt.template.today() %> */\n',
                mangle: true,
                compress: {
                    drop_console: true
                },
            },
            task1: {
                files: [{
                    expand: true,
                    cwd: './dist/',
                    src: ['microconnectAdmin.js'],
                    dest: './dist/',
                    ext: '.min.js'
                }]
            }
        },
        cssmin: {
            minStyle: {
                files: [{
                    expand: true,
                    cwd: './assets/css/',
                    src: 'Admin.css',
                    dest: './dist/css',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            js: {
                files: ['./app/**/*.js'],
                tasks: ['concat', 'uglify'],
                option: {
                    spawn: false
                }
            },
            css: {
                files: ['./assets/css/Admin.css'],
                tasks: ['cssmin'],
                option: {
                    spawn: false
                }
            }
        }
    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //  grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    //grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-concurrent');

    //grunt.registerTask('default',['concat','uglify','less', 'cssmin','concurrent']);
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'concurrent']);
    //grunt.registerTask('start',['concurrent']);

    grunt.event.on('watch', function (action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });
};