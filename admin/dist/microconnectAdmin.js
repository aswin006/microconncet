/*Mon Sep 20 2021 18:49:53 */

var admin = angular.module('microconnectAdmin', ['microconnectServices',
    'ui.router', 'ngMessages', 'ngFileUpload','ckeditor','ngCsv', 'ngTagsInput','psi.sortable'])

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
admin.constant('__env', env);
setLocalStorage('cacheVersion', env.cacheVersion)


var retrievedObject = getLocalStorageData("adminUserdata");

window.onpopstate = function (e) {
    window.history.forward(1);
}
admin.run(['$location', '$rootScope', '$window', '$http', '$stateParams', '$anchorScroll', 'services','$state', function ($location, $rootScope, $window, $http, $stateParams, $anchorScroll, services,$state) {

    $rootScope.logout = function () {
        //debugger;
        sessionStorage.clear();
        localStorage.clear();
        var newURL = __env.loginpage
        window.location.href = newURL;
    };
    // $rootScope.$on('$viewContentLoaded', function() {
    //     $templateCache.removeAll();
    //  });
    $rootScope.$on('$stateChangeSuccess', function (event, current, previous, from, error) {
        debugger;
        //window.scrollTo(0, 0);
        $rootScope.pagetitle = current.title;
        $rootScope.previous_path = from.name;
        $rootScope.current_path = current.name;
        $rootScope.controllerName = current.controller;
        $rootScope.passedStateParams = $stateParams;

       



        if (sessionStorage.getItem('token') == null) {
            $rootScope.logout();
        }else{
              /***begin-for developing testing purpose  */
        //  if (retrievedObject.email === __env.defaultEmail) {
            if (retrievedObject.email === 'developer@microconnect.co.in') {
                 /***end-for developing testing purpose  */
                $rootScope.enableNewBtn = true;
                $rootScope.enableEditBtn = true;
                $rootScope.enableViewBtn = true;
            } else {
                var enableviews = getLocalStorageData('enableViews');
                $rootScope.enableNewBtn = enableviews.new;
                $rootScope.enableEditBtn = enableviews.edit;
                $rootScope.enableViewBtn = enableviews.view;
            }
        }

    });

    $rootScope.addImgDefUrl = function (img) {
        debugger;
        if (img != null && img != "") {
            var first_part = img.substring(0, 8);

            if (first_part === 'http://' || first_part === 'https://') {
                var image = img
            } else {
                var image = __env.commonImgUrl + img
            }
        } else {
            var image = img
        }

        return image
    };
    services.commonJsonData().then((res) => {

        $rootScope.commonJsonData = res;
    });
    if (localStorage.getItem('adminUserdata') != null) {
        $rootScope.userDetails = getLocalStorageData('adminUserdata');
        if ($rootScope.userDetails.userimage == '') {
            $rootScope.userImage = LetterAvatar($rootScope.userDetails.username, 60, 1);
        } else {
            $rootScope.userImage = $rootScope.addImgDefUrl($rootScope.userDetails.userimage)
        }
    }

    $rootScope.pageReload = function(){
       // $state.reload();
        $window.location.reload()
    }


}]);

admin.factory('httpInterceptorSerivce', ['$rootScope', function httpInterceptorSerivce($rootScope) {
    return {

        request: function (config) {
            //debugger;
            //$rootScope.isLoader = 1;
            mainShowLoader()
            return config;
        },

        requestError: function (config) {
            //debugger;
            //$rootScope.isLoader = 0;
            mainHideLoader()
            return config;
        },

        response: function (res) {
            //$rootScope.isLoader = 0;
            // setTimeout(function() {
            mainHideLoader()
            // }, 2000);

            return res;
        },

        responseError: function (res) {
            //debugger;

            return res;
        }
    }
}]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptorSerivce');
}]);
 admin.factory('preventTemplateCache', function($injector) {
   // var ENV = $injector.get('ENV');
return {
      'request': function(config) {
          var date = new Date().getTime();
        if (config.url.indexOf('views') !== -1) {
          config.url = config.url + '?t=' + date;
        }
        return config;
      }
    }
  })
.config(function($httpProvider) {
    $httpProvider.interceptors.push('preventTemplateCache');
  });;admin.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider

        .state('home', {
            url: '/',
            template: '<main-component></main-component>',

        })
        .state('home.dashboard', {
            title: 'Dashboard',
            url: 'dashboard',
            templateUrl: './views/dashboard.html',
            controller: 'dashboardCtrl',
        })
        /***begin-07-12-2020-microconnect-new-changes */
        .state('home.studentslist', {
            title: 'Student List',
            url: 'studentlist',
            templateUrl: './views/studentlist.html',
            controller: 'studentlistctrl'

        })

        .state('home.student', {
            title: 'Manage Users',
            url: 'student/:studentid',
            templateUrl: './views/student.html',
            controller: 'studentctrl',
            resolve: {
                student: ['services', '$stateParams', function (services, $stateParams) {

                    debugger;
                    var studentID = $stateParams.studentid;

                    if (studentID != 0) {
                        return services.getEditEnrollUser(studentID);
                    }
                }]
            }

        })
        /***end-07-12-2020-microconnect-new-changes */
        .state('home.accasyllabuslist', {
            title: 'ACCA Syllabus List',
            url: 'accasyllabuslist',
            templateUrl: './views/accasyllabuslist.html',
            controller: 'accasyllabuslistctrl'

        })


        .state('home.accasubjectlist', {
            title: 'ACCA Subject List',
            url: 'accasubjectlist',
            templateUrl: './views/accasubjectlist.html',
            controller: 'accasubjectlistctrl'

        })
        .state('home.faqlist', {
            title: 'ACCA FAQ List',
            url: 'faqlist',
            templateUrl: './views/faqlist.html',
            controller: 'faqlistctrl'

        })
        .state('home.requestdemouserlist', {
            title: 'Request Demo Customers List',
            url: 'requestdemouserlist',
            templateUrl: './views/requestDemoUser.html',
            controller: 'requestdemouserlistctrl'

        })
        .state('home.getintouchuserlist', {
            title: ' Get In Touch Customers List',
            url: 'getintouchuserlist',
            templateUrl: './views/getintouchUser.html',
            controller: 'getintouchuserlistctrl'

        })
        .state('home.popupentry', {
            title: ' POPUP Entry',
            url: 'popupentry',
            templateUrl: './views/popupentry.html',
            controller: 'popupentryctrl'

        })
        .state('home.paymentdetail', {
            title: 'Payment Detail',
            url: 'paymentdetail',
            templateUrl: './views/paymentdetails.html',
            controller: 'paymentdetailctrl'

        })
        .state('home.userrolecreation', {
            title: 'User Role Creation',
            url: 'userrolecreation',
            templateUrl: './views/userRoleCreation.html',
            controller: 'userrolecreationctrl'

        })

        /***begin-07-12-2020-microconnect-new-changes */
        .state('home.userslist', {
            title: 'Users List',
            url: 'userlist',
            templateUrl: './views/userslist.html',
            controller: 'userlistctrl',
            resolve: {
                userrole: ['services', function (services) {
                    return services.getUserRoleData();
                }]
            }

        })
        .state('home.users', {
            title: 'Manage Users',
            url: 'users/:userid',
            templateUrl: './views/users.html',
            controller: 'userctrl',
            resolve: {
                user: ['services', '$stateParams', function (services, $stateParams) {

                    debugger;
                    var userID = $stateParams.userid;

                    if (userID != 0) {
                        return services.getEditEnrollUser(userID);
                    }
                }]
            }

        })
        
    /***end-07-12-2020-microconnect-new-changes */

    .state('home.paymentschedulelist', {
        title: 'Payment Shedule List',
        url: 'paymentschedulelist',
        templateUrl: './views/paymentschedulelist.html',
        controller: 'paymentschedulelistctrl',
    })
    .state('home.paymentshedule', {
        title: 'Manage Payment Shedule',
        url: 'paymentschedule/:studentid',
        templateUrl: './views/paymentschedule.html',
        controller: 'paymentScheduleCtrl',
        // resolve: {
        //     paymentshedule: ['services', '$stateParams', function (services, $stateParams) {

        //         debugger;
        //         var studentID = $stateParams.studentid;

        //         if (studentID != 0) {
        //             return services.getEditPaymentShedule(studentID);
        //         }
        //     }]
        // }

    })
    .state('home.payoutdetails', {
        title: 'Manage Payout Details',
        url: 'payoutdetailslist',
        templateUrl: './views/payoutdetailslist.html',
        controller: 'payoutdetailslistCtrl',
       

    })
    .state('home.invoicemaintaion', {
        title: 'Invoice Maintain',
        url: 'invoicemaintaion/:studentid',
        templateUrl: './views/invoicemaintain.html',
        controller: 'invoicemaintainCtrl',
       

    })
    


}]);;admin.directive('changepwdComponent', function () {
    return {
        restrict: 'EA',
        controller: 'changepwdCtrl',
        templateUrl: './views/changepassword.html'
    };
});

admin.controller('changepwdCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    
   






}]);;admin.controller('dashboardCtrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    
   


}]);;/****syllabus list */
admin.controller('faqlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.faqEntryData = {};
    services.getFaqData().then(function (res) {
        debugger;
        $scope.faqData = res;
        for(var i =0 ; i < $scope.faqData.length; i++){
            $scope.faqData[i]['order'] =    i + 1; 
        }
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#faqlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showFaqModal = function () {
        $('#faqentryModal').modal('show')
    };
    $scope.hideFaqModal = function () {
        $('#faqentryModal').modal('hide')
    };


    $scope.faqOrderModal = function(){
        $scope.list = angular.copy($scope.faqData);
        $('#faqOrderModal').modal('show')
    }
    $scope.hidefaqOrderModal = function(){
        $('#faqOrderModal').modal('hide')
    }

    $scope.faqModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "FAQ Entry";
            $scope.buttonText = "Save";
            $scope.faq_id = 0;
            $scope.faqEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showFaqModal();
        } else {
            $scope.title = "Edit FAQ  ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit FAQ';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View FAQ';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.faq_id = id;
            services.getEditFaq($scope.faq_id).then((res) => {
                // console.log(res);
                $scope.faqEntryData = res;
                $scope.data.textInput = $scope.faqEntryData.subtitle;
                $scope.showFaqModal();
            })
        }

    }


    $scope.saveFaq = function (faq_id) {
        debugger;
        $scope.faqEntryData.subtitle = $scope.data.textInput;
        if (faq_id == 0) {
            services.insertFaq($scope.faqEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideFaqModal();
                    services.toast('success', res.message);
                    $scope.faqData.push($scope.faqEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateFaq($scope.faqEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideFaqModal();
                    services.toast('success', res.message);
                    // var index = $scope.faqData.findIndex(x => x._id == faq_id);
                    // if (index > -1) {
                    //     $scope.faqData[index] = $scope.faqEntryData;
                    // }
                    $rootScope.pageReload();
                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };

    $scope.savefaqOrderModal = function(){
        var reorderedFaqList = [];
        for(var i =0 ; i < $scope.list.length ; i++){
            $scope.list[i]['reorder'] = i + 1;
        }
        console.log( $scope.list)
        services.updateFaqOrder($scope.list).then(function (res) {
            debugger;
            if (res.statuscode == 'NT-200') {
                $scope.hidefaqOrderModal();
                services.toast('success', res.message);
                $rootScope.pageReload();
            } else {
              
                services.toast('warning', res.message)
            }

        });
    }
}]);;admin.directive('headerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'headerctrl',
        templateUrl: './views/partials/header/header.html'
    };
});

admin.controller('headerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    
   $scope.logout = function(){
    $rootScope.logout();
   };

   $scope.passwordChangedata = {};

   $rootScope.openPasswordModal = function(data){
    $scope.passwordChangedata['username'] = data.username;
       $('#passwordModal').modal('show')
   };

   $rootScope.closePasswordModal = function(){
    $scope.passwordChangedata = {};
    $('#passwordModal').modal('hide');

}

    $scope.showPassword = false;
    $scope.toggleShowPassword = function () {
        $scope.showPassword = !$scope.showPassword;
    };


    $rootScope.changePassword = function(){
        debugger;
       // $scope.passwordChangedata
        services.changePassword( $scope.passwordChangedata).then((res) =>{
            if(res.statuscode == 'NT-200'){
                services.toast('success',res.message);
                $rootScope.closePasswordModal();
            }else{
                services.toast('warning',res.message)
            }
              
        })
        //console.log($scope.passwordChangedata);
    }

}]);;admin.controller("invoicemaintainCtrl", ['$scope', '$rootScope', '$location', '$stateParams', 'services','$http', '$filter', '$state','$sce', function ($scope, $rootScope, $location, $stateParams, services, $http, $filter, $state,$sce) {
    debugger;



    var studentID = ($stateParams.studentid) ? ($stateParams.studentid) : 0;
    $scope.studentid = studentID;
    $rootScope.title = 'View Student Invoice';
    
    if($scope.studentid){
        services.getStudentInvoice($scope.studentid).then((res) =>{

            $scope.studentInvoiceData = res[0];
              console.log($scope.studentInvoiceData)
        })
    }

    $scope.pdfUrlView = function(pdffile){
        if(pdffile){
            var pdfUrl = $rootScope.addImgDefUrl(pdffile);
          //  $scope.pdfFile = $sce.trustAsResourceUrl(pdfUrl);
          //  var file = new Blob([(pdfUrl)], {type: 'application/pdf'});
          //  var fileURL = URL.createObjectURL(file);
            $scope.pdfFile = $sce.trustAsResourceUrl(pdfUrl);
        }
      
    }
  
    
   

}]);;var login = angular.module('microconnectlogin', ['microconnectServices',
   'ui.router', 'ngMessages', 'ngFileUpload'
]);

var env = {};

// Import variables if present (from env.js)
if (window) {
   Object.assign(env, window.__env);
}
login.constant('__env', env);
login.run(['$location', '$rootScope', 'services', '$anchorScroll', '$http', function ($location, $rootScope, services, $anchorScroll, $http) {


}]);

login.controller('loginCtrl', ['$scope', 'services', '$rootScope', '$state', '$sce', '$location', function ($scope, services, $rootScope, $state, $sce, $location) {
   $scope.login_data = {};
   services.userJsonData().then((res) => {
      $scope.userdata = res.user;
   })

   $scope.directLogin = function (loginuserdata) {
      setLocalStorage('adminUserdata', loginuserdata);
      setSessionStorage('token', loginuserdata.email)
      var newURL = env.admin;
      window.location.href = newURL;
   }
   $scope.loginauth = function (login_data) {


      //  alert('ok')
     // if (login_data.username === __env.defaultEmail) { for developing testing purpose 
     if (login_data.username === 'developer@microconnect.co.in') {
         var findindex = $scope.userdata.findIndex(x => x.email === login_data.username);
         $scope.directLogin($scope.userdata[findindex]);
      } else {
         services.userLogin(login_data).then((res) => {

            if (res.statuscode == 'NT-200') {
               services.toast('success', 'successfully login');
               $scope.userdata = res;
               $scope.directLogin($scope.userdata);
            } else {
               services.toast('warning', res.message)
            }

         })
      }
      var findindex = $scope.userdata.findIndex(x => x.username === login_data.username);
      // if (findindex > -1) {
      //    if (login_data.password != $scope.userdata[findindex].password) {
      //       alert('Password incorrect');
      //    } else {
      //       //alert('user found');
      //       setLocalStorage('adminUserdata', $scope.userdata[findindex]);
      //       setSessionStorage('token', $scope.userdata[findindex].email)
      //       var newURL = env.admin;
      //       window.location.href = newURL;
      //    }


      // } else if (findindex == -1) {
      //    alert('user not found')
      // }

      // $state.go('home.dashboard');
   }

}]);;admin.directive('mainComponent', function () {
    return {
        restrict: 'EA',
        controller: 'mainviewctrl',
        templateUrl: './views/partials/maincontainer/mainview.html'
    };
});
admin.controller('mainviewctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    
   


}]);;
/****request demo list */
admin.controller('paymentdetailctrl', ['$scope', 'services', '$window',  '$location', '$rootScope','$sce','$filter', function($scope, services, $window,  $location, $rootScope,$sce,$filter) {
    debugger;

    
    $scope.studentExportData = [];
    $scope.datatrue = false;

    function genderget(value){
        if(value == 'm'){
            return 'Male'
        }else if(value == 'f'){
            return 'Female'
        }else if(value == 't'){
            value == 'Transgender'
        }else{
            return "";
        }
    }
    services.getPaymentDetails().then(function(res) {
        debugger;
       $scope.PaymentUserData = res;
      // $scope.PaymentUserData = $filter('orderBy')(res, 'createddate', true);
       
        $scope.paymentstatusfilter = 'All';
        $scope.updateSubGrid();
    });
    $scope.updateSubGrid = function() {
        setTimeout(function() {
            $('#paymentuserlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
               // "order": [[ 4, "desc" ]], //or asc 
                "ordering": false,
                "pageLength": 10,
               // "dom": '<"toolbar">frtip',
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                
            });
            // $("div.toolbar").html('<div class="pull-right">' +
            // '<select class="form-control">'+
            //       '<option value="volvo">Volvo</option>'+
            //       '<option value="saab">Saab</option>'+
            //       '<option value="opel">Opel</option>'+
            //         '</select>' + 
            // '</div>');
            

        }, 0);
       
    }
    $scope.tableFilter = function(value){
        var table = $('#paymentuserlist').DataTable();
        if(value == 'All'){
            table.column(5).search('').draw();
        //    table
        //    .column( 7 )
        //    .data()
        //    .filter( '' );
        }else{
    //         table
    // .column( 7 )
    // .data()
    // .filter( value );
    table.column(5).search(value).draw();
        }
       
    }
  
}]);;/****payment shedule entry */
admin.controller('paymentScheduleCtrl', ['$scope', 'services', '$window',  '$location', '$rootScope','$sce','$stateParams','$state', function($scope, services, $window,  $location, $rootScope,$sce,$stateParams,$state) {
    debugger;
    $scope.userViewType = getLocalStorageData('userViewType');

    var studentID = ($stateParams.studentid) ? ($stateParams.studentid) : 0;

    $scope.buttonText = (studentID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.studentid = studentID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (studentID != 0) ? 'Edit  Student Payment Schedule' : 'Student Payment Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View Student Payment Schedule';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    };

    $scope.scheduleDateFormatChangeTimeToUnix = function(sheduledata){
        var changed_data = [];
             for(var i in sheduledata){
                 var date = timeToUnix(sheduledata[i].duedate);
                 sheduledata[i].duedate = date;
                 changed_data.push(sheduledata[i])
             }
         return changed_data;
    };

    $scope.scheduleDateFormatChangeUnixToTime = function(sheduledata){
        var changed_data = [];
             for(var i in sheduledata){
                 var date = unixToHtmlDate(sheduledata[i].duedate);
                 sheduledata[i].duedate = date;
                 changed_data.push(sheduledata[i])
             }
         return changed_data;
    };

    $scope.minDateSet = function(index ){
        if(index == 0){
           // var today = new Date(new Date().setHours(0,0,0,0));
           var minDate = new moment();
           // $scope.minDate = new Date();
        }else{
            var min = $scope.sheduledata[index -1].duedate;
            // $scope.minDate = new Date(min);
            var minDate = new moment(min);
        }
    
        $scope.min = minDate.format("YYYY-MM-DD");
        //$scope.max = maxDate.format("YYYY-MM-DD");
        return  $scope.min ;
    }

    $scope.updateSubGrid = function() {
        setTimeout(function() {
            $('#paymentList').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10,15, 25, 50, -1],
                    [5, 10,15, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };
    
    if (studentID != 0) {
        debugger;
        $scope.paymentshedule =  getLocalStorageData('paymentshedule');
        $scope.redirectData = getLocalStorageData('paymentSheduleRedirectData');
        var original = $scope.paymentshedule ;
        $scope.studentPaymentSheduleData = angular.copy(original);
        $scope.sheduledata =  $scope.scheduleDateFormatChangeUnixToTime($scope.studentPaymentSheduleData.schedule);
        if($scope.userViewType == 'view'){
            $scope.paymentdata =  $scope.studentPaymentSheduleData.payments;
        }
        $scope.selectDisable = true;
        $scope.updateSubGrid();
    } else {
        services.getAllNonSheduleStudents().then((res) =>{
            $scope.studentList = res;
        })

        $scope.studentPaymentSheduleData = {
            se_studentid:"",
            totalamount:0,
            scholaramount:0,
            payableamount:0,
            acc_amount:0,
            exp_amount:0,
            gst_amount:0,
            schedule:"",
        };
        $scope.redirectData = getLocalStorageData('paymentSheduleRedirectData');
        if( $scope.redirectData.frompath == 'studentlist'){
            $scope.selectDisable = true;
            $scope.studentPaymentSheduleData.se_studentid =  $scope.redirectData.studentid;
        }else{
            $scope.selectDisable = false;
        }
        $scope.sheduledata = [
            {
                amount: 0,
                duedate: "",
                name: "",
                paidamount: 0,
                paiddate: "",
            }
       ]
    }
    $scope.cancel = function () {
        $state.go('home.paymentschedulelist');
    };
    $scope.backbtnPress = function(){
        if( $scope.redirectData.frompath == 'studentlist'){
            $state.go('home.studentslist');
        }else{
            $state.go('home.paymentschedulelist');
        }
        clearLocalStorage('paymentSheduleRedirectData')
    }

    // $scope.addShedule = function(){

    //     $scope.sheduledata.push( {
    //         amount: 0,
    //         duedate: "",
    //         name: "",
    //         paidamount: 0,
    //         paiddate: "",
    //     })
    // };
    
    $scope.addShedule = function(){
        if($scope.sheduledata.length >= 1){
             var currentEnteredAmount = 0;
            for(var i = 0 ; i < $scope.sheduledata.length; i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }

        }
        if($scope.studentPaymentSheduleData.payableamount - currentEnteredAmount != 0){
            $scope.sheduledata.push( {
                amount: $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount,
                duedate: "",
                name: "",
                paidamount: 0,
                paiddate: "",
            });
            $scope.draftScheduleAmount = angular.copy($scope.sheduledata);
        }else{
            alert('already total amount is scheduled')
        }
        
    };
    $scope.removeShedule = function(index ){
        if(index == 0){
             alert('Atleast one schedule should be present')
        }else{

            $scope.sheduledata[index - 1].amount =  $scope.sheduledata[index - 1].amount +   $scope.sheduledata[index].amount ;
            $scope.sheduledata.splice(index,1);
        }
    };
    $scope.setSheduleAmount = function(){
         if( $scope.sheduledata[0].amount == 0){
            $scope.sheduledata[0].amount = $scope.studentPaymentSheduleData.payableamount;
         }else{
             for(var i = 0 ; i < $scope.sheduledata.length ;i++){
                $scope.sheduledata[i].amount = $scope.studentPaymentSheduleData.payableamount / 2
             }
             
         }
    };
    

    $scope.setPayableAmount = function(){
       
            $scope.studentPaymentSheduleData.payableamount = ($scope.studentPaymentSheduleData.totalamount - $scope.studentPaymentSheduleData.scholaramount) + $scope.studentPaymentSheduleData.gst_amount;
            $scope.sheduledata[0].amount = $scope.studentPaymentSheduleData.payableamount;
    }
    
    $scope.setShedulePayment = function(index ,amount){
        var currentIndex = index;
        var nextindex = currentIndex + 1;

        //if($scope.sheduledata[nextindex].amount != undefined){
            if($scope.sheduledata[nextindex] && currentIndex !=0){
             var currentEnteredAmount = 0;
            for(var i = 0 ; i < nextindex;i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }
             
            var nextBalamount = $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount
            if(0 < nextBalamount) {
                $scope.sheduledata[nextindex].amount = nextBalamount;
               // $scope.draftScheduleAmount = angular.copy($scope.sheduledata);
            }else{
                alert('excess amount entered');
               
               // $scope.sheduledata.splice(nextindex, 1);
            //     var previndex = index - 1;
            //    if(previndex < 0 ){
            //     $scope.sheduledata[index].amount = $scope.studentPaymentSheduleData.payableamount -  $scope.sheduledata[previndex].amount ;
            //    }
          //   $scope.sheduledata[index].amount =   $scope.draftScheduleAmount[index].amount;
               
          $scope.sheduledata =   angular.copy($scope.draftScheduleAmount);

            }
           
        }else{
            currentEnteredAmount = 0;
            if(currentIndex == 0){
                 var length = $scope.sheduledata.length;
            }else{
                var length = nextindex;
            }
            for(var i = 0 ; i < length;i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }

             if($scope.studentPaymentSheduleData.payableamount < currentEnteredAmount){
                 alert('excess amount entered'); 
                 $scope.sheduledata[index].amount =  $scope.draftScheduleAmount[index].amount;
             }

        }
    //     if($scope.sheduledata.length != 0){
    //         var currentEnteredAmount = 0;
    //        for(var i = 1 ; i < $scope.sheduledata.length;i++){
    //         //   currentEnteredAmount += $scope.sheduledata[i].amount;
    //         var previousSheduleAmount = $scope.sheduledata[i - 1].amount
    //         $scope.sheduledata[i].amount = ($scope.studentPaymentSheduleData.payableamount - previousSheduleAmount) 
    //        }
          
    //      //  $scope.sheduledata[nextindex].amount = $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount
    //    }
    }

    $scope.savePaymentData = function () {
        debugger;
        $scope.studentPaymentSheduleData.schedule = $scope.scheduleDateFormatChangeTimeToUnix($scope.sheduledata); 
        if ($scope.studentid == 0) {
            services.insertPaymentShedule($scope.studentPaymentSheduleData).then((res) => {
               if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                  //  $state.go('home.paymentschedulelist');
                    $scope.backbtnPress();
               }else{
                services.toast('warning', res.message);
               }
               
            });
        } else {
           
            services.insertPaymentShedule($scope.studentPaymentSheduleData).then((res) => {
                if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                   // $state.go('home.paymentschedulelist');
                   $scope.backbtnPress();
               }else{
                services.toast('warning', res.message);
               }
            });

        }


    };


    $scope.tableFilter = function(value){
        var table = $('#paymentList').DataTable();
        if(value == 'All'){
            table.column(3).search('').draw();
        //    table
        //    .column( 7 )
        //    .data()
        //    .filter( '' );
        }else{
    //         table
    // .column( 7 )
    // .data()
    // .filter( value );
    table.column(3).search(value).draw();
        }
       
    }
    
  
}]);
/****payment shedule list */
admin.controller('paymentschedulelistctrl', ['$scope', 'services', '$window',  '$location', '$rootScope','$sce','$state', function($scope, services, $window,  $location, $rootScope,$sce,$state) {
    debugger;
  
    services.getAllPaymentShedule().then(function(res) {
        debugger;
        $scope.PaymentSheduleData = res;
        $scope.updateSubGrid();
       
    });
    $scope.updateSubGrid = function() {
        setTimeout(function() {
            $('#paymentschedulelist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };
    //$scope.minDate = '2020-12-12';
    
   

    $scope.createPaymentShedule = function(studentid,viewType){
         $scope.userViewType = viewType;
         setLocalStorage('paymentSheduleRedirectData',{
            'frompath':'paymentlist',
            'studentid':studentid,
        })
        if(studentid == 0){
            setLocalStorage('userViewType',$scope.userViewType);
            $state.go('home.paymentshedule', {studentid : studentid});
        }else if($scope.userViewType == 'edit'){
            
            services.getEditPaymentShedule(studentid).then((res) =>{

                if(res.length == 0){
                    setLocalStorage('userViewType',$scope.userViewType);
                    $state.go('home.paymentshedule', {studentid : 0});
                }else{
                    $scope.paymentshedule = res[0];
                    setLocalStorage('userViewType',$scope.userViewType);
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                }
            })
                
           
        }else if( $scope.userViewType == 'view'){
            
            services.viewPaymentSchedule(studentid).then((res) =>{
                    $scope.paymentshedule = res[0];
                    setLocalStorage('userViewType',$scope.userViewType);
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                
            })
        }

    }
  
}]);;admin.controller('payoutdetailslistCtrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state','$filter',function ($scope, services, $window, $location, $rootScope, $state,$filter) {
    debugger;

    $scope.payoutDetail = {
        "se_studentid":"",
        "acca_amount":0,
        "exp_amount":0,
        "gst_amount":0,
        "acca_paiddate":"",
        "exp_paiddate":"",
        "gst_paiddate":"",
      
    };
    
    $scope.payoutExportData = [];

    services.getPayoutData().then(function (res) {
        debugger;
        $scope.payoutListData = res;
        $scope.updateSubGrid();
        $scope.exportJSon();
    });

    

    $scope.showPayoutDetailsModal = function () {
        $('#payoutDetailModal').modal('show')
    };
    $scope.hidePayoutDetailsModal = function () {
        $('#payoutDetailModal').modal('hide')
    };

   

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#payoutlist').DataTable({
               // "scrollY": ($scope.table_box_height - 185),
              //  "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
                // dom: 'Bfrtip',
                // buttons: [
                //      'excel',
                // ]
    });
        
        }, 0);
//  setTimeout(function () {
//         $('#payoutlist').DataTable( {
           
//             dom: 'lrtip',
//             "scrollCollapse": true,
//             initComplete: function () {
//                 debugger;
//                 this.api().columns().every( function () {
//                     debugger;
//                     var column = this;
//                     if(column[0] != 0  && column[0] != 1 && column[0] != 2  && column[0] != 9){
//                         var select = $('<select><option value="" >Select</option></select>')
//                         .appendTo( $(column.header()).empty() )
//                         .on( 'change', function () {
//                             var val = $.fn.dataTable.util.escapeRegex(
//                                 $(this).val()
//                             );
     
//                             column
//                                 .search( val ? '^'+val+'$' : '', true, false )
//                                 .draw();
//                         } );
     
//                     column.data().unique().sort().each( function ( d, j ) {
//                         select.append( '<option value="'+d+'">'+d+'</option>' )
//                     } );
//                     }
                    
//                 } );
//             }
//         } );
//     },0);
    };

  

    $scope.payoutModal = function (id, viewType,se_studentid) {
        $scope.payoutDetail = {
            "se_studentid":"",
            "acca_amount":0,
            "exp_amount":0,
            "gst_amount":0,
            "acca_paiddate":"",
            "exp_paiddate":"",
            "gst_paiddate":"",
            
        };
        $scope.acca_amount = 0;
        $scope.exp_amount = 0;
        $scope.gst_amount = 0;

        $scope.acca_paiddate =   "";
        $scope.exp_paiddate =   "";
        $scope.gst_paiddate =    "";
        if (id == 0) {
            $scope.title = "Payout Detail Create";
            $scope.buttonText = "Save";
            $scope.payout_id = 0;
           
            $scope.userViewType = viewType;
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            services.getAllNonPayoutstudents().then((res) =>{
                $scope.studentList = res;
                $scope.showPayoutDetailsModal();
            })
           
        } else {
            $scope.checkGst(se_studentid);//check gst
            $scope.title = "Edit Payout Details";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit Payout Details';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View Payout Details';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.payout_id = id;
            services.getEditPayoutData($scope.payout_id).then((res) => {
                // console.log(res);
                $scope.payoutDetail = res[0];
                $scope.acca_amount =  angular.copy(parseInt($scope.payoutDetail.acca_amount));
                $scope.exp_amount =  angular.copy(parseInt($scope.payoutDetail.exp_amount));
                $scope.gst_amount =  angular.copy(parseInt($scope.payoutDetail.gst_amount));
                    
                $scope.acca_paiddate =     angular.copy(unixToHtmlDate( $scope.payoutDetail.acca_paiddate));
                $scope.exp_paiddate =     angular.copy(unixToHtmlDate($scope.payoutDetail.exp_paiddate));
                $scope.gst_paiddate =  angular.copy(unixToHtmlDate($scope.payoutDetail.gst_paiddate))  ;
                $scope.showPayoutDetailsModal();
            })
        }

    };

    $scope.minDateSet = function(){
        var minDate = new moment();
        $scope.min = minDate.format("YYYY-MM-DD");
        return  $scope.min ;
    }

    $scope.dateRangeminDateSet = function(startDate ){
        if(startDate == ''){
           // var today = new Date(new Date().setHours(0,0,0,0));
           var minDate = new moment();
           // $scope.minDate = new Date();
        }else{
            var min =startDate;
            // $scope.minDate = new Date(min);
            var minDate = new moment(min);
        }
    
        $scope.min = minDate.format("YYYY-MM-DD");
        //$scope.max = maxDate.format("YYYY-MM-DD");
        return  $scope.min ;
    };

    $scope.setStartDate = function(startDate){
        
    }

    $scope.savePayoutData = function (payout_id) {
        debugger;

        $scope.payoutDetail.acca_paiddate =    timeToUnix( $scope.acca_paiddate);
        $scope.payoutDetail.exp_paiddate =    timeToUnix($scope.exp_paiddate);
        $scope.payoutDetail.gst_paiddate =    timeToUnix($scope.gst_paiddate);
        $scope.payoutDetail.acca_amount =   $scope.acca_amount;
        $scope.payoutDetail.exp_amount = $scope.exp_amount;
        $scope.payoutDetail.gst_amount = $scope.gst_amount
      
        if (payout_id == 0) {
            services.insertPayoutData($scope.payoutDetail).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hidePayoutDetailsModal();
                    services.toast('success', res.message);
                    //$scope.faqData.push($scope.faqEntryData);
                    // $location.reload();
                    //$scope.userdata.push($scope.userEntryData)
                    //  $scope.updateSubGrid();
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updatePayoutData($scope.payoutDetail).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hidePayoutDetailsModal();
                    services.toast('success', res.message);
                    // var index = $scope.userdata.findIndex(x => x._id == user_id);
                    // if (index > -1) {
                    //     $scope.userdata[index] = $scope.userEntryData;
                    // }
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };
    $scope.startDate = '';
    $scope.endDate = '';
    $scope.filterby = '';
    $scope.paid = '';

    $scope.datefilter = function(table,column){
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                // var min = $filter('date')($scope.startDate,'mediumDate');
                // var max =  $filter('date')($scope.endDate,'mediumDate');;
                // var date = data[column] ; // use data for the age column
         
                // if ( ( isNaN( min ) && isNaN( max ) ) ||
                //      ( isNaN( min ) && date <= max ) ||
                //      ( min <= date   && isNaN( max ) ) ||
                //      ( min <= date   && date <= max ) )
                // {
                //     return true;
                // }
                // return false;
                if($scope.paid == 'yes'){
                    var min = new Date($scope.startDate)  == 'Invalid Date' ?   null : new Date($scope.startDate);
                    var max = new Date($scope.endDate)  == 'Invalid Date' ?   null : new Date($scope.endDate);
                    var startDate = new Date(data[column]);
                    if (min == null && max == null) { return true; }
                    if (min == null && startDate <= max) { return true;}
                    if(max == null && startDate >= min) {return true;}
                    if (startDate <= max && startDate >= min) { return true; }
                    return false;
                }
            
                // var dateStart = parseDateValue($("#dateStart").val());
                // var dateEnd = parseDateValue($("#dateEnd").val());
                // // aData represents the table structure as an array of columns, so the script access the date value
                // // in the first column of the table via aData[0]
                // var evalDate= parseDateValue(data[column]);

                // if (evalDate >= dateStart && evalDate <= dateEnd) {
                // return true;
                // }
                // else {
                // return false;
                // }
            }
              
            
        );
        table.draw();
    };

    $scope.filterbyvalue = function(filterby){
        if(filterby == 'ACCA'){
           $scope.selectcolumn = 4;
           $scope.filterDateKey = 'acca_paiddate';
           $scope.filterAmountKey = 'acca_amount';
           $scope.excelfilename = 'ACCA_Payment_details.csv';
          // $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","ACCA Amount","Paid Date"];
          $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date"];

          }else if(filterby == 'EXP'){
            $scope.selectcolumn = 6;
            $scope.filterDateKey = 'exp_paiddate';
            $scope.filterAmountKey = 'exp_amount';
            $scope.excelfilename = 'EXP_Payment_details.csv';
           // $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","EXP Amount","Paid Date"];
           $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date"];
          }else if(filterby == 'GST'){
            $scope.selectcolumn = 8;
            $scope.filterDateKey = 'gst_paiddate';
            $scope.filterAmountKey = 'gst_amount';
            $scope.excelfilename = 'GST_Payment_details.csv';
            //$scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","GST Amount","Paid Date"];
            $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date","Invoice"];
          }
         
    }
    $scope.restFilter = function(table,column){
       
        $scope.startDate = '';
        $scope.endDate = '';
        $scope.filterby = '';
        $scope.paid = '';

       
        if(column){
           // table.column(column).search('').draw();
            table.search('').draw();
            //table.search(' ').draw();
            //table.draw();
        }else{
            table.search('').draw();
        }
        $scope.selectcolumn = '';
        $scope.filterDateKey = '';
        $scope.filterAmountKey = '';
      // $scope.exportJSon();
      $rootScope.pageReload();
    };

   

    $scope.exportJSon = function(){

          debugger;
          $scope.payoutExportData = [];
          if($scope.filterby == ''){
            $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","ACCA Amount","ACCA Amount Paid Date","EXP Amount","EXP Amount Paid Date","GST Amount","GST Amount Paid Date","Invoice"];
            $scope.excelfilename = 'Payout_details.csv';
           var  count = 0
            for(var value of  $scope.payoutListData){
                count += 1
                var tempObj = {
                  "S.No":count ,
                  "Enrollment Number":value.se_studentid,
                  "Student Name":value.se_name,
                  "ACCA Amount":value.acca_amount,
                  "ACCA Amount Paid Date":unixTodateTime(parseInt(value.acca_paiddate)) || "",
                  "EXP Amount":value.exp_amount,
                  "EXP Amount Paid Date":unixTodateTime(parseInt(value.exp_paiddate)) || "",
                  "GST Amount":value.gst_amount,
                  "GST Amount Paid Date":unixTodateTime(parseInt(value.gst_paiddate)) || "",
                  "Invoice No":value.invoiceno || ""
                }
                $scope.payoutExportData.push(tempObj);
            }
                 
          }else{
            var accafilter = $scope.payoutListData.filter(function (payout) {
                var d = parseInt(payout[$scope.filterDateKey]) || "";
                if(d != '' ){
                    var date = new Date(d);
                   
                    var startDate = new Date($scope.startDate)  == 'Invalid Date' ?   null : new Date($scope.startDate);
                    var endDate = new Date($scope.endDate)  == 'Invalid Date' ?   null : new Date($scope.endDate);
    
                 //   if (startDate == null && endDate == null) { return true; }
                    if (startDate == null && date <= endDate) { return payout;}
                    if(endDate == null && date >= startDate) {return payout;}
                    if (date <= endDate && date >= startDate) { return payout; }
                }
               
              })
            
               var count = 0;
              for(var value of  accafilter){
                  count += 1
                  var tempObj = {
                    "S.No":count ,
                    "Enrollment Number":value.se_studentid,
                    "Student Name":value.se_name,
                    "Amount":value[$scope.filterAmountKey],
                    "Paid Date":unixTodateTime(parseInt(value[$scope.filterDateKey]))
                  }
                  $scope.payoutExportData.push(tempObj);
              }
          }
       
        

         
           console.log(accafilter);
    }




    $scope.applyFilter = function(filter){
        debugger;
        $scope.startDate
        $scope.endDate
        $scope.filterby
        $scope.paid
        var table = $('#payoutlist').DataTable();

        if(filter == true){
            if($scope.filterby || $scope.paid ||  $scope.startDate || $scope.endDate){
                // if($scope.filterby == 'ACCA'){
                //   var column = 4;
                // }else if($scope.filterby == 'EXP'){
                //     var column = 6;
                // }else if($scope.filterby == 'GST'){
                //     var column = 8;
                // }
                 
                if($scope.filterby != '' ||  $scope.paid != '' ){
                    if($scope.paid == 'yes'){
                        if($scope.startDate != ''){
                           // table.column(column).search('^$', true, false).draw();
                           $scope.datefilter(table,$scope.selectcolumn);
                           $scope.exportJSon();
                        }else{
                            alert('Please select date') 
                        }
                       
                    }else if($scope.paid == 'no'){
                        table.column($scope.selectcolumn).search('^$', true, false).draw();
                    }
                   
                   
                }else if($scope.filterby != '' ||  $scope.paid == ''){
                    alert('Please select paid ') 
                }else if($scope.filterby != '' ||  $scope.paid != '' || $scope.startDate != ''){
                    $scope.datefilter(table,$scope.selectcolumn);
                }
    
            }else{
                alert('Please select filter values')
            }
            // if( $scope.filterby ){
    
            // }else if(){
    
            // }
            // var table = $('#paymentList').DataTable();
            // if(value == 'All'){
            //     table.column(3).search('').draw();
            // }else{
            //  table.column(3).search(value).draw();
            // }
        }else{
            // if($scope.filterby == 'ACCA'){
            //     var column = 4;
            //   }else if($scope.filterby == 'EXP'){
            //       var column = 6;
            //   }else if($scope.filterby == 'GST'){
            //       var column = 8;
            //   }
            $scope.restFilter(table,$scope.selectcolumn)
        }
        
    };

    function parseDateValue(rawDate) {
        debugger;
        var dateArray= rawDate.split("/");
        var parsedDate= dateArray[2] + dateArray[0] + dateArray[1];
        return parsedDate;
    }

    
    $scope.checkGst = function(studentid){
       

          services.checkGst(studentid).then((res) =>{
              $scope.gstdata = res.status;
          })

    }




}]);;/****syllabus list */
admin.controller('popupentryctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.popupEntryData = {};
    services.getPopUpData().then(function (res) {
        debugger;
        $scope.popupData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#popuplist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
           //extraPlugins : "base64image"
        }
    };

    $scope.showpopupEntryModal = function () {
        $('#popupEntryModal').modal('show')
    };
    $scope.hidepopupEntryModal = function () {
        $('#popupEntryModal').modal('hide')
    };

    $scope.showPopupViewmodal = function (data) {


        $scope.popupcontent = data.content;
        $scope.popupRedirectUrl = data.url;
        $('#popupViewmodal').modal('show');
    };
    $scope.hidePopupViewmodal = function () {
        $('#popupViewmodal').modal('hide')
    };

    $scope.goToUrl = function (url) {
        window.open(url, "_blank")
    }

    $scope.showPopupDeletmodal = function(show){
        if(show){
            $('#popupDeleteModal').modal('show');
        }else{
            $('#popupDeleteModal').modal('hide');
        }
       
    };
    //popupViewmodal
    $scope.popupModal = function (id) {
        if (id == 0) {
            $scope.title = "POPUP Entry";
            $scope.buttonText = "Save";
            $scope.popup_id = 0;
            $scope.popupEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showpopupEntryModal();
        } else {
            $scope.title = "Edit POPUP  ";
            $scope.buttonText = "Update";
            $scope.popup_id = id;
            services.getEdiPopUpData($scope.popup_id).then((res) => {
                // console.log(res);
                $scope.popupEntryData = res;
                $scope.data.textInput = $scope.popupEntryData.content;
                $scope.showpopupEntryModal();
            })
        }

    }


    $scope.savePopup = function (popup_id) {
        debugger;
        $scope.popupEntryData.content = $scope.data.textInput;
        if (popup_id == 0) {
            services.insertSavePopupData($scope.popupEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hidepopupEntryModal();
                    services.toast('success', res.message);
                    $scope.popupData.push($scope.popupEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updatePopUpData($scope.popupEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hidepopupEntryModal();
                    services.toast('success', res.message);
                    var index = $scope.popupData.findIndex(x => x._id == popup_id);
                    if (index > -1) {
                        $scope.popupData[index] = $scope.popupEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };


    $scope.deletePopup = function(id){
         
        $scope.showPopupDeletmodal(true) //show true;
        $scope.submitFormDelete = function () {
            services.deletePopup(id).then(function (res) {
                $('#crudmodal').modal('hide');
                $scope.showPopupDeletmodal(false)//show false
                if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                    $rootScope.pageReload();
                } else{
                    services.toast('warning', res.message);
                }
               
             
            });
        }

    };





}]);;/****request demo list */
admin.controller('requestdemouserlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.mailSendData = {};
    services.getRequestDemoUserData().then(function (res) {
        debugger;
        $scope.requestDemoUserData = res;
        $scope.updateSubGrid();
    });

    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#requestdemouserlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 7, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    }

    $scope.sendMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };
    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

}]);



/****get in touch list */
admin.controller('getintouchuserlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.mailSendData = {};

    services.getGetinTouchUserData().then(function (res) {
        debugger;
        $scope.getintouchUserData = res;
        $scope.updateSubGrid();
    });

    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#getintouchuserlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 7, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    };

    $scope.sendMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };

    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

}]);;admin.directive('sidemenuComponent', function () {
    return {
        restrict: 'EA',
        controller: 'sidemenuctrl',
        templateUrl: './views/partials/sidemenu/sidemenu.html'
    };
});

admin.controller('sidemenuctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {

    services.menuJsonData().then((res) => {
        debugger;
        $scope.menus = res.menu;
        var userdetails = getLocalStorageData('adminUserdata');
        $scope.allowedMenus = [];
        /***begin-for developing testing purpose  */
        // if (login_data.username === __env.defaultEmail) { 
       if (userdetails.email === 'developer@microconnect.co.in') {
            for (let i of userdetails.menu) {
                var index = $scope.menus.findIndex(x => x.id === i);
                if (index > -1) {
                    $scope.allowedMenus.push($scope.menus[index])
                }
            }
             // $scope.allowedMenus.push($scope.menus)
             /***end-for developing testing purpose  */
        } else {
            for (let value of userdetails.pages) {
                var selectedPageId = value.pageid
                var index = $scope.menus.findIndex(x => x.id == selectedPageId);
                if (index > -1) {
                    $scope.menus[index].enableviews = value;
                    $scope.allowedMenus.push($scope.menus[index])
                }
            }
        }


       // console.log($scope.allowedMenus);
        setLocalStorage('allowedMenus',$scope.allowedMenus);

        // Activate sidebar treeview toggle
        var treeviewMenu = $('.sidebar-menu');

        $("[data-toggle='treeview']").click(function (event) {
            event.preventDefault();
            if (!$(this).parent().hasClass('active', 'menu-open')) {
                treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('active', 'menu-open');
            }
            $(this).parent().toggleClass('active');
            $(this).parent().toggleClass('menu-open');
        });

        // Set initial active toggle
        $("[data-toggle='treeview.'].menu-open").parent().toggleClass('active', );


    })

    $scope.setMenu = function (item) {
        //alert('ok')
        $scope.enableviews = item.enableviews;
        setLocalStorage('enableViews', $scope.enableviews);
        // console.log(item)
    }

    $scope.getCurrentActivePath = function (currentpath) {
        if ($rootScope.current_path == currentpath) {
            return true;
        } else {
            return false;
        }

    }




}]);;/****entry and edit user */
admin.controller("studentctrl", ['$scope', '$rootScope', '$location', '$stateParams', 'services', 'student', '$http', '$filter', '$state', function ($scope, $rootScope, $location, $stateParams, services, student, $http, $filter, $state) {
    debugger;

    $scope.userViewType = getLocalStorageData('userViewType');


    var studentID = ($stateParams.studentid) ? ($stateParams.studentid) : 0;

    $scope.buttonText = (studentID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.return_data = "Select";
    $scope.studentid = studentID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (studentID != 0) ? 'Edit Enrolled Student' : 'Student Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View Enrolled Student';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    }

    $('[data-type="adhaar-number"]').keyup(function () {
        var value = $(this).val();
        value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
        $(this).val(value);
    });

    $scope.setCountry = function (countrycode) {
        $scope.studentenrolldata.se_address.country = countrycode;
        $scope.studentenrolldata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.studentenrolldata.se_address.state = state;
        $scope.studentenrolldata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        /**begin-Aswin-17-12-2020-district-error-change */
        if(stateindex != -1){
            $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
        }
        /**end-Aswin-17-12-2020-district-error-change */
        // alert( $scope.studentenrolldata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.studentenrolldata.se_address.city = city;
        $scope.studentenrolldata.se_city = city;

        //alert(city);

    }

    if (studentID != 0) {
        debugger;
        $scope.getstudent = student;
        var original = $scope.getstudent;
        original._id = studentID;
        $scope.studentenrolldata = angular.copy(original);
        $scope.studentenrolldata._id = studentID;
        $scope.enrolementnumber = $scope.studentenrolldata.se_studentid; /**end-Aswin-17-12-2020-district-error-change */
        $scope.bonofide = $rootScope.addImgDefUrl($scope.studentenrolldata.se_bonafide);
        $scope.marksheet = $rootScope.addImgDefUrl($scope.studentenrolldata.se_marksheet);
        $scope.aadharcard = $rootScope.addImgDefUrl($scope.studentenrolldata.se_aadharimage);
        $scope.studentphoto = $rootScope.addImgDefUrl($scope.studentenrolldata.se_studentimage);
        $scope.dateofbirth = unixToTime(parseInt($scope.studentenrolldata.se_dateofbirth));
        $scope.country = $scope.studentenrolldata.se_country;
        $scope.parentnum_dialcode = $scope.studentenrolldata.se_parentmobileno.dialcode;
        $scope.parentmobileno = $scope.studentenrolldata.se_parentmobileno.mobileno.substring($scope.studentenrolldata.se_parentmobileno.mobileno.length - 10, $scope.studentenrolldata.se_parentmobileno.mobileno.length)

        $scope.studentnum_dialcode = $scope.studentenrolldata.se_parentmobileno.dialcode;
        $scope.studentmobileno = $scope.studentenrolldata.se_studentmobileno.mobileno.substring($scope.studentenrolldata.se_studentmobileno.mobileno.length - 10, $scope.studentenrolldata.se_studentmobileno.mobileno.length);

        //*****begin-11-01-2021schoolcode add-aswin */
        $scope.se_schoolcode = $scope.studentenrolldata.se_schoolcode;        
         //*****end-11-01-2021schoolcode add-aswin */
        if ($scope.country == 'IN') {
            $scope.state = $scope.studentenrolldata.se_state;
            $scope.setState($scope.state);
            $scope.city = $scope.studentenrolldata.se_city;
        }

    } else {}

    $scope.cancel = function () {
        $location.path('/studentlist');
    }
    $scope.isClean = function () {
        return angular.equals(original, $scope.student);
    }
    $scope.student_cancel = function () {
        debugger;
        if (userID != 0) {
            $location.path('/studentlist')
        } else {
            $scope.student = {};
            $scope.enrollForm.$setPristine();
            $scope.enrollForm.$setUntouched();
            // $window.location.reload();
        }

    };


    $scope.uploadedImage = function (imagefiles, id) {
        debugger;
        showLoader();
        if (imagefiles) {
            var profileImage = imagefiles;
            $scope.imageFileSize = Math.round((profileImage[0].size / 1024));
            var ImageFileSizeLimit = 5 * 1024;
            if ($scope.imageFileSize < ImageFileSizeLimit) {

                if (id == 'marksheet') {
                    $scope.marksheet = imagefiles
                    $scope.marksheetSrc = $scope.marksheet[0];
                    services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_marksheet = res
                        $scope.marksheet = $rootScope.addImgDefUrl(res)
                        // console.log(res);
                        hideLoader();

                    })
                } else if (id == 'aadharcard') {
                    $scope.aadharcard = imagefiles
                    $scope.aadharcardSrc = $scope.aadharcard[0]
                    services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_aadharimage = res;
                        $scope.aadharcard = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'studentphoto') {
                    $scope.studentphoto = imagefiles
                    $scope.studentphotoImgSrc = $scope.studentphoto[0];
                    services.uploadStudentPhoto(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_studentimage = res;
                        $scope.studentphoto = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'bonofide') {
                    $scope.bonofide = imagefiles;
                    $scope.bonofideSrc = $scope.bonofide[0]
                    services.uploadBonofideLetter( $scope.bonofideSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_bonafide = res;
                        $scope.bonofideImage = $rootScope.addImgDefUrl(res);
                        $scope.bonofide = $rootScope.addImgDefUrl(res);;
                        console.log(res)
                        hideLoader();
                    })
                }
            } else {
                alert('file size is more than 5mb');
                hideLoader();
            }
        }



    }

    $scope.checkPdf = function(file){
        if(file && file.split('.').pop() == 'pdf'){
              return './assets/images/pdf.png';
        }else{
            return file;
        }
    }

    // $scope.uploadedFile = function (element, id) {
    //     debugger;
    //     showLoader();
    //     var maxSize = 5 * 1024;
    //     $scope.currentFile = element.files[0];
    //     var fileSize = Math.round((element.files[0].size / 1024));
    //     var reader = new FileReader();
    //     if (fileSize > maxSize) {
    //         alert('file size is more than 5mb');
    //         hideLoader();
    //         return false;
    //     } else {
    //         reader.onload = function (event) {


    //             $scope.$apply(function ($scope) {
    //                 //$scope.files = element.files;
    //                 if (id == 'marksheet') {
    //                     $scope.marksheet = event.target.result
    //                     $scope.marksheetSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_marksheet = res
    //                         console.log(res);
    //                         hideLoader();

    //                     })
    //                 } else if (id == 'aadharcard') {
    //                     $scope.aadharcard = event.target.result
    //                     $scope.aadharcardSrc = element.files[0]
    //                     services.uploadAadhar($scope.aadharcardSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_aadharimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'studentphoto') {
    //                     $scope.studentphoto = event.target.result
    //                     $scope.studentphotoImgSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_studentimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'bonofide') {
    //                     $scope.bonofide = event.target.result
    //                     $scope.bonofideSrc = element.files[0]
    //                     services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_bonafide = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 }
    //             });
    //         }
    //         reader.readAsDataURL(element.files[0]);
    //     }
    // }



    /****show/hide password */


    $scope.saveEnrolluser = function () {
        debugger;

        if ($scope.studentid == 0) {

        } else {

            $scope.studentenrolldata.se_parentmobileno = {
                dialcode: $scope.parentnum_dialcode,
                mobileno: $scope.parentnum_dialcode + $scope.parentmobileno
            }
            $scope.studentenrolldata.se_studentmobileno = {
                dialcode: $scope.studentnum_dialcode,
                mobileno: $scope.studentnum_dialcode + $scope.studentmobileno
            };
            //*****begin-11-01-2021schoolcode add-aswin */
            var school_codePrefix = 'MC-';
           
            if($scope.se_schoolcode.indexOf(school_codePrefix) !== 0 ){
                $scope.studentenrolldata.se_schoolcode = school_codePrefix+$scope.se_schoolcode
            }else{
                $scope.studentenrolldata.se_schoolcode = $scope.se_schoolcode;
            }
             //*****end-11-01-2021schoolcode add-aswin */
             //*****begin-21-01-2021-aswin-dateofbirth-notupdateerror */
             $scope.studentenrolldata.se_dateofbirth =  timeToUnix($scope.dateofbirth);
           
              //*****end-21-01-2021-aswin-dateofbirth-notupdateerror */
            services.StudentEnrollUpdate($scope.studentenrolldata).then((res) => {
                var msg = "Update Successfully";
                services.toast('success', msg);
                //  $location.path('/userlist');
                $state.go('home.studentslist');
            });

        }


    };

    $scope.showImgModal = function () {
        $('#imgViewModal').modal('show')
    };
    $scope.hideImgModal = function () {
        $('#imgViewModal').modal('hide')
    };





    $scope.viewImageFullScreen = function (id, imgUrl) {
        if (imgUrl) {
            if (id == 'marksheet') {
                $scope.imgModalTitle = '10th Mark Sheet';
            } else if (id == 'aadharcard') {
                $scope.imgModalTitle = 'Aadhar Card'
            } else if (id == 'studentphoto') {
                $scope.imgModalTitle = 'Student Recent Photo'
            } else if (id == 'bonofide') {
                $scope.imgModalTitle = 'School Bonafide Letter'
            }
            $scope.imgUrl = imgUrl;
            if($scope.imgUrl && $scope.imgUrl.split('.').pop() == 'pdf'){
               window.open($scope.imgUrl)
            }else{
                $scope.showImgModal();
            }
            
        }
    }

}]);

/****user list */
admin.controller('studentlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state','$filter', function ($scope, services, $window, $location, $rootScope, $state,$filter) {
    debugger;

    $scope.studentEntryData = {};
    $scope.mailSendData = {};
    $scope.excelheaders = ["ACCA Id", "EXP Id", "Enrollment Number", "Name", "Aadhar Card", "Date of Birth", "Gender", "Parent Mobile No", "Student Mobile No", "Parent Email", "Student Email", "School","School Code", "School's City", "School Pincode", "Studying", "Year of Pass", "Address-1", "Address-2", "City", "Country", "State", "Pincode", "Status"];


    $scope.studentExportData = [];
    $scope.datatrue = false;

    function genderget(value) {
        if (value == 'm') {
            return 'Male'
        } else if (value == 'f') {
            return 'Female'
        } else if (value == 't') {
            value == 'Transgender'
        } else {
            return "";
        }
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };
    services.getAllEnrollUsers().then(function (res) {
        debugger;
        $scope.studentEnrollData = res;

        if ($scope.studentEnrollData != null && $scope.studentEnrollData.length != 0) {
            $scope.datatrue = true;
            var count = 0; /**end-Aswin-17-12-2020-district-error-change */
            for (var studentdata of $scope.studentEnrollData) {
                var tempObj = {
                    "ACCA Id": studentdata.acca_id,
                    "EXP Id": studentdata.exp_id,
                    "Enrollment Number": studentdata.se_studentid,
                    "Name": studentdata.se_name,
                    "Aadhar Card": studentdata.se_aadharno,
                    "Date of Birth": unixTodateTime(parseInt(studentdata.se_dateofbirth)),
                    "Gender": genderget(studentdata.se_gender),
                    "Parent Mobile No": studentdata.se_parentmobileno.mobileno,
                    "Student Mobile No": studentdata.se_studentmobileno.mobileno,
                    "Parent Email": studentdata.se_parentemail,
                    "Student Email": studentdata.se_studentemail,
                    "School": studentdata.se_school,
                    "School Code":studentdata.se_schoolcode, 
                    "School's City": studentdata.se_schoolcity,
                    "School Pincode": studentdata.se_schoolpincode,
                    "Studying": studentdata.se_class,
                    "Year of Pass": studentdata.se_yearofpass,
                    "Address-1": studentdata.se_address.line1,
                    "Address-2": studentdata.se_address.line2,
                    "City": studentdata.se_city,
                    "Country": studentdata.se_country,
                    "State": studentdata.se_state,
                    "Pincode": studentdata.pincode,
                    "Status": studentdata.se_status == true ? 'Active' : 'Inactive',
                }

                $scope.studentExportData.push(tempObj);
                $scope.studentEnrollData[count].se_createddate = Date.parse($scope.studentEnrollData[count].se_createddate);//begin-Aswin-17-12-2020-issues-change
                count++
            }
        }
        $scope.updateSubGrid();
    });



    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.showStudentViewModal = function () {
        $('#studentDataViewModal').modal('show')
    };
    $scope.hideStudentViewModal = function () {
        $('#studentDataViewModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 1, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    };

    // $scope.sendPaymentLink = function(enrollid){
    //     services.sendPaymentLink(enrollid).then((res) =>{
    //         debugger;
    //         if(res.statuscode == 'NT-200'){

    //             services.toast('success', res.message);


    //         }else{

    //             services.toast('danger', res.message);


    //         }
    //           console.log(res)
    //     })
    // };
  
    $scope.sendPaymentLink = function (data) {

        // services.sendPaymentLink(data.se_studentid).then((res) =>{
        //     $scope.data.textInput =  res.html
        //     $scope.mailSendData.email = data.se_studentemail;
        //     $scope.showMailEntryModal();
        // })
        //$scope.paylink = 'no';//06-01-2021-student_payment_invoice_function_change
        $scope.paylink = 'none';//06-01-2021-student_payment_invoice_function_change
        $scope.mailSendData.email = data.se_studentemail;
        $scope.mailSendData.studentid = data.se_studentid;
        $scope.showMailEntryModal();


    };

    $scope.ordercheck = function(paylink){
        debugger;
       // if(paylink == 'yes'){ //06-01-2021-student_payment_invoice_function_change
       if(paylink == 'pi' || paylink == 'fi'){
           $scope.invoicetype = paylink;
            services.sendPaymentLink($scope.mailSendData.studentid,$scope.invoicetype).then((res) => {
                 /****checking */
                 if(res.statuscode == 'NT-200'){
                    var paymentUrl = __env.paymentUrl;
                    var studentname = res.se_name;
                    var enc_studentid = encrypt(res.se_studentid);
                    var enc_studentemail = encrypt(res.se_studentemail);
                    var enc_orderid = encrypt(res.orederid);
                   // var enc_amount = encrypt(res.total) ;
                   var enc_amount = encrypt(res.totalamount) ;
                    ///*****proforma invoice pdf */
                    $scope.pdflink = $rootScope.addImgDefUrl(res.pdflink);
                    if(res.gst){
                        var gst_amount = res.gst_amount;
                        // var gstTd = '<td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">GST</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+gst_amount+'</span></td>';

                        var gstTd = '<tr style="background-color: white;font-weight: 400;text-align: center;height: 13px;"><td colspan="2" style="text-align: left; width: 618px; height: 13px;padding-left:15px;">GST</td><td colspan="2" style="width: 164px; height: 13px;">'+gst_amount+'</td></tr>';

                        
                    }else{
                        var gst_amount = 0;
                        var gstTd = ''
                       
                    };

                        /*****begin-26-12-2020-Aswin-overdueamountmismatch in mail template */
                    // var description = 'Overdue';
                    // var amount  = res.overdueamount;
                    // var total = res.overdueamount;
                  
                    var scheduleamount = res.scheduleamount;
                    var overdueamount = res.overdueamount;
                    var total = res.totalamount;
                    var schedulcount = res.schedulecount;
                    /*****begin-new proforma email */
                   var totalAmountInWords = convertNumberToWords(total);
                   var schedule = res.schedule;
                   /*****end-new proforma email */
                  /*****end-26-12-2020-Aswin-overdueamountmismatch in mail template */
                  // var gstamount = '0';
                  // $scope.order_id = res.orederid; //begin06-01-2021-student_payment_invoice_function_change
                   var date = $filter('date') 
                   ( res.createddate, 'medium'); 
                   
            //begin06-01-2021-student_payment_invoice_function_change
                   //    var action_url = paymentUrl+`?param1=${enc_studentid}&param2=${enc_studentemail}&param3=${enc_orderid}&param4=${enc_amount}`;

                 
                    if($scope.invoicetype == 'pi'){
                        var titleMsg = 'Thanks for using microconnect. This is an invoice for your recent due amount details.';
                        var buttonText = 'Pay Now';
                        var action_url = paymentUrl+`?param1=${enc_studentid}&param2=${enc_studentemail}&param3=${enc_orderid}&param4=${enc_amount}`;
                        $scope.order_id = res.orederid;

                        /*****begin-10-01-2021-Aswin-new proforma email */
                         if(schedulcount == 0){
                             
                             var installmentTrData = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Overdue</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(overdueamount) +'</td></tr>'// /*****begin-19-01-2021-Aswin-amount format change */
                             var  veryfirstInstallmentTrData = '';
                             var mailTemplatemessage = '';
                             var paymentNoSuffix = 'Final';
                             var bottomPaymentTagline =  'Overdue Payment';
                         }else{
                             if(schedulcount == 1){
                                // if(schedule.length != 0){
                                    var scheduleTrData = '';

                                            for(var i = 0 ;i < schedule.length;i++){
                                                // var trdata = '<tr><td style="border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="bottom" bgcolor="#F2F2F2" height="21">Installment No.'+ (i + 1) +' </td> <td style="width: 97px;" align="left" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td> <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; width: 155px;" align="center" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; width: 119px;" align="right"valign="bottom" bgcolor="#F2F2F2">Rs.'+ schedule[i].amount  +'</td></tr>';
                                                var shedulePaymentNo = (i + 1)+' <sup>'+numberSuffixAdd(i + 1)+'</sup> Payment'
                                                var trdata  = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;"><td style="text-align: center">'+shedulePaymentNo +'</td><td>'+ amountFormat(schedule[i].amount) +'</td><td>'+ momentUnixConvert(schedule[i].duedate) +'</td></tr>'
                                                scheduleTrData += trdata;
                                            }
                                            var veryfirstInstallmentTrData = '<tr style="height: 78px;"><td style="height: 78px;"><table style="width: 800px; border: 1; border-color: #ffffff;" cellspacing="1"><tbody><tr style="background-color: #007dc1;font-weight: 600;text-align: center;color: #fff;"><td>Schedule No.</td><td>Total Amount <br>(Rs)</td><td>Payable on or before</td></tr>'+ scheduleTrData +'</tbody></table></td></tr>'
                                 //  }
                                 var mailTemplatemessage = '<p>We have received your document and found is in order.</p><p>As per our discussion we here by sharing the schedule of payment with dates. Kindly ensure the payment is made on time.</p>'

                                 var paymentNoSuffix =  numberSuffixAdd(schedulcount);
                                 var bottomPaymentTagline =   schedulcount +'<sup>'+ paymentNoSuffix + '</sup> Payment&nbsp';
                                

                             }else{
                               var  veryfirstInstallmentTrData = '';
                               var mailTemplatemessage = '';
                             }
                         
                           

                            var installmentTrData = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Instalment No.'+ schedulcount + '</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(scheduleamount) +'</td></tr><tr style="background-color: white;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Overdue</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(overdueamount) +'</td></tr>';
                            var paymentNoSuffix =  numberSuffixAdd(schedulcount);
                            var bottomPaymentTagline =   schedulcount +'<sup>'+ paymentNoSuffix + '</sup> Payment&nbsp';
                         }


                        

                        /*****end-10-01-2021-Aswin-new proforma email */
                        var amountTableTrData = '<tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Installment No.'+ schedulcount +'</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ amountFormat(scheduleamount) +'</span></td></tr><tr><tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Overdue</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ amountFormat(overdueamount) +'</span></td></tr>';

                         $scope.data.textInput = '<table id="m_1975787499329414483bodyTable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #ffffff;height: 100%!important;width: 100%!important;background-color: #f1f7fd;padding:20px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td id="m_1975787499329414483bodyCell" style="margin: 0; padding: 20px; border-top: 0; height: 100%!important; width: 100%!important;" align="center" valign="top"><table id="m_1975787499329414483templateContainer" style="border-collapse: collapse;/* border: 1px solid black; */" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td align="center" valign="top">&nbsp;</td></tr><tr><td align="center" valign="top"><table id="m_1975787499329414483templateHeader" style="border-collapse: collapse;min-width: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;background: #fff;font: 14px sans-serif;color: #686f7a;border-top: 4px solid #007dc1;/* margin-bottom: 20px; */border-bottom: 1px solid #dadee4;box-shadow: -1px 1px 4px 0 rgba(117,138,172,.12);" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td class="m_1975787499329414483headerContainer" valign="top"><table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr style="height: 66px;"><td style="height: 66px;" valign="top"><div style="width: 100%; text-align: center; margin: 12px 0;"><a style="word-wrap: break-word;" href="https://www.microconnect.co.in/" target="_blank"> <img class="CToWUd" src="https://api.microconnect.co.in/logo/microconnect1.png" alt=""> </a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr style=" border-bottom: 4px solid #007dc1;"><td align="center" valign="top"><table id="m_1975787499329414483templateBody" style="border-collapse: collapse; min-width: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0;" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td class="m_1975787499329414483bodyContainer" valign="top" style=" padding: 10px;"><table style="min-width: 100%; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td style="padding-top: 9px;" valign="top"><table style="max-width: 100%; min-width: 100%; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left"><tbody><tr><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px;text-align: left;" valign="top"><strong>Hi '+ studentname +'</strong>,<br><p>Thanks for enrolling with MICROCONNECT</p>'+ mailTemplatemessage +'</td></tr>'+ veryfirstInstallmentTrData +'<tr style="height: 78px;"><td style="height: 78px;"><table style="width: 800px; border: 1; border-color: #ffffff;" cellspacing="1"><tbody> <tr style="background-color: whitesmoke;height:60px;text-align: center;"> <td style="font-weight:bolder;font-size:larger;color: #000;">Amount: Rs.'+amountFormat(total)+'<br><span>('+ totalAmountInWords +'rupees only)</span></td> </tr></tbody></table></td></tr><tr><td align="right"><h3 style=" padding-right: 10px;"><strong>Proforma Invoice No :'+  $scope.order_id +'</strong></h3></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table style=" border: 1; border-color: #ffffff;" cellspacing="1"><tbody><tr style="background-color: #ffcc66; font-weight: 600; text-align: center; height: 26px;"><td style="text-align: left;height: 26px;padding-left: 15px;" colspan="2">Description.</td><td style="width: 164px; height: 26px;" colspan="2">Amount <br>(Rs)</td></tr>'+ installmentTrData +'<tr style="background-color: #ffcc99; font-weight: 400; text-align: center; height: 13px;"><td style="width: 322.5px;text-align: right;height: 13px;padding-right: 30px;"><strong>Total :</strong></td><td style="width: 295.5px; text-align: left; height: 13px;"><strong style="padding-left:7px";>Amount in words :</strong><br>&nbsp; '+ totalAmountInWords  +'rupees only</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(total) +'</td></tr></tbody></table></td></tr><tr style="height: 37px;"><td style="height: 37px;"><table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr><tr style="height: 261px;"><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,"Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 150%; text-align: left; padding: 0px 18px 9px; height: 261px;" valign="top">Please find attached the Proforma Invoice for the '+ bottomPaymentTagline +'<br><p>If you have any clarification feel free to contact us on the coordinates given below. Please do quote your enrolment number on all your communication for speedy resolutions.</p><p>Happy Learning,</p><p>With warm regards,</p><p>Team MICROCONNECT,</p>Mobile:+91 96770 96011,<br> Mail – <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>&nbsp;,<br>Visit – <a href="http://www.microconnect.co.in">www.microconnect.co.in</a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'

                    }else{
                        //final invoice
                        var titleMsg = 'Thanks for using microconnect. This is an invoice for your payment details.' 
                        var buttonText = 'Download Invoice';
                        var action_url = $scope.pdflink;
                        $scope.order_id = res.invoiceno;
                        // var amountTableTrData = '<tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Paid Amount</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ total +'</span></td></tr>';
                        var amountTableTrData = '<tr style="background-color:whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td colspan="2" style="text-align: left; width: 618px; height: 13px;padding-left:15px;">Paid Amount</td><td colspan="2"style="width: 164px; height: 13px;">'+ amountFormat(total) +'</td></tr>'
                    /*****begin-10-01-2021-Aswin-new proforma email */
                        // $scope.data.textInput = '<p>&nbsp;</p><table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><table style="height: 1086px; width: 100%;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><a style="align-item: center;" href="http://microconnect.co.in/" target="_blank" rel="noopener"> <img style="height: 13vh; border: none; display: block; margin-left: auto; margin-right: auto;" src="https://api.microconnect.co.in/logo/microconnect.png" /> </a></td></tr><tr style="height: 718px;"><td style="width: 100%; margin: 0px; padding: 0px; background-color: #ffffff; height: 718px;"><table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;"><h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hi  '+ studentname +',</h1><p style="color: #51545e;">'+ titleMsg +'</p><table class="attributes" style="margin: 0 0 21px;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_content" style="background-color: #f4f4f7; padding: 16px;"><table role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_item" style="padding: 0;"><span class="f-fallback"> <strong>Amount:</strong>'+ total +'</span></td></tr></tbody></table></td></tr></tbody></table><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td align="center"><table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr></tbody></table><table class="purchase" style="width: 100%; margin: 0; padding: 35px 0;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><h3 style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;">Proforma Invoice No :'+  $scope.order_id +'</h3></td><td><h3 class="align-right" style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;"></h3></td></tr><tr><td colspan="2"><br /><table class="purchase_content" style="width: 100%; margin: 0px; padding: 25px 0px 0px; height: 108px;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="left"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Description</p></th><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="right"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Amount</p></th></tr>'+amountTableTrData+'<tr>'+ gstTd +'</tr><tr><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="80%"><p class="f-fallback purchase_total purchase_total--label" style="margin: 0; text-align: right; font-weight: bold; color: #333333; padding: 0 15px 0 0;">Total</p></td><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="20%"><p class="f-fallback purchase_total" style="margin: 0; text-align: right; font-weight: bold; color: #333333;">'+total+'</p></td></tr></tbody></table></td></tr></tbody></table><p style="color: #51545e;">If you have any questions about this invoice, simply reply to this email or reach out to our <a style="color: #3869d4;" href="http://microconnect.co.in/">support team</a> for help.</p><p>Cheers, <br />The Microconnect Team</p></td></tr></tbody></table></td></tr><tr style="height: 288px;"><td style="height: 288px;"><table class="email-footer" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;" align="center"><p class="" style="color: #6b6e76;">&copy; 2020 <a href="http://microconnect.co.in/" target="_blank" rel="noopener"><span style="color: #3366ff;">microconnect</span></a>. All rights reserved.</p><p class="f-fallback sub align-center" style="color: #6b6e76;">Sai Anugragh,<br />16 Ashtalakshmi Street,<br />Muthulakshmi Nagar,<br />Chitlapakkam,<br />Chennai 600064,</p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Phone:</strong><a href="tel:+919677096011">&nbsp; +91 96770 96011</a></p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Email:</strong><a href="mailto:info@microconnect.co.in">&nbsp;&nbsp; info@microconnect.co.in</a></p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>';
                       $scope.data.textInput = '<table align="center" border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483bodyTable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #ffffff;height: 100%!important;width: 100%!important;background-color: #f1f7fd;padding:20px;" width="100%"><tbody><tr><td align="center" id="m_1975787499329414483bodyCell" style="margin: 0; padding: 20px; border-top: 0; height: 100%!important; width: 100%!important;" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateContainer" style="border-collapse: collapse;/* border: 1px solid black; */" width="600"><tbody><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateHeader" style="border-collapse: collapse;min-width: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;background: #fff;font: 14px sans-serif;color: #686f7a;border-top: 4px solid #007dc1;/* margin-bottom: 20px; */border-bottom: 1px solid #dadee4;box-shadow: -1px 1px 4px 0 rgba(117,138,172,.12);" width="600"><tbody><tr><td class="m_1975787499329414483headerContainer" valign="top"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" width="100%"><tbody><tr style="height: 66px;"><td style="height: 66px;" valign="top"><div style="width: 100%; text-align: center; margin: 12px 0;"><a href="https://www.microconnect.co.in/" style="word-wrap: break-word;" target="_blank"><img alt="" class="CToWUd" src="https://api.microconnect.co.in/logo/microconnect1.png" /> </a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr style=" border-bottom: 4px solid #007dc1;"><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateBody" style="border-collapse: collapse; min-width: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0;" width="600"><tbody><tr><td class="m_1975787499329414483bodyContainer" style=" padding: 10px;" valign="top"><table border="0" cellpadding="0" cellspacing="0" style="min-width: 100%; border-collapse: collapse;" width="100%"><tbody><tr><td style="padding-top: 9px;" valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%; min-width: 100%; border-collapse: collapse;" width="100%"><tbody><tr><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px;text-align: left;" valign="top"><strong>Hi '+studentname+'</strong>,<p>Thanks for using microconnect. This is an invoice for your payment details.</p></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table cellspacing="1" style="width: 800px; border: 1; border-color: #ffffff;"><tbody><tr style="background-color: whitesmoke;height:60px;text-align: center;"><td style="font-weight:bolder;font-size:larger;color: #000;">Amount: Rs.'+ amountFormat(total) +'<br><span>('+ totalAmountInWords +'rupees only)</span></td></tr></tbody></table></td></tr><tr><td align="right"><h3 style=" padding-right: 10px;"><strong>Invoice No :'+ $scope.order_id +'</strong></h3></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table cellspacing="1" style=" border: 1; border-color: #ffffff;"><tbody><tr style="background-color: #ffcc66; font-weight: 600; text-align: center; height: 26px;"><td colspan="2" style="text-align: left;height: 26px;padding-left: 15px;">Description.</td><td colspan="2" style="width: 164px; height: 26px;">Amount<br />(Rs)</td></tr>'+ amountTableTrData +''+gstTd+'<tr style="background-color: #ffcc99; font-weight: 400; text-align: center; height: 13px;"><td style="width: 322.5px;text-align: right;height: 13px;padding-right: 30px;"><strong>Total :</strong></td><td style="width: 295.5px; text-align: left; height: 13px;"><strong style="padding-left:7px">Amount in words :</strong><br />&nbsp;'+ totalAmountInWords +' rupees only</td><td colspan="2" style="width: 164px; height: 13px;">'+ amountFormat(total) +'</td></tr></tbody></table></td></tr><tr style="height: 37px;"><td style="height: 37px;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr><tr style="height: 261px;"><td class="m_1975787499329414483mcnTextContent" helvetica="" neue="" style="color: #554d56; font-family: Roboto,">Please find attached the Invoice for the Payment<p>If you have any clarification feel free to contact us on the coordinates given below. Please do quote your enrolment number on all your communication for speedy resolutions.</p><p>Happy Learning,</p><p>With warm regards,</p><p>Team MICROCONNECT,</p>Mobile:+91 96770 96011,<br> Mail – <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>&nbsp;,<br>Visit – <a href="http://www.microconnect.co.in">www.microconnect.co.in</a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'
 /*****end-10-01-2021-Aswin-new proforma email */
                    }
                  
             //end-06-01-2021-student_payment_invoice_function_change
                  
                 /*****begin-10-01-2021-Aswin-new proforma email */

                //   $scope.data.textInput = '<p>&nbsp;</p><table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><table style="height: 1086px; width: 100%;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><a style="align-item: center;" href="http://microconnect.co.in/" target="_blank" rel="noopener"> <img style="height: 13vh; border: none; display: block; margin-left: auto; margin-right: auto;" src="https://api.microconnect.co.in/logo/microconnect.png" /> </a></td></tr><tr style="height: 718px;"><td style="width: 100%; margin: 0px; padding: 0px; background-color: #ffffff; height: 718px;"><table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;"><h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hi  '+ studentname +',</h1><p style="color: #51545e;">'+ titleMsg +'</p><table class="attributes" style="margin: 0 0 21px;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_content" style="background-color: #f4f4f7; padding: 16px;"><table role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_item" style="padding: 0;"><span class="f-fallback"> <strong>Amount:</strong>'+ total +'</span></td></tr></tbody></table></td></tr></tbody></table><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td align="center"><table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr></tbody></table><table class="purchase" style="width: 100%; margin: 0; padding: 35px 0;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><h3 style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;">Proforma Invoice No :'+  $scope.order_id +'</h3></td><td><h3 class="align-right" style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;"></h3></td></tr><tr><td colspan="2"><br /><table class="purchase_content" style="width: 100%; margin: 0px; padding: 25px 0px 0px; height: 108px;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="left"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Description</p></th><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="right"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Amount</p></th></tr>'+amountTableTrData+'<tr>'+ gstTd +'</tr><tr><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="80%"><p class="f-fallback purchase_total purchase_total--label" style="margin: 0; text-align: right; font-weight: bold; color: #333333; padding: 0 15px 0 0;">Total</p></td><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="20%"><p class="f-fallback purchase_total" style="margin: 0; text-align: right; font-weight: bold; color: #333333;">'+total+'</p></td></tr></tbody></table></td></tr></tbody></table><p style="color: #51545e;">If you have any questions about this invoice, simply reply to this email or reach out to our <a style="color: #3869d4;" href="http://microconnect.co.in/">support team</a> for help.</p><p>Cheers, <br />The Microconnect Team</p></td></tr></tbody></table></td></tr><tr style="height: 288px;"><td style="height: 288px;"><table class="email-footer" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;" align="center"><p class="" style="color: #6b6e76;">&copy; 2020 <a href="http://microconnect.co.in/" target="_blank" rel="noopener"><span style="color: #3366ff;">microconnect</span></a>. All rights reserved.</p><p class="f-fallback sub align-center" style="color: #6b6e76;">Sai Anugragh,<br />16 Ashtalakshmi Street,<br />Muthulakshmi Nagar,<br />Chitlapakkam,<br />Chennai 600064,</p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Phone:</strong><a href="tel:+919677096011">&nbsp; +91 96770 96011</a></p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Email:</strong><a href="mailto:info@microconnect.co.in">&nbsp;&nbsp; info@microconnect.co.in</a></p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>';
                
                             //   $scope.data.textInput = res.html;
    
                    $scope.orderid =  res.orederid;
                           
                 }else{
                    // services.toast('warning','Payment link not genrated');
                    services.toast('warning',res.message);//end-06-01-2021-student_payment_invoice_function_change
                    $scope.paylink = 'none';
                 }
            })
        }else{
            $scope.data.textInput = '';
            $scope.paylink = 'none';
        }

    };
      
    $scope.templateCreate = function(data){
        debugger;
        var minDate = new moment();
        $scope.min = minDate.format("YYYY-MM-DD");
        var invoicedate =  $scope.min;
        var name  = data.se_name;
        var  invoiceno = data.invoice;
        var  studentid = data.se_studentid;
        var  duedate =  $scope.min ;
        var  address1 = data.se_address.line1 + ', '+ data.se_address.line2;
        var  address2 = data.se_address.city;
        var   pincode = data.pincode;
        var   mobileno = data.se_studentmobileno.mobileno;
        var   email = data.se_studentemail;
        var   subtotal = data.totalamount;
        var     total = data.totalamount;
        var schedule = data.schedule;
       if(schedule.length != 0){
        var scheduleTrData = '';
                for(var i = 0 ;i < schedule.length;i++){
                    var trdata = '<tr><td style="border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="bottom" bgcolor="#F2F2F2" height="21">Installment No.'+ (i + 1) +' </td> <td style="width: 97px;" align="left" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td> <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; width: 155px;" align="center" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; width: 119px;" align="right"valign="bottom" bgcolor="#F2F2F2">Rs.'+ schedule[i].amount  +'</td></tr>';
                    scheduleTrData += trdata;
                }
       }else{

       }
       
       
         var invoicehtml = '<table style="/* height: 931px; */width: 14.8cm; height: 21cm; border-collapse: collapse; font-size: 14px; font-family: ui-sans-serif;" border="0" width="763" cellspacing="0"><colgroup width="138"></colgroup><colgroup width="120"></colgroup><colgroup span="2" width="94"></colgroup><colgroup width="146"></colgroup><colgroup width="112"></colgroup><tbody><tr><td style="border-top: 2px solid #000000; border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="middle" height="58"><span style="color: #2c3964;"><br /><img style="height: 60px;" src="https://api.microconnect.co.in/logo/microconnect1.png" alt="" /> </span></td><td style="border-top: 2px solid #000000; width: 97px; border-bottom: 2px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-top: 2px solid #000000; border-right: 2px solid #000000; width: 278px; border-bottom: 2px solid #000;" colspan="2" align="right" valign="middle"><strong><span style="color: #3a5d9c; font-size: x-large;"><img style="height: 50px; padding-right: 10px;" src="https://api.microconnect.co.in/logo/silver_learning_partner.png" alt="" /> </span></strong></td></tr><tr><td style="border-top: 2px solid #000000; border-left: 2px solid #000000; width: 372px; border-bottom: 2px solid #000;" colspan="3" align="left" valign="middle" height="58"><p>16 Ashtalakshmi Street, Muthulakshmi Nagar, Chitlapakkam Chennai 600064.</p></td><td style="border-top: 2px solid #000000; width: 97px; border-bottom: 2px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-top: 2px solid #000000; border-right: 2px solid #000000; width: 278px; border-bottom: 2px solid #000;" colspan="2" align="right" valign="middle">&nbsp;</td></tr><tr><td style="border-left: 2px solid #000000; width: 271px;" colspan="4" align="center" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">BILL TO</span></strong></td><td style="width: 155px;" align="right" valign="bottom">DATE</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom">'+ invoicedate +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21"><strong>'+ name + '</strong></td><td style="width: 200px;" align="left" valign="bottom"></td><td colspan="3" align="right" valign="bottom"><strong> Proforma Invoice #</strong></td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5;" colspan="1" align="center" valign="bottom">'+ invoiceno +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="3">' + address1 +','+ address2 +'</td><td style="width: 155px;" align="right" valign="bottom" colspan="2"><strong>Enrolment No.</strong></td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom">'+ studentid +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="3">Pincode-'+ pincode +'</td><td style="width: 97px;" align="left" valign="bottom">&nbsp;</td><td style="width: 155px;" align="right" valign="bottom">Due On</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom" bgcolor="#D2D8EC">'+ duedate +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="6">Mobile : '+ mobileno +' ,Email : '+ email +'</td></tr><tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 473px;" colspan="4" align="center" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">DESCRIPTION</span></strong></td><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; width: 155px;" align="center" valign="bottom" bgcolor="#3B4E87"><strong><span style="color: #ffffff;">TAXED</span></strong></td><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-right: 2px solid #000000; width: 119px;" align="center" valign="bottom" bgcolor="#3B4E87"><strong><span style="color: #ffffff;">AMOUNT</span></strong></td></tr> '+ scheduleTrData +'<tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">OTHER COMMENTS</span></strong></td><td style="border: 1px solid #000000; width: 155px;" align="left" valign="bottom">Subtotal</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="right" valign="bottom">Rs.'+ subtotal +'</td></tr><tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="bottom" height="21">Total payment due</td><td style="border: 1px solid #000000; width: 155px;" align="left" valign="bottom">&nbsp;</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="left" valign="bottom">&nbsp;</td></tr><tr><td style="border-top: 1px solid #a5a5a5; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="top" height="21">&nbsp;</td><td style="border: 1px solid #000000; width: 155px;" align="right" valign="bottom">&nbsp;</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="left" valign="bottom">&nbsp;</td></tr><tr><td style="border-width: 1px 1px 1px 2px; border-style: solid; border-color: #000000; width: 372px;" colspan="4" rowspan="2" align="left" valign="top" height="42">Invoice with Tax compoenent will be shared upon credit of the full payment towards the course.</td><td style="border-left: 1px solid #000000; width: 97px; border-bottom: 1px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; border-bottom: 1px solid #000;" align="right" valign="bottom">&nbsp;</td></tr><tr><td style="border-left: 1px solid #000000; width: 97px;" align="left" valign="bottom"><strong>Total</strong></td><td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000; width: 155px;" align="right" valign="bottom">Rs.'+ total +'</td></tr><tr><td style="border-top: 1px solid #000000; border-bottom: 1px solid #a5a5a5; border-left: 2px solid #000000; border-right: 2px solid #000;" colspan="6" align="left" height="50">Please do the payment through NEFT on the MICROCONNECT`s Current a/c No. 268705000566. Bank - ICICI Bank - Chitlapakkam Branch. IFSC-Code - ICIC0002687. or click the link &amp; Pay&nbsp;</td></tr><tr><td style="border-top: 1px solid #000000; border-bottom: 1px solid #a5a5a5; border-left: 2px solid #000000; border-right: 2px solid #000;" colspan="6" align="left" height="50"><table style="border-collapse: collapse; width: 100%;" border="1"><tbody><tr><td style="width: 50%; text-align: center;"><strong>MICRCONNECT</strong></td><td style="width: 50%;" rowspan="3"><p style="text-align: center;">For MICROCONNECT</p><p><img style="display: block; margin-left: auto; margin-right: auto;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABtAIkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9UccUY4rK1W5nstLvZ7WNLm5jjeSOHOzzJAhwn41X8L69D4m0Sy1O3BjjuE3+XJjzI/8AYqbac4G/RRRVAFFFFABRRRQAUUUUAFFchBfy6f4ok0y8eS4tL/fc2ksg/wBXIn+siz+AkT/tp/zzrr6ACiiigAooooAKKKKAIpf9VXBeA4RpMlrbxx+Xb39jFepF6SCONJP/AGn/AORK9Crz3zV02x8N3qL+7s77+z5Pn+4kkj2//ozyvyq4gdZq0V4+mXkdi8aXvlSC3Mn3Ek/5Z1Q8KazH4k0Gz1JkWIzpiSFznypc7JI/qj70rpa5LQbZdL8Qa1YRsPss7pqMef4DJv8AM/8AIkZk/wC2lQB0Uk0cbojyeWX+5VojNc14s/4/fD//AGE4/wD0VJWL8RfibafDuzt4fIk1PWbw+XZaVa/6y4k/XYnvV8nPogOvvb6302zkurudLe3jG+SWWTYifjXCL8YbbVBOvhnS7/xKI/8Al8ij8iy/8CJfLjP/AGz8yvGrzWNR8VT3F74gkt9b+x/vbgSySf8ACO6R/wBM/wB3/wAfkn/LPy6v3lx4b1r4e+IfEenai/xB1jREjJt9XSSOztxv/gs/3caJ5e//AL910+x5PjOb2h3dx8WNSkXbb6t4QsQP9ZHHeyajLH/2zjjjob4q3hAz4r8NqZP9X9v0i7s4/wDv48tcv4Z+Jt3ZeDfE0EF3aXNxZ6P/AGrp9zbWkdtH/q/Lkjkj/wBX+7kregv7v4beFte1G48SS+JbmPS7a4is9Rk8x45JP3fmeZ/zzkkH+r/6Z1c4cn2A9oXL34gaq1il/wD2Gmt21rLFL9t8Mail7GO0nmRny3/1fmf89K7zw7440Pxjbl9H1aC+MY/eRQyfvI/+uiH50/GvKLDQ7a68QeIYfFBtGudItI7+TX9Lt3s7i3kkSTzI/MjP7zZ5fmfiOK4nSTHrCQzxw3t7p9pB9osNVsjHb69aW+PLjkkjj/4+I/Mjkjo9jCYc/IfWoIpa8i8B/E+eWex0nxHPaXMl/H5mla3ZfJZ6mn9zn/Vyf9M69bB4Fcc4ezNIT5x1FFFQahRRRQAg6Vxltpba94R1SwkxD59xqEcZ/uf6RL5b12Y6VgeE5PN0+5/6Z6heD/yYkpQAn8Oal/bOiWN9jy3uLeOQiqOobrXxZo05jHkzxT2eff5JE/8ARb0ng5vLttRsgI0+x38seIv7j/vI/wDyHJHR4xItdOs77eENnexSnP8AceTy5D/37kkq/t8gGD8W/F1p8P8ARNP128SS4js7z93FH1kkkjkjjT/yJXjnhjw9r3xA8S3kd49xBq1wgfXL2OX97pdtIfMj0+3z/q5JP+Wn/POk+Onj2a4+JWlaZZ6eb228OSpJHF/yzl1GRD9nT/gH7s/jJT/EeoWfgWPT/B3iPTdatTJL/aFv4r064MkkmoOS8snlx/8APOST/V/vP3f8GK9KFPkpw/nOafvzOg8EX9vffDjWf+EifSdI8JRmTS30u0t5Ek0uXzPL8uSTMnrHJ5n9+TfWV4e8Lf8ACA77vW9WuJrzULmXSdUs7xHk/te3Ef7uSzjjj8ySTY8Y/wC/lUdXs4tetR5nl6/rOryx29pe6De/Z7fW9n+s+2R/8s/LT95IK9w8I/D+28MGW/vbiTVvEV3/AMferSj95J/sR/8APOPj/Vx1lOfsw5Oc8i+HHgq+0bXNdt7Lw3dXsFxZR2+PFGoR28kdnJ5n7vy445P9ZJHJWxcfC7WNI0HVdHTwzZXNlqnlCR7HWpDexeWf3ZjkuItn7v8A5Zx9BXrd5i28V6dOpAiuIZbeTHeT93In/jkctM8Na02stqKTQSWktleSWbxyYxlDvjk/4HHJG9Zzrzfvmns4HkEelWN876VqUmrWOo3959sv7bxAI47jU5I4/wB3HHJH/o8kfmeX+7jrz+LxHrXwa0i3dIo5fF2p2Yt3spD+70yP/l38z/ppJ+8/d/8AxuvrDWtCsPE+nSWGqWkd9ZyffilTg14L8VfDbadpDaPq882oaV5iXmn3uf3s728ckhs7iT/ponmbJK6KFbn9yZnOHIRzaZLr3hnUNVwmraUZI49bt7GDy4rySOP95eWcn/PSOT/lp+7jk8uu/wDhV4onu2k8P6rfpf6hbwxXVnqJ6anZvxFcf7/8EnvXIaBpni7xvbaVqOnaoulaOLOK70SPSXH2O3kSX/j3uP8AlpJ+7+Tj93/rPaqniiwk+G/iC4ksbfK6K/8Abunwx/8ALSzkk8u9t/8AtnJJ5kf/AF0rOfv+4H8P3z6OxRis6z1GDVLKC7t286CeNJI5PVH6VLc38NqV8xwhf7if368/kOku0Vlwy3Ut3v8ALEdmI/8Alp/rHrUpgNxXA6V4nsfDulyPfP5YuNbuLKNI08zzJJLmTy678HNeTvbRy28U8g8ySPxXJ5f/AIEVVP3wOwhWO18aXcYAxf28dwmf4pI/kk/8ceKtDxFax33h7VrST7klvJH/AOQ6oeJoljm0vUgiCSzvE+c/885P3cn/AKHn/gFa+pRvPYXCIfnMbgVS3QHx98GNKk8ReJ/A99rM7317qmsahqskkknmSf6PH5cfmf8AbTzK67xh+zP4itWkk0TxJf63p0knmSaTe3n2eTzP+ekcn+r8z/tnHXCfAeK48O/ELwJO7yXVvqFvcfZ4v9Z5f7ySOT/0X5leheF/G/jK6+OvmeILY2Oi+Vcafbvc28lvbvGkn+sj3n/WSSRx/f8Awr1K7nTqfuzm5IHefCzS5LvxFrWoPcSXsemCPQ7S4udhlk8vH2iWTy/+Wkkn8f8A0zr1sygfJXkfwpR73QroWV5Jby2l3qMcksXzxvcSXDvvkT+Monl/9/K7Kw0salYyy6rZG1v98n+okxJGM7N8bphx5mzf/wDqrzqnx++a0/gJfHl3HpGiDVvLeX+y5Y7zy4+0f+rk/wDIbyVN4QtpbPSEkuR5d5cyyXEysOkjneU/7Z/6v/tnXO63o2ua3ZR6TdXVnNaRv5kl1cRF/tEf/LNJY0kjw/mH+D5H8v8Ag/1dbtloutBWTUNdST5dgFjafZ9/GOd8kn/jmylf3TQyLXxze3E9nHdaPJZpcGN47ozxvGY5DsR/9/e8Y8v/AKaVq+PvCsHjHwjqWiXGA9xHiKTP+rl/5Zyf991lWPhK40zRrzTpbsi12eXZykPJLHgyPHI+f9Y6fu/++K6zTZbu6s45J7X7FOfvxSSeZRPTWAHz54V8OS+NIfD51PTb+/8ACUmmW32OKzuUt7K3ljjk8yS4j8yOR5PMqxcaFZeDm023+0w3o/tuS3TStOleSO30+8j8vyvn/wCmkccn/XSuq8DeD7278Lx22na/Jo9vbahqEZjsYI5IzsvJR+73x/u/+AUzxF4UvPCx1TVL/VpNXtruXR4s3KRpIkkd7H/zzjjj8v8AeV2c/vnFyF34Hale6t8O7SxuHeyk0yWTTpIv+Wv7tynz/wBw8V6XZabBY79gzLJ/rJX++9effBt44Lzx5Cicp4nu3/7+Rxyf1r1Dsa4K3xnTAdRRRUGg0CvJ5rpbXQdRv3Aji0/xOZJJD/yzj+0/vJP+/chr1muJ8D20cqeJo5I0kik1i4+ST+PiOlCfIBteJLU6p4dvoEEcnmW8nl/9dP8AlnVzTtQivtPt7pB+7uIo5R/wOuMsPDOu+FJtmgNay6M/zjTb6SSL7Kf+mcib/k4/1dbnhC2udNtbqxnBYWly8ccmf9ZH9+P8vM2f9s6YHz38NfL0jxloMd+kcg0fXNU8Peb/AM85JP3kf/fz95HXtPir4k+HNNvJNHmF3rl55eZ9N0yzkvZEj/6apH/q/wDtpXmfxY8HyaL49uHtES2j8VRxizuSdkdvrFv+8tif+un+rq5/aN0NBvfFPhmeCKz1tJJ9Tj1a4jt7KwuEjjjk8x/L8zzPMj8vy/M8vg16FT95yTM/4Z1Xwi1jTI7zWNK04p/Z7yf2xphQCPzLa45OyPt5cgkj/KvVyK+VPBnjzTpNK8O6doz39lrulxxyWl7qqJHb3gkz5lmJP+WaSeX+78z/AJ5x1714f8WR+MrCOfTne28s7Lu2uY9lxbyf885I6yrUWmKE7nQ3mqW1rIEeX9+4+SKPmQ/8AqaGR7iIfu3ij/6acPSQ2McDSvGiLJK/mSEfx1drkNSrHEkX+/8A89Kw/GfiKHwh4Z1TV5lUpZwSSiM8eY/RI/8Agb8fjW3LNHbQySSP5caf8tDXi+reJ7Hxp4h0aa5aRfC1re5sYzbyPJqd5H/y1jj/AOfeLh/M/wBXWkIc5lUnyEOt+E/Gth8HNF0vw1NJ/bz5S9QGOPmQO8nzv9zy5P7lSrqOv6hofhDRvE8tudV1O8t5/LCSRyeXb/6RJ5n/AH7j/wC/lWvh98UtduvEsvhXX9KE95aSSW9xq2mxSSRof+WZkj8v935g/eeZ/q6wPifrt3qWq6pdacUIh/4prSP78uoXEkf2iSP0Ecf7vzP+enmV1w0nyTM/c5Dsf2f5G1TwrqutSR+X/bGsXmoRj/YeTYP/AEXXq56Vz/hDw9B4T8N6do1uP3VnBHEK3/4a46kuaehrDYdRRRUGhDXHfDy5SaTxPs/5Z65cR/8AouuwPUVkaLoVpoEuoi13br27kvZR/wBNJOv/AKBXDTqfGaG6DVK6uobGJ7ieZIo4/vySv5aCrFU77T4L6zltbyCO5tpI9jxSx+ZG/wDwCuinO5mefeNNR8G/ErwjeWR160uonf8A0eSyk82W3uY5P3ckaR/vPMjkx0ryDSvE97ZvqkfiGyt7KIyRxeIrK5g8ySzuP+WWqRR/885P3fmf9NK+m9G0TTdItzHpumWumx/887aJIh/45XF/Er4Zf8Jg9tq2jXUek+JtPSRLe5ki8yKSOQ/vIpY/40eu6nU5PcMpwPA/HHh278C+B7xII5ZLi9j+0ap4ijuPMt7uOST93JH5n/LxJ/0z/wBXHXRWHxLGueMdR/tS2vNB1GCOMWeo6dKBeDzJI447eWP/AFdx/rPM/ef6v95UGg6ld6beS+GpNFSSRPLkuPAurS/u/Mjk/wBZp1xJ+7kj/wCWnlyU9fBOneMPEFl/wiutJpurWlvJbyabrXmR6hFJJJJJJcSR/wDLSTy5K9BThU/iHN+8PSvD/jXxBdSMNNv/AA94sso55LOOT7RJp9w8kf8ArU2CORJPL/6Z+lW7/wCJXiCwF7BfaboeiXNtB9ok+2a35myP+/5ccfmV594A+Hninw14qis2022lSwk1G5s55F8u2kldLaOOV/L8zy96eZ8n/XSsLxd4P8Wa94t1mxezjk1PWJEl3G08yzSP7P8A62O4ePzI5EkjjTy65lThOdjXnqch2J8UaX4t1CBPEmsT+INO8yPzItNg8vSreSSXy447j955kn7yP/lp+7rnfiDY3x1u50nWdZu7WS41Ty7O2l/1clnJJ5cclv8A9c/M8uSP/lpHSar4O1G5/wCEq1/VZLDwRHq4+z3FtrcsZjkj8uPzJI/Lk/1kckfmR/8AXSuq8T/FKW/0yOTTrhNN06ONMeK9Ws/LEkn/AE528n7yST/yHWn8Of7sy/xlHSJb7wS+s+HoJ9Msr66k+23EumxyfYtEtPLjEkkm8/6x/wDlnF7j/lmK2PhD4Rg1q4sfEn2SS00LT45LfQLK4/1nlyf6y8k/6aSVR8BfDZ/E0Hn6tY3em+GjcfaI7HUZPMvdXkz/AMfF5J+Xlx17xHEkcewJxXPWqGsKZNRRRXIdIUUUUAQ0UUV5nIaBUo6VFUo6V1UzMKWiiukDlPF3gTRfHWn/AGTXdPivo4yWikkX95E/9+N+qH6V5jr3wR16O3ihtdS03xdpVvHi3svFluZLi3/653kf7yveMCjFXCpKBnyI+YR4d8ZeGbOOCPw34wshGP8AWaB4kjvI/wDtnHcVWW18Z67HJaTab8Rr2OT93HHLf2dnH5f/AE0k8uvqfFGK6PrL7ByHzj4X+E3ieeUSW/hvRfCUQj8uO91W5k1nUY/+ufmfu469I8MfB3S9C1OLWdQnu/EmvJHj+0tVl8ySP/rnH9yP8K9F4Bp1Z1K85ihDkCiiisDUKKKKACiiigD/2Q==" alt=""  height="80"/></p><p style="text-align: center;">Authorized Signatory</p></td></tr><tr><td style="width: 50%;"><p><strong>PAN - ABCFM7023N</strong><br /><strong>GST No. - 33ABCFM7023N1Z7</strong></p></td></tr><tr><td style="width: 50%;"><p style="text-align: center;">If you have any questions about this Proforma invoice, please contact<br /><strong>[K. Venkata Ramanan, +91 7358512225, kvenkat@microconnect.co.in ]</strong></p></td></tr></tbody></table></td></tr></tbody></table>';

         var pdfcreateData = 
         {"se_studentid":studentid,
         "html":invoicehtml,
         "invoice":invoiceno
        }
         services.proformaPdfCreate(pdfcreateData).then((res) =>{
               if(res.statuscode == 'NT-200'){
                   var action_url = res.path;
  var previewProformaHtml = '<table style="width: 600px;background: #600200;/* border-top: 4px solid #1977cc; */border: 1px solid black;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="background: #fff;" align="center" width="600"><a target="_blank"><img class="CToWUd" style="display: block; width: 50%; margin: 0px;" title="" src="https://api.microconnect.co.in/logo/microconnect1.png" alt="Microconnect" width="300"></a></td> <td style="background: #fff;" align="center" width="600"><img class="CToWUd" style="width: 50%; margin-top: 0px; display: block;" title="" src="https://api.microconnect.co.in/logo/silver_learning_partner.png" alt="" width="300"></td> </tr> </tbody></table><table style="width: 600px;background: #ffffff;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="background: #ffffff; width: 600px; padding: 20px; border-left: #cccccc 1px solid; border-right: #cccccc 1px solid;" align="center" width="600"> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> <strong>Hi '+name +','+ '-'+ studentid +'</strong></p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> Greetings from MICROCONNECT</p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> This is Proforma invoice for your payable amount details.</p><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td align="center"> <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank"  download>Download</a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> If you have any clarification regarding this proforma invoice No. feel free to write to us at <a href="mailto:support@microconnect.org">support@microconnect.org</a> or call / WhatsAPP us at the details given below</p><a style="text-decoration: none; text-align: center; display: table-cell; vertical-align: middle;" target="_blank"><img class="CToWUd" alt=""></a> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> Cheers!</p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 10px; font-weight: normal; padding: 0 12px;"> Team Microconnect</p> </td> </tr> </tbody></table><table style="width: 600px;border-top: 1px solid black;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="border-left: #cccccc 1px solid; border-right: #cccccc 1px solid;" align="center" width="600"><a target="_blank"><img class="CToWUd" style="width: 100%; display: block;" title="" src="https://api.microconnect.co.in/logo/microconnectword.png" alt="e" border="0"></a> <div class="a6S" dir="ltr" style="opacity: 0.01;">&nbsp;</div> </td> </tr> </tbody></table>'

         $scope.data.textInput = previewProformaHtml;
               }else{

               }
         })

         

       
    }

    $scope.proformaInvoiceTemplateCreate = function(){

        services.getEditPaymentShedule($scope.mailSendData.studentid).then((res) =>{
           
            if(res.length == 0){
                services.toast('warning','Schedule not create');
            }else{
                $scope.templateCreate(res[0]);

            }
           
        })

    }

  $scope.chooseMailTemplate = function(mailtemplate){
        
       if(mailtemplate == 'paymentlink'){
           $scope.ordercheck('yes');
       }else if(mailtemplate == 'proformainvoice'){
        $scope.ordercheck('no');
        $scope.proformaInvoiceTemplateCreate()
      }else if(mailtemplate == 'normal'){
        $scope.ordercheck('no');
      }
  }

    $scope.discard = function(paylink){
        //if(paylink == 'yes'){//06-01-2021-student_payment_invoice_function_change
        if(paylink == 'pi' || paylink == 'fi' ){//06-01-2021-student_payment_invoice_function_change
            var discardData = {
                discard : true,
                orderid: $scope.orderid
            }
            services.discardPaymentLink(discardData).then((res) => {
                if(res.statuscode == 'NT-200'){
                    $scope.data.textInput =''; //06-01-2021-student_payment_invoice_function_change
                    $scope.paylink = 'none';
                    $scope.mailSendData = {};
                }
               
            })
        }else{
            $scope.data.textInput = '';
            $scope.mailtemplate = '';
        }

        $scope.hideMailEntryModal();
    }

    $scope.viewStudentData = function (enroll_id, userViewType) {
        if (enroll_id != "" || enroll_id != null || enroll_id != undefined) {

            // var findindex =  $scope.studentExportData.findIndex(x => x.se_studentid == enroll_id);

            // $scope.studentViewData  = $scope.studentExportData[findindex];

            // $scope.showStudentViewModal();
            //$state.go('users/'+enroll_id);
            setLocalStorage('userViewType', userViewType);
            $location.path('/student/' + enroll_id)
        }

    };
    

    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            //if( $scope.paylink == 'yes'){//06-01-2021-student_payment_invoice_function_change
            if( $scope.paylink == 'pi' || $scope.paylink == 'fi' ){
                $scope.mailSendData.pdflink = $scope.pdflink;
            }
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.mailSendData = {};
                    $scope.data.textInput = '';
                    $scope.mailtemplate = '';
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

    $scope.checkPaymenyShedule = function(studentid,paymentpageid){
        var allowedMenus = getLocalStorageData('allowedMenus');
       
        
        var index = allowedMenus.findIndex(x => x.id == paymentpageid);
        if (index == -1) {
            services.toast('warning','Your not allowed to payment shedule')
        }else{
            services.getEditPaymentShedule(studentid).then((res) =>{
                setLocalStorage('paymentSheduleRedirectData',{
                    'frompath':'studentlist',
                    'studentid':studentid,
                })
                if(res.length == 0){
                    setLocalStorage('userViewType','new');
                   
                    $state.go('home.paymentshedule', {studentid : 0});
                }else{
                    $scope.paymentshedule = res[0];
                   // setLocalStorage('userViewType','edit');
                   setLocalStorage('userViewType','view');
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                }
               
            })

        }

    }


}]);


;/****ACCA Subject list */
admin.controller('accasubjectlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.subjectEntryData = {};
    services.getACCASubjectData().then(function (res) {
        debugger;
        $scope.subjectData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#subjectlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showSubjectModal = function () {
        $('#subjectsentryModal').modal('show')
    }
    $scope.subjectModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "ACCA Subject Entry";
            $scope.buttonText = "Save";
            $scope.subject_id = 0;
            $scope.subjectEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showSubjectModal();
        } else {
            // $scope.title = "Edit ACCA Subject ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit ACCA Subject';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View ACCA Subject';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }

            $scope.buttonText = "Update";
            $scope.subject_id = id;
            services.getEdiACCASubject($scope.subject_id).then((res) => {
                // console.log(res);
                $scope.subjectEntryData = res;
                $scope.data.textInput = $scope.subjectEntryData.subtitle;
                $scope.showSubjectModal();
            })
        }

    }
    $scope.hideSubjectModal = function () {
        $('#subjectsentryModal').modal('hide')
    };

    $scope.saveACCASubject = function (subject_id) {
        $scope.subjectEntryData.subtitle = $scope.data.textInput;
        if (subject_id == 0) {
            services.insertACCASubject($scope.subjectEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideSubjectModal();
                    services.toast('success', res.message);
                    $scope.subjectData.push($scope.subjectEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateACCASubject($scope.subjectEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideSubjectModal();
                    services.toast('success', res.message);
                    var index = $scope.subjectData.findIndex(x => x._id == subject_id);
                    if (index > -1) {
                        $scope.subjectData[index] = $scope.subjectEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    }
}]);;/****syllabus list */
admin.controller('accasyllabuslistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.syllabusEntryData = {};
    services.getSyllabusData().then(function (res) {
        debugger;
        $scope.syllabusData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#syllabuslist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showSyllabusModal = function () {
        $('#syllabusentryModal').modal('show')
    }
    $scope.syllabusModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "ACCA Syllabus Entry";
            $scope.buttonText = "Save";
            $scope.syllabus_id = 0;
            $scope.syllabusEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showSyllabusModal();
        } else {
            $scope.userViewType = viewType;
            // $scope.title = viewType == 'edit' ? "Edit ACCA Syllabus " : "View ACCA Syllabus";
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit ACCA Syllabus';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View ACCA Syllabus';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.syllabus_id = id;
            services.getEdiSyllabus($scope.syllabus_id).then((res) => {
                // console.log(res);
                $scope.syllabusEntryData = res;
                $scope.data.textInput = $scope.syllabusEntryData.subtitle;
                $scope.showSyllabusModal();
            })
        }

    }
    $scope.hideSyllabusModal = function () {
        // $scope.syllabusEntryData = {};
        $('#syllabusentryModal').modal('hide');
    };

    $scope.saveSyllabus = function (syllabus_id) {
        debugger;
        $scope.syllabusEntryData.subtitle = $scope.data.textInput;
        if (syllabus_id == 0) {
            services.insertSyllabus($scope.syllabusEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideSyllabusModal();
                    services.toast('success', res.message);
                    $scope.syllabusData.push($scope.syllabusEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateSyllabus($scope.syllabusEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideSyllabusModal();
                    services.toast('success', res.message);
                    var index = $scope.syllabusData.findIndex(x => x._id == syllabus_id);
                    if (index > -1) {
                        $scope.syllabusData[index] = $scope.syllabusEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    }
}]);;/****syllabus list */
admin.controller('userrolecreationctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce','$state', function ($scope, services, $window, $location, $rootScope, $sce,$state) {
    debugger;

    $scope.userRoleEntryData = {};
    //$scope.selectedMenu = {};//change to selectmodal function
    services.getUserRoleData().then(function (res) {
        debugger;
        $scope.userRoleData = res;

        $scope.updateSubGrid();
    });

    $scope.setMenu = function (menus) {
        for (let i in menus) {

            $scope.selectedMenu[i] = {
                pageid: $scope.menus[i].id,
                menuchecked: false,
                new: false,
                edit: false,
                view: true
            }
            // $scope.selectedMenu[i]["pageid"] = $scope.menus[i].id;
            // $scope.selectedMenu[i]["menuchecked"] = false;
            // $scope.selectedMenu[i]["new"] = false;
            // $scope.selectedMenu[i]["edit"] = false;
            // $scope.selectedMenu[i]["view"] = true;

        }
    }
    services.menuJsonData().then((res) => {
        $scope.menus = res.menu;


    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    };


    $scope.generateMenuTableValues = function () {
        debugger;
        var selectedPages = [];
        var rows = $("#menulisttable tbody tr").each(function (index) {
            var cells = $(this).find("td");
            var obj = {
                "pageid": "",
                "edit": false,
                "new": false,
                "view": false,
            }
            if (cells[0].children[0].checked) {
                var obj = {
                    "pageid": cells[0].children[0].attributes['data-value'].value,
                    "new": cells[1].children[0].checked,//cellindex = 1 is refered to "th new"
                    "edit": cells[2].children[0].checked,//cellindex = 2 is refered to "th edit"
                    "view": cells[3].children[0].checked,//cellindex = 3 is refered to "th view"
                }
                selectedPages.push(obj);
            }
        });
        console.log(selectedPages);
        return selectedPages
       
    };

    $scope.assignTableValues = function (selectedmenus,userViewType) {
        var selectedPages = selectedmenus;

     
            for (var value of selectedPages) {
                var s_pageid = value.pageid;
                var menu_td_id = "menu_td_id_" + s_pageid;
               
                    var rows = $("#menulisttable tbody tr").each(function (index) {
                        var cells = $(this).find("td");
                        var cellId = cells[0].attributes['id'].value;
                        if (cellId == menu_td_id) {
                             $scope.selectedMenu[index]['menuchecked'] = true;
                             $scope.selectedMenu[index]["new"] = value.new;
                             $scope.selectedMenu[index]["edit"] = value.edit;
                             $scope.selectedMenu[index]["view"] = value.view;
                        }
                    });
               
                
    
            }
        
      
    };

    

    $scope.chooseMenu = function (index, id, checked) {

        $scope.selectedMenu[index]["pageid"] = id;
        var findIndex = $scope.mappedMenusIndex.indexOf(index);
        if (findIndex > -1) {
            if (checked) {
                $scope.mappedMenusIndex.push(index)
            } else {
                $scope.mappedMenusIndex.splice(findIndex, 1);
            }
        } else {
            $scope.mappedMenusIndex.push(index)
        }

        console.log($scope.mappedMenusIndex);
    };


    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userrolelist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }


    $scope.showUserRoleEntryModal = function () {
        $('#userEntryModal').modal('show')
    };
    $scope.hideUserRoleEntryModal = function () {
        $('#userEntryModal').modal('hide')
    };


    $scope.userRoleCreatorModal = function (id, viewType) {
        $scope.selectedMenu = {};
        $scope.mappedMenusIndex = [];//userrole mapped index init
        if (id == 0) {
            $rootScope.title = "User Role Creation";
            $scope.buttonText = "Save";
            $scope.userrole_id = 0;
            $scope.userRoleEntryData = {};
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.viewmenuTable = false;
           
            $scope.setMenu($scope.menus);
            $scope.showUserRoleEntryModal();
        } else {
            // $scope.title = "Edit POPUP  ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit') {
                $rootScope.title = 'Edit User Role';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
                $scope.viewmenuTable = false;
            } else {
                $rootScope.title = 'View User Role';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
                $scope.viewmenuTable = true;
            }
            $scope.buttonText = "Update";
            $scope.userrole_id = id;
            $scope.setMenu($scope.menus);
            services.getEditUserRoleData($scope.userrole_id).then((res) => {
                // console.log(res);
                $scope.userRoleEntryData = res;

                $scope.selectedViewMenu = [];
               
                if ($scope.userViewType == 'view') {
                    for (var i in $scope.userRoleEntryData.pages) {
                        var selectedPageid = $scope.userRoleEntryData.pages[i].pageid;
                     
                        var m_index = $scope.menus.findIndex(x => x.id == selectedPageid)
                        if (m_index > -1) {
                         
                            var obj = {
                                menuname : $scope.menus[m_index].menuname,
                                new: $scope.userRoleEntryData.pages[i].new == true ? 'Yes':'No',
                                edit: $scope.userRoleEntryData.pages[i].edit == true ? 'Yes':'No',
                                view: $scope.userRoleEntryData.pages[i].view == true ? 'Yes':'No',
                            }
                            $scope.selectedViewMenu.push(obj);
                           
                        }
                    }
                   
                }
                else if($scope.userViewType == 'edit'){
                   
                    $scope.assignTableValues($scope.userRoleEntryData.pages);
                }
              $scope.showUserRoleEntryModal();
            })
        }

    };

    $scope.getPages = function () {
        var pages = [];

        for (var key of $scope.mappedMenusIndex) {
            pages.push($scope.selectedMenu[key])

        }
        return pages;
    };




    $scope.saveUserRole = function (userrole_id) {
        debugger;
        $scope.userRoleEntryData.pages = $scope.generateMenuTableValues();

        if ($scope.userRoleEntryData.pages.length == 0) {
            alert('Please select any one of menu');
        } else {
           // $scope.userRoleEntryData.pages = $scope.getPages();
            if (userrole_id == 0) {
                services.insertUserRoleData($scope.userRoleEntryData).then(function (res) {
                    debugger;
                    if (res.statuscode == 'NT-200') {
                        // alert(res.message);

                        $scope.hideUserRoleEntryModal();
                        services.toast('success', res.message);
                        $scope.userRoleData.push($scope.userRoleEntryData)
                        // $location.reload();
                        //$state.reload($rootScope.current_path);
                        $rootScope.pageReload();

                    } else {
                        services.toast('warning', res.message)
                        // alert(res.message); 
                    }

                });
            } else {
                services.updateUserRoleData($scope.userRoleEntryData).then(function (res) {
                    debugger;
                    if (res.statuscode == 'NT-200') {
                        // alert(res.message);
                        $scope.hideUserRoleEntryModal();
                        services.toast('success', res.message);
                        // var index = $scope.userRoleData.findIndex(x => x._id == userrole_id);
                        // if (index > -1) {
                        //     $scope.userRoleData[index] = $scope.userRoleEntryData;
                        // }
                        //$state.reload($rootScope.current_path);
                        $rootScope.pageReload();

                    } else {
                        //  alert(res.message); 
                        services.toast('warning', res.message)
                    }

                });
            }
        }

    };


   

   
       

    



}]);;;/****entry and edit user */
admin.controller("userctrl", ['$scope', '$rootScope', '$location', '$stateParams', 'services', 'user', '$http', '$filter', '$state', function ($scope, $rootScope, $location, $stateParams, services, user, $http, $filter, $state) {
    debugger;

    $scope.userViewType = getLocalStorageData('userViewType');

    var userID = ($stateParams.userid) ? ($stateParams.userid) : 0;

    $scope.buttonText = (userID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.return_data = "Select";
    $scope.userID = userID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (userID != 0) ? 'Edit  User' : 'User Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View  User';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    }

    //$scope.userRole = userrole;




    $scope.setCountry = function (countrycode) {
        $scope.userdata.se_address.country = countrycode;
        $scope.userdata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.userdata.se_address.state = state;
        $scope.userdata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
        // alert( $scope.userdata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.userdata.se_address.city = city;
        $scope.userdata.se_city = city;

        //alert(city);

    }

    if (userID != 0) {
        debugger;
        $scope.getuser = user;
        var original = $scope.getuser;
        original._id = userID;
        $scope.userdata = angular.copy(original);
        $scope.userdata._id = userID;

        $scope.userimage = $rootScope.addImgDefUrl($scope.userdata.userimage);
    } else {
        $scope.userdata = {};
    }


    $scope.cancel = function () {
        $location.path('/userlist');
    }
    $scope.isClean = function () {
        return angular.equals(original, $scope.user);
    }
    $scope.user_cancel = function () {
        debugger;
        if (userID != 0) {
            $location.path('/userlist')
        } else {
            $scope.user = {};
            $scope.enrollForm.$setPristine();
            $scope.enrollForm.$setUntouched();
            // $window.location.reload();
        }

    };


    $scope.uploadedImage = function (imagefiles, id) {
        debugger;
        showLoader();
        if (imagefiles) {
            var profileImage = imagefiles;
            $scope.imageFileSize = Math.round((profileImage[0].size / 1024));
            var ImageFileSizeLimit = 5 * 1024;
            if ($scope.imageFileSize < ImageFileSizeLimit) {

                if (id == 'marksheet') {
                    $scope.marksheet = imagefiles
                    $scope.marksheetSrc = $scope.marksheet[0];
                    services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                        debugger;
                        $scope.userdata.se_marksheet = res
                        $scope.marksheet = $rootScope.addImgDefUrl(res)
                        // console.log(res);
                        hideLoader();

                    })
                } else if (id == 'aadharcard') {
                    $scope.aadharcard = imagefiles
                    $scope.aadharcardSrc = $scope.aadharcard[0]
                    services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                        debugger;
                        $scope.userdata.se_aadharimage = res;
                        $scope.aadharcard = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'studentphoto') {
                    $scope.studentphoto = imagefiles
                    $scope.studentphotoImgSrc = $scope.studentphoto[0];
                    services.uploadStudentPhoto(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.userdata.se_studentimage = res;
                        $scope.studentphoto = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'bonofide') {
                    $scope.bonofide = imagefiles;
                    $scope.bonofideSrc = $scope.bonofide[0]
                    services.uploadBonofideLetter(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.userdata.se_bonafide = res;
                        $scope.bonofideImage = $rootScope.addImgDefUrl(res)
                        console.log(res)
                        hideLoader();
                    })
                }
            } else {
                alert('file size is more than 5mb');
                hideLoader();
            }
        }



    }

    // $scope.uploadedFile = function (element, id) {
    //     debugger;
    //     showLoader();
    //     var maxSize = 5 * 1024;
    //     $scope.currentFile = element.files[0];
    //     var fileSize = Math.round((element.files[0].size / 1024));
    //     var reader = new FileReader();
    //     if (fileSize > maxSize) {
    //         alert('file size is more than 5mb');
    //         hideLoader();
    //         return false;
    //     } else {
    //         reader.onload = function (event) {


    //             $scope.$apply(function ($scope) {
    //                 //$scope.files = element.files;
    //                 if (id == 'marksheet') {
    //                     $scope.marksheet = event.target.result
    //                     $scope.marksheetSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_marksheet = res
    //                         console.log(res);
    //                         hideLoader();

    //                     })
    //                 } else if (id == 'aadharcard') {
    //                     $scope.aadharcard = event.target.result
    //                     $scope.aadharcardSrc = element.files[0]
    //                     services.uploadAadhar($scope.aadharcardSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_aadharimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'studentphoto') {
    //                     $scope.studentphoto = event.target.result
    //                     $scope.studentphotoImgSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_studentimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'bonofide') {
    //                     $scope.bonofide = event.target.result
    //                     $scope.bonofideSrc = element.files[0]
    //                     services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_bonafide = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 }
    //             });
    //         }
    //         reader.readAsDataURL(element.files[0]);
    //     }
    // }



    /****show/hide password */


    $scope.saveEnrolluser = function () {
        debugger;

        if ($scope.userID == 0) {

        } else {

            $scope.userdata.se_parentmobileno = {
                dialcode: $scope.parentnum_dialcode,
                mobileno: $scope.parentnum_dialcode + $scope.parentmobileno
            }
            $scope.userdata.se_studentmobileno = {
                dialcode: $scope.studentnum_dialcode,
                mobileno: $scope.studentnum_dialcode + $scope.studentmobileno
            }
            services.StudentEnrollUpdate($scope.userdata).then((res) => {
                var msg = "Update Successfully";
                services.toast('success', msg);
                //  $location.path('/userlist');
                $state.go('home.userslist');
            });

        }


    };

    $scope.showImgModal = function () {
        $('#imgViewModal').modal('show')
    };
    $scope.hideImgModal = function () {
        $('#imgViewModal').modal('hide')
    };





    $scope.viewImageFullScreen = function (id, imgUrl) {
        if (imgUrl) {
            if (id == 'marksheet') {
                $scope.imgModalTitle = '10th Mark Sheet';
            } else if (id == 'aadharcard') {
                $scope.imgModalTitle = 'Aadhar Card'
            } else if (id == 'studentphoto') {
                $scope.imgModalTitle = 'Student Recent Photo'
            } else if (id == 'bonofide') {
                $scope.imgModalTitle = 'School Bonafide Letter'
            }
            $scope.imgUrl = imgUrl;
            $scope.showImgModal();
        }
    }

}]);

/****user list */
admin.controller('userlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state', 'userrole', function ($scope, services, $window, $location, $rootScope, $state, userrole) {
    debugger;

    $scope.userEntryData = {};
    $scope.mailSendData = {};
    $scope.excelheaders = ["Name", "Email", "Mobile No", "Role", "Status"];


    // "username":"kannadhasanthondur",
    // "password":"jkkk",
    // "email":"kanna@gmail.com",
    // "phonenumber":"875484684",
    // "userimage":"",
    // "status":true,
    // "roleid":"12522"

    $scope.userExportData = [];
    $scope.datatrue = false;
    $scope.userRole = userrole;



    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };
    services.getUserData().then(function (res) {
        debugger;
        $scope.userdata = res;

        if ($scope.userdata != null && $scope.userdata.length != 0) {
            $scope.datatrue = true;
            // for (var studentdata of $scope.userdata) {
            //     var tempObj = {
            //         "ACCA Id": studentdata.acca_id,
            //         "EXP Id": studentdata.exp_id,
            //         "Enrollment Number": studentdata.se_studentid,
            //         "Name": studentdata.se_name,
            //         "Aadhar Card": studentdata.se_aadharno,
            //         "Date of Birth": unixTodateTime(parseInt(studentdata.se_dateofbirth)),
            //         "Gender": genderget(studentdata.se_gender),
            //         "Parent Mobile No": studentdata.se_parentmobileno.mobileno,
            //         "Student Mobile No": studentdata.se_studentmobileno.mobileno,
            //         "Parent Email": studentdata.se_parentemail,
            //         "Student Email": studentdata.se_studentemail,
            //         "School": studentdata.se_school,
            //         "School's City": studentdata.se_schoolcity,
            //         "School Pincode": studentdata.se_schoolpincode,
            //         "Studying": studentdata.se_class,
            //         "Year of Pass": studentdata.se_yearofpass,
            //         "Address-1": studentdata.se_address.line1,
            //         "Address-2": studentdata.se_address.line2,
            //         "City": studentdata.se_city,
            //         "Country": studentdata.se_country,
            //         "State": studentdata.se_state,
            //         "Pincode": studentdata.pincode,
            //         "Status": studentdata.se_status == true ? 'Active' : 'Inactive',
            //     }

            //     $scope.studentExportData.push(tempObj);

            // }
        }
        $scope.updateSubGrid();
    });



    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.showUserViewModal = function () {
        $('#userDataViewModal').modal('show')
    };
    $scope.hideUserViewModal = function () {
        $('#userDataViewModal').modal('hide')
    };

    $scope.showPassword = false;

    $scope.toggleShowPassword = function () {
        $scope.showPassword = !$scope.showPassword;
    }

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };

    $scope.userModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "User Creation";
            $scope.buttonText = "Save";
            $scope.user_id = 0;
            $scope.userEntryData = {};
            $scope.data.textInput = '';
            $scope.userViewType = viewType;
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showUserViewModal();
        } else {
            $scope.title = "Edit User  Details";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit User Details';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View User Details';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.user_id = id;
            services.getEditUserData($scope.user_id).then((res) => {
                // console.log(res);
                $scope.userEntryData = res;
                $scope.showUserViewModal();
            })
        }

    };

    $scope.createNewUser = function () {
        if ($scope.userRole.length != 0) {
            // $location.path('/users/0');
            $scope.userModal(0, 'new')
        } else {
            services.toast('danger', 'please create user role ');
            $location.path('/userrolecreation');
        }
    }



    $scope.viewUserData = function (enroll_id, userViewType) {
        if (enroll_id != "" || enroll_id != null || enroll_id != undefined) {

            // var findindex =  $scope.studentExportData.findIndex(x => x.se_studentid == enroll_id);

            // $scope.studentViewData  = $scope.studentExportData[findindex];

            // $scope.showStudentViewModal();
            //$state.go('users/'+enroll_id);
            setLocalStorage('userViewType', userViewType);
            $location.path('/users/' + enroll_id)
        }

    };

    $scope.saveUser = function (user_id) {
        debugger;

        if (user_id == 0) {
            services.insertUserData($scope.userEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideUserViewModal();
                    services.toast('success', res.message);
                    //$scope.faqData.push($scope.faqEntryData);
                    // $location.reload();
                    //$scope.userdata.push($scope.userEntryData)
                    //  $scope.updateSubGrid();
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateUserData($scope.userEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideUserViewModal();
                    services.toast('success', res.message);
                    // var index = $scope.userdata.findIndex(x => x._id == user_id);
                    // if (index > -1) {
                    //     $scope.userdata[index] = $scope.userEntryData;
                    // }
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };



    $scope.composeMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };
    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };


}]);;admin.directive('allowOnlyNumbers', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9]/g, '')
                $input.val(value);
                if (event.which == 64 || event.which == 16) {
                    // to allow numbers  
                    return false;
                } else if (event.which >= 48 && event.which <= 57) {
                    // to allow numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // to allow numpad number  
                    return true;
                } else if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // to allow backspace, enter, escape, arrows  
                    return true;
                } else {
                    event.preventDefault();
                    // to stop others  
                    //alert("Sorry Only Numbers Allowed");  
                    return false;
                }
            });
        }
    }
});


admin.directive('dateformat', function (dateFilter) { 
    return { 
        require: 'ngModel', 
        link: function (scope,  
                       elm, attrs, ctrl) { 

            var dateFormat = 
                attrs['date'] || 'yyyy-MM-dd'; 

            ctrl.$formatters.unshift( 
                    function (modelValue) { 
                return dateFilter( 
                    modelValue, dateFormat); 
            }); 
        } 
    }; 
}) 


admin.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


admin.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: {
            ngModel: "=",
            minDate: "=",
            maxDate: "="
        },
        link: function (scope, elem, attrs, ngModelCtrl) {


            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: "dd/mm/yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:Y",                    
                onSelect: function (dateText) {
                    updateModel(dateText);
                    this.focus();
                },
                onClose: function (ele) {
                    this.blur();
                },

                defaultDate: new Date() 

            };

            scope.$watch('maxDate', function (newValue, oldValue) {
                $(elem).datepicker('option', 'maxDate', newValue);
            })

            scope.$watch('minDate', function (newValue, oldValue) {
                $(elem).datepicker('option', 'minDate', newValue);
                $(elem).datepicker('option', 'defaultDate', newValue);
            })

            elem.datepicker(options);
        }
    }
});


var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

admin.directive("compareTo", compareTo);


admin.filter('countryname',['$rootScope' ,function($rootScope) {
    return function(input) {
     var index =  $rootScope.commonJsonData.countrylist.findIndex(x => x.code == input);
     if(index > 0){
        return $rootScope.commonJsonData.countrylist[index].name;
     }else{
        return  '';
     }
     
    };
  }]);


  admin.directive('beautifyText', function() {
    return {
      link: function(scope, iElement, iAttrs, ngModelController) {
        // Adds the prefix and suffix
        (function() {
          var prefix = iAttrs.beautifyTextWithPrefix;
          var suffix = iAttrs.beautifyTextWithSuffix;

          ngModelController.$parsers.push(function(value) {
            if (angular.isString(value)) {
              return value.replace(new RegExp('^' + prefix), '').replace(new RegExp(suffix + '$'), '');
            }

            return '';
          });

          scope.$watch(function() {
            return ngModelController.$viewValue;
          }, function(newValue) {
            if (angular.isString(newValue) && newValue.length > 0) {
              if (angular.isString(ngModelController.$modelValue) && ngModelController.$modelValue.length === 0 && newValue.length === 1) {
                ngModelController.$viewValue = '';
                ngModelController.$render();
                return;
              }

              if (!isBeautifiedWithPrefix(newValue)) {
                ngModelController.$viewValue = prefix + newValue;
              }

              if (!isBeautifiedWithSuffix(newValue)) {
                ngModelController.$viewValue = newValue + suffix;
              }

              ngModelController.$render();
            } else {
              ngModelController.$viewValue = '';
              ngModelController.$render();
            }
          });

          function isBeautifiedWithPrefix(value) {
            return value.match(new RegExp('^' + prefix)) !== null;
          }

          function isBeautifiedWithSuffix(value) {
            return value.match(new RegExp(suffix + '$')) !== null;
          }
        })();

        // Changes the caret position
        (function() {
          var element = iElement[0];

       
          function getCursorPos() {
            if ("selectionStart" in element && document.activeElement == element) {
              return {
                start: element.selectionStart,
                end: element.selectionEnd
              };
            } else if (element.createTextRange) {
              var sel = document.selection.createRange();
              if (sel.parentElement() === element) {
                var rng = element.createTextRange();
                rng.moveToBookmark(sel.getBookmark());
                for (var len = 0; rng.compareEndPoints("EndToStart", rng) > 0; rng.moveEnd("character", -1)) {
                  len++;
                }
                rng.setEndPoint("StartToStart", element.createTextRange());
                for (var pos = {
                  start: 0,
                  end: len
                }; rng.compareEndPoints("EndToStart", rng) > 0; rng.moveEnd("character", -1)) {
                  pos.start++;
                  pos.end++;
                }
                return pos;
              }
            }
            return -1;
          }
          function setCursorPos(start, end) {
            if (arguments.length < 2) {
              end = start;
            }

            if ("selectionStart" in element) {
              element.selectionStart = start;
              element.selectionEnd = end;
            } else if (element.createTextRange) {
              var rng = element.createTextRange();
              rng.moveStart("character", start);
              rng.collapse();
              rng.moveEnd("character", end - start);
              rng.select();
            }
          }

          iElement.bind('mousedown mouseup keydown keyup', function() {
            if (ngModelController.$viewValue.length > 0) {
              var caretPosition = getCursorPos();

              if (caretPosition.start === 0) {
                setCursorPos(1, caretPosition.end < 1 ? 1 : caretPosition.end);
              }

              if (caretPosition.end === ngModelController.$viewValue.length) {
                setCursorPos(caretPosition.start, ngModelController.$viewValue.length - 1);
              }
            }
          });
        })();
      },
      restrict: 'A',
      require: 'ngModel'
    }
  });

  admin.directive('customPrefix',function(){
      debugger;
      return {
          restrict:'A',
          require: 'ngModel',
          link: function(scope,elm,attr,ctrl){
              debugger;
            var prefix = attr.customPrefix;
              elm.on('keydown',function(event){
                  debugger;
                  var $input = $(this);
                  var value = $input.val();
                 // var oldvalue = $input.val();
                  if(value.indexOf(prefix) !== 0 ){
                    $input.val(prefix + this.value);
                  }
                
              })
          }
      }
  })



  admin.directive('aadharCheck', function() {
    var aadharcardRegex = /^\d{4}-\d{4}-\d{4}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attr, ngModelCtrl) {
      elm.on('keyup', function (event) {
      debugger;
              var $input = $(this);
              var value = $input.val();
              value =
                   value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s =>s.length > 0).join("-");
              $input.val(value);
              
               if(value.length == 14){
                event.preventDefault();
                return false;
               }
               
      });
        scope.$watch(attr.ngModel, function (value) {
              debugger;
            
              console.log('value changed, new value is: ' + value);
          });
        // ngModelCtrl.$parsers.unshift(function(input) {
        //   var valid = aadharcardRegex.test(input);
        //   ngModelCtrl.$setValidity('aadharcard', valid);
        //   return input;
        // }); 
        ngModelCtrl.$validators.aadharcard = function(modelValue, viewValue) {
            var valid = aadharcardRegex.test(modelValue);

      return valid;
    };
      }
    };
  });


  admin.filter('amountformat',['$rootScope' ,function($rootScope) {
    return function(input) {
        return amountFormat(input)
     
    };
  }]);;angular.module('microconnectServices', [
    'ngFileUpload'
]).factory('services', services);
services.$inject = ['$http', 'Upload'];

function services($http, Upload) {

    //const apiUrl = 'http://104.218.53.125/mcconnect/api/v1.0/'
    var baseurl = __env.apiUrl;
    var services = {
        toast: toast,
        commonJsonData: commonJsonData,
        userJsonData: userJsonData,
        userLogin: userLogin,
        menuJsonData: menuJsonData,
        getAllEnrollUsers: getAllEnrollUsers,
        getEditEnrollUser: getEditEnrollUser,
        // FaqJsonData: FaqJsonData,
        //  ExamTabJsonData: ExamTabJsonData,
        //  SyllabusTabJsonData: SyllabusTabJsonData,
        uploadMarkSheet: uploadMarkSheet,
        uploadAadhar: uploadAadhar,
        uploadStudentPhoto: uploadStudentPhoto,
        uploadBonofideLetter: uploadBonofideLetter,
        StudentEnroll: StudentEnroll,
        StudentEnrollUpdate: StudentEnrollUpdate,
        sendPaymentLink: sendPaymentLink,
        discardPaymentLink:discardPaymentLink,
        getInTouch: getInTouch,
        demoRegData: demoRegData,
        getSyllabusData: getSyllabusData,
        insertSyllabus: insertSyllabus,
        getEdiSyllabus: getEdiSyllabus,
        updateSyllabus: updateSyllabus,
        getACCASubjectData: getACCASubjectData,
        insertACCASubject: insertACCASubject,
        getEdiACCASubject: getEdiACCASubject,
        updateACCASubject: updateACCASubject,
        getFaqData: getFaqData,
        insertFaq: insertFaq,
        getEditFaq: getEditFaq,
        updateFaq: updateFaq,
        updateFaqOrder:updateFaqOrder,
        getRequestDemoUserData: getRequestDemoUserData,
        getGetinTouchUserData: getGetinTouchUserData,
        getPopUpData: getPopUpData,
        insertSavePopupData: insertSavePopupData,
        getEdiPopUpData: getEdiPopUpData,
        updatePopUpData: updatePopUpData,
        deletePopup:deletePopup,//04-01-2021-deletepopup-feature-add
        getUserRoleData: getUserRoleData,
        insertUserRoleData: insertUserRoleData,
        getEditUserRoleData: getEditUserRoleData,
        updateUserRoleData: updateUserRoleData,
        mailSend: mailSend,
        getUserData: getUserData,
        getEditUserData: getEditUserData,
        insertUserData: insertUserData,
        updateUserData: updateUserData,
        getAllPaymentShedule: getAllPaymentShedule,
        getEditPaymentShedule: getEditPaymentShedule,
        insertPaymentShedule: insertPaymentShedule,
        updatePaymentShedule: updatePaymentShedule,
        getAllNonSheduleStudents:getAllNonSheduleStudents,
        viewPaymentSchedule:viewPaymentSchedule,
        getPaymentDetails:getPaymentDetails,
        getPayoutData: getPayoutData,
        getEditPayoutData: getEditPayoutData,
        insertPayoutData: insertPayoutData,
        updatePayoutData: updatePayoutData,
        getAllNonPayoutstudents:getAllNonPayoutstudents,
        changePassword:changePassword,
        proformaPdfCreate:proformaPdfCreate,
        getStudentInvoice:getStudentInvoice,
        checkGst:checkGst
        
    }
    return services;

    function toast(alertType, msg) {
        //debugger;
        //toaster.pop(alertType, msg, "", 4000, 'trustedHtml');
        if (alertType == 'success') {
            var color = 'green'

        } else if (alertType == 'warning') {
            var color = 'red'
        } else if (alertType == 'danger') {
            var color = 'red'
        } else if (alertType == 'error') {
            var color = 'red'
        }
        var title = '';
        var position = 'topRight'
        customToast(alertType, color, title, msg, position)
    }


    function commonJsonData() {
        return $http.get('./assets/data/common.json').then(function (res) {
            return res.data;
        });
    }

    function userJsonData() {
        return $http.get('../assets/data/user.json').then(function (res) {
            return res.data;
        });
    }

    function userLogin(loginData) {
        return $http.post(baseurl + 'userlogin', loginData).then(function (res) {
            return res.data;
        });
    }

    function menuJsonData() {
        return $http.get('./assets/data/menu.json').then(function (res) {
            return res.data;
        });
    }



    function getAllEnrollUsers() {
        return $http.post(baseurl + 'getuser', {}).then(function (res) {
            return res.data;
        });
    }

    function getEditEnrollUser(id) {
        return $http.post(baseurl + 'edituser', {
            _id: id
        }).then(function (res) {
            return res.data;
        });
    }

    function sendPaymentLink(studentid,invoicetype) {
        return $http.post(baseurl + 'paymentemail', {
            se_studentid: studentid,invoicetype : invoicetype
        }).then(function (res) {
            return res.data;
        });
    }

    function discardPaymentLink(discardData) {
        return $http.post(baseurl + 'ordercheck', 
            discardData
        ).then(function (res) {
            return res.data;
        });
    }
    

    function SlideJsonData() {
        return $http.get('./common/data/slide.json').then(function (res) {
            return res.data;
        });
        // return $http.get(baseurl + 'slides').then(function (res) {
        //     return res.data;
        // });
    }

    // function FaqJsonData() {
    //     return $http.get('./common/data/faq.json').then(function (res) {
    //         return res.data;
    //     });
    // }

    // function ExamTabJsonData() {
    //     return $http.get('./common/data/examtab.json').then(function (res) {
    //         return res.data;
    //     });
    // }

    // function SyllabusTabJsonData() {
    //     return $http.get('./common/data/syllabustab.json').then(function (res) {
    //         return res.data;
    //     });
    // }
    /////****** ACCA Syllabus ******* */
    function getSyllabusData() {
        return $http.post(baseurl + 'getexams', {}).then(function (res) {
            return res.data;
        });
    }

    function insertSyllabus(syllabusdata) {
        return $http.post(baseurl + 'saveexams', syllabusdata).then(function (res) {
            return res.data;
        });
    }

    function getEdiSyllabus(_id) {
        return $http.post(baseurl + 'editexams', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateSyllabus(syllabusdata) {
        return $http.post(baseurl + 'updateexams', syllabusdata).then(function (res) {
            return res.data;
        });
    }

    /******** ACCA Subjects********* */
    function getACCASubjectData() {
        return $http.post(baseurl + 'getsyllabus', {}).then(function (res) {
            return res.data;
        });
    }

    function insertACCASubject(subjectdata) {
        return $http.post(baseurl + 'savesyllabus', subjectdata).then(function (res) {
            return res.data;
        });
    }

    function getEdiACCASubject(_id) {
        return $http.post(baseurl + 'editsyllabus', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateACCASubject(subjectdata) {
        return $http.post(baseurl + 'updatesyllabus', subjectdata).then(function (res) {
            return res.data;
        });
    }

    /********FAQ********* */
    function getFaqData() {
        return $http.post(baseurl + 'getfaqdata', {}).then(function (res) {
            return res.data;
        });
    }

    function insertFaq(faqdata) {
        return $http.post(baseurl + 'faqdata', faqdata).then(function (res) {
            return res.data;
        });
    }

    function getEditFaq(_id) {
        return $http.post(baseurl + 'editfaq', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateFaq(faqdata) {
        return $http.post(baseurl + 'updatefaq', faqdata).then(function (res) {
            return res.data;
        });
    }
    
    function updateFaqOrder(faqdata) {
        return $http.post(baseurl + 'updatefaqorder', faqdata).then(function (res) {
            return res.data;
        });
    }
    /*******Request demo user */
    function getRequestDemoUserData() {
        return $http.post(baseurl + 'demousers', {}).then(function (res) {
            return res.data;
        });
    }

    /*******Get in touch  user */
    function getGetinTouchUserData() {
        return $http.post(baseurl + 'contactlist', {}).then(function (res) {
            return res.data;
        });
    }


    function uploadMarkSheet(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'marksheet',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadAadhar(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'aadharsave',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadStudentPhoto(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'profileimage',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadBonofideLetter(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'studentbonafide',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function StudentEnroll(studentEnrollData) {
        return $http.post(baseurl + 'studentenroll', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function StudentEnrollUpdate(studentEnrollData) {
        return $http.post(baseurl + 'userupdate', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function getInTouch(getInTouchData) {
        return $http.post(baseurl + 'contactemail', getInTouchData).then(function (res) {
            return res.data;
        });
    }

    function demoRegData(demoRegData) {
        return $http.post(baseurl + 'contactemail', demoRegData).then(function (res) {
            return res.data;
        });
    }

    ////******POPUP Data */
    function getPopUpData() {
        return $http.post(baseurl + 'getpobup', {}).then(function (res) {
            return res.data;
        });
    }

    function insertSavePopupData(data) {
        return $http.post(baseurl + 'popubsave', data).then(function (res) {
            return res.data;
        });
    }

    function getEdiPopUpData(_id) {
        return $http.post(baseurl + 'editpobup', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePopUpData(data) {
        return $http.post(baseurl + 'updatepobup', data).then(function (res) {
            return res.data;
        });
    }
  //04-01-2021-deletepopup-feature-add
    function deletePopup(id) {
        return $http.post(baseurl + 'deletepopbup', {"_id":id}).then(function (res) {
            return res.data;
        });
    }
   //04-01-2021-deletepopup-feature-add

    /*****user role creation */


    function getUserRoleData() {
        return $http.post(baseurl + 'getrole', {}).then(function (res) {
            return res.data;
        });
    }

    function insertUserRoleData(data) {
        return $http.post(baseurl + 'rolecreate', data).then(function (res) {
            return res.data;
        });
    }

    function getEditUserRoleData(_id) {
        return $http.post(baseurl + 'editrole', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateUserRoleData(data) {
        return $http.post(baseurl + 'updaterole', data).then(function (res) {
            return res.data;
        });
    }

    /****mail send  */
    function mailSend(data) {
        return $http.post(baseurl + 'emaildynamic', data).then(function (res) {
            return res.data;
        });
    }
    /*****user creation */


    function getUserData() {
        return $http.post(baseurl + 'getadminusers', {}).then(function (res) {
            return res.data;
        });
    }

    function insertUserData(data) {
        return $http.post(baseurl + 'userregister', data).then(function (res) {
            return res.data;
        });
    }

    function getEditUserData(_id) {
        return $http.post(baseurl + 'editadminuser', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateUserData(data) {
        return $http.post(baseurl + 'updateadminuser', data).then(function (res) {
            return res.data;
        });
    }
    
    /****** Payment shedule ***** */
    
    function getAllPaymentShedule() {
        return $http.post(baseurl + 'shceduleusers', {}).then(function (res) {
            return res.data;
        });
    }

    function getAllNonSheduleStudents() {
        return $http.post(baseurl + 'nonschedulesstudents', {}).then(function (res) {
            return res.data;
        });
    }
    
    function insertPaymentShedule(data) {
        return $http.post(baseurl + 'paymentschedule', data).then(function (res) {
            return res.data;
        });
    }

    function getEditPaymentShedule(studentid) {
        return $http.post(baseurl + 'schedulecheck', {
            "se_studentid": studentid
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePaymentShedule(data) {
        return $http.post(baseurl + 'updateadminuser', data).then(function (res) {
            return res.data;
        });
    }
    function viewPaymentSchedule(studentid) {
        return $http.post(baseurl + 'paymentdetails',  {
            "se_studentid": studentid
        }).then(function (res) {
            return res.data;
        });
    }
    
    function  getPaymentDetails(){
        return $http.post(baseurl + 'paymentusers',  {}).then(function (res) {
            return res.data;
        });
    }
    
    /****Payout Details  */
    function getPayoutData() {
        return $http.post(baseurl + 'payoutdetails', {}).then(function (res) {
            return res.data;
        });
    }

    function getAllNonPayoutstudents() {
        return $http.post(baseurl + 'nonpayout', {}).then(function (res) {
            return res.data;
        });
    }
    
    function insertPayoutData(data) {
        return $http.post(baseurl + 'payoutsave', data).then(function (res) {
            return res.data;
        });
    }

    function getEditPayoutData(studentid) {
        return $http.post(baseurl + 'payoutdetails', {
            "_id": studentid
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePayoutData(data) {
        return $http.post(baseurl + 'payoutsave', data).then(function (res) {
            return res.data;
        });
    }
    function changePassword(data) {
        return $http.post(baseurl + 'changepassword', data).then(function (res) {
            return res.data;
        });
    }
    
    function proformaPdfCreate(data) {
        return $http.post(baseurl + 'pdfcreate', data).then(function (res) {
            return res.data;
        });
    }


    function getStudentInvoice(studentid){
        return $http.post(baseurl + 'invoice', {'se_studentid':studentid}).then(function (res) {
            return res.data;
        });
    }

    function checkGst(studentid){
        return $http.post(baseurl + 'gstcheck', {'se_studentid':studentid}).then(function (res) {
            return res.data;
        });
        
    }

    
};function setLocalStorage(key, value) {
    debugger;
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorageData(key) {
    debugger;
    var data = JSON.parse(localStorage.getItem(key));
    return data;
}


function setSessionStorage(key, value) {
    debugger;
    sessionStorage.setItem(key, value);
}

function getSessionStorageData(key) {
    debugger;
    var data = sessionStorage.getItem(key);
    return data;
}

function clearLocalStorage(key) {
    localStorage.removeItem(key);
}
function clearSessionStorage(key) {
    sessionStorage.removeItem(key);
}


function timeToUnix(dateTime) {
    var UnixtimeStamp = new Date(dateTime).getTime();
    return UnixtimeStamp
}

function unixToTime(unix) {
    debugger;
    if (unix) {
        var time = new Date(unix);
    }
    // else{
    //     var time = new Date(); 
    // }
    return time;
}

function unixTodateTime(Unix) {
    debugger;
    if (Unix) {
        var unixtime = parseInt(Unix)
        var userEnteredDate = new Date(unixtime);

        var date = new Date(userEnteredDate);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var dt = date.getDate();

        if (dt < 10) {
            dt = '0' + dt;
        }
        if (month < 10) {
            month = '0' + month;
        }
        var dateString = dt + '-' + month + '-' + year;
    } else {
        var dateString = 'dd' + '-' + 'mm' + '-' + 'yyyy';
    }
    return dateString;
}
function unixToHtmlDate(Unix){
    if (Unix) {
        var unixtime = parseInt(Unix)
        var userEnteredDate = new Date(unixtime);
       
    }else{
        var userEnteredDate = '';
    }
    return userEnteredDate;
}

function hideLoader() {
    $("#showLoading").hide()

}

function encrypt(value){
    if(value){
        var enc =  window.btoa(value);
    }else{
        var enc = '';
    }
    return enc;
}
function decrypt(value){
    if(value){
        var dec =  window.atob(value);
    }else{
        var dec = '';
    }
    return dec;
}

function showLoader(e) {
    e || (e = "Loading....!");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
}




function customToast(id, color, title, msg, position) {
    iziToast.show({
        id: id,
        backgroundColor: '',
        color: color, // blue, red, green, yellow
        title: title,
        message: msg,
        position: position,
        maxWidth: 400,
        // close: false,
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX'
    });
}


function mainHideLoader() {
    $("#showLoading").hide()
}

function mainShowLoader(e) {
    e || (e = "Loading. Please Wait.");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:#fff;z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="d-flex justify-content-center"><div class="spinner-border" role="status"> <span class="sr-only">Loading...</span></div></div><h4 class="text-center space font-weight-normal">Loading...</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
}


function LetterAvatar(name, size, wordcount) {

    name = name || '';
    size = size || 60;
    wordcount = wordcount || 1;

    var colours = [
            "#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50",
            "#f1c40f", "#e67e22", "#e74c3c", "#ecf0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"
        ],

        nameSplit = String(name).toUpperCase().split(' '),
        initials, charIndex, colourIndex, canvas, context, dataURI;


    // if (nameSplit.length == 1) {
    //     initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
    // } else {
    //     initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    // }
    if (wordcount == 1) {
        initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
    } else {
        initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    }

    if (window.devicePixelRatio) {
        size = (size * window.devicePixelRatio);
    }

    charIndex = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
    colourIndex = charIndex % 10;
    canvas = document.createElement('canvas');
    canvas.width = size;
    canvas.height = size;
    context = canvas.getContext("2d");

    context.fillStyle = colours[colourIndex - 1];
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.font = Math.round(canvas.width / 2) + "px Arial";
    context.textAlign = "center";
    context.fillStyle = "#FFF";
    context.fillText(initials, size / 2, size / 1.5);

    dataURI = canvas.toDataURL();
    canvas = null;

    return dataURI;
}


/*****begin-10-01-2021-Aswin-new proforma email */

/******amount in words convert******/
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
};

function numberSuffixAdd(number){
    
    if(number){
        var num = parseInt(number);
        switch (num) {
            case 1:
                var suffix = 'st'
                break;
            case 2:
                var suffix = 'nd'
                break;
            case 3:
                var suffix = 'rd'
                break;
            default:
                var suffix = 'th'
                break;
        }
      return   suffix;
    }

}
function momentUnixConvert(unix) {
     if(unix){
        let epoch = parseInt(unix);
        let result = moment(epoch).format('DD-MM-YYYY');
      // let result2 =  moment(epoch).utcOffset("+05:30").format('DD-MM-YYYY')
        //console.log(result2);
        return result;
     }else{
        return '';
     }
    
  }

/*****end-10-01-2021-Aswin-new proforma email */

/*****begin-19-01-2021-Aswin-amount format change */

function amountFormat(amount) {
    if(amount){
        var amountValue = parseInt(amount)
        const formatter = new Intl.NumberFormat('en-IN', {
     
            currency: 'INR',
            minimumFractionDigits: 2
          })
          
          
          var value = formatter.format(amountValue) // "10.00"
          
         return value;
    }
    
    }
/*****end-19-01-2021-Aswin-amount format change */
