<?php
 $rel = '/uploadimages';
 $dir = public_path() . '/assets/img/wysiwyg';
 $iterator = $this->finder->in($dir)->name('*.png')->name('*.jpg');
 $files = [];
 $count = 0;
 foreach($iterator as $file) {
     $files[$count]['thumb'] = $rel . '/' . $file->getFilename();
     $files[$count]['image'] = $rel . '/' . $file->getFilename();
     $files[$count]['title'] = $file->getFilename();
     $count ++;
 }
 return Response::json($files);