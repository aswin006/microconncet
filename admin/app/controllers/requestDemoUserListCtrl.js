/****request demo list */
admin.controller('requestdemouserlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.mailSendData = {};
    services.getRequestDemoUserData().then(function (res) {
        debugger;
        $scope.requestDemoUserData = res;
        $scope.updateSubGrid();
    });

    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#requestdemouserlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 7, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    }

    $scope.sendMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };
    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

}]);



/****get in touch list */
admin.controller('getintouchuserlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.mailSendData = {};

    services.getGetinTouchUserData().then(function (res) {
        debugger;
        $scope.getintouchUserData = res;
        $scope.updateSubGrid();
    });

    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#getintouchuserlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 7, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    };

    $scope.sendMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };

    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

}]);