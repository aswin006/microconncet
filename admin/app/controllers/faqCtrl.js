/****syllabus list */
admin.controller('faqlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.faqEntryData = {};
    services.getFaqData().then(function (res) {
        debugger;
        $scope.faqData = res;
        for(var i =0 ; i < $scope.faqData.length; i++){
            $scope.faqData[i]['order'] =    i + 1; 
        }
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#faqlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showFaqModal = function () {
        $('#faqentryModal').modal('show')
    };
    $scope.hideFaqModal = function () {
        $('#faqentryModal').modal('hide')
    };


    $scope.faqOrderModal = function(){
        $scope.list = angular.copy($scope.faqData);
        $('#faqOrderModal').modal('show')
    }
    $scope.hidefaqOrderModal = function(){
        $('#faqOrderModal').modal('hide')
    }

    $scope.faqModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "FAQ Entry";
            $scope.buttonText = "Save";
            $scope.faq_id = 0;
            $scope.faqEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showFaqModal();
        } else {
            $scope.title = "Edit FAQ  ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit FAQ';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View FAQ';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.faq_id = id;
            services.getEditFaq($scope.faq_id).then((res) => {
                // console.log(res);
                $scope.faqEntryData = res;
                $scope.data.textInput = $scope.faqEntryData.subtitle;
                $scope.showFaqModal();
            })
        }

    }


    $scope.saveFaq = function (faq_id) {
        debugger;
        $scope.faqEntryData.subtitle = $scope.data.textInput;
        if (faq_id == 0) {
            services.insertFaq($scope.faqEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideFaqModal();
                    services.toast('success', res.message);
                    $scope.faqData.push($scope.faqEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateFaq($scope.faqEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideFaqModal();
                    services.toast('success', res.message);
                    // var index = $scope.faqData.findIndex(x => x._id == faq_id);
                    // if (index > -1) {
                    //     $scope.faqData[index] = $scope.faqEntryData;
                    // }
                    $rootScope.pageReload();
                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };

    $scope.savefaqOrderModal = function(){
        var reorderedFaqList = [];
        for(var i =0 ; i < $scope.list.length ; i++){
            $scope.list[i]['reorder'] = i + 1;
        }
        console.log( $scope.list)
        services.updateFaqOrder($scope.list).then(function (res) {
            debugger;
            if (res.statuscode == 'NT-200') {
                $scope.hidefaqOrderModal();
                services.toast('success', res.message);
                $rootScope.pageReload();
            } else {
              
                services.toast('warning', res.message)
            }

        });
    }
}]);