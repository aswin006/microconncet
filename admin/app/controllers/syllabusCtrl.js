/****syllabus list */
admin.controller('accasyllabuslistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.syllabusEntryData = {};
    services.getSyllabusData().then(function (res) {
        debugger;
        $scope.syllabusData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#syllabuslist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showSyllabusModal = function () {
        $('#syllabusentryModal').modal('show')
    }
    $scope.syllabusModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "ACCA Syllabus Entry";
            $scope.buttonText = "Save";
            $scope.syllabus_id = 0;
            $scope.syllabusEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showSyllabusModal();
        } else {
            $scope.userViewType = viewType;
            // $scope.title = viewType == 'edit' ? "Edit ACCA Syllabus " : "View ACCA Syllabus";
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit ACCA Syllabus';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View ACCA Syllabus';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.syllabus_id = id;
            services.getEdiSyllabus($scope.syllabus_id).then((res) => {
                // console.log(res);
                $scope.syllabusEntryData = res;
                $scope.data.textInput = $scope.syllabusEntryData.subtitle;
                $scope.showSyllabusModal();
            })
        }

    }
    $scope.hideSyllabusModal = function () {
        // $scope.syllabusEntryData = {};
        $('#syllabusentryModal').modal('hide');
    };

    $scope.saveSyllabus = function (syllabus_id) {
        debugger;
        $scope.syllabusEntryData.subtitle = $scope.data.textInput;
        if (syllabus_id == 0) {
            services.insertSyllabus($scope.syllabusEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideSyllabusModal();
                    services.toast('success', res.message);
                    $scope.syllabusData.push($scope.syllabusEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateSyllabus($scope.syllabusEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideSyllabusModal();
                    services.toast('success', res.message);
                    var index = $scope.syllabusData.findIndex(x => x._id == syllabus_id);
                    if (index > -1) {
                        $scope.syllabusData[index] = $scope.syllabusEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    }
}]);