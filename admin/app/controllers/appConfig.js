
var admin = angular.module('microconnectAdmin', ['microconnectServices',
    'ui.router', 'ngMessages', 'ngFileUpload','ckeditor','ngCsv', 'ngTagsInput','psi.sortable'])

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
admin.constant('__env', env);
setLocalStorage('cacheVersion', env.cacheVersion)


var retrievedObject = getLocalStorageData("adminUserdata");

window.onpopstate = function (e) {
    window.history.forward(1);
}
admin.run(['$location', '$rootScope', '$window', '$http', '$stateParams', '$anchorScroll', 'services','$state', function ($location, $rootScope, $window, $http, $stateParams, $anchorScroll, services,$state) {

    $rootScope.logout = function () {
        //debugger;
        sessionStorage.clear();
        localStorage.clear();
        var newURL = __env.loginpage
        window.location.href = newURL;
    };
    // $rootScope.$on('$viewContentLoaded', function() {
    //     $templateCache.removeAll();
    //  });
    $rootScope.$on('$stateChangeSuccess', function (event, current, previous, from, error) {
        debugger;
        //window.scrollTo(0, 0);
        $rootScope.pagetitle = current.title;
        $rootScope.previous_path = from.name;
        $rootScope.current_path = current.name;
        $rootScope.controllerName = current.controller;
        $rootScope.passedStateParams = $stateParams;

       



        if (sessionStorage.getItem('token') == null) {
            $rootScope.logout();
        }else{
              /***begin-for developing testing purpose  */
        //  if (retrievedObject.email === __env.defaultEmail) {
            if (retrievedObject.email === 'developer@microconnect.co.in') {
                 /***end-for developing testing purpose  */
                $rootScope.enableNewBtn = true;
                $rootScope.enableEditBtn = true;
                $rootScope.enableViewBtn = true;
            } else {
                var enableviews = getLocalStorageData('enableViews');
                $rootScope.enableNewBtn = enableviews.new;
                $rootScope.enableEditBtn = enableviews.edit;
                $rootScope.enableViewBtn = enableviews.view;
            }
        }

    });

    $rootScope.addImgDefUrl = function (img) {
        debugger;
        if (img != null && img != "") {
            var first_part = img.substring(0, 8);

            if (first_part === 'http://' || first_part === 'https://') {
                var image = img
            } else {
                var image = __env.commonImgUrl + img
            }
        } else {
            var image = img
        }

        return image
    };
    services.commonJsonData().then((res) => {

        $rootScope.commonJsonData = res;
    });
    if (localStorage.getItem('adminUserdata') != null) {
        $rootScope.userDetails = getLocalStorageData('adminUserdata');
        if ($rootScope.userDetails.userimage == '') {
            $rootScope.userImage = LetterAvatar($rootScope.userDetails.username, 60, 1);
        } else {
            $rootScope.userImage = $rootScope.addImgDefUrl($rootScope.userDetails.userimage)
        }
    }

    $rootScope.pageReload = function(){
       // $state.reload();
        $window.location.reload()
    }


}]);

admin.factory('httpInterceptorSerivce', ['$rootScope', function httpInterceptorSerivce($rootScope) {
    return {

        request: function (config) {
            //debugger;
            //$rootScope.isLoader = 1;
            mainShowLoader()
            return config;
        },

        requestError: function (config) {
            //debugger;
            //$rootScope.isLoader = 0;
            mainHideLoader()
            return config;
        },

        response: function (res) {
            //$rootScope.isLoader = 0;
            // setTimeout(function() {
            mainHideLoader()
            // }, 2000);

            return res;
        },

        responseError: function (res) {
            //debugger;

            return res;
        }
    }
}]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptorSerivce');
}]);
 admin.factory('preventTemplateCache', function($injector) {
   // var ENV = $injector.get('ENV');
return {
      'request': function(config) {
          var date = new Date().getTime();
        if (config.url.indexOf('views') !== -1) {
          config.url = config.url + '?t=' + date;
        }
        return config;
      }
    }
  })
.config(function($httpProvider) {
    $httpProvider.interceptors.push('preventTemplateCache');
  });