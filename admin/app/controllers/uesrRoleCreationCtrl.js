/****syllabus list */
admin.controller('userrolecreationctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce','$state', function ($scope, services, $window, $location, $rootScope, $sce,$state) {
    debugger;

    $scope.userRoleEntryData = {};
    //$scope.selectedMenu = {};//change to selectmodal function
    services.getUserRoleData().then(function (res) {
        debugger;
        $scope.userRoleData = res;

        $scope.updateSubGrid();
    });

    $scope.setMenu = function (menus) {
        for (let i in menus) {

            $scope.selectedMenu[i] = {
                pageid: $scope.menus[i].id,
                menuchecked: false,
                new: false,
                edit: false,
                view: true
            }
            // $scope.selectedMenu[i]["pageid"] = $scope.menus[i].id;
            // $scope.selectedMenu[i]["menuchecked"] = false;
            // $scope.selectedMenu[i]["new"] = false;
            // $scope.selectedMenu[i]["edit"] = false;
            // $scope.selectedMenu[i]["view"] = true;

        }
    }
    services.menuJsonData().then((res) => {
        $scope.menus = res.menu;


    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    };


    $scope.generateMenuTableValues = function () {
        debugger;
        var selectedPages = [];
        var rows = $("#menulisttable tbody tr").each(function (index) {
            var cells = $(this).find("td");
            var obj = {
                "pageid": "",
                "edit": false,
                "new": false,
                "view": false,
            }
            if (cells[0].children[0].checked) {
                var obj = {
                    "pageid": cells[0].children[0].attributes['data-value'].value,
                    "new": cells[1].children[0].checked,//cellindex = 1 is refered to "th new"
                    "edit": cells[2].children[0].checked,//cellindex = 2 is refered to "th edit"
                    "view": cells[3].children[0].checked,//cellindex = 3 is refered to "th view"
                }
                selectedPages.push(obj);
            }
        });
        console.log(selectedPages);
        return selectedPages
       
    };

    $scope.assignTableValues = function (selectedmenus,userViewType) {
        var selectedPages = selectedmenus;

     
            for (var value of selectedPages) {
                var s_pageid = value.pageid;
                var menu_td_id = "menu_td_id_" + s_pageid;
               
                    var rows = $("#menulisttable tbody tr").each(function (index) {
                        var cells = $(this).find("td");
                        var cellId = cells[0].attributes['id'].value;
                        if (cellId == menu_td_id) {
                             $scope.selectedMenu[index]['menuchecked'] = true;
                             $scope.selectedMenu[index]["new"] = value.new;
                             $scope.selectedMenu[index]["edit"] = value.edit;
                             $scope.selectedMenu[index]["view"] = value.view;
                        }
                    });
               
                
    
            }
        
      
    };

    

    $scope.chooseMenu = function (index, id, checked) {

        $scope.selectedMenu[index]["pageid"] = id;
        var findIndex = $scope.mappedMenusIndex.indexOf(index);
        if (findIndex > -1) {
            if (checked) {
                $scope.mappedMenusIndex.push(index)
            } else {
                $scope.mappedMenusIndex.splice(findIndex, 1);
            }
        } else {
            $scope.mappedMenusIndex.push(index)
        }

        console.log($scope.mappedMenusIndex);
    };


    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userrolelist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }


    $scope.showUserRoleEntryModal = function () {
        $('#userEntryModal').modal('show')
    };
    $scope.hideUserRoleEntryModal = function () {
        $('#userEntryModal').modal('hide')
    };


    $scope.userRoleCreatorModal = function (id, viewType) {
        $scope.selectedMenu = {};
        $scope.mappedMenusIndex = [];//userrole mapped index init
        if (id == 0) {
            $rootScope.title = "User Role Creation";
            $scope.buttonText = "Save";
            $scope.userrole_id = 0;
            $scope.userRoleEntryData = {};
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.viewmenuTable = false;
           
            $scope.setMenu($scope.menus);
            $scope.showUserRoleEntryModal();
        } else {
            // $scope.title = "Edit POPUP  ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit') {
                $rootScope.title = 'Edit User Role';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
                $scope.viewmenuTable = false;
            } else {
                $rootScope.title = 'View User Role';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
                $scope.viewmenuTable = true;
            }
            $scope.buttonText = "Update";
            $scope.userrole_id = id;
            $scope.setMenu($scope.menus);
            services.getEditUserRoleData($scope.userrole_id).then((res) => {
                // console.log(res);
                $scope.userRoleEntryData = res;

                $scope.selectedViewMenu = [];
               
                if ($scope.userViewType == 'view') {
                    for (var i in $scope.userRoleEntryData.pages) {
                        var selectedPageid = $scope.userRoleEntryData.pages[i].pageid;
                     
                        var m_index = $scope.menus.findIndex(x => x.id == selectedPageid)
                        if (m_index > -1) {
                         
                            var obj = {
                                menuname : $scope.menus[m_index].menuname,
                                new: $scope.userRoleEntryData.pages[i].new == true ? 'Yes':'No',
                                edit: $scope.userRoleEntryData.pages[i].edit == true ? 'Yes':'No',
                                view: $scope.userRoleEntryData.pages[i].view == true ? 'Yes':'No',
                            }
                            $scope.selectedViewMenu.push(obj);
                           
                        }
                    }
                   
                }
                else if($scope.userViewType == 'edit'){
                   
                    $scope.assignTableValues($scope.userRoleEntryData.pages);
                }
              $scope.showUserRoleEntryModal();
            })
        }

    };

    $scope.getPages = function () {
        var pages = [];

        for (var key of $scope.mappedMenusIndex) {
            pages.push($scope.selectedMenu[key])

        }
        return pages;
    };




    $scope.saveUserRole = function (userrole_id) {
        debugger;
        $scope.userRoleEntryData.pages = $scope.generateMenuTableValues();

        if ($scope.userRoleEntryData.pages.length == 0) {
            alert('Please select any one of menu');
        } else {
           // $scope.userRoleEntryData.pages = $scope.getPages();
            if (userrole_id == 0) {
                services.insertUserRoleData($scope.userRoleEntryData).then(function (res) {
                    debugger;
                    if (res.statuscode == 'NT-200') {
                        // alert(res.message);

                        $scope.hideUserRoleEntryModal();
                        services.toast('success', res.message);
                        $scope.userRoleData.push($scope.userRoleEntryData)
                        // $location.reload();
                        //$state.reload($rootScope.current_path);
                        $rootScope.pageReload();

                    } else {
                        services.toast('warning', res.message)
                        // alert(res.message); 
                    }

                });
            } else {
                services.updateUserRoleData($scope.userRoleEntryData).then(function (res) {
                    debugger;
                    if (res.statuscode == 'NT-200') {
                        // alert(res.message);
                        $scope.hideUserRoleEntryModal();
                        services.toast('success', res.message);
                        // var index = $scope.userRoleData.findIndex(x => x._id == userrole_id);
                        // if (index > -1) {
                        //     $scope.userRoleData[index] = $scope.userRoleEntryData;
                        // }
                        //$state.reload($rootScope.current_path);
                        $rootScope.pageReload();

                    } else {
                        //  alert(res.message); 
                        services.toast('warning', res.message)
                    }

                });
            }
        }

    };


   

   
       

    



}]);