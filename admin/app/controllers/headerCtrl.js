admin.directive('headerComponent', function () {
    return {
        restrict: 'EA',
        controller: 'headerctrl',
        templateUrl: './views/partials/header/header.html'
    };
});

admin.controller('headerctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {
    
   $scope.logout = function(){
    $rootScope.logout();
   };

   $scope.passwordChangedata = {};

   $rootScope.openPasswordModal = function(data){
    $scope.passwordChangedata['username'] = data.username;
       $('#passwordModal').modal('show')
   };

   $rootScope.closePasswordModal = function(){
    $scope.passwordChangedata = {};
    $('#passwordModal').modal('hide');

}

    $scope.showPassword = false;
    $scope.toggleShowPassword = function () {
        $scope.showPassword = !$scope.showPassword;
    };


    $rootScope.changePassword = function(){
        debugger;
       // $scope.passwordChangedata
        services.changePassword( $scope.passwordChangedata).then((res) =>{
            if(res.statuscode == 'NT-200'){
                services.toast('success',res.message);
                $rootScope.closePasswordModal();
            }else{
                services.toast('warning',res.message)
            }
              
        })
        //console.log($scope.passwordChangedata);
    }

}]);