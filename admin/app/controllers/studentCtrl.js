/****entry and edit user */
admin.controller("studentctrl", ['$scope', '$rootScope', '$location', '$stateParams', 'services', 'student', '$http', '$filter', '$state', function ($scope, $rootScope, $location, $stateParams, services, student, $http, $filter, $state) {
    debugger;

    $scope.userViewType = getLocalStorageData('userViewType');


    var studentID = ($stateParams.studentid) ? ($stateParams.studentid) : 0;

    $scope.buttonText = (studentID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.return_data = "Select";
    $scope.studentid = studentID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (studentID != 0) ? 'Edit Enrolled Student' : 'Student Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View Enrolled Student';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    }

    $('[data-type="adhaar-number"]').keyup(function () {
        var value = $(this).val();
        value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
        $(this).val(value);
    });

    $scope.setCountry = function (countrycode) {
        $scope.studentenrolldata.se_address.country = countrycode;
        $scope.studentenrolldata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.studentenrolldata.se_address.state = state;
        $scope.studentenrolldata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        /**begin-Aswin-17-12-2020-district-error-change */
        if(stateindex != -1){
            $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
        }
        /**end-Aswin-17-12-2020-district-error-change */
        // alert( $scope.studentenrolldata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.studentenrolldata.se_address.city = city;
        $scope.studentenrolldata.se_city = city;

        //alert(city);

    }

    if (studentID != 0) {
        debugger;
        $scope.getstudent = student;
        var original = $scope.getstudent;
        original._id = studentID;
        $scope.studentenrolldata = angular.copy(original);
        $scope.studentenrolldata._id = studentID;
        $scope.enrolementnumber = $scope.studentenrolldata.se_studentid; /**end-Aswin-17-12-2020-district-error-change */
        $scope.bonofide = $rootScope.addImgDefUrl($scope.studentenrolldata.se_bonafide);
        $scope.marksheet = $rootScope.addImgDefUrl($scope.studentenrolldata.se_marksheet);
        $scope.aadharcard = $rootScope.addImgDefUrl($scope.studentenrolldata.se_aadharimage);
        $scope.studentphoto = $rootScope.addImgDefUrl($scope.studentenrolldata.se_studentimage);
        $scope.dateofbirth = unixToTime(parseInt($scope.studentenrolldata.se_dateofbirth));
        $scope.country = $scope.studentenrolldata.se_country;
        $scope.parentnum_dialcode = $scope.studentenrolldata.se_parentmobileno.dialcode;
        $scope.parentmobileno = $scope.studentenrolldata.se_parentmobileno.mobileno.substring($scope.studentenrolldata.se_parentmobileno.mobileno.length - 10, $scope.studentenrolldata.se_parentmobileno.mobileno.length)

        $scope.studentnum_dialcode = $scope.studentenrolldata.se_parentmobileno.dialcode;
        $scope.studentmobileno = $scope.studentenrolldata.se_studentmobileno.mobileno.substring($scope.studentenrolldata.se_studentmobileno.mobileno.length - 10, $scope.studentenrolldata.se_studentmobileno.mobileno.length);

        //*****begin-11-01-2021schoolcode add-aswin */
        $scope.se_schoolcode = $scope.studentenrolldata.se_schoolcode;        
         //*****end-11-01-2021schoolcode add-aswin */
        if ($scope.country == 'IN') {
            $scope.state = $scope.studentenrolldata.se_state;
            $scope.setState($scope.state);
            $scope.city = $scope.studentenrolldata.se_city;
        }

    } else {}

    $scope.cancel = function () {
        $location.path('/studentlist');
    }
    $scope.isClean = function () {
        return angular.equals(original, $scope.student);
    }
    $scope.student_cancel = function () {
        debugger;
        if (userID != 0) {
            $location.path('/studentlist')
        } else {
            $scope.student = {};
            $scope.enrollForm.$setPristine();
            $scope.enrollForm.$setUntouched();
            // $window.location.reload();
        }

    };


    $scope.uploadedImage = function (imagefiles, id) {
        debugger;
        showLoader();
        if (imagefiles) {
            var profileImage = imagefiles;
            $scope.imageFileSize = Math.round((profileImage[0].size / 1024));
            var ImageFileSizeLimit = 5 * 1024;
            if ($scope.imageFileSize < ImageFileSizeLimit) {

                if (id == 'marksheet') {
                    $scope.marksheet = imagefiles
                    $scope.marksheetSrc = $scope.marksheet[0];
                    services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_marksheet = res
                        $scope.marksheet = $rootScope.addImgDefUrl(res)
                        // console.log(res);
                        hideLoader();

                    })
                } else if (id == 'aadharcard') {
                    $scope.aadharcard = imagefiles
                    $scope.aadharcardSrc = $scope.aadharcard[0]
                    services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_aadharimage = res;
                        $scope.aadharcard = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'studentphoto') {
                    $scope.studentphoto = imagefiles
                    $scope.studentphotoImgSrc = $scope.studentphoto[0];
                    services.uploadStudentPhoto(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_studentimage = res;
                        $scope.studentphoto = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'bonofide') {
                    $scope.bonofide = imagefiles;
                    $scope.bonofideSrc = $scope.bonofide[0]
                    services.uploadBonofideLetter( $scope.bonofideSrc).then((res) => {
                        debugger;
                        $scope.studentenrolldata.se_bonafide = res;
                        $scope.bonofideImage = $rootScope.addImgDefUrl(res);
                        $scope.bonofide = $rootScope.addImgDefUrl(res);;
                        console.log(res)
                        hideLoader();
                    })
                }
            } else {
                alert('file size is more than 5mb');
                hideLoader();
            }
        }



    }

    $scope.checkPdf = function(file){
        if(file && file.split('.').pop() == 'pdf'){
              return './assets/images/pdf.png';
        }else{
            return file;
        }
    }

    // $scope.uploadedFile = function (element, id) {
    //     debugger;
    //     showLoader();
    //     var maxSize = 5 * 1024;
    //     $scope.currentFile = element.files[0];
    //     var fileSize = Math.round((element.files[0].size / 1024));
    //     var reader = new FileReader();
    //     if (fileSize > maxSize) {
    //         alert('file size is more than 5mb');
    //         hideLoader();
    //         return false;
    //     } else {
    //         reader.onload = function (event) {


    //             $scope.$apply(function ($scope) {
    //                 //$scope.files = element.files;
    //                 if (id == 'marksheet') {
    //                     $scope.marksheet = event.target.result
    //                     $scope.marksheetSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_marksheet = res
    //                         console.log(res);
    //                         hideLoader();

    //                     })
    //                 } else if (id == 'aadharcard') {
    //                     $scope.aadharcard = event.target.result
    //                     $scope.aadharcardSrc = element.files[0]
    //                     services.uploadAadhar($scope.aadharcardSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_aadharimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'studentphoto') {
    //                     $scope.studentphoto = event.target.result
    //                     $scope.studentphotoImgSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_studentimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'bonofide') {
    //                     $scope.bonofide = event.target.result
    //                     $scope.bonofideSrc = element.files[0]
    //                     services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
    //                         debugger;
    //                         $scope.studentenrolldata.se_bonafide = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 }
    //             });
    //         }
    //         reader.readAsDataURL(element.files[0]);
    //     }
    // }



    /****show/hide password */


    $scope.saveEnrolluser = function () {
        debugger;

        if ($scope.studentid == 0) {

        } else {

            $scope.studentenrolldata.se_parentmobileno = {
                dialcode: $scope.parentnum_dialcode,
                mobileno: $scope.parentnum_dialcode + $scope.parentmobileno
            }
            $scope.studentenrolldata.se_studentmobileno = {
                dialcode: $scope.studentnum_dialcode,
                mobileno: $scope.studentnum_dialcode + $scope.studentmobileno
            };
            //*****begin-11-01-2021schoolcode add-aswin */
            var school_codePrefix = 'MC-';
           
            if($scope.se_schoolcode.indexOf(school_codePrefix) !== 0 ){
                $scope.studentenrolldata.se_schoolcode = school_codePrefix+$scope.se_schoolcode
            }else{
                $scope.studentenrolldata.se_schoolcode = $scope.se_schoolcode;
            }
             //*****end-11-01-2021schoolcode add-aswin */
             //*****begin-21-01-2021-aswin-dateofbirth-notupdateerror */
             $scope.studentenrolldata.se_dateofbirth =  timeToUnix($scope.dateofbirth);
           
              //*****end-21-01-2021-aswin-dateofbirth-notupdateerror */
            services.StudentEnrollUpdate($scope.studentenrolldata).then((res) => {
                var msg = "Update Successfully";
                services.toast('success', msg);
                //  $location.path('/userlist');
                $state.go('home.studentslist');
            });

        }


    };

    $scope.showImgModal = function () {
        $('#imgViewModal').modal('show')
    };
    $scope.hideImgModal = function () {
        $('#imgViewModal').modal('hide')
    };





    $scope.viewImageFullScreen = function (id, imgUrl) {
        if (imgUrl) {
            if (id == 'marksheet') {
                $scope.imgModalTitle = '10th Mark Sheet';
            } else if (id == 'aadharcard') {
                $scope.imgModalTitle = 'Aadhar Card'
            } else if (id == 'studentphoto') {
                $scope.imgModalTitle = 'Student Recent Photo'
            } else if (id == 'bonofide') {
                $scope.imgModalTitle = 'School Bonafide Letter'
            }
            $scope.imgUrl = imgUrl;
            if($scope.imgUrl && $scope.imgUrl.split('.').pop() == 'pdf'){
               window.open($scope.imgUrl)
            }else{
                $scope.showImgModal();
            }
            
        }
    }

}]);

/****user list */
admin.controller('studentlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state','$filter', function ($scope, services, $window, $location, $rootScope, $state,$filter) {
    debugger;

    $scope.studentEntryData = {};
    $scope.mailSendData = {};
    $scope.excelheaders = ["ACCA Id", "EXP Id", "Enrollment Number", "Name", "Aadhar Card", "Date of Birth", "Gender", "Parent Mobile No", "Student Mobile No", "Parent Email", "Student Email", "School","School Code", "School's City", "School Pincode", "Studying", "Year of Pass", "Address-1", "Address-2", "City", "Country", "State", "Pincode", "Status"];


    $scope.studentExportData = [];
    $scope.datatrue = false;

    function genderget(value) {
        if (value == 'm') {
            return 'Male'
        } else if (value == 'f') {
            return 'Female'
        } else if (value == 't') {
            value == 'Transgender'
        } else {
            return "";
        }
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };
    services.getAllEnrollUsers().then(function (res) {
        debugger;
        $scope.studentEnrollData = res;

        if ($scope.studentEnrollData != null && $scope.studentEnrollData.length != 0) {
            $scope.datatrue = true;
            var count = 0; /**end-Aswin-17-12-2020-district-error-change */
            for (var studentdata of $scope.studentEnrollData) {
                var tempObj = {
                    "ACCA Id": studentdata.acca_id,
                    "EXP Id": studentdata.exp_id,
                    "Enrollment Number": studentdata.se_studentid,
                    "Name": studentdata.se_name,
                    "Aadhar Card": studentdata.se_aadharno,
                    "Date of Birth": unixTodateTime(parseInt(studentdata.se_dateofbirth)),
                    "Gender": genderget(studentdata.se_gender),
                    "Parent Mobile No": studentdata.se_parentmobileno.mobileno,
                    "Student Mobile No": studentdata.se_studentmobileno.mobileno,
                    "Parent Email": studentdata.se_parentemail,
                    "Student Email": studentdata.se_studentemail,
                    "School": studentdata.se_school,
                    "School Code":studentdata.se_schoolcode, 
                    "School's City": studentdata.se_schoolcity,
                    "School Pincode": studentdata.se_schoolpincode,
                    "Studying": studentdata.se_class,
                    "Year of Pass": studentdata.se_yearofpass,
                    "Address-1": studentdata.se_address.line1,
                    "Address-2": studentdata.se_address.line2,
                    "City": studentdata.se_city,
                    "Country": studentdata.se_country,
                    "State": studentdata.se_state,
                    "Pincode": studentdata.pincode,
                    "Status": studentdata.se_status == true ? 'Active' : 'Inactive',
                }

                $scope.studentExportData.push(tempObj);
                $scope.studentEnrollData[count].se_createddate = Date.parse($scope.studentEnrollData[count].se_createddate);//begin-Aswin-17-12-2020-issues-change
                count++
            }
        }
        $scope.updateSubGrid();
    });



    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.showStudentViewModal = function () {
        $('#studentDataViewModal').modal('show')
    };
    $scope.hideStudentViewModal = function () {
        $('#studentDataViewModal').modal('hide')
    };

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
               // "order": [[ 1, "desc" ]], //or asc 
               "ordering": false
            });
        }, 0);
    };

    // $scope.sendPaymentLink = function(enrollid){
    //     services.sendPaymentLink(enrollid).then((res) =>{
    //         debugger;
    //         if(res.statuscode == 'NT-200'){

    //             services.toast('success', res.message);


    //         }else{

    //             services.toast('danger', res.message);


    //         }
    //           console.log(res)
    //     })
    // };
  
    $scope.sendPaymentLink = function (data) {

        // services.sendPaymentLink(data.se_studentid).then((res) =>{
        //     $scope.data.textInput =  res.html
        //     $scope.mailSendData.email = data.se_studentemail;
        //     $scope.showMailEntryModal();
        // })
        //$scope.paylink = 'no';//06-01-2021-student_payment_invoice_function_change
        $scope.paylink = 'none';//06-01-2021-student_payment_invoice_function_change
        $scope.mailSendData.email = data.se_studentemail;
        $scope.mailSendData.studentid = data.se_studentid;
        $scope.showMailEntryModal();


    };

    $scope.ordercheck = function(paylink){
        debugger;
       // if(paylink == 'yes'){ //06-01-2021-student_payment_invoice_function_change
       if(paylink == 'pi' || paylink == 'fi'){
           $scope.invoicetype = paylink;
            services.sendPaymentLink($scope.mailSendData.studentid,$scope.invoicetype).then((res) => {
                 /****checking */
                 if(res.statuscode == 'NT-200'){
                    var paymentUrl = __env.paymentUrl;
                    var studentname = res.se_name;
                    var enc_studentid = encrypt(res.se_studentid);
                    var enc_studentemail = encrypt(res.se_studentemail);
                    var enc_orderid = encrypt(res.orederid);
                   // var enc_amount = encrypt(res.total) ;
                   var enc_amount = encrypt(res.totalamount) ;
                    ///*****proforma invoice pdf */
                    $scope.pdflink = $rootScope.addImgDefUrl(res.pdflink);
                    if(res.gst){
                        var gst_amount = res.gst_amount;
                        // var gstTd = '<td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">GST</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+gst_amount+'</span></td>';

                        var gstTd = '<tr style="background-color: white;font-weight: 400;text-align: center;height: 13px;"><td colspan="2" style="text-align: left; width: 618px; height: 13px;padding-left:15px;">GST</td><td colspan="2" style="width: 164px; height: 13px;">'+gst_amount+'</td></tr>';

                        
                    }else{
                        var gst_amount = 0;
                        var gstTd = ''
                       
                    };

                        /*****begin-26-12-2020-Aswin-overdueamountmismatch in mail template */
                    // var description = 'Overdue';
                    // var amount  = res.overdueamount;
                    // var total = res.overdueamount;
                  
                    var scheduleamount = res.scheduleamount;
                    var overdueamount = res.overdueamount;
                    var total = res.totalamount;
                    var schedulcount = res.schedulecount;
                    /*****begin-new proforma email */
                   var totalAmountInWords = convertNumberToWords(total);
                   var schedule = res.schedule;
                   /*****end-new proforma email */
                  /*****end-26-12-2020-Aswin-overdueamountmismatch in mail template */
                  // var gstamount = '0';
                  // $scope.order_id = res.orederid; //begin06-01-2021-student_payment_invoice_function_change
                   var date = $filter('date') 
                   ( res.createddate, 'medium'); 
                   
            //begin06-01-2021-student_payment_invoice_function_change
                   //    var action_url = paymentUrl+`?param1=${enc_studentid}&param2=${enc_studentemail}&param3=${enc_orderid}&param4=${enc_amount}`;

                 
                    if($scope.invoicetype == 'pi'){
                        var titleMsg = 'Thanks for using microconnect. This is an invoice for your recent due amount details.';
                        var buttonText = 'Pay Now';
                        var action_url = paymentUrl+`?param1=${enc_studentid}&param2=${enc_studentemail}&param3=${enc_orderid}&param4=${enc_amount}`;
                        $scope.order_id = res.orederid;

                        /*****begin-10-01-2021-Aswin-new proforma email */
                         if(schedulcount == 0){
                             
                             var installmentTrData = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Overdue</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(overdueamount) +'</td></tr>'// /*****begin-19-01-2021-Aswin-amount format change */
                             var  veryfirstInstallmentTrData = '';
                             var mailTemplatemessage = '';
                             var paymentNoSuffix = 'Final';
                             var bottomPaymentTagline =  'Overdue Payment';
                         }else{
                             if(schedulcount == 1){
                                // if(schedule.length != 0){
                                    var scheduleTrData = '';

                                            for(var i = 0 ;i < schedule.length;i++){
                                                // var trdata = '<tr><td style="border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="bottom" bgcolor="#F2F2F2" height="21">Installment No.'+ (i + 1) +' </td> <td style="width: 97px;" align="left" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td> <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; width: 155px;" align="center" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; width: 119px;" align="right"valign="bottom" bgcolor="#F2F2F2">Rs.'+ schedule[i].amount  +'</td></tr>';
                                                var shedulePaymentNo = (i + 1)+' <sup>'+numberSuffixAdd(i + 1)+'</sup> Payment'
                                                var trdata  = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;"><td style="text-align: center">'+shedulePaymentNo +'</td><td>'+ amountFormat(schedule[i].amount) +'</td><td>'+ momentUnixConvert(schedule[i].duedate) +'</td></tr>'
                                                scheduleTrData += trdata;
                                            }
                                            var veryfirstInstallmentTrData = '<tr style="height: 78px;"><td style="height: 78px;"><table style="width: 800px; border: 1; border-color: #ffffff;" cellspacing="1"><tbody><tr style="background-color: #007dc1;font-weight: 600;text-align: center;color: #fff;"><td>Schedule No.</td><td>Total Amount <br>(Rs)</td><td>Payable on or before</td></tr>'+ scheduleTrData +'</tbody></table></td></tr>'
                                 //  }
                                 var mailTemplatemessage = '<p>We have received your document and found is in order.</p><p>As per our discussion we here by sharing the schedule of payment with dates. Kindly ensure the payment is made on time.</p>'

                                 var paymentNoSuffix =  numberSuffixAdd(schedulcount);
                                 var bottomPaymentTagline =   schedulcount +'<sup>'+ paymentNoSuffix + '</sup> Payment&nbsp';
                                

                             }else{
                               var  veryfirstInstallmentTrData = '';
                               var mailTemplatemessage = '';
                             }
                         
                           

                            var installmentTrData = '<tr style="background-color: whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Instalment No.'+ schedulcount + '</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(scheduleamount) +'</td></tr><tr style="background-color: white;font-weight: 400;text-align: center;height: 13px;"><td style="text-align: left; width: 618px; height: 13px;padding-left:15px;" colspan="2">Overdue</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(overdueamount) +'</td></tr>';
                            var paymentNoSuffix =  numberSuffixAdd(schedulcount);
                            var bottomPaymentTagline =   schedulcount +'<sup>'+ paymentNoSuffix + '</sup> Payment&nbsp';
                         }


                        

                        /*****end-10-01-2021-Aswin-new proforma email */
                        var amountTableTrData = '<tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Installment No.'+ schedulcount +'</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ amountFormat(scheduleamount) +'</span></td></tr><tr><tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Overdue</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ amountFormat(overdueamount) +'</span></td></tr>';

                         $scope.data.textInput = '<table id="m_1975787499329414483bodyTable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #ffffff;height: 100%!important;width: 100%!important;background-color: #f1f7fd;padding:20px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td id="m_1975787499329414483bodyCell" style="margin: 0; padding: 20px; border-top: 0; height: 100%!important; width: 100%!important;" align="center" valign="top"><table id="m_1975787499329414483templateContainer" style="border-collapse: collapse;/* border: 1px solid black; */" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td align="center" valign="top">&nbsp;</td></tr><tr><td align="center" valign="top"><table id="m_1975787499329414483templateHeader" style="border-collapse: collapse;min-width: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;background: #fff;font: 14px sans-serif;color: #686f7a;border-top: 4px solid #007dc1;/* margin-bottom: 20px; */border-bottom: 1px solid #dadee4;box-shadow: -1px 1px 4px 0 rgba(117,138,172,.12);" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td class="m_1975787499329414483headerContainer" valign="top"><table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr style="height: 66px;"><td style="height: 66px;" valign="top"><div style="width: 100%; text-align: center; margin: 12px 0;"><a style="word-wrap: break-word;" href="https://www.microconnect.co.in/" target="_blank"> <img class="CToWUd" src="https://api.microconnect.co.in/logo/microconnect1.png" alt=""> </a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr style=" border-bottom: 4px solid #007dc1;"><td align="center" valign="top"><table id="m_1975787499329414483templateBody" style="border-collapse: collapse; min-width: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0;" border="0" width="600" cellspacing="0" cellpadding="0"><tbody><tr><td class="m_1975787499329414483bodyContainer" valign="top" style=" padding: 10px;"><table style="min-width: 100%; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td style="padding-top: 9px;" valign="top"><table style="max-width: 100%; min-width: 100%; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left"><tbody><tr><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px;text-align: left;" valign="top"><strong>Hi '+ studentname +'</strong>,<br><p>Thanks for enrolling with MICROCONNECT</p>'+ mailTemplatemessage +'</td></tr>'+ veryfirstInstallmentTrData +'<tr style="height: 78px;"><td style="height: 78px;"><table style="width: 800px; border: 1; border-color: #ffffff;" cellspacing="1"><tbody> <tr style="background-color: whitesmoke;height:60px;text-align: center;"> <td style="font-weight:bolder;font-size:larger;color: #000;">Amount: Rs.'+amountFormat(total)+'<br><span>('+ totalAmountInWords +'rupees only)</span></td> </tr></tbody></table></td></tr><tr><td align="right"><h3 style=" padding-right: 10px;"><strong>Proforma Invoice No :'+  $scope.order_id +'</strong></h3></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table style=" border: 1; border-color: #ffffff;" cellspacing="1"><tbody><tr style="background-color: #ffcc66; font-weight: 600; text-align: center; height: 26px;"><td style="text-align: left;height: 26px;padding-left: 15px;" colspan="2">Description.</td><td style="width: 164px; height: 26px;" colspan="2">Amount <br>(Rs)</td></tr>'+ installmentTrData +'<tr style="background-color: #ffcc99; font-weight: 400; text-align: center; height: 13px;"><td style="width: 322.5px;text-align: right;height: 13px;padding-right: 30px;"><strong>Total :</strong></td><td style="width: 295.5px; text-align: left; height: 13px;"><strong style="padding-left:7px";>Amount in words :</strong><br>&nbsp; '+ totalAmountInWords  +'rupees only</td><td style="width: 164px; height: 13px;" colspan="2">'+ amountFormat(total) +'</td></tr></tbody></table></td></tr><tr style="height: 37px;"><td style="height: 37px;"><table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr><tr style="height: 261px;"><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,"Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 150%; text-align: left; padding: 0px 18px 9px; height: 261px;" valign="top">Please find attached the Proforma Invoice for the '+ bottomPaymentTagline +'<br><p>If you have any clarification feel free to contact us on the coordinates given below. Please do quote your enrolment number on all your communication for speedy resolutions.</p><p>Happy Learning,</p><p>With warm regards,</p><p>Team MICROCONNECT,</p>Mobile:+91 96770 96011,<br> Mail – <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>&nbsp;,<br>Visit – <a href="http://www.microconnect.co.in">www.microconnect.co.in</a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'

                    }else{
                        //final invoice
                        var titleMsg = 'Thanks for using microconnect. This is an invoice for your payment details.' 
                        var buttonText = 'Download Invoice';
                        var action_url = $scope.pdflink;
                        $scope.order_id = res.invoiceno;
                        // var amountTableTrData = '<tr><td class="purchase_item" style="padding: 10px 0; color: #51545e; font-size: 15px; line-height: 18px;" width="80%"><span class="f-fallback">Paid Amount</span></td><td class="align-right" style="text-align: right;" width="20%"><span class="f-fallback">'+ total +'</span></td></tr>';
                        var amountTableTrData = '<tr style="background-color:whitesmoke;font-weight: 400;text-align: center;height: 13px;"><td colspan="2" style="text-align: left; width: 618px; height: 13px;padding-left:15px;">Paid Amount</td><td colspan="2"style="width: 164px; height: 13px;">'+ amountFormat(total) +'</td></tr>'
                    /*****begin-10-01-2021-Aswin-new proforma email */
                        // $scope.data.textInput = '<p>&nbsp;</p><table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><table style="height: 1086px; width: 100%;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><a style="align-item: center;" href="http://microconnect.co.in/" target="_blank" rel="noopener"> <img style="height: 13vh; border: none; display: block; margin-left: auto; margin-right: auto;" src="https://api.microconnect.co.in/logo/microconnect.png" /> </a></td></tr><tr style="height: 718px;"><td style="width: 100%; margin: 0px; padding: 0px; background-color: #ffffff; height: 718px;"><table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;"><h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hi  '+ studentname +',</h1><p style="color: #51545e;">'+ titleMsg +'</p><table class="attributes" style="margin: 0 0 21px;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_content" style="background-color: #f4f4f7; padding: 16px;"><table role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_item" style="padding: 0;"><span class="f-fallback"> <strong>Amount:</strong>'+ total +'</span></td></tr></tbody></table></td></tr></tbody></table><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td align="center"><table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr></tbody></table><table class="purchase" style="width: 100%; margin: 0; padding: 35px 0;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><h3 style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;">Proforma Invoice No :'+  $scope.order_id +'</h3></td><td><h3 class="align-right" style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;"></h3></td></tr><tr><td colspan="2"><br /><table class="purchase_content" style="width: 100%; margin: 0px; padding: 25px 0px 0px; height: 108px;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="left"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Description</p></th><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="right"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Amount</p></th></tr>'+amountTableTrData+'<tr>'+ gstTd +'</tr><tr><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="80%"><p class="f-fallback purchase_total purchase_total--label" style="margin: 0; text-align: right; font-weight: bold; color: #333333; padding: 0 15px 0 0;">Total</p></td><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="20%"><p class="f-fallback purchase_total" style="margin: 0; text-align: right; font-weight: bold; color: #333333;">'+total+'</p></td></tr></tbody></table></td></tr></tbody></table><p style="color: #51545e;">If you have any questions about this invoice, simply reply to this email or reach out to our <a style="color: #3869d4;" href="http://microconnect.co.in/">support team</a> for help.</p><p>Cheers, <br />The Microconnect Team</p></td></tr></tbody></table></td></tr><tr style="height: 288px;"><td style="height: 288px;"><table class="email-footer" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;" align="center"><p class="" style="color: #6b6e76;">&copy; 2020 <a href="http://microconnect.co.in/" target="_blank" rel="noopener"><span style="color: #3366ff;">microconnect</span></a>. All rights reserved.</p><p class="f-fallback sub align-center" style="color: #6b6e76;">Sai Anugragh,<br />16 Ashtalakshmi Street,<br />Muthulakshmi Nagar,<br />Chitlapakkam,<br />Chennai 600064,</p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Phone:</strong><a href="tel:+919677096011">&nbsp; +91 96770 96011</a></p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Email:</strong><a href="mailto:info@microconnect.co.in">&nbsp;&nbsp; info@microconnect.co.in</a></p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>';
                       $scope.data.textInput = '<table align="center" border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483bodyTable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #ffffff;height: 100%!important;width: 100%!important;background-color: #f1f7fd;padding:20px;" width="100%"><tbody><tr><td align="center" id="m_1975787499329414483bodyCell" style="margin: 0; padding: 20px; border-top: 0; height: 100%!important; width: 100%!important;" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateContainer" style="border-collapse: collapse;/* border: 1px solid black; */" width="600"><tbody><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateHeader" style="border-collapse: collapse;min-width: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;background: #fff;font: 14px sans-serif;color: #686f7a;border-top: 4px solid #007dc1;/* margin-bottom: 20px; */border-bottom: 1px solid #dadee4;box-shadow: -1px 1px 4px 0 rgba(117,138,172,.12);" width="600"><tbody><tr><td class="m_1975787499329414483headerContainer" valign="top"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" width="100%"><tbody><tr style="height: 66px;"><td style="height: 66px;" valign="top"><div style="width: 100%; text-align: center; margin: 12px 0;"><a href="https://www.microconnect.co.in/" style="word-wrap: break-word;" target="_blank"><img alt="" class="CToWUd" src="https://api.microconnect.co.in/logo/microconnect1.png" /> </a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr style=" border-bottom: 4px solid #007dc1;"><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" id="m_1975787499329414483templateBody" style="border-collapse: collapse; min-width: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0;" width="600"><tbody><tr><td class="m_1975787499329414483bodyContainer" style=" padding: 10px;" valign="top"><table border="0" cellpadding="0" cellspacing="0" style="min-width: 100%; border-collapse: collapse;" width="100%"><tbody><tr><td style="padding-top: 9px;" valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%; min-width: 100%; border-collapse: collapse;" width="100%"><tbody><tr><td class="m_1975787499329414483mcnTextContent" style="color: #554d56; font-family: Roboto,Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px;text-align: left;" valign="top"><strong>Hi '+studentname+'</strong>,<p>Thanks for using microconnect. This is an invoice for your payment details.</p></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table cellspacing="1" style="width: 800px; border: 1; border-color: #ffffff;"><tbody><tr style="background-color: whitesmoke;height:60px;text-align: center;"><td style="font-weight:bolder;font-size:larger;color: #000;">Amount: Rs.'+ amountFormat(total) +'<br><span>('+ totalAmountInWords +'rupees only)</span></td></tr></tbody></table></td></tr><tr><td align="right"><h3 style=" padding-right: 10px;"><strong>Invoice No :'+ $scope.order_id +'</strong></h3></td></tr><tr style="height: 78px;"><td style="height: 78px;"><table cellspacing="1" style=" border: 1; border-color: #ffffff;"><tbody><tr style="background-color: #ffcc66; font-weight: 600; text-align: center; height: 26px;"><td colspan="2" style="text-align: left;height: 26px;padding-left: 15px;">Description.</td><td colspan="2" style="width: 164px; height: 26px;">Amount<br />(Rs)</td></tr>'+ amountTableTrData +''+gstTd+'<tr style="background-color: #ffcc99; font-weight: 400; text-align: center; height: 13px;"><td style="width: 322.5px;text-align: right;height: 13px;padding-right: 30px;"><strong>Total :</strong></td><td style="width: 295.5px; text-align: left; height: 13px;"><strong style="padding-left:7px">Amount in words :</strong><br />&nbsp;'+ totalAmountInWords +' rupees only</td><td colspan="2" style="width: 164px; height: 13px;">'+ amountFormat(total) +'</td></tr></tbody></table></td></tr><tr style="height: 37px;"><td style="height: 37px;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr><tr style="height: 261px;"><td class="m_1975787499329414483mcnTextContent" helvetica="" neue="" style="color: #554d56; font-family: Roboto,">Please find attached the Invoice for the Payment<p>If you have any clarification feel free to contact us on the coordinates given below. Please do quote your enrolment number on all your communication for speedy resolutions.</p><p>Happy Learning,</p><p>With warm regards,</p><p>Team MICROCONNECT,</p>Mobile:+91 96770 96011,<br> Mail – <a href="mailto:support@microconnect.co.in">support@microconnect.co.in</a>&nbsp;,<br>Visit – <a href="http://www.microconnect.co.in">www.microconnect.co.in</a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'
 /*****end-10-01-2021-Aswin-new proforma email */
                    }
                  
             //end-06-01-2021-student_payment_invoice_function_change
                  
                 /*****begin-10-01-2021-Aswin-new proforma email */

                //   $scope.data.textInput = '<p>&nbsp;</p><table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><table style="height: 1086px; width: 100%;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><a style="align-item: center;" href="http://microconnect.co.in/" target="_blank" rel="noopener"> <img style="height: 13vh; border: none; display: block; margin-left: auto; margin-right: auto;" src="https://api.microconnect.co.in/logo/microconnect.png" /> </a></td></tr><tr style="height: 718px;"><td style="width: 100%; margin: 0px; padding: 0px; background-color: #ffffff; height: 718px;"><table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;"><h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hi  '+ studentname +',</h1><p style="color: #51545e;">'+ titleMsg +'</p><table class="attributes" style="margin: 0 0 21px;" role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_content" style="background-color: #f4f4f7; padding: 16px;"><table role="presentation" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="attributes_item" style="padding: 0;"><span class="f-fallback"> <strong>Amount:</strong>'+ total +'</span></td></tr></tbody></table></td></tr></tbody></table><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td align="center"><table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank" rel="noopener">'+ buttonText +'</a></td></tr></tbody></table></td></tr></tbody></table><table class="purchase" style="width: 100%; margin: 0; padding: 35px 0;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td><h3 style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;">Proforma Invoice No :'+  $scope.order_id +'</h3></td><td><h3 class="align-right" style="margin-top: 0; color: #333333; font-size: 14px; font-weight: bold; text-align: right;"></h3></td></tr><tr><td colspan="2"><br /><table class="purchase_content" style="width: 100%; margin: 0px; padding: 25px 0px 0px; height: 108px;" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="left"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Description</p></th><th class="purchase_heading" style="padding-bottom: 8px; border-bottom: 1px solid #EAEAEC;" align="right"><p class="f-fallback" style="margin: 0; color: #85878e; font-size: 12px;">Amount</p></th></tr>'+amountTableTrData+'<tr>'+ gstTd +'</tr><tr><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="80%"><p class="f-fallback purchase_total purchase_total--label" style="margin: 0; text-align: right; font-weight: bold; color: #333333; padding: 0 15px 0 0;">Total</p></td><td class="purchase_footer" style="padding-top: 15px; border-top: 1px solid #EAEAEC;" valign="middle" width="20%"><p class="f-fallback purchase_total" style="margin: 0; text-align: right; font-weight: bold; color: #333333;">'+total+'</p></td></tr></tbody></table></td></tr></tbody></table><p style="color: #51545e;">If you have any questions about this invoice, simply reply to this email or reach out to our <a style="color: #3869d4;" href="http://microconnect.co.in/">support team</a> for help.</p><p>Cheers, <br />The Microconnect Team</p></td></tr></tbody></table></td></tr><tr style="height: 288px;"><td style="height: 288px;"><table class="email-footer" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="content-cell" style="padding: 35px;" align="center"><p class="" style="color: #6b6e76;">&copy; 2020 <a href="http://microconnect.co.in/" target="_blank" rel="noopener"><span style="color: #3366ff;">microconnect</span></a>. All rights reserved.</p><p class="f-fallback sub align-center" style="color: #6b6e76;">Sai Anugragh,<br />16 Ashtalakshmi Street,<br />Muthulakshmi Nagar,<br />Chitlapakkam,<br />Chennai 600064,</p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Phone:</strong><a href="tel:+919677096011">&nbsp; +91 96770 96011</a></p><p class="f-fallback sub align-center" style="color: #6b6e76;"><strong>Email:</strong><a href="mailto:info@microconnect.co.in">&nbsp;&nbsp; info@microconnect.co.in</a></p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>';
                
                             //   $scope.data.textInput = res.html;
    
                    $scope.orderid =  res.orederid;
                           
                 }else{
                    // services.toast('warning','Payment link not genrated');
                    services.toast('warning',res.message);//end-06-01-2021-student_payment_invoice_function_change
                    $scope.paylink = 'none';
                 }
            })
        }else{
            $scope.data.textInput = '';
            $scope.paylink = 'none';
        }

    };
      
    $scope.templateCreate = function(data){
        debugger;
        var minDate = new moment();
        $scope.min = minDate.format("YYYY-MM-DD");
        var invoicedate =  $scope.min;
        var name  = data.se_name;
        var  invoiceno = data.invoice;
        var  studentid = data.se_studentid;
        var  duedate =  $scope.min ;
        var  address1 = data.se_address.line1 + ', '+ data.se_address.line2;
        var  address2 = data.se_address.city;
        var   pincode = data.pincode;
        var   mobileno = data.se_studentmobileno.mobileno;
        var   email = data.se_studentemail;
        var   subtotal = data.totalamount;
        var     total = data.totalamount;
        var schedule = data.schedule;
       if(schedule.length != 0){
        var scheduleTrData = '';
                for(var i = 0 ;i < schedule.length;i++){
                    var trdata = '<tr><td style="border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="bottom" bgcolor="#F2F2F2" height="21">Installment No.'+ (i + 1) +' </td> <td style="width: 97px;" align="left" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td> <td style="border-left: 1px solid #000000; border-right: 1px solid #000000; width: 155px;" align="center" valign="bottom" bgcolor="#F2F2F2">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; width: 119px;" align="right"valign="bottom" bgcolor="#F2F2F2">Rs.'+ schedule[i].amount  +'</td></tr>';
                    scheduleTrData += trdata;
                }
       }else{

       }
       
       
         var invoicehtml = '<table style="/* height: 931px; */width: 14.8cm; height: 21cm; border-collapse: collapse; font-size: 14px; font-family: ui-sans-serif;" border="0" width="763" cellspacing="0"><colgroup width="138"></colgroup><colgroup width="120"></colgroup><colgroup span="2" width="94"></colgroup><colgroup width="146"></colgroup><colgroup width="112"></colgroup><tbody><tr><td style="border-top: 2px solid #000000; border-left: 2px solid #000000; width: 372px;" colspan="3" align="left" valign="middle" height="58"><span style="color: #2c3964;"><br /><img style="height: 60px;" src="https://api.microconnect.co.in/logo/microconnect1.png" alt="" /> </span></td><td style="border-top: 2px solid #000000; width: 97px; border-bottom: 2px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-top: 2px solid #000000; border-right: 2px solid #000000; width: 278px; border-bottom: 2px solid #000;" colspan="2" align="right" valign="middle"><strong><span style="color: #3a5d9c; font-size: x-large;"><img style="height: 50px; padding-right: 10px;" src="https://api.microconnect.co.in/logo/silver_learning_partner.png" alt="" /> </span></strong></td></tr><tr><td style="border-top: 2px solid #000000; border-left: 2px solid #000000; width: 372px; border-bottom: 2px solid #000;" colspan="3" align="left" valign="middle" height="58"><p>16 Ashtalakshmi Street, Muthulakshmi Nagar, Chitlapakkam Chennai 600064.</p></td><td style="border-top: 2px solid #000000; width: 97px; border-bottom: 2px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-top: 2px solid #000000; border-right: 2px solid #000000; width: 278px; border-bottom: 2px solid #000;" colspan="2" align="right" valign="middle">&nbsp;</td></tr><tr><td style="border-left: 2px solid #000000; width: 271px;" colspan="4" align="center" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">BILL TO</span></strong></td><td style="width: 155px;" align="right" valign="bottom">DATE</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom">'+ invoicedate +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21"><strong>'+ name + '</strong></td><td style="width: 200px;" align="left" valign="bottom"></td><td colspan="3" align="right" valign="bottom"><strong> Proforma Invoice #</strong></td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5;" colspan="1" align="center" valign="bottom">'+ invoiceno +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="3">' + address1 +','+ address2 +'</td><td style="width: 155px;" align="right" valign="bottom" colspan="2"><strong>Enrolment No.</strong></td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom">'+ studentid +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="3">Pincode-'+ pincode +'</td><td style="width: 97px;" align="left" valign="bottom">&nbsp;</td><td style="width: 155px;" align="right" valign="bottom">Due On</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #a5a5a5 #000000 #a5a5a5 #a5a5a5; width: 119px;" align="center" valign="bottom" bgcolor="#D2D8EC">'+ duedate +'</td></tr><tr><td style="border-left: 2px solid #000000; width: 143px;" align="left" valign="bottom" height="21" colspan="6">Mobile : '+ mobileno +' ,Email : '+ email +'</td></tr><tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 473px;" colspan="4" align="center" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">DESCRIPTION</span></strong></td><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; width: 155px;" align="center" valign="bottom" bgcolor="#3B4E87"><strong><span style="color: #ffffff;">TAXED</span></strong></td><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-right: 2px solid #000000; width: 119px;" align="center" valign="bottom" bgcolor="#3B4E87"><strong><span style="color: #ffffff;">AMOUNT</span></strong></td></tr> '+ scheduleTrData +'<tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="bottom" bgcolor="#3B4E87" height="21"><strong><span style="color: #ffffff;">OTHER COMMENTS</span></strong></td><td style="border: 1px solid #000000; width: 155px;" align="left" valign="bottom">Subtotal</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="right" valign="bottom">Rs.'+ subtotal +'</td></tr><tr><td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="bottom" height="21">Total payment due</td><td style="border: 1px solid #000000; width: 155px;" align="left" valign="bottom">&nbsp;</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="left" valign="bottom">&nbsp;</td></tr><tr><td style="border-top: 1px solid #a5a5a5; border-left: 2px solid #000000; width: 372px;" colspan="4" align="left" valign="top" height="21">&nbsp;</td><td style="border: 1px solid #000000; width: 155px;" align="right" valign="bottom">&nbsp;</td><td style="border-width: 1px 2px 1px 1px; border-style: solid; border-color: #000000; width: 119px;" align="left" valign="bottom">&nbsp;</td></tr><tr><td style="border-width: 1px 1px 1px 2px; border-style: solid; border-color: #000000; width: 372px;" colspan="4" rowspan="2" align="left" valign="top" height="42">Invoice with Tax compoenent will be shared upon credit of the full payment towards the course.</td><td style="border-left: 1px solid #000000; width: 97px; border-bottom: 1px solid #000;" align="left" valign="bottom">&nbsp;</td><td style="border-left: 1px solid #000000; border-right: 2px solid #000000; border-bottom: 1px solid #000;" align="right" valign="bottom">&nbsp;</td></tr><tr><td style="border-left: 1px solid #000000; width: 97px;" align="left" valign="bottom"><strong>Total</strong></td><td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000; width: 155px;" align="right" valign="bottom">Rs.'+ total +'</td></tr><tr><td style="border-top: 1px solid #000000; border-bottom: 1px solid #a5a5a5; border-left: 2px solid #000000; border-right: 2px solid #000;" colspan="6" align="left" height="50">Please do the payment through NEFT on the MICROCONNECT`s Current a/c No. 268705000566. Bank - ICICI Bank - Chitlapakkam Branch. IFSC-Code - ICIC0002687. or click the link &amp; Pay&nbsp;</td></tr><tr><td style="border-top: 1px solid #000000; border-bottom: 1px solid #a5a5a5; border-left: 2px solid #000000; border-right: 2px solid #000;" colspan="6" align="left" height="50"><table style="border-collapse: collapse; width: 100%;" border="1"><tbody><tr><td style="width: 50%; text-align: center;"><strong>MICRCONNECT</strong></td><td style="width: 50%;" rowspan="3"><p style="text-align: center;">For MICROCONNECT</p><p><img style="display: block; margin-left: auto; margin-right: auto;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABtAIkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9UccUY4rK1W5nstLvZ7WNLm5jjeSOHOzzJAhwn41X8L69D4m0Sy1O3BjjuE3+XJjzI/8AYqbac4G/RRRVAFFFFABRRRQAUUUUAFFchBfy6f4ok0y8eS4tL/fc2ksg/wBXIn+siz+AkT/tp/zzrr6ACiiigAooooAKKKKAIpf9VXBeA4RpMlrbxx+Xb39jFepF6SCONJP/AGn/AORK9Crz3zV02x8N3qL+7s77+z5Pn+4kkj2//ozyvyq4gdZq0V4+mXkdi8aXvlSC3Mn3Ek/5Z1Q8KazH4k0Gz1JkWIzpiSFznypc7JI/qj70rpa5LQbZdL8Qa1YRsPss7pqMef4DJv8AM/8AIkZk/wC2lQB0Uk0cbojyeWX+5VojNc14s/4/fD//AGE4/wD0VJWL8RfibafDuzt4fIk1PWbw+XZaVa/6y4k/XYnvV8nPogOvvb6302zkurudLe3jG+SWWTYifjXCL8YbbVBOvhnS7/xKI/8Al8ij8iy/8CJfLjP/AGz8yvGrzWNR8VT3F74gkt9b+x/vbgSySf8ACO6R/wBM/wB3/wAfkn/LPy6v3lx4b1r4e+IfEenai/xB1jREjJt9XSSOztxv/gs/3caJ5e//AL910+x5PjOb2h3dx8WNSkXbb6t4QsQP9ZHHeyajLH/2zjjjob4q3hAz4r8NqZP9X9v0i7s4/wDv48tcv4Z+Jt3ZeDfE0EF3aXNxZ6P/AGrp9zbWkdtH/q/Lkjkj/wBX+7kregv7v4beFte1G48SS+JbmPS7a4is9Rk8x45JP3fmeZ/zzkkH+r/6Z1c4cn2A9oXL34gaq1il/wD2Gmt21rLFL9t8Mail7GO0nmRny3/1fmf89K7zw7440Pxjbl9H1aC+MY/eRQyfvI/+uiH50/GvKLDQ7a68QeIYfFBtGudItI7+TX9Lt3s7i3kkSTzI/MjP7zZ5fmfiOK4nSTHrCQzxw3t7p9pB9osNVsjHb69aW+PLjkkjj/4+I/Mjkjo9jCYc/IfWoIpa8i8B/E+eWex0nxHPaXMl/H5mla3ZfJZ6mn9zn/Vyf9M69bB4Fcc4ezNIT5x1FFFQahRRRQAg6Vxltpba94R1SwkxD59xqEcZ/uf6RL5b12Y6VgeE5PN0+5/6Z6heD/yYkpQAn8Oal/bOiWN9jy3uLeOQiqOobrXxZo05jHkzxT2eff5JE/8ARb0ng5vLttRsgI0+x38seIv7j/vI/wDyHJHR4xItdOs77eENnexSnP8AceTy5D/37kkq/t8gGD8W/F1p8P8ARNP128SS4js7z93FH1kkkjkjjT/yJXjnhjw9r3xA8S3kd49xBq1wgfXL2OX97pdtIfMj0+3z/q5JP+Wn/POk+Onj2a4+JWlaZZ6eb228OSpJHF/yzl1GRD9nT/gH7s/jJT/EeoWfgWPT/B3iPTdatTJL/aFv4r064MkkmoOS8snlx/8APOST/V/vP3f8GK9KFPkpw/nOafvzOg8EX9vffDjWf+EifSdI8JRmTS30u0t5Ek0uXzPL8uSTMnrHJ5n9+TfWV4e8Lf8ACA77vW9WuJrzULmXSdUs7xHk/te3Ef7uSzjjj8ySTY8Y/wC/lUdXs4tetR5nl6/rOryx29pe6De/Z7fW9n+s+2R/8s/LT95IK9w8I/D+28MGW/vbiTVvEV3/AMferSj95J/sR/8APOPj/Vx1lOfsw5Oc8i+HHgq+0bXNdt7Lw3dXsFxZR2+PFGoR28kdnJ5n7vy445P9ZJHJWxcfC7WNI0HVdHTwzZXNlqnlCR7HWpDexeWf3ZjkuItn7v8A5Zx9BXrd5i28V6dOpAiuIZbeTHeT93In/jkctM8Na02stqKTQSWktleSWbxyYxlDvjk/4HHJG9Zzrzfvmns4HkEelWN876VqUmrWOo3959sv7bxAI47jU5I4/wB3HHJH/o8kfmeX+7jrz+LxHrXwa0i3dIo5fF2p2Yt3spD+70yP/l38z/ppJ+8/d/8AxuvrDWtCsPE+nSWGqWkd9ZyffilTg14L8VfDbadpDaPq882oaV5iXmn3uf3s728ckhs7iT/ponmbJK6KFbn9yZnOHIRzaZLr3hnUNVwmraUZI49bt7GDy4rySOP95eWcn/PSOT/lp+7jk8uu/wDhV4onu2k8P6rfpf6hbwxXVnqJ6anZvxFcf7/8EnvXIaBpni7xvbaVqOnaoulaOLOK70SPSXH2O3kSX/j3uP8AlpJ+7+Tj93/rPaqniiwk+G/iC4ksbfK6K/8Abunwx/8ALSzkk8u9t/8AtnJJ5kf/AF0rOfv+4H8P3z6OxRis6z1GDVLKC7t286CeNJI5PVH6VLc38NqV8xwhf7if368/kOku0Vlwy3Ut3v8ALEdmI/8Alp/rHrUpgNxXA6V4nsfDulyPfP5YuNbuLKNI08zzJJLmTy678HNeTvbRy28U8g8ySPxXJ5f/AIEVVP3wOwhWO18aXcYAxf28dwmf4pI/kk/8ceKtDxFax33h7VrST7klvJH/AOQ6oeJoljm0vUgiCSzvE+c/885P3cn/AKHn/gFa+pRvPYXCIfnMbgVS3QHx98GNKk8ReJ/A99rM7317qmsahqskkknmSf6PH5cfmf8AbTzK67xh+zP4itWkk0TxJf63p0knmSaTe3n2eTzP+ekcn+r8z/tnHXCfAeK48O/ELwJO7yXVvqFvcfZ4v9Z5f7ySOT/0X5leheF/G/jK6+OvmeILY2Oi+Vcafbvc28lvbvGkn+sj3n/WSSRx/f8Awr1K7nTqfuzm5IHefCzS5LvxFrWoPcSXsemCPQ7S4udhlk8vH2iWTy/+Wkkn8f8A0zr1sygfJXkfwpR73QroWV5Jby2l3qMcksXzxvcSXDvvkT+Monl/9/K7Kw0salYyy6rZG1v98n+okxJGM7N8bphx5mzf/wDqrzqnx++a0/gJfHl3HpGiDVvLeX+y5Y7zy4+0f+rk/wDIbyVN4QtpbPSEkuR5d5cyyXEysOkjneU/7Z/6v/tnXO63o2ua3ZR6TdXVnNaRv5kl1cRF/tEf/LNJY0kjw/mH+D5H8v8Ag/1dbtloutBWTUNdST5dgFjafZ9/GOd8kn/jmylf3TQyLXxze3E9nHdaPJZpcGN47ozxvGY5DsR/9/e8Y8v/AKaVq+PvCsHjHwjqWiXGA9xHiKTP+rl/5Zyf991lWPhK40zRrzTpbsi12eXZykPJLHgyPHI+f9Y6fu/++K6zTZbu6s45J7X7FOfvxSSeZRPTWAHz54V8OS+NIfD51PTb+/8ACUmmW32OKzuUt7K3ljjk8yS4j8yOR5PMqxcaFZeDm023+0w3o/tuS3TStOleSO30+8j8vyvn/wCmkccn/XSuq8DeD7278Lx22na/Jo9vbahqEZjsYI5IzsvJR+73x/u/+AUzxF4UvPCx1TVL/VpNXtruXR4s3KRpIkkd7H/zzjjj8v8AeV2c/vnFyF34Hale6t8O7SxuHeyk0yWTTpIv+Wv7tynz/wBw8V6XZabBY79gzLJ/rJX++9effBt44Lzx5Cicp4nu3/7+Rxyf1r1Dsa4K3xnTAdRRRUGg0CvJ5rpbXQdRv3Aji0/xOZJJD/yzj+0/vJP+/chr1muJ8D20cqeJo5I0kik1i4+ST+PiOlCfIBteJLU6p4dvoEEcnmW8nl/9dP8AlnVzTtQivtPt7pB+7uIo5R/wOuMsPDOu+FJtmgNay6M/zjTb6SSL7Kf+mcib/k4/1dbnhC2udNtbqxnBYWly8ccmf9ZH9+P8vM2f9s6YHz38NfL0jxloMd+kcg0fXNU8Peb/AM85JP3kf/fz95HXtPir4k+HNNvJNHmF3rl55eZ9N0yzkvZEj/6apH/q/wDtpXmfxY8HyaL49uHtES2j8VRxizuSdkdvrFv+8tif+un+rq5/aN0NBvfFPhmeCKz1tJJ9Tj1a4jt7KwuEjjjk8x/L8zzPMj8vy/M8vg16FT95yTM/4Z1Xwi1jTI7zWNK04p/Z7yf2xphQCPzLa45OyPt5cgkj/KvVyK+VPBnjzTpNK8O6doz39lrulxxyWl7qqJHb3gkz5lmJP+WaSeX+78z/AJ5x1714f8WR+MrCOfTne28s7Lu2uY9lxbyf885I6yrUWmKE7nQ3mqW1rIEeX9+4+SKPmQ/8AqaGR7iIfu3ij/6acPSQ2McDSvGiLJK/mSEfx1drkNSrHEkX+/8A89Kw/GfiKHwh4Z1TV5lUpZwSSiM8eY/RI/8Agb8fjW3LNHbQySSP5caf8tDXi+reJ7Hxp4h0aa5aRfC1re5sYzbyPJqd5H/y1jj/AOfeLh/M/wBXWkIc5lUnyEOt+E/Gth8HNF0vw1NJ/bz5S9QGOPmQO8nzv9zy5P7lSrqOv6hofhDRvE8tudV1O8t5/LCSRyeXb/6RJ5n/AH7j/wC/lWvh98UtduvEsvhXX9KE95aSSW9xq2mxSSRof+WZkj8v935g/eeZ/q6wPifrt3qWq6pdacUIh/4prSP78uoXEkf2iSP0Ecf7vzP+enmV1w0nyTM/c5Dsf2f5G1TwrqutSR+X/bGsXmoRj/YeTYP/AEXXq56Vz/hDw9B4T8N6do1uP3VnBHEK3/4a46kuaehrDYdRRRUGhDXHfDy5SaTxPs/5Z65cR/8AouuwPUVkaLoVpoEuoi13br27kvZR/wBNJOv/AKBXDTqfGaG6DVK6uobGJ7ieZIo4/vySv5aCrFU77T4L6zltbyCO5tpI9jxSx+ZG/wDwCuinO5mefeNNR8G/ErwjeWR160uonf8A0eSyk82W3uY5P3ckaR/vPMjkx0ryDSvE97ZvqkfiGyt7KIyRxeIrK5g8ySzuP+WWqRR/885P3fmf9NK+m9G0TTdItzHpumWumx/887aJIh/45XF/Er4Zf8Jg9tq2jXUek+JtPSRLe5ki8yKSOQ/vIpY/40eu6nU5PcMpwPA/HHh278C+B7xII5ZLi9j+0ap4ijuPMt7uOST93JH5n/LxJ/0z/wBXHXRWHxLGueMdR/tS2vNB1GCOMWeo6dKBeDzJI447eWP/AFdx/rPM/ef6v95UGg6ld6beS+GpNFSSRPLkuPAurS/u/Mjk/wBZp1xJ+7kj/wCWnlyU9fBOneMPEFl/wiutJpurWlvJbyabrXmR6hFJJJJJJcSR/wDLSTy5K9BThU/iHN+8PSvD/jXxBdSMNNv/AA94sso55LOOT7RJp9w8kf8ArU2CORJPL/6Z+lW7/wCJXiCwF7BfaboeiXNtB9ok+2a35myP+/5ccfmV594A+Hninw14qis2022lSwk1G5s55F8u2kldLaOOV/L8zy96eZ8n/XSsLxd4P8Wa94t1mxezjk1PWJEl3G08yzSP7P8A62O4ePzI5EkjjTy65lThOdjXnqch2J8UaX4t1CBPEmsT+INO8yPzItNg8vSreSSXy447j955kn7yP/lp+7rnfiDY3x1u50nWdZu7WS41Ty7O2l/1clnJJ5cclv8A9c/M8uSP/lpHSar4O1G5/wCEq1/VZLDwRHq4+z3FtrcsZjkj8uPzJI/Lk/1kckfmR/8AXSuq8T/FKW/0yOTTrhNN06ONMeK9Ws/LEkn/AE528n7yST/yHWn8Of7sy/xlHSJb7wS+s+HoJ9Msr66k+23EumxyfYtEtPLjEkkm8/6x/wDlnF7j/lmK2PhD4Rg1q4sfEn2SS00LT45LfQLK4/1nlyf6y8k/6aSVR8BfDZ/E0Hn6tY3em+GjcfaI7HUZPMvdXkz/AMfF5J+Xlx17xHEkcewJxXPWqGsKZNRRRXIdIUUUUAQ0UUV5nIaBUo6VFUo6V1UzMKWiiukDlPF3gTRfHWn/AGTXdPivo4yWikkX95E/9+N+qH6V5jr3wR16O3ihtdS03xdpVvHi3svFluZLi3/653kf7yveMCjFXCpKBnyI+YR4d8ZeGbOOCPw34wshGP8AWaB4kjvI/wDtnHcVWW18Z67HJaTab8Rr2OT93HHLf2dnH5f/AE0k8uvqfFGK6PrL7ByHzj4X+E3ieeUSW/hvRfCUQj8uO91W5k1nUY/+ufmfu469I8MfB3S9C1OLWdQnu/EmvJHj+0tVl8ySP/rnH9yP8K9F4Bp1Z1K85ihDkCiiisDUKKKKACiiigD/2Q==" alt=""  height="80"/></p><p style="text-align: center;">Authorized Signatory</p></td></tr><tr><td style="width: 50%;"><p><strong>PAN - ABCFM7023N</strong><br /><strong>GST No. - 33ABCFM7023N1Z7</strong></p></td></tr><tr><td style="width: 50%;"><p style="text-align: center;">If you have any questions about this Proforma invoice, please contact<br /><strong>[K. Venkata Ramanan, +91 7358512225, kvenkat@microconnect.co.in ]</strong></p></td></tr></tbody></table></td></tr></tbody></table>';

         var pdfcreateData = 
         {"se_studentid":studentid,
         "html":invoicehtml,
         "invoice":invoiceno
        }
         services.proformaPdfCreate(pdfcreateData).then((res) =>{
               if(res.statuscode == 'NT-200'){
                   var action_url = res.path;
  var previewProformaHtml = '<table style="width: 600px;background: #600200;/* border-top: 4px solid #1977cc; */border: 1px solid black;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="background: #fff;" align="center" width="600"><a target="_blank"><img class="CToWUd" style="display: block; width: 50%; margin: 0px;" title="" src="https://api.microconnect.co.in/logo/microconnect1.png" alt="Microconnect" width="300"></a></td> <td style="background: #fff;" align="center" width="600"><img class="CToWUd" style="width: 50%; margin-top: 0px; display: block;" title="" src="https://api.microconnect.co.in/logo/silver_learning_partner.png" alt="" width="300"></td> </tr> </tbody></table><table style="width: 600px;background: #ffffff;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="background: #ffffff; width: 600px; padding: 20px; border-left: #cccccc 1px solid; border-right: #cccccc 1px solid;" align="center" width="600"> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> <strong>Hi '+name +','+ '-'+ studentid +'</strong></p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> Greetings from MICROCONNECT</p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> This is Proforma invoice for your payable amount details.</p><table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td align="center"> <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td align="center"><a class="f-fallback button button--green" style="display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box; background-color: #22bc66; border-top: 10px solid #22BC66; border-right: 18px solid #22BC66; border-bottom: 10px solid #22BC66; border-left: 18px solid #22BC66;" href="'+ action_url +'" target="_blank"  download>Download</a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> If you have any clarification regarding this proforma invoice No. feel free to write to us at <a href="mailto:support@microconnect.org">support@microconnect.org</a> or call / WhatsAPP us at the details given below</p><a style="text-decoration: none; text-align: center; display: table-cell; vertical-align: middle;" target="_blank"><img class="CToWUd" alt=""></a> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 26px; font-weight: normal; padding: 0 12px;"> Cheers!</p> <p style="color: #000000; text-align: left; font-size: 13px; line-height: 10px; font-weight: normal; padding: 0 12px;"> Team Microconnect</p> </td> </tr> </tbody></table><table style="width: 600px;border-top: 1px solid black;" border="0" width="600" cellspacing="0" cellpadding="0" align="center"> <tbody> <tr> <td style="border-left: #cccccc 1px solid; border-right: #cccccc 1px solid;" align="center" width="600"><a target="_blank"><img class="CToWUd" style="width: 100%; display: block;" title="" src="https://api.microconnect.co.in/logo/microconnectword.png" alt="e" border="0"></a> <div class="a6S" dir="ltr" style="opacity: 0.01;">&nbsp;</div> </td> </tr> </tbody></table>'

         $scope.data.textInput = previewProformaHtml;
               }else{

               }
         })

         

       
    }

    $scope.proformaInvoiceTemplateCreate = function(){

        services.getEditPaymentShedule($scope.mailSendData.studentid).then((res) =>{
           
            if(res.length == 0){
                services.toast('warning','Schedule not create');
            }else{
                $scope.templateCreate(res[0]);

            }
           
        })

    }

  $scope.chooseMailTemplate = function(mailtemplate){
        
       if(mailtemplate == 'paymentlink'){
           $scope.ordercheck('yes');
       }else if(mailtemplate == 'proformainvoice'){
        $scope.ordercheck('no');
        $scope.proformaInvoiceTemplateCreate()
      }else if(mailtemplate == 'normal'){
        $scope.ordercheck('no');
      }
  }

    $scope.discard = function(paylink){
        //if(paylink == 'yes'){//06-01-2021-student_payment_invoice_function_change
        if(paylink == 'pi' || paylink == 'fi' ){//06-01-2021-student_payment_invoice_function_change
            var discardData = {
                discard : true,
                orderid: $scope.orderid
            }
            services.discardPaymentLink(discardData).then((res) => {
                if(res.statuscode == 'NT-200'){
                    $scope.data.textInput =''; //06-01-2021-student_payment_invoice_function_change
                    $scope.paylink = 'none';
                    $scope.mailSendData = {};
                }
               
            })
        }else{
            $scope.data.textInput = '';
            $scope.mailtemplate = '';
        }

        $scope.hideMailEntryModal();
    }

    $scope.viewStudentData = function (enroll_id, userViewType) {
        if (enroll_id != "" || enroll_id != null || enroll_id != undefined) {

            // var findindex =  $scope.studentExportData.findIndex(x => x.se_studentid == enroll_id);

            // $scope.studentViewData  = $scope.studentExportData[findindex];

            // $scope.showStudentViewModal();
            //$state.go('users/'+enroll_id);
            setLocalStorage('userViewType', userViewType);
            $location.path('/student/' + enroll_id)
        }

    };
    

    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            //if( $scope.paylink == 'yes'){//06-01-2021-student_payment_invoice_function_change
            if( $scope.paylink == 'pi' || $scope.paylink == 'fi' ){
                $scope.mailSendData.pdflink = $scope.pdflink;
            }
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.mailSendData = {};
                    $scope.data.textInput = '';
                    $scope.mailtemplate = '';
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };

    $scope.checkPaymenyShedule = function(studentid,paymentpageid){
        var allowedMenus = getLocalStorageData('allowedMenus');
       
        
        var index = allowedMenus.findIndex(x => x.id == paymentpageid);
        if (index == -1) {
            services.toast('warning','Your not allowed to payment shedule')
        }else{
            services.getEditPaymentShedule(studentid).then((res) =>{
                setLocalStorage('paymentSheduleRedirectData',{
                    'frompath':'studentlist',
                    'studentid':studentid,
                })
                if(res.length == 0){
                    setLocalStorage('userViewType','new');
                   
                    $state.go('home.paymentshedule', {studentid : 0});
                }else{
                    $scope.paymentshedule = res[0];
                   // setLocalStorage('userViewType','edit');
                   setLocalStorage('userViewType','view');
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                }
               
            })

        }

    }


}]);


