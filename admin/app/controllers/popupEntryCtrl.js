/****syllabus list */
admin.controller('popupentryctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.popupEntryData = {};
    services.getPopUpData().then(function (res) {
        debugger;
        $scope.popupData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#popuplist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
           //extraPlugins : "base64image"
        }
    };

    $scope.showpopupEntryModal = function () {
        $('#popupEntryModal').modal('show')
    };
    $scope.hidepopupEntryModal = function () {
        $('#popupEntryModal').modal('hide')
    };

    $scope.showPopupViewmodal = function (data) {


        $scope.popupcontent = data.content;
        $scope.popupRedirectUrl = data.url;
        $('#popupViewmodal').modal('show');
    };
    $scope.hidePopupViewmodal = function () {
        $('#popupViewmodal').modal('hide')
    };

    $scope.goToUrl = function (url) {
        window.open(url, "_blank")
    }

    $scope.showPopupDeletmodal = function(show){
        if(show){
            $('#popupDeleteModal').modal('show');
        }else{
            $('#popupDeleteModal').modal('hide');
        }
       
    };
    //popupViewmodal
    $scope.popupModal = function (id) {
        if (id == 0) {
            $scope.title = "POPUP Entry";
            $scope.buttonText = "Save";
            $scope.popup_id = 0;
            $scope.popupEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showpopupEntryModal();
        } else {
            $scope.title = "Edit POPUP  ";
            $scope.buttonText = "Update";
            $scope.popup_id = id;
            services.getEdiPopUpData($scope.popup_id).then((res) => {
                // console.log(res);
                $scope.popupEntryData = res;
                $scope.data.textInput = $scope.popupEntryData.content;
                $scope.showpopupEntryModal();
            })
        }

    }


    $scope.savePopup = function (popup_id) {
        debugger;
        $scope.popupEntryData.content = $scope.data.textInput;
        if (popup_id == 0) {
            services.insertSavePopupData($scope.popupEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hidepopupEntryModal();
                    services.toast('success', res.message);
                    $scope.popupData.push($scope.popupEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updatePopUpData($scope.popupEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hidepopupEntryModal();
                    services.toast('success', res.message);
                    var index = $scope.popupData.findIndex(x => x._id == popup_id);
                    if (index > -1) {
                        $scope.popupData[index] = $scope.popupEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };


    $scope.deletePopup = function(id){
         
        $scope.showPopupDeletmodal(true) //show true;
        $scope.submitFormDelete = function () {
            services.deletePopup(id).then(function (res) {
                $('#crudmodal').modal('hide');
                $scope.showPopupDeletmodal(false)//show false
                if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                    $rootScope.pageReload();
                } else{
                    services.toast('warning', res.message);
                }
               
             
            });
        }

    };





}]);