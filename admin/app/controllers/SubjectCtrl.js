/****ACCA Subject list */
admin.controller('accasubjectlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$sce', function ($scope, services, $window, $location, $rootScope, $sce) {
    debugger;

    $scope.subjectEntryData = {};
    services.getACCASubjectData().then(function (res) {
        debugger;
        $scope.subjectData = res;
        $scope.updateSubGrid();
    });
    $scope.encodeData = function (data) {
        return $sce.trustAsHtml(data);
    }
    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#subjectlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    }
    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };

    $scope.showSubjectModal = function () {
        $('#subjectsentryModal').modal('show')
    }
    $scope.subjectModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "ACCA Subject Entry";
            $scope.buttonText = "Save";
            $scope.subject_id = 0;
            $scope.subjectEntryData = {};
            $scope.data.textInput = '';
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showSubjectModal();
        } else {
            // $scope.title = "Edit ACCA Subject ";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit ACCA Subject';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View ACCA Subject';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }

            $scope.buttonText = "Update";
            $scope.subject_id = id;
            services.getEdiACCASubject($scope.subject_id).then((res) => {
                // console.log(res);
                $scope.subjectEntryData = res;
                $scope.data.textInput = $scope.subjectEntryData.subtitle;
                $scope.showSubjectModal();
            })
        }

    }
    $scope.hideSubjectModal = function () {
        $('#subjectsentryModal').modal('hide')
    };

    $scope.saveACCASubject = function (subject_id) {
        $scope.subjectEntryData.subtitle = $scope.data.textInput;
        if (subject_id == 0) {
            services.insertACCASubject($scope.subjectEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideSubjectModal();
                    services.toast('success', res.message);
                    $scope.subjectData.push($scope.subjectEntryData);
                    $rootScope.pageReload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateACCASubject($scope.subjectEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideSubjectModal();
                    services.toast('success', res.message);
                    var index = $scope.subjectData.findIndex(x => x._id == subject_id);
                    if (index > -1) {
                        $scope.subjectData[index] = $scope.subjectEntryData;
                    }
                    $rootScope.pageReload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    }
}]);