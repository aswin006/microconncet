admin.directive('sidemenuComponent', function () {
    return {
        restrict: 'EA',
        controller: 'sidemenuctrl',
        templateUrl: './views/partials/sidemenu/sidemenu.html'
    };
});

admin.controller('sidemenuctrl', ['$scope', 'services', '$rootScope', '$location', '$sce', function ($scope, services, $rootScope, $location, $sce) {

    services.menuJsonData().then((res) => {
        debugger;
        $scope.menus = res.menu;
        var userdetails = getLocalStorageData('adminUserdata');
        $scope.allowedMenus = [];
        /***begin-for developing testing purpose  */
        // if (login_data.username === __env.defaultEmail) { 
       if (userdetails.email === 'developer@microconnect.co.in') {
            for (let i of userdetails.menu) {
                var index = $scope.menus.findIndex(x => x.id === i);
                if (index > -1) {
                    $scope.allowedMenus.push($scope.menus[index])
                }
            }
             // $scope.allowedMenus.push($scope.menus)
             /***end-for developing testing purpose  */
        } else {
            for (let value of userdetails.pages) {
                var selectedPageId = value.pageid
                var index = $scope.menus.findIndex(x => x.id == selectedPageId);
                if (index > -1) {
                    $scope.menus[index].enableviews = value;
                    $scope.allowedMenus.push($scope.menus[index])
                }
            }
        }


       // console.log($scope.allowedMenus);
        setLocalStorage('allowedMenus',$scope.allowedMenus);

        // Activate sidebar treeview toggle
        var treeviewMenu = $('.sidebar-menu');

        $("[data-toggle='treeview']").click(function (event) {
            event.preventDefault();
            if (!$(this).parent().hasClass('active', 'menu-open')) {
                treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('active', 'menu-open');
            }
            $(this).parent().toggleClass('active');
            $(this).parent().toggleClass('menu-open');
        });

        // Set initial active toggle
        $("[data-toggle='treeview.'].menu-open").parent().toggleClass('active', );


    })

    $scope.setMenu = function (item) {
        //alert('ok')
        $scope.enableviews = item.enableviews;
        setLocalStorage('enableViews', $scope.enableviews);
        // console.log(item)
    }

    $scope.getCurrentActivePath = function (currentpath) {
        if ($rootScope.current_path == currentpath) {
            return true;
        } else {
            return false;
        }

    }




}]);