/****entry and edit user */
admin.controller("userctrl", ['$scope', '$rootScope', '$location', '$stateParams', 'services', 'user', '$http', '$filter', '$state', function ($scope, $rootScope, $location, $stateParams, services, user, $http, $filter, $state) {
    debugger;

    $scope.userViewType = getLocalStorageData('userViewType');

    var userID = ($stateParams.userid) ? ($stateParams.userid) : 0;

    $scope.buttonText = (userID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.return_data = "Select";
    $scope.userID = userID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (userID != 0) ? 'Edit  User' : 'User Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View  User';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    }

    //$scope.userRole = userrole;




    $scope.setCountry = function (countrycode) {
        $scope.userdata.se_address.country = countrycode;
        $scope.userdata.se_country = countrycode;
        // if (countrycode == "IN") {
        //     alert(countrycode)
        // } else {
        //     alert('other')
        // }

    };

    $scope.setState = function (state) {
        $scope.userdata.se_address.state = state;
        $scope.userdata.se_state = state;
        var stateindex = $rootScope.commonJsonData.indianStates.states.findIndex(x => x.state == state);
        $scope.districts = $rootScope.commonJsonData.indianStates.states[stateindex].districts;
        // alert( $scope.userdata.se_state);

    }
    $scope.setDistrict = function (city) {
        $scope.userdata.se_address.city = city;
        $scope.userdata.se_city = city;

        //alert(city);

    }

    if (userID != 0) {
        debugger;
        $scope.getuser = user;
        var original = $scope.getuser;
        original._id = userID;
        $scope.userdata = angular.copy(original);
        $scope.userdata._id = userID;

        $scope.userimage = $rootScope.addImgDefUrl($scope.userdata.userimage);
    } else {
        $scope.userdata = {};
    }


    $scope.cancel = function () {
        $location.path('/userlist');
    }
    $scope.isClean = function () {
        return angular.equals(original, $scope.user);
    }
    $scope.user_cancel = function () {
        debugger;
        if (userID != 0) {
            $location.path('/userlist')
        } else {
            $scope.user = {};
            $scope.enrollForm.$setPristine();
            $scope.enrollForm.$setUntouched();
            // $window.location.reload();
        }

    };


    $scope.uploadedImage = function (imagefiles, id) {
        debugger;
        showLoader();
        if (imagefiles) {
            var profileImage = imagefiles;
            $scope.imageFileSize = Math.round((profileImage[0].size / 1024));
            var ImageFileSizeLimit = 5 * 1024;
            if ($scope.imageFileSize < ImageFileSizeLimit) {

                if (id == 'marksheet') {
                    $scope.marksheet = imagefiles
                    $scope.marksheetSrc = $scope.marksheet[0];
                    services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
                        debugger;
                        $scope.userdata.se_marksheet = res
                        $scope.marksheet = $rootScope.addImgDefUrl(res)
                        // console.log(res);
                        hideLoader();

                    })
                } else if (id == 'aadharcard') {
                    $scope.aadharcard = imagefiles
                    $scope.aadharcardSrc = $scope.aadharcard[0]
                    services.uploadAadhar($scope.aadharcardSrc).then((res) => {
                        debugger;
                        $scope.userdata.se_aadharimage = res;
                        $scope.aadharcard = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'studentphoto') {
                    $scope.studentphoto = imagefiles
                    $scope.studentphotoImgSrc = $scope.studentphoto[0];
                    services.uploadStudentPhoto(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.userdata.se_studentimage = res;
                        $scope.studentphoto = $rootScope.addImgDefUrl(res)
                        // console.log(res)
                        hideLoader();
                    })
                } else if (id == 'bonofide') {
                    $scope.bonofide = imagefiles;
                    $scope.bonofideSrc = $scope.bonofide[0]
                    services.uploadBonofideLetter(imagefiles[0]).then((res) => {
                        debugger;
                        $scope.userdata.se_bonafide = res;
                        $scope.bonofideImage = $rootScope.addImgDefUrl(res)
                        console.log(res)
                        hideLoader();
                    })
                }
            } else {
                alert('file size is more than 5mb');
                hideLoader();
            }
        }



    }

    // $scope.uploadedFile = function (element, id) {
    //     debugger;
    //     showLoader();
    //     var maxSize = 5 * 1024;
    //     $scope.currentFile = element.files[0];
    //     var fileSize = Math.round((element.files[0].size / 1024));
    //     var reader = new FileReader();
    //     if (fileSize > maxSize) {
    //         alert('file size is more than 5mb');
    //         hideLoader();
    //         return false;
    //     } else {
    //         reader.onload = function (event) {


    //             $scope.$apply(function ($scope) {
    //                 //$scope.files = element.files;
    //                 if (id == 'marksheet') {
    //                     $scope.marksheet = event.target.result
    //                     $scope.marksheetSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.marksheetSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_marksheet = res
    //                         console.log(res);
    //                         hideLoader();

    //                     })
    //                 } else if (id == 'aadharcard') {
    //                     $scope.aadharcard = event.target.result
    //                     $scope.aadharcardSrc = element.files[0]
    //                     services.uploadAadhar($scope.aadharcardSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_aadharimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'studentphoto') {
    //                     $scope.studentphoto = event.target.result
    //                     $scope.studentphotoImgSrc = element.files[0];
    //                     services.uploadStudentPhoto($scope.studentphotoImgSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_studentimage = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 } else if (id == 'bonofide') {
    //                     $scope.bonofide = event.target.result
    //                     $scope.bonofideSrc = element.files[0]
    //                     services.uploadBonofideLetter($scope.bonofideSrc).then((res) => {
    //                         debugger;
    //                         $scope.userdata.se_bonafide = res;
    //                         console.log(res)
    //                         hideLoader();
    //                     })
    //                 }
    //             });
    //         }
    //         reader.readAsDataURL(element.files[0]);
    //     }
    // }



    /****show/hide password */


    $scope.saveEnrolluser = function () {
        debugger;

        if ($scope.userID == 0) {

        } else {

            $scope.userdata.se_parentmobileno = {
                dialcode: $scope.parentnum_dialcode,
                mobileno: $scope.parentnum_dialcode + $scope.parentmobileno
            }
            $scope.userdata.se_studentmobileno = {
                dialcode: $scope.studentnum_dialcode,
                mobileno: $scope.studentnum_dialcode + $scope.studentmobileno
            }
            services.StudentEnrollUpdate($scope.userdata).then((res) => {
                var msg = "Update Successfully";
                services.toast('success', msg);
                //  $location.path('/userlist');
                $state.go('home.userslist');
            });

        }


    };

    $scope.showImgModal = function () {
        $('#imgViewModal').modal('show')
    };
    $scope.hideImgModal = function () {
        $('#imgViewModal').modal('hide')
    };





    $scope.viewImageFullScreen = function (id, imgUrl) {
        if (imgUrl) {
            if (id == 'marksheet') {
                $scope.imgModalTitle = '10th Mark Sheet';
            } else if (id == 'aadharcard') {
                $scope.imgModalTitle = 'Aadhar Card'
            } else if (id == 'studentphoto') {
                $scope.imgModalTitle = 'Student Recent Photo'
            } else if (id == 'bonofide') {
                $scope.imgModalTitle = 'School Bonafide Letter'
            }
            $scope.imgUrl = imgUrl;
            $scope.showImgModal();
        }
    }

}]);

/****user list */
admin.controller('userlistctrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state', 'userrole', function ($scope, services, $window, $location, $rootScope, $state, userrole) {
    debugger;

    $scope.userEntryData = {};
    $scope.mailSendData = {};
    $scope.excelheaders = ["Name", "Email", "Mobile No", "Role", "Status"];


    // "username":"kannadhasanthondur",
    // "password":"jkkk",
    // "email":"kanna@gmail.com",
    // "phonenumber":"875484684",
    // "userimage":"",
    // "status":true,
    // "roleid":"12522"

    $scope.userExportData = [];
    $scope.datatrue = false;
    $scope.userRole = userrole;



    $scope.data = {
        textInput: '',
        options: {
            language: 'en',
            allowedContent: true,
            entities: false
        }
    };
    services.getUserData().then(function (res) {
        debugger;
        $scope.userdata = res;

        if ($scope.userdata != null && $scope.userdata.length != 0) {
            $scope.datatrue = true;
            // for (var studentdata of $scope.userdata) {
            //     var tempObj = {
            //         "ACCA Id": studentdata.acca_id,
            //         "EXP Id": studentdata.exp_id,
            //         "Enrollment Number": studentdata.se_studentid,
            //         "Name": studentdata.se_name,
            //         "Aadhar Card": studentdata.se_aadharno,
            //         "Date of Birth": unixTodateTime(parseInt(studentdata.se_dateofbirth)),
            //         "Gender": genderget(studentdata.se_gender),
            //         "Parent Mobile No": studentdata.se_parentmobileno.mobileno,
            //         "Student Mobile No": studentdata.se_studentmobileno.mobileno,
            //         "Parent Email": studentdata.se_parentemail,
            //         "Student Email": studentdata.se_studentemail,
            //         "School": studentdata.se_school,
            //         "School's City": studentdata.se_schoolcity,
            //         "School Pincode": studentdata.se_schoolpincode,
            //         "Studying": studentdata.se_class,
            //         "Year of Pass": studentdata.se_yearofpass,
            //         "Address-1": studentdata.se_address.line1,
            //         "Address-2": studentdata.se_address.line2,
            //         "City": studentdata.se_city,
            //         "Country": studentdata.se_country,
            //         "State": studentdata.se_state,
            //         "Pincode": studentdata.pincode,
            //         "Status": studentdata.se_status == true ? 'Active' : 'Inactive',
            //     }

            //     $scope.studentExportData.push(tempObj);

            // }
        }
        $scope.updateSubGrid();
    });



    $scope.showMailEntryModal = function () {
        $('#mailEntryModal').modal('show')
    };
    $scope.hideMailEntryModal = function () {
        $('#mailEntryModal').modal('hide')
    };

    $scope.showUserViewModal = function () {
        $('#userDataViewModal').modal('show')
    };
    $scope.hideUserViewModal = function () {
        $('#userDataViewModal').modal('hide')
    };

    $scope.showPassword = false;

    $scope.toggleShowPassword = function () {
        $scope.showPassword = !$scope.showPassword;
    }

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#userlist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };

    $scope.userModal = function (id, viewType) {
        if (id == 0) {
            $scope.title = "User Creation";
            $scope.buttonText = "Save";
            $scope.user_id = 0;
            $scope.userEntryData = {};
            $scope.data.textInput = '';
            $scope.userViewType = viewType;
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            $scope.showUserViewModal();
        } else {
            $scope.title = "Edit User  Details";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit User Details';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View User Details';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.user_id = id;
            services.getEditUserData($scope.user_id).then((res) => {
                // console.log(res);
                $scope.userEntryData = res;
                $scope.showUserViewModal();
            })
        }

    };

    $scope.createNewUser = function () {
        if ($scope.userRole.length != 0) {
            // $location.path('/users/0');
            $scope.userModal(0, 'new')
        } else {
            services.toast('danger', 'please create user role ');
            $location.path('/userrolecreation');
        }
    }



    $scope.viewUserData = function (enroll_id, userViewType) {
        if (enroll_id != "" || enroll_id != null || enroll_id != undefined) {

            // var findindex =  $scope.studentExportData.findIndex(x => x.se_studentid == enroll_id);

            // $scope.studentViewData  = $scope.studentExportData[findindex];

            // $scope.showStudentViewModal();
            //$state.go('users/'+enroll_id);
            setLocalStorage('userViewType', userViewType);
            $location.path('/users/' + enroll_id)
        }

    };

    $scope.saveUser = function (user_id) {
        debugger;

        if (user_id == 0) {
            services.insertUserData($scope.userEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hideUserViewModal();
                    services.toast('success', res.message);
                    //$scope.faqData.push($scope.faqEntryData);
                    // $location.reload();
                    //$scope.userdata.push($scope.userEntryData)
                    //  $scope.updateSubGrid();
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updateUserData($scope.userEntryData).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hideUserViewModal();
                    services.toast('success', res.message);
                    // var index = $scope.userdata.findIndex(x => x._id == user_id);
                    // if (index > -1) {
                    //     $scope.userdata[index] = $scope.userEntryData;
                    // }
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };



    $scope.composeMail = function (data) {
        $scope.mailSendData.email = data.email;
        $scope.showMailEntryModal();
    };
    $scope.mailSend = function () {
        if ($scope.data.textInput != '') {
            $scope.mailSendData.message = $scope.data.textInput;
            services.mailSend($scope.mailSendData).then((res) => {
                if (res.statuscode == 'NT-200') {
                    services.toast('success', res.message);
                    $scope.hideMailEntryModal();

                } else {
                    services.toast('success', res.message)
                }
            })
        } else {
            alert('Please enter message')
        }

    };


}]);