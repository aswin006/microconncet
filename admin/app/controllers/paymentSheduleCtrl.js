/****payment shedule entry */
admin.controller('paymentScheduleCtrl', ['$scope', 'services', '$window',  '$location', '$rootScope','$sce','$stateParams','$state', function($scope, services, $window,  $location, $rootScope,$sce,$stateParams,$state) {
    debugger;
    $scope.userViewType = getLocalStorageData('userViewType');

    var studentID = ($stateParams.studentid) ? ($stateParams.studentid) : 0;

    $scope.buttonText = (studentID != 0) ? 'Update ' : 'Save';
    $scope.buttonCancel = ($scope.userViewType == 'edit') ? 'Cancel' : 'Back';
    $scope.studentid = studentID;

    if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
        $rootScope.title = (studentID != 0) ? 'Edit  Student Payment Schedule' : 'Student Payment Maintenance';
        $scope.enableBtn = true;
        $scope.disableEdit = false;
    } else {
        $rootScope.title = 'View Student Payment Schedule';
        $scope.enableBtn = false;
        $scope.disableEdit = true;
    };

    $scope.scheduleDateFormatChangeTimeToUnix = function(sheduledata){
        var changed_data = [];
             for(var i in sheduledata){
                 var date = timeToUnix(sheduledata[i].duedate);
                 sheduledata[i].duedate = date;
                 changed_data.push(sheduledata[i])
             }
         return changed_data;
    };

    $scope.scheduleDateFormatChangeUnixToTime = function(sheduledata){
        var changed_data = [];
             for(var i in sheduledata){
                 var date = unixToHtmlDate(sheduledata[i].duedate);
                 sheduledata[i].duedate = date;
                 changed_data.push(sheduledata[i])
             }
         return changed_data;
    };

    $scope.minDateSet = function(index ){
        if(index == 0){
           // var today = new Date(new Date().setHours(0,0,0,0));
           var minDate = new moment();
           // $scope.minDate = new Date();
        }else{
            var min = $scope.sheduledata[index -1].duedate;
            // $scope.minDate = new Date(min);
            var minDate = new moment(min);
        }
    
        $scope.min = minDate.format("YYYY-MM-DD");
        //$scope.max = maxDate.format("YYYY-MM-DD");
        return  $scope.min ;
    }

    $scope.updateSubGrid = function() {
        setTimeout(function() {
            $('#paymentList').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10,15, 25, 50, -1],
                    [5, 10,15, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };
    
    if (studentID != 0) {
        debugger;
        $scope.paymentshedule =  getLocalStorageData('paymentshedule');
        $scope.redirectData = getLocalStorageData('paymentSheduleRedirectData');
        var original = $scope.paymentshedule ;
        $scope.studentPaymentSheduleData = angular.copy(original);
        $scope.sheduledata =  $scope.scheduleDateFormatChangeUnixToTime($scope.studentPaymentSheduleData.schedule);
        if($scope.userViewType == 'view'){
            $scope.paymentdata =  $scope.studentPaymentSheduleData.payments;
        }
        $scope.selectDisable = true;
        $scope.updateSubGrid();
    } else {
        services.getAllNonSheduleStudents().then((res) =>{
            $scope.studentList = res;
        })

        $scope.studentPaymentSheduleData = {
            se_studentid:"",
            totalamount:0,
            scholaramount:0,
            payableamount:0,
            acc_amount:0,
            exp_amount:0,
            gst_amount:0,
            schedule:"",
        };
        $scope.redirectData = getLocalStorageData('paymentSheduleRedirectData');
        if( $scope.redirectData.frompath == 'studentlist'){
            $scope.selectDisable = true;
            $scope.studentPaymentSheduleData.se_studentid =  $scope.redirectData.studentid;
        }else{
            $scope.selectDisable = false;
        }
        $scope.sheduledata = [
            {
                amount: 0,
                duedate: "",
                name: "",
                paidamount: 0,
                paiddate: "",
            }
       ]
    }
    $scope.cancel = function () {
        $state.go('home.paymentschedulelist');
    };
    $scope.backbtnPress = function(){
        if( $scope.redirectData.frompath == 'studentlist'){
            $state.go('home.studentslist');
        }else{
            $state.go('home.paymentschedulelist');
        }
        clearLocalStorage('paymentSheduleRedirectData')
    }

    // $scope.addShedule = function(){

    //     $scope.sheduledata.push( {
    //         amount: 0,
    //         duedate: "",
    //         name: "",
    //         paidamount: 0,
    //         paiddate: "",
    //     })
    // };
    
    $scope.addShedule = function(){
        if($scope.sheduledata.length >= 1){
             var currentEnteredAmount = 0;
            for(var i = 0 ; i < $scope.sheduledata.length; i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }

        }
        if($scope.studentPaymentSheduleData.payableamount - currentEnteredAmount != 0){
            $scope.sheduledata.push( {
                amount: $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount,
                duedate: "",
                name: "",
                paidamount: 0,
                paiddate: "",
            });
            $scope.draftScheduleAmount = angular.copy($scope.sheduledata);
        }else{
            alert('already total amount is scheduled')
        }
        
    };
    $scope.removeShedule = function(index ){
        if(index == 0){
             alert('Atleast one schedule should be present')
        }else{

            $scope.sheduledata[index - 1].amount =  $scope.sheduledata[index - 1].amount +   $scope.sheduledata[index].amount ;
            $scope.sheduledata.splice(index,1);
        }
    };
    $scope.setSheduleAmount = function(){
         if( $scope.sheduledata[0].amount == 0){
            $scope.sheduledata[0].amount = $scope.studentPaymentSheduleData.payableamount;
         }else{
             for(var i = 0 ; i < $scope.sheduledata.length ;i++){
                $scope.sheduledata[i].amount = $scope.studentPaymentSheduleData.payableamount / 2
             }
             
         }
    };
    

    $scope.setPayableAmount = function(){
       
            $scope.studentPaymentSheduleData.payableamount = ($scope.studentPaymentSheduleData.totalamount - $scope.studentPaymentSheduleData.scholaramount) + $scope.studentPaymentSheduleData.gst_amount;
            $scope.sheduledata[0].amount = $scope.studentPaymentSheduleData.payableamount;
    }
    
    $scope.setShedulePayment = function(index ,amount){
        var currentIndex = index;
        var nextindex = currentIndex + 1;

        //if($scope.sheduledata[nextindex].amount != undefined){
            if($scope.sheduledata[nextindex] && currentIndex !=0){
             var currentEnteredAmount = 0;
            for(var i = 0 ; i < nextindex;i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }
             
            var nextBalamount = $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount
            if(0 < nextBalamount) {
                $scope.sheduledata[nextindex].amount = nextBalamount;
               // $scope.draftScheduleAmount = angular.copy($scope.sheduledata);
            }else{
                alert('excess amount entered');
               
               // $scope.sheduledata.splice(nextindex, 1);
            //     var previndex = index - 1;
            //    if(previndex < 0 ){
            //     $scope.sheduledata[index].amount = $scope.studentPaymentSheduleData.payableamount -  $scope.sheduledata[previndex].amount ;
            //    }
          //   $scope.sheduledata[index].amount =   $scope.draftScheduleAmount[index].amount;
               
          $scope.sheduledata =   angular.copy($scope.draftScheduleAmount);

            }
           
        }else{
            currentEnteredAmount = 0;
            if(currentIndex == 0){
                 var length = $scope.sheduledata.length;
            }else{
                var length = nextindex;
            }
            for(var i = 0 ; i < length;i++){
                currentEnteredAmount += $scope.sheduledata[i].amount;
            }

             if($scope.studentPaymentSheduleData.payableamount < currentEnteredAmount){
                 alert('excess amount entered'); 
                 $scope.sheduledata[index].amount =  $scope.draftScheduleAmount[index].amount;
             }

        }
    //     if($scope.sheduledata.length != 0){
    //         var currentEnteredAmount = 0;
    //        for(var i = 1 ; i < $scope.sheduledata.length;i++){
    //         //   currentEnteredAmount += $scope.sheduledata[i].amount;
    //         var previousSheduleAmount = $scope.sheduledata[i - 1].amount
    //         $scope.sheduledata[i].amount = ($scope.studentPaymentSheduleData.payableamount - previousSheduleAmount) 
    //        }
          
    //      //  $scope.sheduledata[nextindex].amount = $scope.studentPaymentSheduleData.payableamount - currentEnteredAmount
    //    }
    }

    $scope.savePaymentData = function () {
        debugger;
        $scope.studentPaymentSheduleData.schedule = $scope.scheduleDateFormatChangeTimeToUnix($scope.sheduledata); 
        if ($scope.studentid == 0) {
            services.insertPaymentShedule($scope.studentPaymentSheduleData).then((res) => {
               if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                  //  $state.go('home.paymentschedulelist');
                    $scope.backbtnPress();
               }else{
                services.toast('warning', res.message);
               }
               
            });
        } else {
           
            services.insertPaymentShedule($scope.studentPaymentSheduleData).then((res) => {
                if(res.statuscode == 'NT-200'){
                    services.toast('success', res.message);
                   // $state.go('home.paymentschedulelist');
                   $scope.backbtnPress();
               }else{
                services.toast('warning', res.message);
               }
            });

        }


    };


    $scope.tableFilter = function(value){
        var table = $('#paymentList').DataTable();
        if(value == 'All'){
            table.column(3).search('').draw();
        //    table
        //    .column( 7 )
        //    .data()
        //    .filter( '' );
        }else{
    //         table
    // .column( 7 )
    // .data()
    // .filter( value );
    table.column(3).search(value).draw();
        }
       
    }
    
  
}]);
/****payment shedule list */
admin.controller('paymentschedulelistctrl', ['$scope', 'services', '$window',  '$location', '$rootScope','$sce','$state', function($scope, services, $window,  $location, $rootScope,$sce,$state) {
    debugger;
  
    services.getAllPaymentShedule().then(function(res) {
        debugger;
        $scope.PaymentSheduleData = res;
        $scope.updateSubGrid();
       
    });
    $scope.updateSubGrid = function() {
        setTimeout(function() {
            $('#paymentschedulelist').DataTable({
                // "scrollY": ($scope.table_box_height - 185),
                // "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
            });
        }, 0);
    };
    //$scope.minDate = '2020-12-12';
    
   

    $scope.createPaymentShedule = function(studentid,viewType){
         $scope.userViewType = viewType;
         setLocalStorage('paymentSheduleRedirectData',{
            'frompath':'paymentlist',
            'studentid':studentid,
        })
        if(studentid == 0){
            setLocalStorage('userViewType',$scope.userViewType);
            $state.go('home.paymentshedule', {studentid : studentid});
        }else if($scope.userViewType == 'edit'){
            
            services.getEditPaymentShedule(studentid).then((res) =>{

                if(res.length == 0){
                    setLocalStorage('userViewType',$scope.userViewType);
                    $state.go('home.paymentshedule', {studentid : 0});
                }else{
                    $scope.paymentshedule = res[0];
                    setLocalStorage('userViewType',$scope.userViewType);
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                }
            })
                
           
        }else if( $scope.userViewType == 'view'){
            
            services.viewPaymentSchedule(studentid).then((res) =>{
                    $scope.paymentshedule = res[0];
                    setLocalStorage('userViewType',$scope.userViewType);
                    setLocalStorage('paymentshedule', $scope.paymentshedule);
                    $state.go('home.paymentshedule', {studentid : studentid});
                
            })
        }

    }
  
}]);