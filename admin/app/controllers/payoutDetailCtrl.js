admin.controller('payoutdetailslistCtrl', ['$scope', 'services', '$window', '$location', '$rootScope', '$state','$filter',function ($scope, services, $window, $location, $rootScope, $state,$filter) {
    debugger;

    $scope.payoutDetail = {
        "se_studentid":"",
        "acca_amount":0,
        "exp_amount":0,
        "gst_amount":0,
        "acca_paiddate":"",
        "exp_paiddate":"",
        "gst_paiddate":"",
      
    };
    
    $scope.payoutExportData = [];

    services.getPayoutData().then(function (res) {
        debugger;
        $scope.payoutListData = res;
        $scope.updateSubGrid();
        $scope.exportJSon();
    });

    

    $scope.showPayoutDetailsModal = function () {
        $('#payoutDetailModal').modal('show')
    };
    $scope.hidePayoutDetailsModal = function () {
        $('#payoutDetailModal').modal('hide')
    };

   

    $scope.updateSubGrid = function () {
        setTimeout(function () {
            $('#payoutlist').DataTable({
               // "scrollY": ($scope.table_box_height - 185),
              //  "scrollCollapse": true,
                "retrieve": true,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "pageLength": 10,
                // dom: 'Bfrtip',
                // buttons: [
                //      'excel',
                // ]
    });
        
        }, 0);
//  setTimeout(function () {
//         $('#payoutlist').DataTable( {
           
//             dom: 'lrtip',
//             "scrollCollapse": true,
//             initComplete: function () {
//                 debugger;
//                 this.api().columns().every( function () {
//                     debugger;
//                     var column = this;
//                     if(column[0] != 0  && column[0] != 1 && column[0] != 2  && column[0] != 9){
//                         var select = $('<select><option value="" >Select</option></select>')
//                         .appendTo( $(column.header()).empty() )
//                         .on( 'change', function () {
//                             var val = $.fn.dataTable.util.escapeRegex(
//                                 $(this).val()
//                             );
     
//                             column
//                                 .search( val ? '^'+val+'$' : '', true, false )
//                                 .draw();
//                         } );
     
//                     column.data().unique().sort().each( function ( d, j ) {
//                         select.append( '<option value="'+d+'">'+d+'</option>' )
//                     } );
//                     }
                    
//                 } );
//             }
//         } );
//     },0);
    };

  

    $scope.payoutModal = function (id, viewType,se_studentid) {
        $scope.payoutDetail = {
            "se_studentid":"",
            "acca_amount":0,
            "exp_amount":0,
            "gst_amount":0,
            "acca_paiddate":"",
            "exp_paiddate":"",
            "gst_paiddate":"",
            
        };
        $scope.acca_amount = 0;
        $scope.exp_amount = 0;
        $scope.gst_amount = 0;

        $scope.acca_paiddate =   "";
        $scope.exp_paiddate =   "";
        $scope.gst_paiddate =    "";
        if (id == 0) {
            $scope.title = "Payout Detail Create";
            $scope.buttonText = "Save";
            $scope.payout_id = 0;
           
            $scope.userViewType = viewType;
            $scope.enableBtn = true;
            $scope.disableEdit = false;
            services.getAllNonPayoutstudents().then((res) =>{
                $scope.studentList = res;
                $scope.showPayoutDetailsModal();
            })
           
        } else {
            $scope.checkGst(se_studentid);//check gst
            $scope.title = "Edit Payout Details";
            $scope.userViewType = viewType;
            if ($scope.userViewType == 'edit' || $scope.userViewType == 'new') {
                $rootScope.title = 'Edit Payout Details';
                $scope.enableBtn = true;
                $scope.disableEdit = false;
            } else {
                $rootScope.title = 'View Payout Details';
                $scope.enableBtn = false;
                $scope.disableEdit = true;
            }
            $scope.buttonText = "Update";
            $scope.payout_id = id;
            services.getEditPayoutData($scope.payout_id).then((res) => {
                // console.log(res);
                $scope.payoutDetail = res[0];
                $scope.acca_amount =  angular.copy(parseInt($scope.payoutDetail.acca_amount));
                $scope.exp_amount =  angular.copy(parseInt($scope.payoutDetail.exp_amount));
                $scope.gst_amount =  angular.copy(parseInt($scope.payoutDetail.gst_amount));
                    
                $scope.acca_paiddate =     angular.copy(unixToHtmlDate( $scope.payoutDetail.acca_paiddate));
                $scope.exp_paiddate =     angular.copy(unixToHtmlDate($scope.payoutDetail.exp_paiddate));
                $scope.gst_paiddate =  angular.copy(unixToHtmlDate($scope.payoutDetail.gst_paiddate))  ;
                $scope.showPayoutDetailsModal();
            })
        }

    };

    $scope.minDateSet = function(){
        var minDate = new moment();
        $scope.min = minDate.format("YYYY-MM-DD");
        return  $scope.min ;
    }

    $scope.dateRangeminDateSet = function(startDate ){
        if(startDate == ''){
           // var today = new Date(new Date().setHours(0,0,0,0));
           var minDate = new moment();
           // $scope.minDate = new Date();
        }else{
            var min =startDate;
            // $scope.minDate = new Date(min);
            var minDate = new moment(min);
        }
    
        $scope.min = minDate.format("YYYY-MM-DD");
        //$scope.max = maxDate.format("YYYY-MM-DD");
        return  $scope.min ;
    };

    $scope.setStartDate = function(startDate){
        
    }

    $scope.savePayoutData = function (payout_id) {
        debugger;

        $scope.payoutDetail.acca_paiddate =    timeToUnix( $scope.acca_paiddate);
        $scope.payoutDetail.exp_paiddate =    timeToUnix($scope.exp_paiddate);
        $scope.payoutDetail.gst_paiddate =    timeToUnix($scope.gst_paiddate);
        $scope.payoutDetail.acca_amount =   $scope.acca_amount;
        $scope.payoutDetail.exp_amount = $scope.exp_amount;
        $scope.payoutDetail.gst_amount = $scope.gst_amount
      
        if (payout_id == 0) {
            services.insertPayoutData($scope.payoutDetail).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);

                    $scope.hidePayoutDetailsModal();
                    services.toast('success', res.message);
                    //$scope.faqData.push($scope.faqEntryData);
                    // $location.reload();
                    //$scope.userdata.push($scope.userEntryData)
                    //  $scope.updateSubGrid();
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    services.toast('warning', res.message)
                    // alert(res.message); 
                }

            });
        } else {
            services.updatePayoutData($scope.payoutDetail).then(function (res) {
                debugger;
                if (res.statuscode == 'NT-200') {
                    // alert(res.message);
                    $scope.hidePayoutDetailsModal();
                    services.toast('success', res.message);
                    // var index = $scope.userdata.findIndex(x => x._id == user_id);
                    // if (index > -1) {
                    //     $scope.userdata[index] = $scope.userEntryData;
                    // }
                    $rootScope.pageReload();
                    // $location.reload();

                } else {
                    //  alert(res.message); 
                    services.toast('warning', res.message)
                }

            });
        }
    };
    $scope.startDate = '';
    $scope.endDate = '';
    $scope.filterby = '';
    $scope.paid = '';

    $scope.datefilter = function(table,column){
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                // var min = $filter('date')($scope.startDate,'mediumDate');
                // var max =  $filter('date')($scope.endDate,'mediumDate');;
                // var date = data[column] ; // use data for the age column
         
                // if ( ( isNaN( min ) && isNaN( max ) ) ||
                //      ( isNaN( min ) && date <= max ) ||
                //      ( min <= date   && isNaN( max ) ) ||
                //      ( min <= date   && date <= max ) )
                // {
                //     return true;
                // }
                // return false;
                if($scope.paid == 'yes'){
                    var min = new Date($scope.startDate)  == 'Invalid Date' ?   null : new Date($scope.startDate);
                    var max = new Date($scope.endDate)  == 'Invalid Date' ?   null : new Date($scope.endDate);
                    var startDate = new Date(data[column]);
                    if (min == null && max == null) { return true; }
                    if (min == null && startDate <= max) { return true;}
                    if(max == null && startDate >= min) {return true;}
                    if (startDate <= max && startDate >= min) { return true; }
                    return false;
                }
            
                // var dateStart = parseDateValue($("#dateStart").val());
                // var dateEnd = parseDateValue($("#dateEnd").val());
                // // aData represents the table structure as an array of columns, so the script access the date value
                // // in the first column of the table via aData[0]
                // var evalDate= parseDateValue(data[column]);

                // if (evalDate >= dateStart && evalDate <= dateEnd) {
                // return true;
                // }
                // else {
                // return false;
                // }
            }
              
            
        );
        table.draw();
    };

    $scope.filterbyvalue = function(filterby){
        if(filterby == 'ACCA'){
           $scope.selectcolumn = 4;
           $scope.filterDateKey = 'acca_paiddate';
           $scope.filterAmountKey = 'acca_amount';
           $scope.excelfilename = 'ACCA_Payment_details.csv';
          // $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","ACCA Amount","Paid Date"];
          $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date"];

          }else if(filterby == 'EXP'){
            $scope.selectcolumn = 6;
            $scope.filterDateKey = 'exp_paiddate';
            $scope.filterAmountKey = 'exp_amount';
            $scope.excelfilename = 'EXP_Payment_details.csv';
           // $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","EXP Amount","Paid Date"];
           $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date"];
          }else if(filterby == 'GST'){
            $scope.selectcolumn = 8;
            $scope.filterDateKey = 'gst_paiddate';
            $scope.filterAmountKey = 'gst_amount';
            $scope.excelfilename = 'GST_Payment_details.csv';
            //$scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","GST Amount","Paid Date"];
            $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","Amount","Paid Date","Invoice"];
          }
         
    }
    $scope.restFilter = function(table,column){
       
        $scope.startDate = '';
        $scope.endDate = '';
        $scope.filterby = '';
        $scope.paid = '';

       
        if(column){
           // table.column(column).search('').draw();
            table.search('').draw();
            //table.search(' ').draw();
            //table.draw();
        }else{
            table.search('').draw();
        }
        $scope.selectcolumn = '';
        $scope.filterDateKey = '';
        $scope.filterAmountKey = '';
      // $scope.exportJSon();
      $rootScope.pageReload();
    };

   

    $scope.exportJSon = function(){

          debugger;
          $scope.payoutExportData = [];
          if($scope.filterby == ''){
            $scope.excelheaders = ["S.No", "Enrollment Number", "Student Name","ACCA Amount","ACCA Amount Paid Date","EXP Amount","EXP Amount Paid Date","GST Amount","GST Amount Paid Date","Invoice"];
            $scope.excelfilename = 'Payout_details.csv';
           var  count = 0
            for(var value of  $scope.payoutListData){
                count += 1
                var tempObj = {
                  "S.No":count ,
                  "Enrollment Number":value.se_studentid,
                  "Student Name":value.se_name,
                  "ACCA Amount":value.acca_amount,
                  "ACCA Amount Paid Date":unixTodateTime(parseInt(value.acca_paiddate)) || "",
                  "EXP Amount":value.exp_amount,
                  "EXP Amount Paid Date":unixTodateTime(parseInt(value.exp_paiddate)) || "",
                  "GST Amount":value.gst_amount,
                  "GST Amount Paid Date":unixTodateTime(parseInt(value.gst_paiddate)) || "",
                  "Invoice No":value.invoiceno || ""
                }
                $scope.payoutExportData.push(tempObj);
            }
                 
          }else{
            var accafilter = $scope.payoutListData.filter(function (payout) {
                var d = parseInt(payout[$scope.filterDateKey]) || "";
                if(d != '' ){
                    var date = new Date(d);
                   
                    var startDate = new Date($scope.startDate)  == 'Invalid Date' ?   null : new Date($scope.startDate);
                    var endDate = new Date($scope.endDate)  == 'Invalid Date' ?   null : new Date($scope.endDate);
    
                 //   if (startDate == null && endDate == null) { return true; }
                    if (startDate == null && date <= endDate) { return payout;}
                    if(endDate == null && date >= startDate) {return payout;}
                    if (date <= endDate && date >= startDate) { return payout; }
                }
               
              })
            
               var count = 0;
              for(var value of  accafilter){
                  count += 1
                  var tempObj = {
                    "S.No":count ,
                    "Enrollment Number":value.se_studentid,
                    "Student Name":value.se_name,
                    "Amount":value[$scope.filterAmountKey],
                    "Paid Date":unixTodateTime(parseInt(value[$scope.filterDateKey]))
                  }
                  $scope.payoutExportData.push(tempObj);
              }
          }
       
        

         
           console.log(accafilter);
    }




    $scope.applyFilter = function(filter){
        debugger;
        $scope.startDate
        $scope.endDate
        $scope.filterby
        $scope.paid
        var table = $('#payoutlist').DataTable();

        if(filter == true){
            if($scope.filterby || $scope.paid ||  $scope.startDate || $scope.endDate){
                // if($scope.filterby == 'ACCA'){
                //   var column = 4;
                // }else if($scope.filterby == 'EXP'){
                //     var column = 6;
                // }else if($scope.filterby == 'GST'){
                //     var column = 8;
                // }
                 
                if($scope.filterby != '' ||  $scope.paid != '' ){
                    if($scope.paid == 'yes'){
                        if($scope.startDate != ''){
                           // table.column(column).search('^$', true, false).draw();
                           $scope.datefilter(table,$scope.selectcolumn);
                           $scope.exportJSon();
                        }else{
                            alert('Please select date') 
                        }
                       
                    }else if($scope.paid == 'no'){
                        table.column($scope.selectcolumn).search('^$', true, false).draw();
                    }
                   
                   
                }else if($scope.filterby != '' ||  $scope.paid == ''){
                    alert('Please select paid ') 
                }else if($scope.filterby != '' ||  $scope.paid != '' || $scope.startDate != ''){
                    $scope.datefilter(table,$scope.selectcolumn);
                }
    
            }else{
                alert('Please select filter values')
            }
            // if( $scope.filterby ){
    
            // }else if(){
    
            // }
            // var table = $('#paymentList').DataTable();
            // if(value == 'All'){
            //     table.column(3).search('').draw();
            // }else{
            //  table.column(3).search(value).draw();
            // }
        }else{
            // if($scope.filterby == 'ACCA'){
            //     var column = 4;
            //   }else if($scope.filterby == 'EXP'){
            //       var column = 6;
            //   }else if($scope.filterby == 'GST'){
            //       var column = 8;
            //   }
            $scope.restFilter(table,$scope.selectcolumn)
        }
        
    };

    function parseDateValue(rawDate) {
        debugger;
        var dateArray= rawDate.split("/");
        var parsedDate= dateArray[2] + dateArray[0] + dateArray[1];
        return parsedDate;
    }

    
    $scope.checkGst = function(studentid){
       

          services.checkGst(studentid).then((res) =>{
              $scope.gstdata = res.status;
          })

    }




}]);