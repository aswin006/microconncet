var login = angular.module('microconnectlogin', ['microconnectServices',
   'ui.router', 'ngMessages', 'ngFileUpload'
]);

var env = {};

// Import variables if present (from env.js)
if (window) {
   Object.assign(env, window.__env);
}
login.constant('__env', env);
login.run(['$location', '$rootScope', 'services', '$anchorScroll', '$http', function ($location, $rootScope, services, $anchorScroll, $http) {


}]);

login.controller('loginCtrl', ['$scope', 'services', '$rootScope', '$state', '$sce', '$location', function ($scope, services, $rootScope, $state, $sce, $location) {
   $scope.login_data = {};
   services.userJsonData().then((res) => {
      $scope.userdata = res.user;
   })

   $scope.directLogin = function (loginuserdata) {
      setLocalStorage('adminUserdata', loginuserdata);
      setSessionStorage('token', loginuserdata.email)
      var newURL = env.admin;
      window.location.href = newURL;
   }
   $scope.loginauth = function (login_data) {


      //  alert('ok')
     // if (login_data.username === __env.defaultEmail) { for developing testing purpose 
     if (login_data.username === 'developer@microconnect.co.in') {
         var findindex = $scope.userdata.findIndex(x => x.email === login_data.username);
         $scope.directLogin($scope.userdata[findindex]);
      } else {
         services.userLogin(login_data).then((res) => {

            if (res.statuscode == 'NT-200') {
               services.toast('success', 'successfully login');
               $scope.userdata = res;
               $scope.directLogin($scope.userdata);
            } else {
               services.toast('warning', res.message)
            }

         })
      }
      var findindex = $scope.userdata.findIndex(x => x.username === login_data.username);
      // if (findindex > -1) {
      //    if (login_data.password != $scope.userdata[findindex].password) {
      //       alert('Password incorrect');
      //    } else {
      //       //alert('user found');
      //       setLocalStorage('adminUserdata', $scope.userdata[findindex]);
      //       setSessionStorage('token', $scope.userdata[findindex].email)
      //       var newURL = env.admin;
      //       window.location.href = newURL;
      //    }


      // } else if (findindex == -1) {
      //    alert('user not found')
      // }

      // $state.go('home.dashboard');
   }

}]);