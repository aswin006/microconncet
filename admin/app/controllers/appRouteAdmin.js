admin.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider

        .state('home', {
            url: '/',
            template: '<main-component></main-component>',

        })
        .state('home.dashboard', {
            title: 'Dashboard',
            url: 'dashboard',
            templateUrl: './views/dashboard.html',
            controller: 'dashboardCtrl',
        })
        /***begin-07-12-2020-microconnect-new-changes */
        .state('home.studentslist', {
            title: 'Student List',
            url: 'studentlist',
            templateUrl: './views/studentlist.html',
            controller: 'studentlistctrl'

        })

        .state('home.student', {
            title: 'Manage Users',
            url: 'student/:studentid',
            templateUrl: './views/student.html',
            controller: 'studentctrl',
            resolve: {
                student: ['services', '$stateParams', function (services, $stateParams) {

                    debugger;
                    var studentID = $stateParams.studentid;

                    if (studentID != 0) {
                        return services.getEditEnrollUser(studentID);
                    }
                }]
            }

        })
        /***end-07-12-2020-microconnect-new-changes */
        .state('home.accasyllabuslist', {
            title: 'ACCA Syllabus List',
            url: 'accasyllabuslist',
            templateUrl: './views/accasyllabuslist.html',
            controller: 'accasyllabuslistctrl'

        })


        .state('home.accasubjectlist', {
            title: 'ACCA Subject List',
            url: 'accasubjectlist',
            templateUrl: './views/accasubjectlist.html',
            controller: 'accasubjectlistctrl'

        })
        .state('home.faqlist', {
            title: 'ACCA FAQ List',
            url: 'faqlist',
            templateUrl: './views/faqlist.html',
            controller: 'faqlistctrl'

        })
        .state('home.requestdemouserlist', {
            title: 'Request Demo Customers List',
            url: 'requestdemouserlist',
            templateUrl: './views/requestDemoUser.html',
            controller: 'requestdemouserlistctrl'

        })
        .state('home.getintouchuserlist', {
            title: ' Get In Touch Customers List',
            url: 'getintouchuserlist',
            templateUrl: './views/getintouchUser.html',
            controller: 'getintouchuserlistctrl'

        })
        .state('home.popupentry', {
            title: ' POPUP Entry',
            url: 'popupentry',
            templateUrl: './views/popupentry.html',
            controller: 'popupentryctrl'

        })
        .state('home.paymentdetail', {
            title: 'Payment Detail',
            url: 'paymentdetail',
            templateUrl: './views/paymentdetails.html',
            controller: 'paymentdetailctrl'

        })
        .state('home.userrolecreation', {
            title: 'User Role Creation',
            url: 'userrolecreation',
            templateUrl: './views/userRoleCreation.html',
            controller: 'userrolecreationctrl'

        })

        /***begin-07-12-2020-microconnect-new-changes */
        .state('home.userslist', {
            title: 'Users List',
            url: 'userlist',
            templateUrl: './views/userslist.html',
            controller: 'userlistctrl',
            resolve: {
                userrole: ['services', function (services) {
                    return services.getUserRoleData();
                }]
            }

        })
        .state('home.users', {
            title: 'Manage Users',
            url: 'users/:userid',
            templateUrl: './views/users.html',
            controller: 'userctrl',
            resolve: {
                user: ['services', '$stateParams', function (services, $stateParams) {

                    debugger;
                    var userID = $stateParams.userid;

                    if (userID != 0) {
                        return services.getEditEnrollUser(userID);
                    }
                }]
            }

        })
        
    /***end-07-12-2020-microconnect-new-changes */

    .state('home.paymentschedulelist', {
        title: 'Payment Shedule List',
        url: 'paymentschedulelist',
        templateUrl: './views/paymentschedulelist.html',
        controller: 'paymentschedulelistctrl',
    })
    .state('home.paymentshedule', {
        title: 'Manage Payment Shedule',
        url: 'paymentschedule/:studentid',
        templateUrl: './views/paymentschedule.html',
        controller: 'paymentScheduleCtrl',
        // resolve: {
        //     paymentshedule: ['services', '$stateParams', function (services, $stateParams) {

        //         debugger;
        //         var studentID = $stateParams.studentid;

        //         if (studentID != 0) {
        //             return services.getEditPaymentShedule(studentID);
        //         }
        //     }]
        // }

    })
    .state('home.payoutdetails', {
        title: 'Manage Payout Details',
        url: 'payoutdetailslist',
        templateUrl: './views/payoutdetailslist.html',
        controller: 'payoutdetailslistCtrl',
       

    })
    .state('home.invoicemaintaion', {
        title: 'Invoice Maintain',
        url: 'invoicemaintaion/:studentid',
        templateUrl: './views/invoicemaintain.html',
        controller: 'invoicemaintainCtrl',
       

    })
    


}]);