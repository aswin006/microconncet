angular.module('microconnectServices', [
    'ngFileUpload'
]).factory('services', services);
services.$inject = ['$http', 'Upload'];

function services($http, Upload) {

    //const apiUrl = 'http://104.218.53.125/mcconnect/api/v1.0/'
    var baseurl = __env.apiUrl;
    var services = {
        toast: toast,
        commonJsonData: commonJsonData,
        userJsonData: userJsonData,
        userLogin: userLogin,
        menuJsonData: menuJsonData,
        getAllEnrollUsers: getAllEnrollUsers,
        getEditEnrollUser: getEditEnrollUser,
        // FaqJsonData: FaqJsonData,
        //  ExamTabJsonData: ExamTabJsonData,
        //  SyllabusTabJsonData: SyllabusTabJsonData,
        uploadMarkSheet: uploadMarkSheet,
        uploadAadhar: uploadAadhar,
        uploadStudentPhoto: uploadStudentPhoto,
        uploadBonofideLetter: uploadBonofideLetter,
        StudentEnroll: StudentEnroll,
        StudentEnrollUpdate: StudentEnrollUpdate,
        sendPaymentLink: sendPaymentLink,
        discardPaymentLink:discardPaymentLink,
        getInTouch: getInTouch,
        demoRegData: demoRegData,
        getSyllabusData: getSyllabusData,
        insertSyllabus: insertSyllabus,
        getEdiSyllabus: getEdiSyllabus,
        updateSyllabus: updateSyllabus,
        getACCASubjectData: getACCASubjectData,
        insertACCASubject: insertACCASubject,
        getEdiACCASubject: getEdiACCASubject,
        updateACCASubject: updateACCASubject,
        getFaqData: getFaqData,
        insertFaq: insertFaq,
        getEditFaq: getEditFaq,
        updateFaq: updateFaq,
        updateFaqOrder:updateFaqOrder,
        getRequestDemoUserData: getRequestDemoUserData,
        getGetinTouchUserData: getGetinTouchUserData,
        getPopUpData: getPopUpData,
        insertSavePopupData: insertSavePopupData,
        getEdiPopUpData: getEdiPopUpData,
        updatePopUpData: updatePopUpData,
        deletePopup:deletePopup,//04-01-2021-deletepopup-feature-add
        getUserRoleData: getUserRoleData,
        insertUserRoleData: insertUserRoleData,
        getEditUserRoleData: getEditUserRoleData,
        updateUserRoleData: updateUserRoleData,
        mailSend: mailSend,
        getUserData: getUserData,
        getEditUserData: getEditUserData,
        insertUserData: insertUserData,
        updateUserData: updateUserData,
        getAllPaymentShedule: getAllPaymentShedule,
        getEditPaymentShedule: getEditPaymentShedule,
        insertPaymentShedule: insertPaymentShedule,
        updatePaymentShedule: updatePaymentShedule,
        getAllNonSheduleStudents:getAllNonSheduleStudents,
        viewPaymentSchedule:viewPaymentSchedule,
        getPaymentDetails:getPaymentDetails,
        getPayoutData: getPayoutData,
        getEditPayoutData: getEditPayoutData,
        insertPayoutData: insertPayoutData,
        updatePayoutData: updatePayoutData,
        getAllNonPayoutstudents:getAllNonPayoutstudents,
        changePassword:changePassword,
        proformaPdfCreate:proformaPdfCreate,
        getStudentInvoice:getStudentInvoice,
        checkGst:checkGst
        
    }
    return services;

    function toast(alertType, msg) {
        //debugger;
        //toaster.pop(alertType, msg, "", 4000, 'trustedHtml');
        if (alertType == 'success') {
            var color = 'green'

        } else if (alertType == 'warning') {
            var color = 'red'
        } else if (alertType == 'danger') {
            var color = 'red'
        } else if (alertType == 'error') {
            var color = 'red'
        }
        var title = '';
        var position = 'topRight'
        customToast(alertType, color, title, msg, position)
    }


    function commonJsonData() {
        return $http.get('./assets/data/common.json').then(function (res) {
            return res.data;
        });
    }

    function userJsonData() {
        return $http.get('../assets/data/user.json').then(function (res) {
            return res.data;
        });
    }

    function userLogin(loginData) {
        return $http.post(baseurl + 'userlogin', loginData).then(function (res) {
            return res.data;
        });
    }

    function menuJsonData() {
        return $http.get('./assets/data/menu.json').then(function (res) {
            return res.data;
        });
    }



    function getAllEnrollUsers() {
        return $http.post(baseurl + 'getuser', {}).then(function (res) {
            return res.data;
        });
    }

    function getEditEnrollUser(id) {
        return $http.post(baseurl + 'edituser', {
            _id: id
        }).then(function (res) {
            return res.data;
        });
    }

    function sendPaymentLink(studentid,invoicetype) {
        return $http.post(baseurl + 'paymentemail', {
            se_studentid: studentid,invoicetype : invoicetype
        }).then(function (res) {
            return res.data;
        });
    }

    function discardPaymentLink(discardData) {
        return $http.post(baseurl + 'ordercheck', 
            discardData
        ).then(function (res) {
            return res.data;
        });
    }
    

    function SlideJsonData() {
        return $http.get('./common/data/slide.json').then(function (res) {
            return res.data;
        });
        // return $http.get(baseurl + 'slides').then(function (res) {
        //     return res.data;
        // });
    }

    // function FaqJsonData() {
    //     return $http.get('./common/data/faq.json').then(function (res) {
    //         return res.data;
    //     });
    // }

    // function ExamTabJsonData() {
    //     return $http.get('./common/data/examtab.json').then(function (res) {
    //         return res.data;
    //     });
    // }

    // function SyllabusTabJsonData() {
    //     return $http.get('./common/data/syllabustab.json').then(function (res) {
    //         return res.data;
    //     });
    // }
    /////****** ACCA Syllabus ******* */
    function getSyllabusData() {
        return $http.post(baseurl + 'getexams', {}).then(function (res) {
            return res.data;
        });
    }

    function insertSyllabus(syllabusdata) {
        return $http.post(baseurl + 'saveexams', syllabusdata).then(function (res) {
            return res.data;
        });
    }

    function getEdiSyllabus(_id) {
        return $http.post(baseurl + 'editexams', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateSyllabus(syllabusdata) {
        return $http.post(baseurl + 'updateexams', syllabusdata).then(function (res) {
            return res.data;
        });
    }

    /******** ACCA Subjects********* */
    function getACCASubjectData() {
        return $http.post(baseurl + 'getsyllabus', {}).then(function (res) {
            return res.data;
        });
    }

    function insertACCASubject(subjectdata) {
        return $http.post(baseurl + 'savesyllabus', subjectdata).then(function (res) {
            return res.data;
        });
    }

    function getEdiACCASubject(_id) {
        return $http.post(baseurl + 'editsyllabus', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateACCASubject(subjectdata) {
        return $http.post(baseurl + 'updatesyllabus', subjectdata).then(function (res) {
            return res.data;
        });
    }

    /********FAQ********* */
    function getFaqData() {
        return $http.post(baseurl + 'getfaqdata', {}).then(function (res) {
            return res.data;
        });
    }

    function insertFaq(faqdata) {
        return $http.post(baseurl + 'faqdata', faqdata).then(function (res) {
            return res.data;
        });
    }

    function getEditFaq(_id) {
        return $http.post(baseurl + 'editfaq', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateFaq(faqdata) {
        return $http.post(baseurl + 'updatefaq', faqdata).then(function (res) {
            return res.data;
        });
    }
    
    function updateFaqOrder(faqdata) {
        return $http.post(baseurl + 'updatefaqorder', faqdata).then(function (res) {
            return res.data;
        });
    }
    /*******Request demo user */
    function getRequestDemoUserData() {
        return $http.post(baseurl + 'demousers', {}).then(function (res) {
            return res.data;
        });
    }

    /*******Get in touch  user */
    function getGetinTouchUserData() {
        return $http.post(baseurl + 'contactlist', {}).then(function (res) {
            return res.data;
        });
    }


    function uploadMarkSheet(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'marksheet',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadAadhar(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'aadharsave',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadStudentPhoto(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'profileimage',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function uploadBonofideLetter(imgfile) {
        debugger;
        return Upload.upload({
            url: baseurl + 'studentbonafide',
            method: 'POST',
            file: imgfile
        }).then(function (response) {
            return response.data;
        }, function (err) {
            return err.data;
        });

    };

    function StudentEnroll(studentEnrollData) {
        return $http.post(baseurl + 'studentenroll', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function StudentEnrollUpdate(studentEnrollData) {
        return $http.post(baseurl + 'userupdate', studentEnrollData).then(function (res) {
            return res.data;
        });
    }

    function getInTouch(getInTouchData) {
        return $http.post(baseurl + 'contactemail', getInTouchData).then(function (res) {
            return res.data;
        });
    }

    function demoRegData(demoRegData) {
        return $http.post(baseurl + 'contactemail', demoRegData).then(function (res) {
            return res.data;
        });
    }

    ////******POPUP Data */
    function getPopUpData() {
        return $http.post(baseurl + 'getpobup', {}).then(function (res) {
            return res.data;
        });
    }

    function insertSavePopupData(data) {
        return $http.post(baseurl + 'popubsave', data).then(function (res) {
            return res.data;
        });
    }

    function getEdiPopUpData(_id) {
        return $http.post(baseurl + 'editpobup', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePopUpData(data) {
        return $http.post(baseurl + 'updatepobup', data).then(function (res) {
            return res.data;
        });
    }
  //04-01-2021-deletepopup-feature-add
    function deletePopup(id) {
        return $http.post(baseurl + 'deletepopbup', {"_id":id}).then(function (res) {
            return res.data;
        });
    }
   //04-01-2021-deletepopup-feature-add

    /*****user role creation */


    function getUserRoleData() {
        return $http.post(baseurl + 'getrole', {}).then(function (res) {
            return res.data;
        });
    }

    function insertUserRoleData(data) {
        return $http.post(baseurl + 'rolecreate', data).then(function (res) {
            return res.data;
        });
    }

    function getEditUserRoleData(_id) {
        return $http.post(baseurl + 'editrole', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateUserRoleData(data) {
        return $http.post(baseurl + 'updaterole', data).then(function (res) {
            return res.data;
        });
    }

    /****mail send  */
    function mailSend(data) {
        return $http.post(baseurl + 'emaildynamic', data).then(function (res) {
            return res.data;
        });
    }
    /*****user creation */


    function getUserData() {
        return $http.post(baseurl + 'getadminusers', {}).then(function (res) {
            return res.data;
        });
    }

    function insertUserData(data) {
        return $http.post(baseurl + 'userregister', data).then(function (res) {
            return res.data;
        });
    }

    function getEditUserData(_id) {
        return $http.post(baseurl + 'editadminuser', {
            "_id": _id
        }).then(function (res) {
            return res.data;
        });
    }

    function updateUserData(data) {
        return $http.post(baseurl + 'updateadminuser', data).then(function (res) {
            return res.data;
        });
    }
    
    /****** Payment shedule ***** */
    
    function getAllPaymentShedule() {
        return $http.post(baseurl + 'shceduleusers', {}).then(function (res) {
            return res.data;
        });
    }

    function getAllNonSheduleStudents() {
        return $http.post(baseurl + 'nonschedulesstudents', {}).then(function (res) {
            return res.data;
        });
    }
    
    function insertPaymentShedule(data) {
        return $http.post(baseurl + 'paymentschedule', data).then(function (res) {
            return res.data;
        });
    }

    function getEditPaymentShedule(studentid) {
        return $http.post(baseurl + 'schedulecheck', {
            "se_studentid": studentid
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePaymentShedule(data) {
        return $http.post(baseurl + 'updateadminuser', data).then(function (res) {
            return res.data;
        });
    }
    function viewPaymentSchedule(studentid) {
        return $http.post(baseurl + 'paymentdetails',  {
            "se_studentid": studentid
        }).then(function (res) {
            return res.data;
        });
    }
    
    function  getPaymentDetails(){
        return $http.post(baseurl + 'paymentusers',  {}).then(function (res) {
            return res.data;
        });
    }
    
    /****Payout Details  */
    function getPayoutData() {
        return $http.post(baseurl + 'payoutdetails', {}).then(function (res) {
            return res.data;
        });
    }

    function getAllNonPayoutstudents() {
        return $http.post(baseurl + 'nonpayout', {}).then(function (res) {
            return res.data;
        });
    }
    
    function insertPayoutData(data) {
        return $http.post(baseurl + 'payoutsave', data).then(function (res) {
            return res.data;
        });
    }

    function getEditPayoutData(studentid) {
        return $http.post(baseurl + 'payoutdetails', {
            "_id": studentid
        }).then(function (res) {
            return res.data;
        });
    }

    function updatePayoutData(data) {
        return $http.post(baseurl + 'payoutsave', data).then(function (res) {
            return res.data;
        });
    }
    function changePassword(data) {
        return $http.post(baseurl + 'changepassword', data).then(function (res) {
            return res.data;
        });
    }
    
    function proformaPdfCreate(data) {
        return $http.post(baseurl + 'pdfcreate', data).then(function (res) {
            return res.data;
        });
    }


    function getStudentInvoice(studentid){
        return $http.post(baseurl + 'invoice', {'se_studentid':studentid}).then(function (res) {
            return res.data;
        });
    }

    function checkGst(studentid){
        return $http.post(baseurl + 'gstcheck', {'se_studentid':studentid}).then(function (res) {
            return res.data;
        });
        
    }

    
}