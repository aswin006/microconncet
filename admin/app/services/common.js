function setLocalStorage(key, value) {
    debugger;
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorageData(key) {
    debugger;
    var data = JSON.parse(localStorage.getItem(key));
    return data;
}


function setSessionStorage(key, value) {
    debugger;
    sessionStorage.setItem(key, value);
}

function getSessionStorageData(key) {
    debugger;
    var data = sessionStorage.getItem(key);
    return data;
}

function clearLocalStorage(key) {
    localStorage.removeItem(key);
}
function clearSessionStorage(key) {
    sessionStorage.removeItem(key);
}


function timeToUnix(dateTime) {
    var UnixtimeStamp = new Date(dateTime).getTime();
    return UnixtimeStamp
}

function unixToTime(unix) {
    debugger;
    if (unix) {
        var time = new Date(unix);
    }
    // else{
    //     var time = new Date(); 
    // }
    return time;
}

function unixTodateTime(Unix) {
    debugger;
    if (Unix) {
        var unixtime = parseInt(Unix)
        var userEnteredDate = new Date(unixtime);

        var date = new Date(userEnteredDate);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var dt = date.getDate();

        if (dt < 10) {
            dt = '0' + dt;
        }
        if (month < 10) {
            month = '0' + month;
        }
        var dateString = dt + '-' + month + '-' + year;
    } else {
        var dateString = 'dd' + '-' + 'mm' + '-' + 'yyyy';
    }
    return dateString;
}
function unixToHtmlDate(Unix){
    if (Unix) {
        var unixtime = parseInt(Unix)
        var userEnteredDate = new Date(unixtime);
       
    }else{
        var userEnteredDate = '';
    }
    return userEnteredDate;
}

function hideLoader() {
    $("#showLoading").hide()

}

function encrypt(value){
    if(value){
        var enc =  window.btoa(value);
    }else{
        var enc = '';
    }
    return enc;
}
function decrypt(value){
    if(value){
        var dec =  window.atob(value);
    }else{
        var dec = '';
    }
    return dec;
}

function showLoader(e) {
    e || (e = "Loading....!");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:rgba(134,161,174,0.8);z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="main-loader"></div><h4 class="text-center space font-weight-normal">Loading...!</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
}




function customToast(id, color, title, msg, position) {
    iziToast.show({
        id: id,
        backgroundColor: '',
        color: color, // blue, red, green, yellow
        title: title,
        message: msg,
        position: position,
        maxWidth: 400,
        // close: false,
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX'
    });
}


function mainHideLoader() {
    $("#showLoading").hide()
}

function mainShowLoader(e) {
    e || (e = "Loading. Please Wait.");
    var t = $("#showLoading");
    t.length || $("body").append('<div id="showLoading" class="modal" style="display:none;background-color:#fff;z-index: 1060;"><div class="modal-dialog"><div class="modal-content" style="max-width: 300px;margin: auto;"><div class="modal-body text-center"><div class="d-flex justify-content-center"><div class="spinner-border" role="status"> <span class="sr-only">Loading...</span></div></div><h4 class="text-center space font-weight-normal">Loading...</h4></div></div></div></div></div>'),
        $("#showLoading h4").html(e),
        $("#showLoading").show()
}


function LetterAvatar(name, size, wordcount) {

    name = name || '';
    size = size || 60;
    wordcount = wordcount || 1;

    var colours = [
            "#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50",
            "#f1c40f", "#e67e22", "#e74c3c", "#ecf0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"
        ],

        nameSplit = String(name).toUpperCase().split(' '),
        initials, charIndex, colourIndex, canvas, context, dataURI;


    // if (nameSplit.length == 1) {
    //     initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
    // } else {
    //     initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    // }
    if (wordcount == 1) {
        initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
    } else {
        initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    }

    if (window.devicePixelRatio) {
        size = (size * window.devicePixelRatio);
    }

    charIndex = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
    colourIndex = charIndex % 10;
    canvas = document.createElement('canvas');
    canvas.width = size;
    canvas.height = size;
    context = canvas.getContext("2d");

    context.fillStyle = colours[colourIndex - 1];
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.font = Math.round(canvas.width / 2) + "px Arial";
    context.textAlign = "center";
    context.fillStyle = "#FFF";
    context.fillText(initials, size / 2, size / 1.5);

    dataURI = canvas.toDataURL();
    canvas = null;

    return dataURI;
}


/*****begin-10-01-2021-Aswin-new proforma email */

/******amount in words convert******/
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
};

function numberSuffixAdd(number){
    
    if(number){
        var num = parseInt(number);
        switch (num) {
            case 1:
                var suffix = 'st'
                break;
            case 2:
                var suffix = 'nd'
                break;
            case 3:
                var suffix = 'rd'
                break;
            default:
                var suffix = 'th'
                break;
        }
      return   suffix;
    }

}
function momentUnixConvert(unix) {
     if(unix){
        let epoch = parseInt(unix);
        let result = moment(epoch).format('DD-MM-YYYY');
      // let result2 =  moment(epoch).utcOffset("+05:30").format('DD-MM-YYYY')
        //console.log(result2);
        return result;
     }else{
        return '';
     }
    
  }

/*****end-10-01-2021-Aswin-new proforma email */

/*****begin-19-01-2021-Aswin-amount format change */

function amountFormat(amount) {
    if(amount){
        var amountValue = parseInt(amount)
        const formatter = new Intl.NumberFormat('en-IN', {
     
            currency: 'INR',
            minimumFractionDigits: 2
          })
          
          
          var value = formatter.format(amountValue) // "10.00"
          
         return value;
    }
    
    }
/*****end-19-01-2021-Aswin-amount format change */
