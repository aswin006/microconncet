admin.directive('allowOnlyNumbers', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9]/g, '')
                $input.val(value);
                if (event.which == 64 || event.which == 16) {
                    // to allow numbers  
                    return false;
                } else if (event.which >= 48 && event.which <= 57) {
                    // to allow numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // to allow numpad number  
                    return true;
                } else if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // to allow backspace, enter, escape, arrows  
                    return true;
                } else {
                    event.preventDefault();
                    // to stop others  
                    //alert("Sorry Only Numbers Allowed");  
                    return false;
                }
            });
        }
    }
});


admin.directive('dateformat', function (dateFilter) { 
    return { 
        require: 'ngModel', 
        link: function (scope,  
                       elm, attrs, ctrl) { 

            var dateFormat = 
                attrs['date'] || 'yyyy-MM-dd'; 

            ctrl.$formatters.unshift( 
                    function (modelValue) { 
                return dateFilter( 
                    modelValue, dateFormat); 
            }); 
        } 
    }; 
}) 


admin.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


admin.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: {
            ngModel: "=",
            minDate: "=",
            maxDate: "="
        },
        link: function (scope, elem, attrs, ngModelCtrl) {


            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: "dd/mm/yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:Y",                    
                onSelect: function (dateText) {
                    updateModel(dateText);
                    this.focus();
                },
                onClose: function (ele) {
                    this.blur();
                },

                defaultDate: new Date() 

            };

            scope.$watch('maxDate', function (newValue, oldValue) {
                $(elem).datepicker('option', 'maxDate', newValue);
            })

            scope.$watch('minDate', function (newValue, oldValue) {
                $(elem).datepicker('option', 'minDate', newValue);
                $(elem).datepicker('option', 'defaultDate', newValue);
            })

            elem.datepicker(options);
        }
    }
});


var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

admin.directive("compareTo", compareTo);


admin.filter('countryname',['$rootScope' ,function($rootScope) {
    return function(input) {
     var index =  $rootScope.commonJsonData.countrylist.findIndex(x => x.code == input);
     if(index > 0){
        return $rootScope.commonJsonData.countrylist[index].name;
     }else{
        return  '';
     }
     
    };
  }]);


  admin.directive('beautifyText', function() {
    return {
      link: function(scope, iElement, iAttrs, ngModelController) {
        // Adds the prefix and suffix
        (function() {
          var prefix = iAttrs.beautifyTextWithPrefix;
          var suffix = iAttrs.beautifyTextWithSuffix;

          ngModelController.$parsers.push(function(value) {
            if (angular.isString(value)) {
              return value.replace(new RegExp('^' + prefix), '').replace(new RegExp(suffix + '$'), '');
            }

            return '';
          });

          scope.$watch(function() {
            return ngModelController.$viewValue;
          }, function(newValue) {
            if (angular.isString(newValue) && newValue.length > 0) {
              if (angular.isString(ngModelController.$modelValue) && ngModelController.$modelValue.length === 0 && newValue.length === 1) {
                ngModelController.$viewValue = '';
                ngModelController.$render();
                return;
              }

              if (!isBeautifiedWithPrefix(newValue)) {
                ngModelController.$viewValue = prefix + newValue;
              }

              if (!isBeautifiedWithSuffix(newValue)) {
                ngModelController.$viewValue = newValue + suffix;
              }

              ngModelController.$render();
            } else {
              ngModelController.$viewValue = '';
              ngModelController.$render();
            }
          });

          function isBeautifiedWithPrefix(value) {
            return value.match(new RegExp('^' + prefix)) !== null;
          }

          function isBeautifiedWithSuffix(value) {
            return value.match(new RegExp(suffix + '$')) !== null;
          }
        })();

        // Changes the caret position
        (function() {
          var element = iElement[0];

       
          function getCursorPos() {
            if ("selectionStart" in element && document.activeElement == element) {
              return {
                start: element.selectionStart,
                end: element.selectionEnd
              };
            } else if (element.createTextRange) {
              var sel = document.selection.createRange();
              if (sel.parentElement() === element) {
                var rng = element.createTextRange();
                rng.moveToBookmark(sel.getBookmark());
                for (var len = 0; rng.compareEndPoints("EndToStart", rng) > 0; rng.moveEnd("character", -1)) {
                  len++;
                }
                rng.setEndPoint("StartToStart", element.createTextRange());
                for (var pos = {
                  start: 0,
                  end: len
                }; rng.compareEndPoints("EndToStart", rng) > 0; rng.moveEnd("character", -1)) {
                  pos.start++;
                  pos.end++;
                }
                return pos;
              }
            }
            return -1;
          }
          function setCursorPos(start, end) {
            if (arguments.length < 2) {
              end = start;
            }

            if ("selectionStart" in element) {
              element.selectionStart = start;
              element.selectionEnd = end;
            } else if (element.createTextRange) {
              var rng = element.createTextRange();
              rng.moveStart("character", start);
              rng.collapse();
              rng.moveEnd("character", end - start);
              rng.select();
            }
          }

          iElement.bind('mousedown mouseup keydown keyup', function() {
            if (ngModelController.$viewValue.length > 0) {
              var caretPosition = getCursorPos();

              if (caretPosition.start === 0) {
                setCursorPos(1, caretPosition.end < 1 ? 1 : caretPosition.end);
              }

              if (caretPosition.end === ngModelController.$viewValue.length) {
                setCursorPos(caretPosition.start, ngModelController.$viewValue.length - 1);
              }
            }
          });
        })();
      },
      restrict: 'A',
      require: 'ngModel'
    }
  });

  admin.directive('customPrefix',function(){
      debugger;
      return {
          restrict:'A',
          require: 'ngModel',
          link: function(scope,elm,attr,ctrl){
              debugger;
            var prefix = attr.customPrefix;
              elm.on('keydown',function(event){
                  debugger;
                  var $input = $(this);
                  var value = $input.val();
                 // var oldvalue = $input.val();
                  if(value.indexOf(prefix) !== 0 ){
                    $input.val(prefix + this.value);
                  }
                
              })
          }
      }
  })



  admin.directive('aadharCheck', function() {
    var aadharcardRegex = /^\d{4}-\d{4}-\d{4}$/;

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attr, ngModelCtrl) {
      elm.on('keyup', function (event) {
      debugger;
              var $input = $(this);
              var value = $input.val();
              value =
                   value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s =>s.length > 0).join("-");
              $input.val(value);
              
               if(value.length == 14){
                event.preventDefault();
                return false;
               }
               
      });
        scope.$watch(attr.ngModel, function (value) {
              debugger;
            
              console.log('value changed, new value is: ' + value);
          });
        // ngModelCtrl.$parsers.unshift(function(input) {
        //   var valid = aadharcardRegex.test(input);
        //   ngModelCtrl.$setValidity('aadharcard', valid);
        //   return input;
        // }); 
        ngModelCtrl.$validators.aadharcard = function(modelValue, viewValue) {
            var valid = aadharcardRegex.test(modelValue);

      return valid;
    };
      }
    };
  });


  admin.filter('amountformat',['$rootScope' ,function($rootScope) {
    return function(input) {
        return amountFormat(input)
     
    };
  }]);